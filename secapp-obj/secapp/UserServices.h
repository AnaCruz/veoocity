//
//  UserServices.h
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Service.h"

@interface UserServices : Service

/**
 * Create account of SecApp.
 *
 * @param firstName Frist Name
 * @param lastName Last Name
 * @param pin Security Pin
 * @param phone Number Phone
 * @param email EMail
 * @param userName User Name
 * @param gender Gender
 * @param image Image
 * @param facebookId Facebook id.
 * @param twitterId Twitter id.
 * @param delegate In this object will notifies response of service.
 */
+ (void) createAccountWithFirstName:(NSString*) firstName LastName:(NSString*) lastName Pin:(NSString*) pin Phone:(NSString*) phone Email:(NSString*) email UserName:(NSString*) userName Gender:(NSString*) gender Image:(NSString*) image FacebookId:(NSString*) facebookId TwitterId:(NSString*) twitterId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
/**
 * Update account of SecApp.
 *
 * @param firstName Frist Name
 * @param lastName Last Name
 * @param pin Security
 * @param userName User Name
 * @param gender Gender
 * @param image Image
 * @param facebookId Facebook id.
 * @param twitterId Twitter id.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) updateAccountWithFirstName:(NSString*) firstName LastName:(NSString*) lastName Pin:(NSString*) pin UserName:(NSString*) userName Gender:(NSString*) gender Image:(NSString*) image FacebookId:(NSString*) facebookId TwitterId:(NSString*) twitterId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
/**
 * Get data of a single user.
 *
 * @param userIdGetting ID user to find
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getUserWithUserIdGetting:( NSString *) userIdGetting Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

/**
 * Get profile data of a single user.
 *
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getProfileUserWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;
/**
 * Get some profile data of a single user.
 *
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getProfileUserOptimizedWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;
/**
 * Get BASIC profile data of a single user.
 *
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getProfileUserBasicWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;
/**
 * Add Email or Phone to User.
 *
 * @param data A string with the email or phone.
 * @param isEmail YES if is email or NO if is a number phone.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) addEmailPhone:(NSString*) data IsEmail:(BOOL) isEmail Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

/**
 * Delete Email or Phone to User.
 *
 * @param data A string with the email or phone.
 * @param isEmail YES if is email or NO if is a number phone.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) deleteEmailPhone:(NSString*) data IsEmail:(BOOL) isEmail Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
/**
 *  Confirm the security pin.
 *
 *  @param pin      Security pin of the user
 *  @param wait     Show the progress of a task with known duration.
 *  @param delegate In this object will notifies response of service.
 */
+(void) confirmPassword:(NSString*) pin WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

+(void) resetPassword: (NSString *)mail WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) aceptToBeMonitoredWithInvitationId: (NSString *)invitationId WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;

+(void) rejectToBeMonitoredWithInvitationId: (NSString *)invitationId WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;
@end
