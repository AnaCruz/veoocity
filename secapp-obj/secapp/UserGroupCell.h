//
//  UserGroupCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 07/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserGroupCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameUser;
@property (weak, nonatomic) IBOutlet UIImageView *pictureUser;

@end
