//
//  NotificationCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 14/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleNotification;
@property (weak, nonatomic) IBOutlet UILabel *messageNotification;
@property (weak, nonatomic) IBOutlet UILabel *dateNotification;

@end
