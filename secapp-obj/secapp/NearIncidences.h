//
//  NearIncidences.h
//  secapp
//
//  Created by SecApp on 07/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@interface NearIncidences : GenericObject
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * occId;
@property (nonatomic, retain) NSNumber * type;

@end

@interface NearIncidences (CoreDataGeneratedAccessors)
/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;
/**
 * Find a NearOccurrence across a occurrenceId in the data base.
 * @param occurrenceId NearOccurrence Identifier.
 * @return array Array with near occurrence data.
 */
+ (NSArray*) findWithNearOccurrenceId:(NSString*) occurrenceId;
/**
 * Find all occurrrences in the data base
 * @return array Array with all near occurrences data.
 */
+ (NSArray*) findAll;
@end