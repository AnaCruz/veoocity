//
//  AddFriendsToChatGroupVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
#import "EditGroupVC.h"

@interface AddFriendsToChatGroupVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>


@property (nonatomic) NSString *groupName;
@property (nonatomic) NSString *image64;
@property (nonatomic) NSNumber * groupID;
@property BOOL isNew;

@end
