//
//  AlertInputPin.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 29/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AlertInputPin.h"
#import "UserServices.h"
#import "AppDelegate.h"

@interface AlertInputPin() <ResponseDelegate>

@property (copy) void (^callback)(BOOL complete);

@end

@implementation AlertInputPin

+ (instancetype) shareInstance {
    static id shared = nil;
    @synchronized(self) {
        if (shared == nil)
            shared = [[self alloc] init];
    }
    return shared;
}

+ (void) showAlertInViewController:(UIViewController*) controller
                         WithTitle:(NSString*) title
                           Message:(NSString*) message
                       Placeholder:(NSString*) placeholder
                          Callback:(void (^)(BOOL complete)) callback {
    AlertInputPin* alertPin = [AlertInputPin shareInstance];
    alertPin.callback = callback;
    if ([UIAlertController class]) {
        UIAlertController* alert = [alertPin alertControllerWithTitle:title AndMessage:message Placeholder:placeholder];
        [controller presentViewController:alert animated:YES completion:nil];
        return;
    }
    UIAlertView* alert = [alertPin alertViewWithTitle:title AndMessage:message Placeholder:placeholder];
    alert.delegate = alertPin;
    [alert show];
}

/**
 *  Build to Alert controller to intput a secret text
 *
 *  @param title        Title of the alert
 *  @param message      Message of the alert
 *  @param placeholder  Placeholder of the alert
 *
 *  @return Alert Controller
 */
- (UIAlertController*) alertControllerWithTitle:(NSString*) title AndMessage:(NSString*) message Placeholder:(NSString*) placeholder {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Aceptar"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action){
                             NSString *text = ((UITextField *)[alert.textFields objectAtIndex:0]).text;
                             [UserServices confirmPassword:text WithWait:YES Delegate:self];
                        }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancelar"
                                                     style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction *action) {
                                                   
                                               }];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        [textField setSecureTextEntry:YES];
        [textField setPlaceholder:placeholder];
    }];
    [alert addAction:ok];
    [alert addAction:cancel];
    return alert;
}

/**
 *  Build to Alert Veiw
 *
 *  @param title        Title of the alert
 *  @param message      Message of the alert
 *  @param placeholder  Placeholder of the alert
 *
 *  @return Alert View
 */
- (UIAlertView*) alertViewWithTitle:(NSString*) title AndMessage:(NSString*) message Placeholder:(NSString*) placeholder {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:@"Aceptar",nil];
    alert.alertViewStyle = UIAlertViewStyleSecureTextInput;
    [[alert textFieldAtIndex:0] setPlaceholder:placeholder];
    return alert;
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        [UserServices confirmPassword:[alertView textFieldAtIndex:0].text WithWait:YES Delegate:self];
    }
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
 
    if (service == kCONFIRM_PASSWORD) {
        dictionary = [dictionary objectForKey:@"ConfirmPasswordResult"];
        responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
        if (responseCode == 200) {
            if (self.callback) {
                self.callback(YES);
            }
        } else if (responseCode == 404) {
            if (self.callback) {
                self.callback(NO);
            }
        }
        if (responseCode==405) {
            AppDelegate* app = [[UIApplication sharedApplication] delegate];
            [app forceCloseSession];
            
        }
    }
}

@end
