//
//  CheckPin.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 13/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CheckPin : NSObject
- (void) checkPinWithController:(UIViewController*) controller Callback:(void (^)(BOOL complete)) callback;
@end
