//
//  ConvesationGroupVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ConvesationGroupVC.h"
#import <UIKit/UIKit.h>
#import "MessagesService.h"
#import "Service.h"
#import "ImageUtils.h"
#import <CoreLocation/CoreLocation.h>
#import "MapSecApp.h"
#import "Friend.h"
#import "ContactService.h"
#import "Message.h"
#import "ContainerVC.h"
#import "GetNotification.h"
#import "Notification.h"
#import "CellConversationSample.h"
#import "ChatContainerVC.h"
#import "DeviceUtils.h"
#import "ChatRomCell.h"
#import "GetChatList.h"
#import "AppDelegate.h"
#import "AlertView.h"

#import "StringUtils.h"



@interface ConvesationGroupVC () <ResponseDelegate, UITextFieldDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *pictureGroup;
@property (weak, nonatomic) IBOutlet UILabel *nameGroupLabel;
@property (weak, nonatomic) IBOutlet UITextField *messagetText;
@property NSString* textMessage;
@property ContainerVC *container;
@property (nonatomic) NSArray *conversationArray;
@property (weak, nonatomic) IBOutlet UIView *containerConversation;
@property float height;
@property int languaje;
@property BOOL isOpenAnConversation;

@property (strong, nonatomic) NSMutableDictionary *offscreenCells;

@end

@implementation ConvesationGroupVC

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.offscreenCells = [NSMutableDictionary dictionary];
    }
    return self;
}
- (void) drawInView:(UIView*) view {
    CGRect frame =view.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.view.frame = frame;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.isOpenAnConversation=YES;
    self.height = self.view.frame.size.height;
    [self registerForKeyboardNotifications];
    
    self.conversationArray = nil;
    self.messagetText.text=@"";
    if (!self.conversationArray) {
        self.conversationArray = [NSArray array];
    }
    
       [self updateData];
}

- (void) updateData {
    [MessagesService getConversationGroupWithChatGroupID:self.groupID Wait:NO Delegate:self];
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        
        case kGET_CONVERSATIONGROUP:
            dictionary = [dictionary objectForKey:@"ConversationChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayConversation = [dictionary objectForKey:@"MessageChatList"];
                
                self.conversationArray= [arrayConversation mutableCopy];
               
                [self.tableView reloadData];
                [self.tableView beginUpdates];
                [self.tableView layoutIfNeeded];
                [self.tableView endUpdates];
                
                dispatch_async(dispatch_get_main_queue(),^{
                  [self.tableView scrollToRowAtIndexPath:[self indexPathForLastMessage]
                                          atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                 });
                Message * messa= [Message findWithGroupId:self.groupID];
                messa.isReaded= @1;
                [messa mergeObject];

                [[GetChatList shareInstance] callTheCallBackForIconUnreadMessage];
             }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
              }
            break;
        case kSEND_MESSAGE_TO_GROUP:
            dictionary = [dictionary objectForKey:@"SendMessageToChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSTimeInterval milisecondedDate = [[NSDate date] timeIntervalSince1970] * 1000;
                NSDictionary *dictionary = @{
                                             @"Message": self.textMessage,
                                             @"SendDate":[NSString stringWithFormat:@"%f",milisecondedDate],
                                             @"UserIdFrom":@([[Service getUserId] intValue]),
                                             
                                             };
                [self.tableView beginUpdates];
                NSMutableArray * mutableConversation= [self.conversationArray mutableCopy];
                [mutableConversation insertObject:dictionary atIndex:[self.conversationArray count]];
                self.conversationArray = [mutableConversation copy];
                
                NSInteger row = [self.conversationArray count]-1;
                NSIndexPath *indexPath  =[NSIndexPath indexPathForRow:row inSection:0];
                [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
                [self.tableView scrollToRowAtIndexPath:[self indexPathForLastMessage]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
                self.messagetText.text= @"";
                [[GetChatList shareInstance] updateData];
             
                
            }
            if (responseCode==405) {
          AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        default:
            break;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.languaje = [[Service getLanguaje] intValue];
    self.containerConversation.layer.cornerRadius = 5;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.messagetText.delegate = self;
    
    [self unregisterForKeyboardNotifications];
    GetNotification* notification = [GetNotification shareInstance];
    notification.newMessageChat = ^(Notification* notification){
        if ([notification.chatGroupId isEqualToString:self.groupID]) {
            if (self.isOpenAnConversation==YES) {
                [self updateData];
            }
        }
     
    };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //chekar de que grupo viene en sms
        self.groupID = [self.chatGroup objectForKey:@"IdChatGroup"];
        self.nameGroupLabel.text= [self.chatGroup objectForKey:@"Name"];
        
        self.pictureGroup.clipsToBounds = YES;
        int radius = self.pictureGroup.frame.size.width/2;
        self.pictureGroup.layer.cornerRadius = radius;
        self.pictureGroup.layer.borderWidth = 3;
        self.pictureGroup.layer.borderColor = [[UIColor whiteColor] CGColor];
        if([[self.chatGroup objectForKey:@"Image"] isEqual:@""]|| [self.chatGroup objectForKey:@"Image"]==nil||[[self.chatGroup objectForKey:@"Image"] isEqual:[NSNull null]] ){
            self.pictureGroup.image = [UIImage imageNamed:@"chat-29"];
        } else{
            NSString *uri= [self.chatGroup objectForKey:@"Image"];
            if (uri && ![uri isEqual: [NSNull null]])  {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.pictureGroup.image = [UIImage imageWithData:data];
                    });
                    
                });
            }
        }
        
    });
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100;
    [self addPaddingToTextField:self.messagetText];
    
}
- (void) addPaddingToTextField:(UITextField*) sender {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.leftView = paddingView;
    sender.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingViewPin = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.rightView = paddingViewPin;
    sender.rightViewMode = UITextFieldViewModeAlways;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.conversationArray =nil;
    self.isOpenAnConversation=NO;
    //[self.tableView reloadData];
    [self unregisterForKeyboardNotifications];
}

- (IBAction)goBack:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
}

- (IBAction)sendMessage:(id)sender {
    if ([self.messagetText.text isEqualToString:@""]) {
        [self.view endEditing:YES];
        return;
    }
    NSString* message=self.messagetText.text;
    self.textMessage=message;
    [MessagesService sendMessageWithGroupID:self.groupID Message:message Wait:NO Delegate:self];
    [self.view endEditing:YES];
}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
      [self.view endEditing:YES];
}

#pragma mark UITableView Delegate and Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifierCell;
    NSDictionary* data = [self.conversationArray objectAtIndex:indexPath.row];
    if ([[[data objectForKey:@"UserIdFrom"] stringValue] isEqualToString:[MessagesService getUserId]]) {
        // me
        identifierCell = @"MeCell2";
        self.friendSender=nil;
    } else {
        // friend
        identifierCell = @"TheirCell2";
        NSArray *array= [Friend findWithUserId:[data objectForKey:@"UserIdFrom"]];
        if(array && [array count]>0){
        
            self.friendSender =[array objectAtIndex:0];
        }else{
            self.friendSender=nil;
        }
       
        

    }
    
    ChatRomCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierCell];
    if (cell == nil) {
        cell = (ChatRomCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierCell];
    }
    if ([DeviceUtils deviceVersion] < 8) {
        [cell.contentView removeConstraint:cell.down];
        [cell.contentView setNeedsUpdateConstraints];
        [cell.contentView updateConstraintsIfNeeded];
        [cell.contentView layoutIfNeeded];
    }
    cell.message.text = [data objectForKeyedSubscript:@"Message"];
    if (self.friendSender) {
       cell.nameSender.text = self.friendSender.userName;
    }
    else{
        if([[data objectForKey:@"Email"] isEqualToString:@"Email"]){
            if(self.languaje==1){
             cell.nameSender.text = @"Contacto desconocido";
            }else{
             cell.nameSender.text = @"Unknown user";
            }
       }
        else{
            cell.nameSender.text = [data objectForKey:@"Email"];
        }
    }
    NSDate * createdDate =[NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"SendDate"] doubleValue]/1000];
    cell.time.text = [StringUtils formattedDateWithDate:createdDate];
    [cell setNeedsUpdateConstraints];
    [cell updateConstraintsIfNeeded];
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([DeviceUtils deviceVersion] >= 8) {
        return UITableViewAutomaticDimension;
    }
    
    static NSString* identifierCell;
    NSDictionary* data = [self.conversationArray objectAtIndex:indexPath.row];
    if ([[[data objectForKey:@"UserIdFrom"] stringValue] isEqualToString:[MessagesService getUserId]]) {
        // me
        identifierCell = @"MeCell2";
    } else {
        // friend
        identifierCell = @"TheirCell2";
    }
    
    static ChatRomCell *cell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        cell = [tableView dequeueReusableCellWithIdentifier:identifierCell];
    });
    
    [cell.contentView removeConstraint:cell.down];
    [cell.contentView setNeedsUpdateConstraints];
    [cell.contentView updateConstraintsIfNeeded];
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    cell.message.text = [data objectForKeyedSubscript:@"Message"];
    NSDate * createdDate =[NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"SendDate"] doubleValue]/1000];
    cell.time.text = [StringUtils formattedDateWithDate:createdDate];
   // cell.time.text = [self formattedDate:[data objectForKey:@"SendDate"]];
    
    
    [cell setNeedsLayout];
    [cell layoutIfNeeded];
    
    CGSize size = cell.background.frame.size;
    //    return [cell.contentView systemLayoutSizeFittingSize:UILayoutFittingExpandedSize].height;
    return size.height + 26;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.conversationArray count];
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if([indexPath row] == [self.conversationArray count]-1){
        //end of loading
        //for example [activityIndicator stopAnimating];
        [self.tableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

#pragma mark keyboard show and hide

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIView *container= self.view;
    CGRect mainFrame = container.frame;
    mainFrame.size.height = self.height - (kbSize.height-124);
    container.frame = mainFrame;
    [self performSelector:@selector(scrollToBottomTableView) withObject:nil afterDelay:.3];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    CGRect frame = self.view.frame;
    frame.size.height=self.height;
    self.view.frame = frame;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self performSelector:@selector(scrollToBottomTableView) withObject:nil afterDelay:.3];
}

-(void)scrollToBottomTableView {
    if (self.tableView.contentOffset.y > self.tableView.frame.size.height)
    {
        [self.tableView scrollToRowAtIndexPath:[self indexPathForLastMessage]
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

-(NSIndexPath *)indexPathForLastMessage {
    return [NSIndexPath indexPathForRow:[self.conversationArray count]-1 inSection:0];
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=199)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:199];
    }
    
    return NO;
}



@end
