//
//  SimpleTableViewCell.m
//  secapp
//
//  Created by SecApp on 12/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SimpleTableViewCell.h"

@implementation SimpleTableViewCell
@synthesize nameLabel = _nameLabel;
@synthesize messageLabel = _messageLabel;
@synthesize profileImageView = _profileImageView;
@synthesize width= _width;
@synthesize buttonAdd = _buttonAdd;
@synthesize buttonChat= _buttonChat;
@synthesize buttonChatGrupal=_buttonChatGrupal;
@synthesize buttonEdit= _buttonEdit;
@synthesize buttonDelete= _buttonDelete;
@synthesize buttonWithoutPermissions= _buttonWithoutPermissions;
@synthesize buttonWithPermissions= _buttonWithPermissions;
/**
 * Create a cell customizable for the ChatVC and SearchContacts
 */
- (void)awakeFromNib {}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
