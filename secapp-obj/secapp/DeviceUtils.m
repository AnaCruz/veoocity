//
//  DeviceUtils.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 10/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "DeviceUtils.h"
#import <UIKit/UIKit.h>

@implementation DeviceUtils

+ (float) deviceVersion {
    NSString *ver = [[UIDevice currentDevice] systemVersion];
    float ver_float = [ver floatValue];
    return ver_float;
}
@end
