//
//  GetNotification.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 23/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "GetNotification.h"
#import "ResponseDelegate.h"
#import "UserNotifications.h"
#import "Notification.h"
#import "AppDelegate.h"

@interface GetNotification() <ResponseDelegate>

@end

@implementation GetNotification

+ (GetNotification*) shareInstance {
    static id shared = nil;
    @synchronized(self) {
        if (shared == nil)
            shared = [[self alloc] init];
    }
    return shared;
}

- (void) updateData {
    [UserNotifications getNotificationsWait:NO Delegate:self];
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kGET_NOTIFICATIONS:
            dictionary = [dictionary objectForKey:@"NotificationsByUserResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [self parseArrayAPN:[dictionary objectForKey:@"APNList"]];
                if ( self.updatePNotification ) {
                    self.updatePNotification();
                }
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        default:
            break;
    }
}
-(void) addOccurrencesLocalNotification:(NSDictionary *) apn{
    self.cont++;
     Notification* noti=[Notification createObject];
      NSNumber* typeNoti = [self parseTypeIdNotification:[apn objectForKey:@"TypeId"]];
    noti.notificationId = [NSString stringWithFormat:@"local%d",self.cont];
    noti.date = [apn objectForKey:@"Date"];
    noti.groupName = [apn objectForKey:@"GroupName"];
    noti.latitude = [NSNumber numberWithDouble:[[apn objectForKey:@"Latitude"] doubleValue]];
    noti.longitude = [NSNumber numberWithDouble:[[apn objectForKey:@"Longitude"] doubleValue]];
    noti.messege= [apn objectForKey:@"Message"];
   noti.invitationId=[apn objectForKey:@"InvitationId"];
    noti.typeId = typeNoti;
    noti.userIdFrom = [apn objectForKey:@"UserIdFrom"];
    noti.chatGroupName= [apn objectForKey:@"ChatGroupName"];
    [noti mergeObject];
    if ( self.updatePNotification ) {
        self.updatePNotification();
    }
}

/**
 * Parsing the JSON to Notification Object
 * @param array JSON Object
 */
- (void) parseArrayAPN:(NSArray*) array {
    NSArray* notifyInBD = [Notification findAll];
    for (Notification* notification in notifyInBD) {
        //si la notificacion viene remota, elimimar
        if (![notification.notificationId hasPrefix:@"l"]) {
           [notification deleteObject];
        }
    }
   BOOL notifyNewMessage = NO;
    Notification* noti;
    for (NSDictionary* apn in array) {
        
       noti = [Notification findWithNotificationId:[apn objectForKey:@"NotificationId"]];
        NSNumber* typeNoti = [self parseTypeIdNotification:[apn objectForKey:@"TypeId"]];
        if (!noti) {
            noti = [Notification createObject];
            [noti saveObject];
            if ([typeNoti intValue] == 6 && self.newMessageChat) {
                notifyNewMessage = YES;
            }
        }
        noti.notificationId = [NSString stringWithFormat:@"%@",[apn objectForKey:@"NotificationId"]];
        noti.date = [NSDate dateWithTimeIntervalSince1970:[[apn objectForKey:@"Date"] doubleValue]/1000];
        noti.groupId = [apn objectForKey:@"GroupId"];
        if ([apn objectForKey:@"GroupName"]
            && ![[apn objectForKey:@"GroupName"] isEqual:[NSNull null]]) {
            noti.groupName = [apn objectForKey:@"GroupName"];
        }
        noti.invitationId = [apn objectForKey:@"InvitationId"];
        noti.latitude = [NSNumber numberWithDouble:[[apn objectForKey:@"Latitude"] doubleValue]];
        noti.longitude = [NSNumber numberWithDouble:[[apn objectForKey:@"Longitude"] doubleValue]];
        NSString * message= [apn objectForKey:@"Message"];
        if (message == (id)[NSNull null] || message.length == 0 ) {
            noti.messege=@"";
        }
        else{
            noti.messege= [apn objectForKey:@"Message"];
        }

        noti.status = [NSNumber numberWithDouble:[[apn objectForKey:@"Status"] doubleValue]];
        noti.typeId = typeNoti;
        noti.userIdFrom = [apn objectForKey:@"UserIdFrom"];
        noti.chatGroupId= [apn objectForKey:@"ChatGroupId"];
        if ([apn objectForKey:@"ChatGroupName"]
            && ![[apn objectForKey:@"ChatGroupName"] isEqual:[NSNull null]]) {
            noti.chatGroupName= [apn objectForKey:@"ChatGroupName"];
        }
        
        [noti mergeObject];
    }
    if (notifyNewMessage==true) {
        self.newMessageChat(noti);
    }
}
-(NSNumber *)parseTypeIdNotification:(NSString *)type{
    int typeId = -1;
    
    if ([type isEqualToString:@"MaximaAlerta"]) {
        typeId = 1;
    }
    else if ([type isEqualToString:@"AlertaDesactiva"]){
        typeId = 2;
    }
    else if ([type isEqualToString:@"NewUserGroup"]) {
        typeId = 3;
    }
    else if ([type isEqualToString:@"GrupoEliminado"]){
        typeId = 4;
    }
    else if ([type isEqualToString:@"Invitation"]){
        typeId = 5;
    }
    else if ([type isEqualToString:@"NuevoMensaje"]){
        typeId = 6;
    }
    else if ([type isEqualToString:@"ChatGrupalEliminado"]){
        typeId = 7;
    }
    else if ([type isEqualToString:@"UserinChatGroup"]) {
        typeId = 8;
    }
    else if ([type isEqualToString:@"NewNameChatGroup"]){
        typeId = 9;
    }
    else if ([type isEqualToString:@"UserDeleteChatGroup"]){
        typeId = 10;
    }
    else if ([type isEqualToString:@"MessageFromMonitor"]){
        typeId = 11;
    }
    else if ([type isEqualToString:@"OutOfSecureZone"]){
        typeId = 12;
    }
    else if ([type isEqualToString:@"InsideSecureZone"]) {
        typeId = 13;
    }
    else if ([type isEqualToString:@"InsideInsecureZone"]) {
        typeId = 14;
    }
    else if ([type isEqualToString:@"OutOfInsecureZone"]) {
        typeId = 15;
    }
    else if ([type isEqualToString:@"ActiveInsecureZone"]) {
        typeId = 16;
    }
    else if ([type isEqualToString:@"InactiveInsecureZone"]) {
        typeId = 17;
    }
    else if ([type isEqualToString:@"GPSOff"]) {
        typeId = 18;
    }
    else if ([type isEqualToString:@"GPSOn"]) {
        typeId = 19;
    }
    else if ([type isEqualToString:@"UserMonitored"]) {
        typeId = 20;
    }
    else if ([type isEqualToString:@"InsideContactInsecureZone"]) {
        typeId = 21;
    }
    else if ([type isEqualToString:@"OutContactInsecureZone"]) {
        typeId = 22;
    }
    else if ([type isEqualToString:@"NewOccurrence"]) {
        typeId = 23;
    }
    else if ([type isEqualToString:@"NewTag"]) {
        typeId = 24;
    }
    else if ([type isEqualToString:@"NearOccurrence"]) {
        typeId = 25;
    }
    else if ([type isEqualToString:@"FollowOccurrence"]) {
        typeId = 26;
    }
    else if ([type isEqualToString:@"FollowUser"]) {
        typeId = 27;
    }
    else if ([type isEqualToString:@"NewTipNot"]) {
        typeId = 28;
    }
    return [NSNumber numberWithInt:typeId];
}

@end
