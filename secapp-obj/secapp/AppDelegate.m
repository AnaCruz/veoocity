//
//  AppDelegate.m
//  secapp
//
//  Created by iOS on 23/01/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LocationManagement.h"
#import "SessionServices.h"
#import <WindowsAzureMobileServices/WindowsAzureMobileServices.h>
#import "GetNotification.h"
#import "GetChatList.h"

#import "ViewController.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AppDelegate () <ResponseDelegate>
@property NSString * isForceClose;



@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
#if TARGET_IPHONE_SIMULATOR
    // where are you?
    NSLog(@"Documents Directory: %@", [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject]);
#endif
       if ([launchOptions objectForKey:UIApplicationLaunchOptionsLocationKey]) {
        [[LocationManagement sharedInstance] startMonitoringSignificantLocationChanges];
        NSLog(@"initiate location significant");
    }
  
  [Fabric with:@[[Crashlytics class]]];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    application.applicationIconBadgeNumber = 0;
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userId = [userDefault objectForKey:@"userId"];
    NSString *sessionId = [userDefault objectForKey:@"sessionId"];
    if (userId && sessionId) {
        [SessionServices isSessionActiveWithUserId:userId SessionId:sessionId Wait:NO Delegate:self];
        [self goToMainView];
        //notificaciones locales
        UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        if (locationNotification) {
            // Set icon badge number to zero
            application.applicationIconBadgeNumber = 0;
        }
        
    } else {
        [self goToStart];
    }
    [self.window makeKeyAndVisible];

    return YES;
}



- (void) pushNotifications {
      // Registra la app para poder recibir notificaciones.
    UIApplication* application = [UIApplication sharedApplication];
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) { // Para iOS 8 o posteriores
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert) categories:nil];
        [application registerUserNotificationSettings:settings];
        [application registerForRemoteNotifications];
    } else { // Para iOS 7 o anteriores
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge |  UIRemoteNotificationTypeSound];
    }
}

- (void) goToStart {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Start" bundle:nil] instantiateViewControllerWithIdentifier:@"TutorialVC"];
    [self replaceRootViewControllerWithViewWithViewController:vc];
}

- (void) goToSignUp {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"SignUpVC"];
    [self replaceRootViewControllerWithViewWithViewController:vc];
}

- (void) goToLogin {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"LoginVC"];
    ViewController * login = (ViewController *)vc;
    login.isforceClose=self.isForceClose;
    [self replaceRootViewControllerWithViewWithViewController:vc];
    
}

- (void) goToMainView {
    UIViewController* vc = [[UIStoryboard storyboardWithName:@"SecApp" bundle:nil] instantiateViewControllerWithIdentifier:@"ContainerView"];
    [self replaceRootViewControllerWithViewWithViewController:vc];
     UIApplication* application = [UIApplication sharedApplication];
    if(application.isRegisteredForRemoteNotifications==false){
        [self pushNotifications];
    }
}

- (void) replaceRootViewControllerWithViewWithViewController:(UIViewController*) vc {
    if (self.window.rootViewController) {
        [UIView transitionWithView:self.window
                          duration:0.8
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{ self.window.rootViewController = vc; }
                        completion:nil];
    } else {
        self.window.rootViewController = vc;
    }
}

-(void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//MSClient *client = [MSClient clientWithApplicationURLString:@"https://push-notification-secapp.azure-mobile.net/" applicationKey:@"ZBmubcoBORpZBuBklYCVXgppOPyiKI73"]; // produccion
 MSClient *client = [MSClient clientWithApplicationURLString:@"https://testing-secapp.azure-mobile.net/" applicationKey:@"hpDMnWTpuLVTxTkNyyhsGEAdpjQrLt83"]; //testing
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *userId = [userDefault objectForKey:@"userId"];
    [client.push registerNativeWithDeviceToken:deviceToken tags:@[userId] completion:^(NSError *error) {
        if (error != nil) {
            NSLog(@"Error registering for notifications: %@", error);
        }
    }];
}                                                                                                                      

// Handle any failure to register.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:
(NSError *)error {
    NSLog(@"Failed to register for remote notifications: %@", error);
}

// Because alerts don't work when the app is running, the app handles them.
// This uses the userInfo in the payload to display a UIAlertView.
- (void)application:(UIApplication *)application didReceiveRemoteNotification: (NSDictionary *)userInfo {
    NSLog(@"%@", userInfo);
    [[GetChatList shareInstance] updateData];
    [[GetNotification shareInstance] updateData];
}  

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incidencia cercana"
//                                                        message:notification.alertBody
//                                                       delegate:self cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
       // [alert show];
    }
    application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [self saveContext];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
[self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    // entra a la app
   }

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
   
    [FBSDKAppEvents activateApp];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [self saveContext];
   
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.example.CoreDataTest" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VeooCity" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
    
  
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
  
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"VeooCity.sqlite"];
    NSError *error = nil;
    NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil]; //LIGHT MIGRATION
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";


    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
   NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
       //abort();
    }
  return _persistentStoreCoordinator;

}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;

}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        
            if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                //            abort();
            }
    }
}

#pragma mark - ResponseDelgate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    NSDictionary *dicResult;
    int responseCode = -1;
    switch (service) {
        case kSESSIONACTIVE:
            
            dicResult = [dictionary objectForKey:@"IsSessionActiveResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            if (responseCode >= 400 && responseCode <= 410) {
                [self closeSession];
            }
            break;
        case kLOGOUT:
            dicResult = [dictionary objectForKey:@"LogoutResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            NSLog(@"Logout response: %d",responseCode);
             [self goToLogin];
            break;
        default:
            break;
    }
}
-(void) forceCloseSession{
    self.isForceClose=@"1";
   [self closeSession];

}

- (void) closeSession {
    [SessionServices logoutWithUserId:[Service getUserId] SessionId:[Service getSessionId] Wait:NO Delegate:self];
    // Clear UserDefault
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    [[LocationManagement sharedInstance] stopLocation];
    [[LocationManagement sharedInstance] stopMonitoring];
   
    //Clear data base
    [self flushDatabase];
    //[self deleteAllObjectsInCoreData];
    //Logout FB
    
    
    // Unregister the pushnotifications
//MSClient *client = [MSClient clientWithApplicationURLString:@"https://push-notification-secapp.azure-mobile.net/" applicationKey:@"ZBmubcoBORpZBuBklYCVXgppOPyiKI73"]; // produccion
MSClient *client = [MSClient clientWithApplicationURLString:@"https://testing-secapp.azure-mobile.net/" applicationKey:@"hpDMnWTpuLVTxTkNyyhsGEAdpjQrLt83"]; // pruebas
    [client logout];
    UIApplication* application = [UIApplication sharedApplication];
    [application unregisterForRemoteNotifications];
       // Go to Login View
   
}

/**
 * Clear the data of core data
 */
-(void) flushDatabase {
    [_managedObjectContext lock];
    NSError *error;
    NSArray *stores = [_persistentStoreCoordinator persistentStores];
    for(NSPersistentStore *store in stores) {
        [_persistentStoreCoordinator removePersistentStore:store error:&error];
        [[NSFileManager defaultManager] removeItemAtPath:store.URL.path error:&error];
    }
    [_managedObjectContext unlock];
    _managedObjectModel    = nil;
    _managedObjectContext  = nil;
    _persistentStoreCoordinator = nil;
}
- (void)deleteAllObjectsInCoreData
{
    NSArray *allEntities = self.managedObjectModel.entities;
    for (NSEntityDescription *entityDescription in allEntities)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entityDescription];
        
        fetchRequest.includesPropertyValues = NO;
        fetchRequest.includesSubentities = NO;
        
        NSError *error;
        NSArray *items = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        if (error) {
            NSLog(@"Error requesting items from Core Data: %@", [error localizedDescription]);
        }
        
        for (NSManagedObject *managedObject in items) {
            [self.managedObjectContext deleteObject:managedObject];
        }
        
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"Error deleting %@ - error:%@", entityDescription, [error localizedDescription]);
        }
    }
}


@end
