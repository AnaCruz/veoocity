//
//  StringUtils.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 03/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "StringUtils.h"
#import "Service.h"

@implementation StringUtils

+ (BOOL) isEmpty:(NSString*) string {
    if ([string isEqualToString:@""]) {
        return YES;
    }
    return NO;
}

+ (BOOL) isEmptyStringsInArray:(NSArray*) strings {
    for (NSString* tmp in strings) {
        if ([tmp isEqualToString:@""]) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL) validateEmail: (NSString *) mail {
//    NSString *emailRegex = @"[a-zA-Z0-9+._%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-.]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})";
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//    return [emailTest evaluateWithObject:mail];
    
//    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
//    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";

   BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Za-z0-9](([\\_.-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\\.-]?[a-zA-Z0-9]+)*)\\.([A-Za-z]{2,})$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:mail];
}

+ (BOOL) validateNumberPhone: (NSString *) phone {
    NSString *phoneRegex = @"[+0-9]{7,13}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phone];
}
+(NSString *)formattedDateWithDate: (NSDate *)date{
   NSString *language= [Service getLanguaje];
   NSDateFormatter *format = [[NSDateFormatter alloc] init];
    if ([language isEqualToString:@"1"]) {
         [format setDateFormat:@"dd MMM, hh:mm a"];
    }else{
         [format setDateFormat:@"MMM dd, hh:mm a"];

    }
 NSString *dateString = [format stringFromDate:date];
   return dateString;

}

@end
