//
//  NewMessageCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 31/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMessageCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleCell;
@property (weak, nonatomic) IBOutlet UILabel *messageCell;
@property (weak, nonatomic) IBOutlet UILabel *dateMessage;

@end

