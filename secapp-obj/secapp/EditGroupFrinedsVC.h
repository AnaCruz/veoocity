//
//  EditGroupFrinedsVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Group;

@interface EditGroupFrinedsVC : UIViewController

@property Group* group;

@end
