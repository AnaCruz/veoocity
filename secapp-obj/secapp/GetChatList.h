//
//  GetChatList.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 24/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GetChatList : NSObject

/**
 * Block that notify when the "Chat List" was dowload
 */
@property (nonatomic, copy) void (^callback)(void);

/**
 * Download and save all the chats list
 */
- (void) updateData;
/**
 * method for change messages icon with unread messages
 */
-(void)callTheCallBackForIconUnreadMessage;

/**
 * Singleton method
 */
+ (GetChatList*) shareInstance;

@end
