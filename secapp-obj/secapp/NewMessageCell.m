//
//  NewMessageCell.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 31/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "NewMessageCell.h"

@implementation NewMessageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
