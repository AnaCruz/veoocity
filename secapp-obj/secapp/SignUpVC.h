//
//  SignUpVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 04/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SignUpVC : UIViewController

-(void)signUpComplete:(NSDictionary *)data;
@end
