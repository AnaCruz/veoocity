//
//  PhonesListVC.m
//  secapp
//
//  Created by SecApp on 17/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "PhonesListVC.h"
#import "EditProfileVC.h"
#import "Service.h"
#import "UserServices.h"
#import "Profile.h"
#import "Phone.h"
#import "StringUtils.h"
#import "Alert/AlertView.h"
#import "AppDelegate.h"

@interface PhonesListVC () <ResponseDelegate, UITextFieldDelegate>

@property Profile *userProfile;
@property NSArray * arrayPhones;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *inPhone;
@property NSIndexPath* indexPathDelete;

@end

@implementation PhonesListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayPhones = [[NSArray alloc] init];
    [self getArrayPhones];
    self.tableView.layer.cornerRadius = 3;
}

/**
 * Get all phones in the user
 */
-(void)getArrayPhones{
    Profile* profile = [Profile findWithUserId:[UserServices getUserId]];
    if (profile) {
        self.userProfile = profile;
        self.arrayPhones = [self.userProfile.phones allObjects];
        [self.tableView reloadData];
    }
}

/**
 * Validated the phone and if is correct send the phone to WS.
 */
- (IBAction)savePhone:(id)sender {
    if (![self.inPhone.text isEqualToString:@""]
        && [self.inPhone.text length] <= 15
        && [StringUtils validateNumberPhone:self.inPhone.text]) {
        [UserServices addEmailPhone:self.inPhone.text IsEmail:NO Wait:YES Delegate:self];
         [self.view endEditing:YES];
    } else {
        // TODO ALERT Teléfono vacio, mas de 50 caracteres o telefono erroneo
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"telefono.no.valido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
    }
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeKeyboard:(id)sender {
//    if (self.isEditing) {
        [self.view endEditing:YES];
//    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayPhones count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"PhoneCell"];
    NSString *phone = ((Phone*)[self.arrayPhones objectAtIndex:indexPath.row]).phone;
    
    cell.textLabel.text = phone;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self.arrayPhones count] <= 1) {
            // TODO ALERT No puedes eliminar cuando tienes un solo teléfono.
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"eliminar.un.solo.telefono", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
            [self.tableView setEditing:NO animated:YES];
            return;
        }
        self.indexPathDelete = indexPath;
        [UserServices deleteEmailPhone:((Phone*)[self.arrayPhones objectAtIndex:indexPath.row]).phone IsEmail:NO Wait:YES Delegate:self];
    }
}

#pragma mark - ResponseDelegate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
  switch (service) {
        case kUPDATE_MAIL_PHONE:
            dictionary = [dictionary objectForKey:@"AddEmailPhoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Phone *phone = [Phone createObject];
                phone.phone = self.inPhone.text;
                [phone saveObject];
                [self.userProfile addPhonesObject:phone];
                [self.userProfile mergeObject];
                [self getArrayPhones];
                self.inPhone.text = @"";
            }
            if (responseCode == 409) {
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"telefono.ya.existe", nil)  Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];

                self.inPhone.text = @"";
            }
            if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
        case kDELETE_MAIL_PHONE:
            
            dictionary = [dictionary objectForKey:@"DeleteEmailPhoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Phone *phone =  (Phone*)[self.arrayPhones objectAtIndex:self.indexPathDelete.row];
                [self.userProfile removePhonesObject:phone];
                [self.userProfile mergeObject];
                [self getArrayPhones];
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            self.indexPathDelete = nil;
            break;
        default:
            break;
    }
    [self.tableView setEditing:NO animated:YES];
}
#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=15);
    }
   
    return YES;
}

@end
