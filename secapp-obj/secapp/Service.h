//
//  Service.h
//  secapp
//
//  Created by iOS on 29/12/14.
//  Copyright (c) 2014 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseDelegate.h"
#import "RESTClient.h"

@interface Service : NSObject

#pragma mark Login Methods

#pragma mark Create account Methods

#pragma mark Create Comment Methods

/**
 * Send a comment to admin of secapp.
 * @param comment Comment that will send of the user.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) sendComment:(NSString*) comment WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Ask if exist comment of the user.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) hasCommentWithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

#pragma mark Location of the user Methods

#pragma mark Utils Service

/**
 * Encode to bytes
 * @param object Object
 **/
+ (NSData*) parsingWithObject:(id) object;
/**
 * Get the battery level
 *
 **/
+ (NSNumber*) getBatteryLevel;
/**
 * Get the user id.
 *
 **/
+ (NSString*) getUserId;
/**
 * Get the session id.
 *
 **/
+ (NSString*) getSessionId;
/**
 * Get the url of bitbucket
 *
 **/
+ (NSString*) getUrl;
/**
 * Get the application version
 *
 **/
+ (NSString*) getAppVersion;
+ (NSString*) getLanguaje;

@end
