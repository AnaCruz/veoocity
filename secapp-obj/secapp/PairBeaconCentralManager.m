//
//  PairBeaconVC.m
//  secapp
//
//  Created by SecApp on 28/12/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "PairBeaconCentralManager.h"
#import "PairBeaconInterfaceVC.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "ContainerVC.h"
#ifndef LE_Transfer_TransferService_h
#define LE_Transfer_TransferService_h
#define TRANSFER_SERVICE_MAIN  @"0xF000" // servicio BROADCAST
#define TRANSFER_SERVICE_UUID  @"0x180F" // servicio bateria
#define TRANSFER_SERVICE_UUID2  @"0xF003" // servicio de esta conectado
#define TRANSFER_SERVICE_UUID3  @"0x1802" // servicio CONEXION Y DESCONEXION
#define TRANSFER_CHARACTERISTIC_UUID   @"0x2A19" //bateria. Cuando presiona el boton enviar alerta
#define TRANSFER_CHARACTERISTIC_UUID2   @"0xF803" //enviar estatus de conectado correctamente
#define TRANSFER_CHARACTERISTIC_UUID3   @"0x2A06" //hacer sonar cuando se desconecta o no
//#define TRANSFER_SERVICE_UUID  @"0xFFF0" //servicio beacon pasado
//#define TRANSFER_CHARACTERISTIC_UUID   @"0xFFF1" // caracteristica beacon pasado
#define UUID_STRING
#endif
#import "Service.h"
#import "BeaconServices.h"
#import "Profile.h"
#import "AppDelegate.h"

@interface PairBeaconCentralManager () <CBCentralManagerDelegate, CBPeripheralDelegate, ResponseDelegate>
@property (strong, nonatomic) CBCentralManager      *centralManager;
@property (strong, nonatomic) CBPeripheral          *discoveredPeripheral;
@property (strong, nonatomic) NSMutableData         *data;
@property (strong, nonatomic) PairBeaconInterfaceVC * pair;
@property BOOL isConnectedStatus;
@property NSTimer * timer;
@property NSTimer * timerForReconnect;
@property int countScan; //contador para detener escaneo despues de un tiempo
@property NSString *languaje;
@end

@implementation PairBeaconCentralManager
+ (PairBeaconCentralManager*) sharedInstance {
    static PairBeaconCentralManager *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc]init];
    });
    return shared;
}

- (instancetype)init{
    self = [super init];
    if (self) {
    
  // Start up the CBCentralManager
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
   // And somewhere to store the incoming data
    _data = [[NSMutableData alloc] init];
        _pair = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"PairBeacon"];
        self.cont=nil;
        self.languaje = [Service getLanguaje];
     
        
    }
    return self;
}
-(void) startTimer{
    self.timer = [NSTimer scheduledTimerWithTimeInterval:(10.0)
                                                  target: self
                                                selector:@selector(scan)
                                                userInfo: nil repeats: YES];
}
-(void) startTimerForReconect{
    self.timerForReconnect = [NSTimer scheduledTimerWithTimeInterval:(21600)
                                                  target: self
                                                selector:@selector(checkStatusBeaconForReconnect)
                                                userInfo: nil repeats: YES];

}
-(void) checkStatusBeaconForReconnect{
    if (self.isConnectedStatus==YES) {
        return;
    }
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString *beaconName = [userDefault objectForKey:@"beaconName"];
    if (beaconName==nil || [beaconName isEqualToString:@""]) {
        NSLog(@"No se registro un beacon para conectar");
    }else{
        self.uuid= beaconName;
        [self scan];
    }
}

-(void)updateViewDidConnectInOtherClass{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(updateViewDidConnect)]) {
        [self.delegate performSelector:@selector(updateViewDidConnect)];
    }
    else {
        NSLog(@"Delgate doesn't implement connect");
    }
}
-(void) updateViewDidDisconnectInOtherClass{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(updateViewDidDisconnect)]) {
        [self.delegate performSelector:@selector(updateViewDidDisconnect)];
    }
    else {
        NSLog(@"Delgate doesn't implement disconnect");
    }
}
-(void) generateAlert{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(alert)]) {
        [self.delegate performSelector:@selector(alert)];
    }
    else {
        NSLog(@"Delgate doesn't implement alert");
    }
}
-(void)statusBeacon{
    if (self.isConnectedStatus==YES) {
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(isConnected)]) {
            [self.delegate performSelector:@selector(isConnected)];
        }
        else {
            NSLog(@"Delgate doesn't implement status connected");
        }
    }
    else{
        if (self.delegate != nil && [self.delegate respondsToSelector:@selector(notConnected)]) {
            [self.delegate performSelector:@selector(notConnected)];
        }
        else {
            NSLog(@"Delgate doesn't implement status disconnected");
        }
   }
    
}
-(void)timeOutInOtherClass{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(timeOutForConnect)]) {
        [self.delegate performSelector:@selector(timeOutForConnect)];
    }
    else {
        NSLog(@"Delgate doesn't implement time out");
    }

}
-(void)uuidNotMatchInOtherClass{
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(uuidNotMatch)]) {
        [self.delegate performSelector:@selector(uuidNotMatch)];
    }
    else {
        NSLog(@"Delgate doesn't implement time out");
    }
}

- (void)disconnect{
 
    if (self.discoveredPeripheral!=nil) {
        [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
        
    }
   }
-(void)disconnectAutomatic{
[BeaconServices disconnectBeaconWithWait:NO Delegate:self];

}
-(void)searchMyBeacon{
   
 [self.discoveredPeripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID3]]];


}
#pragma mark - Central Methods
/** centralManagerDidUpdateState is a required protocol method.
 *  Usually, you'd check for other states to make sure the current device supports LE, is powered on, etc.
 *  In this instance, we're just using it to wait for CBCentralManagerStatePoweredOn, which indicates
 *  the Central is ready to be used.
 */
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    if (central.state != CBCentralManagerStatePoweredOn) {
        // In a real app, you'd deal with all the states correctly
        [self.timerForReconnect invalidate];
        self.timerForReconnect=nil;
        self.isConnectedStatus=NO;
        self.uuid=nil;
        self.discoveredPeripheral=nil;
        if (self.cont) {
            [self updateViewDidDisconnectInOtherClass];
        }
        return;
    }else{
    
        NSLog(@"Se volvio a encender el bluetooth");
        [self checkStatusBeaconForReconnect];
    
    }
}


/** Scan for peripherals - specifically for our service's 128bit CBUUID
 */
- (void)scan
{
 if (self.isConnectedStatus==YES) {
        if (self.timer) {
            [self.timer invalidate];
            self.timer = nil;
            self.countScan=0;
        }
        return;
    }
    if (self.timer) {
        self.countScan+=1;
        [self stopScan];
        if (self.countScan == 3) {
            NSLog(@"tiempo de espera agotado dispositivo no encontrado verifique que este encendido y dentro de un rango de 6 metros");
            [self timeOutInOtherClass];
            [self.timer invalidate];
            self.timer = nil;
            self.countScan=0;
            return;
        }
        [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_MAIN]]
                                                    options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        NSLog(@"New scan started");
    }
    else{
//        if (self.cont) {
            [self startTimer];
//        }
      [self.centralManager scanForPeripheralsWithServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_MAIN]] options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @YES }];
        NSLog(@"Scanning started");
    }
}
-(void)stopScan{
[self.centralManager stopScan];
    NSLog(@"scan stop");
}


/** This callback comes whenever a peripheral that is advertising the TRANSFER_SERVICE_UUID is discovered.
 *  We check the RSSI, to make sure it's close enough that we're interested in it, and if it is,
 *  we start the connection process
 */
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
NSLog(@"Discovered %@ at %@", peripheral.name, RSSI);
    NSLog(@"I see an advertisement with identifer: %@, state: %ld, name: %@, services: %@, description: %@", [peripheral identifier], (long)[peripheral state],[peripheral name], [peripheral services], [advertisementData description]);
    
    // Ok, it's in range - have we already seen it?
    NSDictionary * cid= advertisementData;
    NSLog(@"%@", cid);
    NSLog(@"%@", peripheral.name);
    NSLog(@"%@", self.uuid);
    if ([peripheral.name isEqualToString:self.uuid]) {
        //self.discoveredPeripheral != peripheral
        if (self.discoveredPeripheral==nil) {
            // Save a local copy of the peripheral, so CoreBluetooth doesn't get rid of it
            self.discoveredPeripheral = peripheral;
            // And connect
            NSLog(@"Connecting to peripheral %@", peripheral);
            peripheral.delegate=self;
            [self.centralManager connectPeripheral:peripheral options:nil];
        }
    }
}


/** If the connection fails for whatever reason, we need to deal with it.
 */
- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSLog(@"Failed to connect to %@. (%@)", peripheral, [error localizedDescription]);
    [self cleanup];
}


/** We've connected to the peripheral, now we need to discover the services and characteristics to find the 'transfer' characteristic.
 */
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"Peripheral Connected");

    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
        
    }
    self.countScan= 0;

    // Stop scanning
    [self.centralManager stopScan];
    NSLog(@"Scanning stopped");
    // Clear the data that we may already have
    [self.data setLength:0];
    // Make sure we get the discovery callbacks
    peripheral.delegate = self;
    // Search only for services that match our UUID
   //[peripheral discoverServices:nil];
    [peripheral discoverServices:@[[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID],[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID2]]];
}


/** The Transfer Service was discovered
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering services: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Discover the characteristic we want...
    
    // Loop through the newly filled peripheral.services array, just in case there's more than one.
    for (CBService *service in peripheral.services) {
         NSLog(@"Service found : %@",service.UUID);
        // [peripheral discoverCharacteristics:nil forService:service];
        if([service.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID]]){
         [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]] forService:service];
        
        }else if ([service.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID2]]){
        
            [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID2]] forService:service];
        }
        else if ([service.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_SERVICE_UUID3]]){
            
            [peripheral discoverCharacteristics:@[[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID3]] forService:service];
        }
    }
}

/** The Transfer characteristic was discovered.
 *  Once this has been found, we want to subscribe to it, which lets the peripheral know we want the data it contains
 */
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    // Deal with errors (if any)
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        [self cleanup];
        return;
    }
    
    // Again, we loop through the array, just in case.
    for (CBCharacteristic *characteristic in service.characteristics) {
        
        // And check if it's the right one
         NSLog(@"characterists found : %@ from service: %@",characteristic.description, service);
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
            
        }else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID2]]){
            NSData  * data = nil;
            uint8_t value = 1;
            data = [NSData dataWithBytes:&value length:sizeof(value)];
            [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
        }
        else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID3]]){
            NSData  * data = nil;
            uint8_t value = 1;
            data = [NSData dataWithBytes:&value length:sizeof(value)];
            [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithoutResponse];
        }
   }
   // Once this is complete, we just need to wait for the data to come in.
}


/** This callback lets us know more data has arrived via notification on the characteristic
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error discovering characteristics: %@", [error localizedDescription]);
        return;
    }
   NSString *stringFromData2=[[NSString alloc] initWithData:characteristic.value encoding:NSASCIIStringEncoding];
    unichar ch = [stringFromData2 characterAtIndex:0];

   // NSLog(@"string %@ unichar: %hu", stringFromData2, ch);
    NSString *dec = [NSString stringWithFormat:@"%hu",ch];
    NSString *hex = [NSString stringWithFormat:@"0x%lX",
                     (unsigned long)[dec integerValue]];
   NSLog(@"Receipt from beacon %@", hex);
    
   // Log it
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
       
        if(ch==128){
          if (self.cont) {
                [self.cont startAlertFromBeacon];
            }else{
                [self generateAlert];
            }
        }
    }
  }


/** The peripheral letting us know whether our subscribe/unsubscribe happened or not
 */
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (error) {
        NSLog(@"Error changing notification state: %@", error.localizedDescription);
    }
    
    // Exit if it's not the transfer characteristic
    if (![characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
        return;
    }
    
    // Notification has started
    if (characteristic.isNotifying) {
        NSLog(@"Notification began on %@", characteristic);
        
        if (self.cont&&self.isConnectedStatus==NO) {
            [self updateViewDidConnectInOtherClass];
        }
        self.isConnectedStatus=YES;
        if (self.timer) {
            [self.timer invalidate];
            self.timer = nil;
        }
        self.countScan= 0;
        Profile * user = [Profile findWithUserId:[Service getUserId]];
        if (!user.beaconId || [user.beaconId isEqual:[NSNull null]]) {
            [BeaconServices connectBeaconWithBeaconUUID:self.uuid Wait:NO Delegate:self];
        }
    }
    
    // Notification has stopped
    else {
        // so disconnect from the peripheral
        NSLog(@"Notification stopped on %@.  Disconnecting", characteristic);
        [self.centralManager cancelPeripheralConnection:peripheral];
    }
}


/** Once the disconnection happens, we need to clean up our local copy of the peripheral
 */
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60];
    if ([self.languaje isEqualToString:@"1"]) {
        localNotification.alertBody = @"Enlace con beacon perdido";
        localNotification.alertAction = @"Ver conexión";
    }else{
        localNotification.alertBody = @"Connection with Beacon was lost";
        localNotification.alertAction = @"Show connection";
    }
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];

    NSLog(@"Peripheral Disconnected");
    // We're disconnected, so start scanning again
//    self.isConnectedStatus=NO;
//    [self scan];
    if (self.cont) {
        [self updateViewDidDisconnectInOtherClass];

    }
    self.discoveredPeripheral = nil;
    self.uuid=nil;
    if (self.isConnectedStatus==YES) {
        [BeaconServices disconnectBeaconWithWait:NO Delegate:self];
        self.isConnectedStatus=NO;
    }

}


/** Call this when things either go wrong, or you're done with the connection.
 *  This cancels any subscriptions if there are any, or straight disconnects if not.
 *  (didUpdateNotificationStateForCharacteristic will cancel the connection if a subscription is involved)
 */
- (void)cleanup
{
    // Don't do anything if we're not connected
        if (!self.discoveredPeripheral.isAccessibilityElement) {
            return;
        }
    
    // See if we are subscribed to a characteristic on the peripheral
    if (self.discoveredPeripheral.services != nil) {
        for (CBService *service in self.discoveredPeripheral.services) {
            if (service.characteristics != nil) {
                for (CBCharacteristic *characteristic in service.characteristics) {
                    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:TRANSFER_CHARACTERISTIC_UUID]]) {
                        if (characteristic.isNotifying) {
                            // It is notifying, so unsubscribe
                            [self.discoveredPeripheral setNotifyValue:NO forCharacteristic:characteristic];
                            
                            // And we're done.
                            return;
                        }
                    }
                }
            }
        }
    }
    
    // If we've got this far, we're connected, but we're not subscribed, so we just disconnect
    [self.centralManager cancelPeripheralConnection:self.discoveredPeripheral];
}
-(void)successResponseWithService:(kService)service Response:(id)response{
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kCONNECT_BEACON:
            dictionary = [dictionary objectForKey:@"ConnectBeaconResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                 Profile * user = [Profile findWithUserId:[Service getUserId]];
                user.beaconId= self.uuid;
                [user mergeObject];
                //acomodar boton de alerta
                NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                [userDefault setObject:self.discoveredPeripheral.name forKey:@"beaconName"];
                [self.timerForReconnect invalidate];
                self.timerForReconnect= nil;
                [self startTimerForReconect];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            
            break;
        case kDISCONNECT_BEACON:
            dictionary = [dictionary objectForKey:@"DisconnectBeaconResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Profile * user = [Profile findWithUserId:[Service getUserId]];
                user.beaconId= nil;
                [user mergeObject];
                //acomodar boton de alerta
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                
            }
            
            break;

        default:
            break;
    }


}


@end
