//
//  NotificationVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Friend.h"

@class ContainerVC;

@interface NotificationVC : UIViewController

@property ContainerVC* container;
-(void)updateView;
@end
