//
//  ReviewVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 18/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReviewVC : UIViewController


+ (instancetype) shareInstance;

/**
 * Show tha view of comments in the current view
 */
- (void) showNow;


/**
 * When the count is equal to 20 then show the view.
 * @param count Number to increase.
 */
- (void) plusCount:(int)plus;
@end
