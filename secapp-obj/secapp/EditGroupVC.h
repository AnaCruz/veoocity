//
//  EditGroupVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddFriendsToChatGroupVC.h"
@class ContainerVC;

@interface EditGroupVC : UIViewController <UINavigationControllerDelegate>
@property ContainerVC* mainContainer;
@property BOOL isNew;
@end
