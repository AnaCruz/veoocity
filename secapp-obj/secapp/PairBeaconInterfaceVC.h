//
//  PairBeaconInterfaceVC.h
//  secapp
//
//  Created by SECAPP on 27/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
//@protocol PairBeaconInterfaceVCDelegate <NSObject>
//-(void)startAlert;
//@end
@class ContainerVC;
@interface PairBeaconInterfaceVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *statusBeaconLabel;
 //@property (assign, nonatomic) id <PairBeaconInterfaceVCDelegate> delegate;
@property  NSString * status;
@property  NSString * instructions;
@property  BOOL isConnect;
@property ContainerVC * container;
-(void)alert;
@end
