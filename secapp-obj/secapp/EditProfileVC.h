//
//  EditProfile.h
//  secapp
//
//  Created by SecApp on 19/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class Profile;
@class ContainerVC;
@interface EditProfileVC : UIViewController

@property (nonatomic) NSMutableDictionary *userP;
@property ContainerVC* mainContainer;

@end
