//
//  InsecureZone.h
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"
#import "ResponseDelegate.h"

@interface InsecureZone :  GenericObject <ResponseDelegate>
@property (nonatomic, retain) NSNumber * radius;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * insecureZoneId;
@property (nonatomic, retain) NSString * timeInitial;
@property (nonatomic, retain) NSString * timeFinal;
@property (nonatomic) NSNumber * isOwner; //If the user isn´t the owner, the insecure zone was created for a monitor

/**
 * Create a object using the name of the entity.
 */
+(instancetype)createObject;
/**
 * Find a insecure zone across a zoneId in the data base.
 * @param zoneId Insecure zone Identifier.
 * @return array Array with insecure zone data.
 */
+(NSArray *) findWithZoneId: (NSString *)zoneId;

/**
 * Find all insecure zones in the data base
 * @return array Array with all insecure zones data.
 */
+ (NSArray*) findAll;

@end
