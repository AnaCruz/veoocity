//
//  PairBeaconVC.h
//  secapp
//
//  Created by SecApp on 28/12/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ContainerVC;
@protocol PairBeaconCentralManagerDelegate <NSObject>
-(void)updateViewDidConnect;
-(void)updateViewDidDisconnect;
-(void)alert;
-(void)isConnected;
-(void)notConnected;
-(void)timeOutForConnect;
-(void)uuidNotMatch;
@end
@interface PairBeaconCentralManager : NSObject
@property (strong, nonatomic) NSString *uuid;
@property ContainerVC *cont;
 @property (assign, nonatomic) id <PairBeaconCentralManagerDelegate> delegate;
+ (PairBeaconCentralManager*) sharedInstance;
- (void)scan;
-(void)stopScan;
- (void)disconnect;
-(void)statusBeacon;
-(void)disconnectAutomatic;
-(void)searchMyBeacon;
@end
