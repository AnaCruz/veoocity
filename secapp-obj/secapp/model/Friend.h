//
//  Friend.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "People.h"

@class Group;

@interface Friend : People
@property (nonatomic, retain) NSNumber * batteryLevel;
@property (nonatomic, retain) NSNumber * currentLatitude;
@property (nonatomic, retain) NSNumber * currentLongitude;
@property (nonatomic, retain) NSSet *groups;
@property (nonatomic, retain) NSDate *positionDate;
@property (nonatomic) NSNumber * permissions;
@end

@interface Friend (CoreDataGeneratedAccessors)
- (void)addGroupsObject:(Group *)value;
- (void)removeGroupsObject:(Group *)value;
- (void)addGroups:(NSSet *)values;
- (void)removeGroups:(NSSet *)values;

/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;

/**
 * Find a user across a userId in the data base.
 * @param userId User Identifier.
 * @return array Array with all user data.
 */
+ (NSArray*) findWithUserId:(NSString*) userId;

/**
 * Find all user friends in the data
 * @return array Array with all friends data.
 */
+ (NSArray*) findAll;

@end
