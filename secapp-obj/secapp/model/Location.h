//
//  Location.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 25/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "GenericObject.h"

#define koLOCATION @"Location"

@interface Location : GenericObject

@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSDate * dateLocation;

/**
 * Create a object.
 */
+ (instancetype) createObject;

/**
 * Find all location data, order by date.
 * @return array Array with locations.
 */
+ (NSArray*) findAllOrderbyDateLocation;

@end
