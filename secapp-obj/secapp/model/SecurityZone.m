//
//  SecurityZone.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SecurityZone.h"
#import "AppDelegate.h"
#import "UserServices.h"
#import "SecurityZonesServices.h"


@implementation SecurityZone

@dynamic radius;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic securitiZoneId;


+(instancetype)createObject {
   SecurityZone *securityZone= [super createObjectWithNameEntity:@"SecurityZone"];
    return securityZone;
}

+(NSArray *) findWithZoneId: (NSString *)zoneId{
    AppDelegate * app = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"SecurityZone"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"securitiZoneId == '%@'", zoneId]];
    [request setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
  return array;
}

+ (NSArray*) findAll {
    return [super findAllWithNameEntity:@"SecurityZone"];
}

@end
