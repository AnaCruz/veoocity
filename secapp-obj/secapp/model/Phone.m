//
//  Phone.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Phone.h"
#import "People.h"


@implementation Phone

@dynamic phone;
@dynamic people;

+ (instancetype) createObject {
    id object = [super createObjectWithNameEntity:@"Phone"];
    return object;
}

@end
