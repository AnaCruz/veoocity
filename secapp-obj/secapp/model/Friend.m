//
//  Friend.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Friend.h"
#import "AppDelegate.h"

@implementation Friend

@dynamic batteryLevel;
@dynamic currentLatitude;
@dynamic currentLongitude;
@dynamic groups;
@dynamic positionDate;
@dynamic permissions;

+ (instancetype)createObject {
    Friend *friend = [super createObjectWithNameEntity:@"Friend"];
    return friend;
}

+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Friend"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
   if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

+ (NSArray*) findWithUserId:(NSString*) userId {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Friend"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"userId == '%@'", userId]];
    [request setPredicate:predicate];

    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

@end
