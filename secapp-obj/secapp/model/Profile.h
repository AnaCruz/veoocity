//
//  Profile.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "People.h"
#import "ResponseDelegate.h"
#import "ContainerVC.h"

@interface Profile : People <ResponseDelegate>
@property (nonatomic, retain) NSString * aplicationType;// 11 for ios 10 for android
@property (nonatomic) NSNumber * hiddenPosition; //If the user hidden their position this property is false
@property (nonatomic, retain) NSString * lastLatitude;
@property (nonatomic, retain) NSString * lastLongitude;
@property (nonatomic) NSNumber * showOccurrenceNotifications; // False for not receive Ocurrence notifications
@property (nonatomic) NSNumber * showSecureZonesNotifications;  // False for not receive Zone notifications
@property (nonatomic) NSString * beaconId; //Beacon name for pair panic button
@property (nonatomic) NSString * countryCode;
@property (nonatomic) NSString * phoneAlertCalls; //Phone number for call in alert status
/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;
/**
 * Find the profile across a userId in the data base.
 * @param userId User Identifier.
 * @return profile Object with profile data.
 */
+ (Profile*) findWithUserId:(NSString*) userId;
/**
 * Update the profile. Call services if necessary
 */
+ (void) updateProfile;
/**
 * Find all profiles saved in the data base
 * @return array Array with all profiles data.
 */
+ (NSArray*) findAllProfiles;
@end
