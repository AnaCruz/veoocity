//
//  Email.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Email.h"
#import "People.h"


@implementation Email

@dynamic email;
@dynamic people;

+ (instancetype) createObject {
    id object = [super createObjectWithNameEntity:@"Email"];
    return object;
}

@end
