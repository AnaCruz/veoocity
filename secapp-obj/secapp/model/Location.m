//
//  Location.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 25/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Location.h"
#import "AppDelegate.h"

@implementation Location

@dynamic latitude;
@dynamic longitude;
@dynamic dateLocation;

+ (instancetype) createObject {
    id object = [super createObjectWithNameEntity:koLOCATION];
    return object;
}

+ (NSArray*) findAllOrderbyDateLocation {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:koLOCATION];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"dateLocation" ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
   NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }  
    return array;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"Location (Lat: %@ Lon: %@ Date: %@)", self.latitude, self.longitude, self.dateLocation];
}

@end
