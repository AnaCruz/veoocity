//
//  SecurityZone.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"
#import "ResponseDelegate.h"

@interface SecurityZone : GenericObject <ResponseDelegate>

@property (nonatomic, retain) NSNumber * radius;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * securitiZoneId;

/**
 * Create a object using the name of the entity.
 */
+(instancetype)createObject;

/**
 * Find a secure zone across a zoneId in the data base.
 * @param zoneId Secure zone Identifier.
 * @return array Array with secure zone data.
 */
+(NSArray *) findWithZoneId: (NSString *)zoneId;

/**
 * Find all secure zones in the data
 * @return array Array with all secure zones data.
 */
+ (NSArray*) findAll;
@end
