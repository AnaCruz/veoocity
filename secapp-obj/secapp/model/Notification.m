//
//  Notification.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Notification.h"
#import "AppDelegate.h"

@implementation Notification

@dynamic date;
@dynamic groupId;
@dynamic groupName;
@dynamic invitationId;
@dynamic latitude;
@dynamic longitude;
@dynamic messege;
@dynamic notificationId;
@dynamic status;
@dynamic typeId;
@dynamic userIdFrom;
@dynamic chatGroupId;
@dynamic chatGroupName;

+ (instancetype)createObject {
    Notification *notification = [super createObjectWithNameEntity:@"Notification"];                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
    return notification; 
}

+ (Notification*) findWithNotificationId:(NSString*) notificationId {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"notificationId == '%@'", notificationId]];
    [request setPredicate:predicate];
    NSArray *array = [context executeFetchRequest:request error:&error];
   if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    if ([array count] < 1) {
        return nil;
    }
    return [array objectAtIndex:0];
}

+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Notification"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}
@end
