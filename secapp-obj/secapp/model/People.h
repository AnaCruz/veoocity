//
//  People.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 12/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@class Email, Phone;

@interface People : GenericObject

@property (nonatomic, retain) NSString * facebookId;
@property (nonatomic, retain) NSNumber * gender;
@property (nonatomic, retain) NSString * lastName;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * pictureProfile;

@property (nonatomic, retain) NSString * userId;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSSet *emails;
@property (nonatomic, retain) NSSet *phones;
@end

@interface People (CoreDataGeneratedAccessors)
/**
 * Add an email object to internal BD
 * @param value Email for add.
 */
- (void)addEmailsObject:(Email *)value;
/**
 * Remove an email object from the internal BD
 * @param value Email for delete.
 */
- (void)removeEmailsObject:(Email *)value;
/**
 * Add an email´s set to internal BD
 * @param values NSSet of emails for add.
 */
- (void)addEmails:(NSSet *)values;
/**
 * Remove an email´s set to internal BD
 * @param values NSSet of emails for delete.
 */
- (void)removeEmails:(NSSet *)values;
/**
 * Add an phone object to internal BD
 * @param value Phone number for add.
 */
- (void)addPhonesObject:(Phone *)value;
/**
 * Remove an phone object from the internal BD
 * @param value Phone Number for delete.
 */
- (void)removePhonesObject:(Phone *)value;
/**
 * Add an phone set to internal BD
 * @param values NSSet of phone numbers for add.
 */
- (void)addPhones:(NSSet *)values;
/**
 * Remove an phone set from the internal BD
 * @param values NSSet of phone numbers for delete.
 */
- (void)removePhones:(NSSet *)values;

@end
