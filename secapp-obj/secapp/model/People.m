//
//  People.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 12/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "People.h"
#import "Email.h"
#import "Phone.h"


@implementation People

@dynamic facebookId;
@dynamic gender;
@dynamic lastName;
@dynamic name;
@dynamic pictureProfile;

@dynamic userId;
@dynamic status;
@dynamic userName;
@dynamic emails;
@dynamic phones;

@end
