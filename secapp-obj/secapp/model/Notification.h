//
//  Notification.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@interface Notification : GenericObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * groupId;
@property (nonatomic, retain) NSString * groupName;
@property (nonatomic, retain) NSString * invitationId;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * messege;
@property (nonatomic, retain) NSString * notificationId;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSNumber * typeId;
@property (nonatomic, retain) NSString * userIdFrom;
@property (nonatomic, retain) NSString * chatGroupId;
@property (nonatomic, retain) NSString * chatGroupName;
/**
 * Create a object using the name of the entity.
 */
+ (instancetype)createObject;
/**
 * Find a notification across a notificationId in the data base.
 * @param notificationId Notification Identifier.
 * @return notification Object type notification.
 */
+ (Notification*) findWithNotificationId:(NSString*) notificationId;
/**
 * Find all notifications in the data base.
 * @return array Array with all notifications data.
 */
+ (NSArray*) findAll;

@end
