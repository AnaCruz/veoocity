//
//  Phone.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@class People;

@interface Phone : GenericObject

@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) People *people;
/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;

@end
