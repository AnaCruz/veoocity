//
//  Message.h
//  secapp
//
//  Created by SecApp on 09/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"


@interface Message : GenericObject

@property (nonatomic, retain) NSNumber * isReaded;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSNumber * messageId;
@property (nonatomic, retain) NSDate * sendDate;
@property (nonatomic, retain) NSNumber * userIdFrom;
@property (nonatomic, retain) NSNumber * userIdTo;
@property (nonatomic, retain) NSString * groupId;
@property (nonatomic, retain) NSString * groupName;
@property (nonatomic, retain) NSString * imageGroup;
@property (nonatomic) NSNumber * isOwner;
@property (nonatomic, retain) NSNumber * fromMonitor;
@property (nonatomic, retain) NSNumber * toMonitor;

@end

@interface Message (CoreDataGeneratedAccessors)

/**
 *Create a new object type message.
 * @return An object type message
 */
+(instancetype) createObject;

/**
 * Find all messages
 * @return array Array with all messages data.
 */
+(NSArray*)findAll;

/**
 * Recives the IdMessage and search the object with all their properties
 * @param messageId Identifier of a message
 * @return message Object with all properties of the message
 */
+(Message*) findWithMessageId:(NSString *)messageId;

/**
 * Recives the UserIdFrom and UserIdTo and search the object with all their properties
 * @param userIdFrom Identifier of a message
 * @param userIdTo Identifier of a message
 * @return message Object with all properties of the message
 */
+(Message*) findWithUserIdFrom:(NSString *)userIdFrom UserIdTo:(NSString*) userIdTo;

/**
 * Recives the UserIdFrom and UserIdTo and search the object with all their properties
 * @param groupId Identifier of chat group
 * @return message Object with all properties of the message
 */
+(Message*) findWithGroupId:(NSString *)groupId;

/**
 * Search for all unread messages
 * @return Array with all messages
 */
+(NSArray*) findUnreadMessage;


@end
