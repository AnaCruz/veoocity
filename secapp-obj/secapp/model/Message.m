//
//  Message.m
//  secapp
//
//  Created by SecApp on 09/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Message.h"
#import "AppDelegate.h"

@implementation Message

@dynamic isReaded;
@dynamic message;
@dynamic messageId;
@dynamic sendDate;
@dynamic userIdFrom;
@dynamic userIdTo;
@dynamic groupId;
@dynamic groupName;
@dynamic imageGroup;
@dynamic isOwner;
@dynamic fromMonitor;
@dynamic toMonitor;

+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sendDate" ascending:NO];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Message (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

+(instancetype)createObject {
    Message *message= [super createObjectWithNameEntity:@"Message"];
    return message;
}


+(Message*) findWithMessageId:(NSString *)messageId{
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSError * error;
    NSPredicate * predicate = [NSPredicate predicateWithFormat:
                               [NSString stringWithFormat:@"messageId == '%@'", messageId]];
    [request setPredicate:predicate];
    NSArray *array= [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Message (ERROR %@)", [error localizedDescription]);
        return nil;
    }
    if ([array count] < 1) {
        return nil;
    }
    return [array objectAtIndex:0];
}
+(Message*) findWithGroupId:(NSString *)groupId{
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSError * error;
    NSPredicate * predicate = [NSPredicate predicateWithFormat:
                               [NSString stringWithFormat:@"groupId == '%@'", groupId]];
    [request setPredicate:predicate];
    NSArray *array= [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Message (ERROR %@)", [error localizedDescription]);
        return nil;
    }
    if ([array count] < 1) {
        return nil;
    }
    return [array objectAtIndex:0];
}


+(Message*) findWithUserIdFrom:(NSString *)userIdFrom UserIdTo:(NSString*) userIdTo {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSError * error;
    NSPredicate * predicate = [NSPredicate predicateWithFormat:
                               [NSString stringWithFormat:@"(userIdFrom == '%@' OR userIdTo == '%@') AND (userIdFrom == '%@' OR userIdTo == '%@')", userIdFrom, userIdFrom, userIdTo, userIdTo]];
    [request setPredicate:predicate];
    NSArray *array= [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Message (ERROR %@)", [error localizedDescription]);
        return nil;
    }
    if ([array count] < 1) {
        return nil;
    }
    return [array objectAtIndex:0];
}

+(NSArray*) findUnreadMessage {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Message"];
    NSError * error;
    NSPredicate * predicate = [NSPredicate predicateWithFormat: [NSString stringWithFormat:@"isReaded == %@", @(NO)]];
    [request setPredicate:predicate];
    NSArray *array= [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Message (ERROR %@)", [error localizedDescription]);
        return nil;
    }
    if ([array count] < 1) {
        return nil;
    }
    return array;
}


@end
