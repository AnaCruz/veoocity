//
//  Profile.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 19/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Profile.h"
#import "AppDelegate.h"
#import "UserServices.h"
#import "Phone.h"
#import "Email.h"
#import "ContainerVC.h"

#import "AppDelegate.h"

@interface Profile()

@end

@implementation Profile
@dynamic aplicationType;
@dynamic hiddenPosition;
@dynamic lastLatitude;
@dynamic lastLongitude;
@dynamic showOccurrenceNotifications;
@dynamic showSecureZonesNotifications;
@dynamic beaconId;
@dynamic countryCode;
@dynamic phoneAlertCalls;
//@dynamic container;
+ (instancetype)createObject{
    Profile * profile = [super createObjectWithNameEntity:@"Profile"];
    return profile;
}

+(Profile *) findWithUserId: (NSString *)userId{
    AppDelegate * app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Profile"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"userId == '%@'", userId]];
    [request setPredicate:predicate];
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Location (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    if (array && [array count] < 1) {
        return nil;
    }
    return [array objectAtIndex:0];
}
+ (NSArray*) findAllProfiles {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Profile"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userId" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"Profile (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

+ (void) updateProfile {
    NSLog(@"userID %@", [UserServices getUserId]);
    if ([UserServices getUserId]!=nil && ![[UserServices getUserId] isEqual:[NSNull null]]) {
        Profile *profile = [Profile findWithUserId:[UserServices getUserId]];
        if (profile==nil) {
            NSArray * array= [Profile findAllProfiles];
            NSLog(@"Profiles saved:%@",array);
            for (Profile *usersToDelete in array) {
                [usersToDelete deleteObject];
            }
            profile = [Profile createObject];
            [profile saveObject];
            [UserServices getProfileUserOptimizedWithWait:NO Delegate:profile Extra:profile];
            
        }else if(![[UserServices getUserId] isEqualToString:profile.userId]){
            NSLog(@"ya existe perfil lleno pero no es el nuevo");
            NSArray * array= [Profile findAllProfiles];
            NSLog(@"Profiles saved:%@",array);
            for (Profile *usersToDelete in array) {
                [usersToDelete deleteObject];
            }
            profile = [Profile createObject];
            [profile saveObject];
            [UserServices getProfileUserOptimizedWithWait:NO Delegate:profile Extra:profile];
        }else{
            NSLog(@"Ya hay perfil lleno correcto");
        }
    }
}

-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   switch (service) {
        case kGET_USER_PROFILE:
            dictionary = [dictionary objectForKey:@"ProfileResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [self parseDictionary:dictionary ToProfile:extra];
              }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
             }
            break;
       case kGET_USER_PROFILE_OPTIMIZED:
           dictionary = [dictionary objectForKey:@"OptimizedProfileResult"];
           responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
           if (responseCode == 200) {
               NSLog(@"profile response code ");
               [self parseDictionary:dictionary ToProfileOptimized:extra];
           }
           if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
               [app forceCloseSession];
           }
           break;
        default:
            break;
    }
}

/**
 * Parsing the JSON to Profile Object
 * @param dictionary JSON Object
 * @param profile Profile Object
 */
- (void) parseDictionary:(NSDictionary*) dictionary ToProfile:(Profile*) profile {
    profile.userId = [UserServices getUserId];
    profile.userName = [dictionary objectForKey:@"Username"];
    profile.name = [dictionary objectForKey:@"Name"];
    profile.lastName = [dictionary objectForKey:@"LastName"];
   // profile.gender = [NSNumber numberWithInt:[[dictionary objectForKey:@"Gender"] intValue]];
    profile.status = [NSNumber numberWithInt:[[dictionary objectForKey:@"Status"] intValue]];
    
    if ([dictionary objectForKey:@"idFacebook"] && ![[dictionary objectForKey:@"idFacebook"] isEqual: [NSNull null]]) {
        profile.facebookId = [dictionary objectForKey:@"idFacebook"];
    }
    NSString *uri= [dictionary objectForKey:@"ProfileImageURL"];
    if (uri && ![uri isEqual: [NSNull null]])  {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
            profile.pictureProfile = data;
             [profile mergeObject];
        });
    }
    [profile removePhones:profile.phones];
    NSArray *tmpPhones = [dictionary objectForKey:@"Telephone"];
    for (NSString* phone in tmpPhones) {
        Phone *objPhone = [Phone createObject];
        objPhone.phone = phone;
        [objPhone saveObject];
        [profile addPhonesObject:objPhone];
    }
    [profile removeEmails:profile.emails];
    NSArray *tmpEmail = [dictionary objectForKey:@"Email"];
    for (NSString* email in tmpEmail) {
        Email *objEmail = [Email createObject];
        objEmail.email = email;
        [objEmail saveObject];
        [profile addEmailsObject:objEmail];
    }
    profile.aplicationType =[dictionary objectForKey:@"ApplicationType"];
    profile.hiddenPosition = [dictionary objectForKey:@"HiddenPosition"];
    profile.showOccurrenceNotifications =[dictionary objectForKey:@"ShowOccurrencesNot"];
    profile.showSecureZonesNotifications=[dictionary objectForKey:@"ShowSecureZonesNot"];
    profile.lastLatitude=[dictionary objectForKey:@"LastPositionLatitude"];
     profile.lastLongitude=[dictionary objectForKey:@"LastPositionLongitude"];
    if ([dictionary objectForKey:@"BeaconId"] && ![[dictionary objectForKey:@"BeaconId"] isEqual: [NSNull null]]) {
        profile.beaconId=[dictionary objectForKey:@"BeaconId"];
    }
    if ([dictionary objectForKey:@"AlertPhoneNumber"] && ![[dictionary objectForKey:@"AlertPhoneNumber"] isEqual: [NSNull null]]) {
        profile.phoneAlertCalls=[dictionary objectForKey:@"AlertPhoneNumber"];
    }
    if ([dictionary objectForKey:@"CountryCode"] && ![[dictionary objectForKey:@"CountryCode"] isEqual: [NSNull null]]) {
        profile.countryCode=[dictionary objectForKey:@"CountryCode"];
    }
    [profile mergeObject];
}

/**
 * Parsing the JSON to Profile Object
 * @param dictionary JSON Object
 * @param profile Profile Object
 */
- (void) parseDictionary:(NSDictionary*) dictionary ToProfileOptimized:(Profile*) profile {
    profile.userId = [UserServices getUserId];
    
    profile.status = [NSNumber numberWithInt:[[dictionary objectForKey:@"Status"] intValue]];
    profile.aplicationType =[dictionary objectForKey:@"ApplicationType"];
    profile.hiddenPosition = [dictionary objectForKey:@"HiddenPosition"];
    profile.showOccurrenceNotifications =[dictionary objectForKey:@"ShowOccurrencesNot"];
    profile.showSecureZonesNotifications=[dictionary objectForKey:@"ShowSecureZonesNot"];
    profile.lastLatitude=[dictionary objectForKey:@"LastPositionLatitude"];
    profile.lastLongitude=[dictionary objectForKey:@"LastPositionLongitude"];
    if ([dictionary objectForKey:@"BeaconId"] && ![[dictionary objectForKey:@"BeaconId"] isEqual: [NSNull null]]) {
        profile.beaconId=[dictionary objectForKey:@"BeaconId"];
    }
    if ([dictionary objectForKey:@"AlertPhoneNumber"] && ![[dictionary objectForKey:@"AlertPhoneNumber"] isEqual: [NSNull null]]) {
        profile.phoneAlertCalls=[dictionary objectForKey:@"AlertPhoneNumber"];
    }
    if ([dictionary objectForKey:@"CountryCode"] && ![[dictionary objectForKey:@"CountryCode"] isEqual: [NSNull null]]) {
        profile.countryCode=[dictionary objectForKey:@"CountryCode"];
    }
    [profile mergeObject];
    NSLog(@"New profile %@",profile);
}

@end
