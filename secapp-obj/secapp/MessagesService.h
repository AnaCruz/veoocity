//
//  Messages.h
//  secapp
//
//  Created by SecApp on 09/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@interface MessagesService : Service

+(void) getMessageListWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) getConversationWithUserTo:(NSString *)userTo ToMonitor:(BOOL)toMonitor Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) sendMessageWithUserTo:(NSString *)userTo Message:(NSString *)message Wait:(BOOL)wait ToMonitor:(BOOL)toMonitor Delegate:(id<ResponseDelegate>) delegate;
+(void) createChatGroupWithName: (NSString *)name AndImage:(NSString *)image Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getChatGroupListWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getContactListOnChatGroup:(NSString *)groupID Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) addUsersToChatGroupWithIdGroup: (NSString *)groupID AndListUsers:(NSArray *)users Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) updateImageAndNameOfChatGroup: (NSString *)newName Image:(NSString *)image64 GroupId:(NSString* )chatGroupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) deleteChatGroupWithGroupID:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) deleteContactFromChatGroupWithUserID:(NSString *)userId ChatGroupId:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) sendMessageWithGroupID:(NSString *)groupID Message:(NSString *)message Wait:(BOOL)wait Delegate:(id<ResponseDelegate>) delegate;
+(void) getConversationGroupWithChatGroupID:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) leaveChatGroupWithChatGroupId:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;

+(void) getChatGroupListWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate Extra:(id)extra;
@end
