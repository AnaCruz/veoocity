//
//  DisableAlertView.m
//  secapp
//
//  Created by SecApp on 18/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "DisableAlertView.h"
#import <CoreLocation/CoreLocation.h>
#import "ContainerVC.h"
#import "DisableAlertsReasonsTable.h"
#import "AppDelegate.h"
#import "Alert/AlertView.h"


@interface DisableAlertView ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewAlert;
@property (weak, nonatomic) IBOutlet UIButton *buttonOk;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property DisableAlertsReasonsTable *reasonsList;
@property   NSArray *  options;
@property NSString *alertType;
@end

@implementation DisableAlertView
-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.buttonCancel.layer.cornerRadius=5;
    self.buttonOk.layer.cornerRadius=5;
    self.viewAlert.layer.cornerRadius=5;
    CGAffineTransform trans = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
    self.viewAlert.transform = trans;

    }
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.reasonsList = segue.destinationViewController;
      __weak typeof(self)weakSelf = self;
      self.reasonsList.callback = ^(int tag){
        weakSelf.alertType=[NSString stringWithFormat:@"%d", tag];
         };
   
}

- (void) showOptionsForDisableAlertInController:(UIViewController*) controller {
    [controller addChildViewController:self];
    [controller.view addSubview:self.view];
    
}

- (IBAction)buttonAlert:(UIButton*)sender {
    if (sender.tag == 1) {
        if(self.alertType){
               if (self.container) {
                [self.container disableAlertWithAlertType:[self.alertType intValue]];
                self.alertType=nil;
            }
        }else{
            return;
        }
    }
    [UIView animateWithDuration:0.15f
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }];
    }

-(void)viewDidAppear:(BOOL)animated {
 
  dispatch_async(dispatch_get_main_queue(), ^{
 
        [UIView animateWithDuration:0.15f
                              delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 100.0, 100.0);
                         }
                         completion:^(BOOL finished) {
                           
                             [self.viewAlert updateConstraints];
                         } ];
    });
}

-(void) animationStopped {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
-(void)viewDidDisappear:(BOOL)animated{
    self.alertType=nil;
}


@end
