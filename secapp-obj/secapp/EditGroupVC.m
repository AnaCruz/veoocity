//
//  EditGroupVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EditGroupVC.h"
#import "ChatContainerVC.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "ContainerVC.h"
#import "MessagesService.h"
#import "AlertInfo.h"
#import "Alert/AlertView.h"
#import "AlertViewSingleInput.h"
#import "AddFriendsToChatGroupVC.h"
@interface EditGroupVC () <ResponseDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *picture;
@property (weak, nonatomic) IBOutlet UITextField *groupName;
@property (weak, nonatomic) NSNumber * groupId;
@property float height;

@end

@implementation EditGroupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.picture.clipsToBounds = YES;
    int radius = 100/2;
    self.picture.layer.cornerRadius = radius;
    self.picture.layer.borderWidth = 3;
    self.picture.layer.borderColor = [[UIColor whiteColor] CGColor];
    [self.picture setBackgroundImage:[UIImage imageNamed:@"chat-29"] forState:UIControlStateNormal];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self registerForKeyboardNotifications];
}

- (IBAction)takePhoto:(id)sender {
    // Show options to select a image.
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Take photo of:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            @"Clear",
                            nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
/**
 * Open the camara or galery for select a picture.
 */
-(void) selectImageFromCamera:(BOOL) fromCamera {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.allowsEditing = YES; // Editing image
    [self.navigationController.parentViewController presentViewController:picker animated:YES completion:nil];
}
#pragma mark - Delegates methods of UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    // Show image edited
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.picture setBackgroundImage:image forState:UIControlStateNormal];
    });

    [self.view setNeedsDisplay];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    
}

#pragma mark - Delegates methods of UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self selectImageFromCamera:YES];
            break;
        case 1:
            [self selectImageFromCamera:NO];
            break;
        case 2:
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageNamed:@"chat-29"] forState:UIControlStateNormal];
            });
            break;
    }
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveGroup:(UIButton *)sender {
     NSString* groupName = self.groupName.text;
    UIImage* imageProfile = self.picture.currentBackgroundImage;
    NSString* image64 = imageProfile ? [ImageUtils encodingImageToBase64:imageProfile] : @"";
    if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:[UIImage imageNamed:@"chat-29"]]]) {
        image64 = @"";
    }
    else{
        UIImage *scaledImage= [ImageUtils resizeImageFor72DPI:imageProfile];
        image64 = scaledImage ? [ImageUtils encodingImageToBase64:scaledImage] : @"";
    }

//    UIImage *originalImage = imageProfile;
//    UIImage *scaledImage= [ImageUtils resizeImageFor72DPI:originalImage];
//    
//    NSString* image64 = scaledImage ? [ImageUtils encodingImageToBase64:scaledImage] : @"";
//    if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:[UIImage imageNamed:@"avatar"]]]) {
//        image64 = @"";
//    }
  
    if ([StringUtils isEmptyStringsInArray:@[groupName]]) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"nombre.chat.grupal.vacio", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        alert.delegate=self;
        return;

    }
    AddFriendsToChatGroupVC * addFriends = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"AddFriendsToGroupVC"];
    addFriends.groupName=groupName;
    addFriends.image64=image64;
    addFriends.isNew=YES;
    [self.navigationController pushViewController:addFriends animated:YES];

        return;

    
}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado

    [self.view endEditing:YES];
}
#pragma mark keyboard show and hide

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
- (IBAction)cancel:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
//    NSDictionary* info = [aNotification userInfo];
//    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
//    UIView *container= self.view;
//    CGRect mainFrame = container.frame;
//    mainFrame.size.height = self.height - (kbSize.height-130);
//    container.frame = mainFrame;
    //[self performSelector:@selector(scrollToBottomTableView) withObject:nil afterDelay:.3];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
//    CGRect frame = self.view.frame;
//    frame.size.height=self.height;
//    self.view.frame = frame;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=49)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:49];
    }
    
    return NO;
}
@end
