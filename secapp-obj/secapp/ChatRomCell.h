//
//  ChatRomCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 15/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRomCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIView *background;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *down;
@property (weak, nonatomic) IBOutlet UILabel *nameSender;
@end
 