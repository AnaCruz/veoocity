//
//  ErrorResult.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 11/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ErrorResult.h"
#import "Alert.h"

@implementation ErrorResult

+ (BOOL) errorResultWithErrorCode:(int) errorCode AndErrorMessage:(NSString*) message {
    NSString* errorMessage = nil;
    switch (errorCode) {
        case 200:
            return YES;
            break;
        case 400:
            errorMessage = message;
            NSLog(@"ErrorResult: JSON de request no esta bien formado(muestra ErrorMessage)");
            break;
        case 401:
            errorMessage = @"Pin Incorrecto";
            NSLog(@"ErrorResult: Password incorrecto");
            break;
        case 403:
            NSLog(@"ErrorResult: Ya hay una sesion para ese usuario, necesita preguntar si desea cerrar la otra, en caso afirmativo reenviar el login con ForceClose \"true\"");
            break;
        case 404:
            NSLog(@"ErrorResult: No se encontro usuario con ese email/telefono/Id de Facebook/Id de twitter");
            break;
        case 409:
            errorMessage = @"Ya existe un usario registrado con estos datos.";
            NSLog(@"ErrorResult: Hay mas de un usuario registrado con ese telefono (muestra ErrorMessage)");
            break;
        case 500:
            NSLog(@"ErrorResult: Surgio un problema al crear la sesion");
            break;
        default:
            return NO;
            break;
    }
    if (errorMessage) {
        [Alert showAlertWithTitle:@"Error" AndMessage:errorMessage];
    }
    return NO;
}

@end
