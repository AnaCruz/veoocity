//
//  ConversationVC.h
//  secapp
//
//  Created by SecApp on 29/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellConversationSample.h"
#import "Friend.h"
#import "ContainerVC.h"

@interface ConversationVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) NSString* userIdTo;
@property Friend * friendSender;
@property BOOL isMonitor;
@property BOOL isVisibleInMapView;
- (void) drawInView:(UIView*) view;
- (IBAction)goBack:(id)sender;

@end
