//
//  OccurenceServices.h
//  secapp
//
//  Created by SecApp on 02/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"

@interface OccurenceServices : Service
+(void) createOccurrenceType: (NSString *)type Details: (NSString * )details Latitude:(NSString *)latitude Longitude: (NSString *)longitude Image:(NSString *)image Anonymous:(BOOL)anonynous HappenedDay:(NSString *)happenedDay Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getAllOccurrencesWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) findNearOccurrenceLatitude:(NSString *)latitude Longitude: (NSString *)longitude Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getOccurrenceInformationWithOccurrenceId:(NSString*)occurrenceId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) likeOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) dislikeOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) commentOccurrencesWithOccurrenceId:(NSString *)occurrenceId Comment: (NSString *)comment Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) likeCommentOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) dislikeCommentOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) tagCommentOccurrencesWithOccurrenceId:(NSString *)occurrenceId UserIdCommented: (NSString *)userIdCommented Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) showNotificationsOccurrencesWithPermission: (BOOL)permission Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) followOccurrencesWithOccurrenceId: (NSString *)occurrenceId Follow: (BOOL)follow Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;


//special incidences
+(void) getTypesSpecialOccurrencesWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) commentSpecialOccurrencesWithOccurrenceId:(NSString *)occurrenceId Comment: (NSString *)comment Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getSpecialOccurrenceInformationWithOccurrenceId:(NSString*)occurrenceId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) followUser: (NSString *)userIdToFollow Follow: (BOOL)follow Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) likeCommentSpecialOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) likeSpecialOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
@end
