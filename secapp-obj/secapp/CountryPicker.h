//
//  CountryPicker.h
//  secapp
//
//  Created by SECAPP on 09/03/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CountryPicker;

@protocol CountryPickerDelegate <UIPickerViewDelegate>

- (void)countryPicker:(CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code;

@end
@interface CountryPicker : UIPickerView
+ (NSArray *)countryNames;
+ (NSArray *)countryCodes;
+ (NSDictionary *)countryNamesByCode;
+ (NSDictionary *)countryCodesByName;

@property (nonatomic, weak) id<CountryPickerDelegate> delegate;

@property (nonatomic, copy) NSString *selectedCountryName;
@property (nonatomic, copy) NSString *selectedCountryCode;
@property (nonatomic, copy) NSLocale *selectedLocale;

@property (nonatomic, strong) UIFont *labelFont;

- (void)setSelectedCountryCode:(NSString *)countryCode animated:(BOOL)animated;
- (void)setSelectedCountryName:(NSString *)countryName animated:(BOOL)animated;
- (void)setSelectedLocale:(NSLocale *)locale animated:(BOOL)animated;


@end
