//
//  EditChatGroupExistingVC.m
//  secapp
//
//  Created by SecApp on 21/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EditChatGroupExistingVC.h"
#import "SimpleTableViewCell.h"
#import "MessagesService.h"
#import "AddFriendsToChatGroupVC.h"
#import "ImageUtils.h"
#import "Friend.h"
#import "AppDelegate.h"
#import "AlertView.h"

@interface EditChatGroupExistingVC () <ResponseDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *friendsArray;
@property (weak, nonatomic) IBOutlet UITextField *nameGroupTextField;
@property (weak, nonatomic) IBOutlet UIButton *picture;
@property int indexTmp;

@end

@implementation EditChatGroupExistingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.picture.clipsToBounds = YES;
    int radius = self.picture.frame.size.width/2;
    self.picture.layer.cornerRadius = radius;
    self.picture.layer.borderWidth = 3;
    self.picture.layer.borderColor = [[UIColor whiteColor] CGColor];
    NSString *uri= self.imageGroup;
    if (uri && ![uri isEqual: [NSNull null]]&&![uri isEqualToString:@""])  {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageWithData:data] forState:UIControlStateNormal];
            });
        });
    }
    else{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.picture setBackgroundImage:[UIImage imageNamed:@"chat-29"] forState:UIControlStateNormal];
        });
    }
    
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.friendsArray = [[NSMutableArray alloc] init];
    self.nameGroupTextField.text = self.nameGroup;
[MessagesService getContactListOnChatGroup:self.groupID Wait:NO Delegate:self];
}
- (IBAction)takePhoto:(id)sender {
    // Show options to select a image.
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Take photo of:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            @"Clear",
                            nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
/**
 * Open the camara or galery for select a picture.
 */
-(void) selectImageFromCamera:(BOOL) fromCamera {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.allowsEditing = YES; // Editing image
    [self.navigationController.parentViewController presentViewController:picker animated:YES completion:nil];
}
#pragma mark - Delegates methods of UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    // Show image edited
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.picture setBackgroundImage:image forState:UIControlStateNormal];
    });
    
    [self.view setNeedsDisplay];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    
}

#pragma mark - Delegates methods of UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self selectImageFromCamera:YES];
            break;
        case 1:
            [self selectImageFromCamera:NO];
            break;
        case 2:
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageNamed:@"chat-29"] forState:UIControlStateNormal];
            });
            break;
    }
}
- (IBAction)updateChatGroup:(UIButton *)sender {
    
    BOOL changeName=NO;
    BOOL changeImage=NO;
    
    if ([self.nameGroupTextField.text isEqualToString:@""]) {
        return;
    }
    if ([self.imageGroup isEqual:[NSNull null]]|| self.imageGroup==nil) {
        self.imageGroup=@"";
    }
     NSString *uri= self.imageGroup;
     NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
     UIImage* imageOriginal= [UIImage imageWithData:data];
    
    UIImage* imageProfile = self.picture.currentBackgroundImage;
    UIImage *scaledImage= [ImageUtils resizeImageFor72DPI:imageProfile];
    NSString* image64 = imageProfile ? [ImageUtils encodingImageToBase64:imageProfile] : @"";
    if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:[UIImage imageNamed:@"chat-29"]]]) {
        image64 = @"";
    }
    else if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:imageOriginal]]) {
        image64 = @"";
    }else{
        image64 = scaledImage ? [ImageUtils encodingImageToBase64:scaledImage] : @"";
        changeImage=YES;
    }
    NSString * newName= self.nameGroupTextField.text;
    if ([newName isEqualToString:self.nameGroup]) {
        newName= @"";
    }else{
        changeName=YES;
    }
    if (changeName==YES||changeImage==YES) {
        [MessagesService updateImageAndNameOfChatGroup:newName Image:image64 GroupId:self.groupID Wait:YES Delegate:self];
    }
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)addFriends:(id)sender {
    AddFriendsToChatGroupVC * addFriend = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"AddFriendsToGroupVC"];
    NSString* val = self.groupID;
    addFriend.groupID=[NSNumber numberWithInt:[val intValue]];
    addFriend.isNew=NO;
    addFriend.groupName= self.nameGroup;

 
    [self.navigationController pushViewController:addFriend animated:YES];

}
-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kGET_CONTACTGROUPLIST:
            dictionary = [dictionary objectForKey:@"UsersInChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayGroupList = [dictionary objectForKey:@"Contacts"];
                self.friendsArray = [arrayGroupList mutableCopy];
                [self.tableView reloadData];
                
                
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
             }
            break;
        case kUPDATE_CHATGROUP:
            dictionary = [dictionary objectForKey:@"UpdateChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
             [self.view endEditing:YES];
                self.nameGroup= self.nameGroupTextField.text;
             }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        case kDELETE_USER_FROM_CHATGROUP:
            dictionary = [dictionary objectForKey:@"DeleteUserFromChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [self.friendsArray removeObjectAtIndex:self.indexTmp];
                [self.tableView reloadData];
                
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        default:
            break;
    }
}
#pragma mark UITableView Delegate and Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactListCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactListCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
   NSDictionary* data = [self.friendsArray objectAtIndex:indexPath.row];
    Friend * friend;
    NSString *email= [data objectForKey:@"Email"];
     NSArray *array= [Friend findWithUserId:[data objectForKey:@"UserId"]];
    if(array && [array count]>0){
    friend = [array objectAtIndex:0];
         cell.nameLabel.text = friend.name;
    }else{
     cell.nameLabel.text = email;
    }
 
  cell.buttonDelete.tag= indexPath.row;
     [cell.buttonDelete addTarget:self action:@selector(buttonDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];

    dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        if([friend.pictureProfile isEqual:@""]|| friend.pictureProfile==nil|| !friend){
            
            cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
        }
        else{
            cell.profileImageView.image = [UIImage imageWithData:friend.pictureProfile];
            
        }
   });
   return cell;
}

-(void)buttonDeleteClicked:(UIButton *)sender{
    if ([self.friendsArray count]==2) {
       
        return;
    }
    NSDictionary * dic = [self.friendsArray objectAtIndex:sender.tag];
    
    self.indexTmp=(int)sender.tag;
    [MessagesService deleteContactFromChatGroupWithUserID:[dic objectForKey:@"UserId"] ChatGroupId:self.groupID Wait:NO Delegate:self];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.friendsArray count];
}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    
    [self.view endEditing:YES];
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=49)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:49];
    }
    
    return NO;
}


@end
