//
//  ChatContainerVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Friend.h"
@class Friend;

@interface ChatContainerVC : UIViewController <UINavigationControllerDelegate>
//-(void)showConversationIndividual;
-(void)notification;
@end
