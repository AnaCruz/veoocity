//
//  ChatVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "SimpleTableViewCell.h"
 
@class ChatVC;
@class ContainerVC;

@protocol ChatDelegate <NSObject>

@optional
-(void) showIndividualChat:(UIViewController*) chatVC;

@end

@interface ChatVC : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property ContainerVC* mainContainer;
@property id<ChatDelegate> delegate;

-(void)updateData;
@end
