//
//  Messages.m
//  secapp
//
//  Created by SecApp on 09/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "MessagesService.h"


@implementation MessagesService

/**
 * Conect with service and execute a consult with the server for
 * to get the message list of the user
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */

+(void) getMessageListWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@LastMessage/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_LAST_MESSAGE Wait:wait Delegate:delegate];
    [request execute];


}
+(void) getConversationWithUserTo:(NSString *)userTo ToMonitor:(BOOL)toMonitor Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@Conversation/%@/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], userTo, toMonitor ? @"TRUE": @"FALSE"];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CONVERSATION Wait:wait Delegate:delegate];
    [request execute];
}
+(void) sendMessageWithUserTo:(NSString *)userTo Message:(NSString *)message Wait:(BOOL)wait ToMonitor:(BOOL)toMonitor Delegate:(id<ResponseDelegate>) delegate {
   NSString* uri = [NSString stringWithFormat:@"%@SendMessage", [super getUrl]];
    NSDictionary* dataDic = @{
                                      @"UserIdFrom":[super getUserId],
                                      @"SessionId": [super getSessionId],
                                      @"UserIdTo":userTo,
                                      @"Message":message,
                                      @"ToMonitor":@(toMonitor)
                                    
                              };
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kSEND_MESSAGE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];


}

+(void) createChatGroupWithName: (NSString *)name AndImage:(NSString *)image Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@CreateChatGroup", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"Name":name,
                              @"Image":image                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_CHATGROUP Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];

}
+(void) getChatGroupListWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ChatGroups/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CHATGROUPLIST Wait:wait Delegate:delegate];
    [request execute];
}
+(void) getChatGroupListWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate Extra:(id)extra {
    NSString* uri = [NSString stringWithFormat:@"%@ChatGroups/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CHATGROUPLIST Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }

    [request execute];
}
+(void) getContactListOnChatGroup:(NSString *)groupID Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@UsersInChatGroup/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId],groupID];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CONTACTGROUPLIST Wait:wait Delegate:delegate];
    [request execute];
}
+(void) addUsersToChatGroupWithIdGroup: (NSString *)groupID AndListUsers:(NSArray *)users Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@AddUserToChatGroup", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"ChatGroupId":groupID,
                              @"UserIdToAdd":users
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_USERS_TO_CHATGROUP Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}

+(void) updateImageAndNameOfChatGroup: (NSString *)newName Image:(NSString *)image64 GroupId:(NSString* )chatGroupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@UpdateChatGroup", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"ChatGroupId":chatGroupId,
                              @"Image":image64,
                              @"Name": newName
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_CHATGROUP Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) deleteContactFromChatGroupWithUserID:(NSString *)userId ChatGroupId:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DeleteUserFromChatGroup/%@/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId],userId, groupId];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_USER_FROM_CHATGROUP Wait:wait Delegate:delegate];
    [request execute];

}

+(void) leaveChatGroupWithChatGroupId:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@LeaveChatGroup/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], groupId];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kLEAVE_CHATGROUP Wait:wait Delegate:delegate];
    [request execute];
    
}

+(void) deleteChatGroupWithGroupID:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DeleteChatGroup/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId],groupId];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_CHATGROUP Wait:wait Delegate:delegate];
    [request execute];

}
+(void) getConversationGroupWithChatGroupID:(NSString *)groupId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ConversationChatGroup/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], groupId];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CONVERSATIONGROUP Wait:wait Delegate:delegate];
    [request execute];
}
+(void) sendMessageWithGroupID:(NSString *)groupID Message:(NSString *)message Wait:(BOOL)wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SendMessageToChatGroup", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserIdFrom":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"ChatGroupId":groupID,
                              @"Message":message
                             
                              };

    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kSEND_MESSAGE_TO_GROUP Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
    
}

@end
