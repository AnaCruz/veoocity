//
//  InfoZoneVC.h
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 28/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CircleOverlay;
@class InfoZoneVC;

@protocol InfoZoneDelegate <NSObject>
@optional
- (void) infoZone:(InfoZoneVC*) infoZone EditeZone:(id) zone;
- (void) infoZone:(InfoZoneVC*) infoZone DeleteZone:(id) zone;

@end

@interface InfoZoneVC : UIViewController

@property id<InfoZoneDelegate> delegate;
@property (weak, nonatomic) CircleOverlay* zone;
@property BOOL isShow;
@property BOOL isOwner;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;

- (void) drawInView:(UIView*) view;

@end
