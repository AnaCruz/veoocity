//
//  ContactService.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ContactService.h"

@implementation ContactService

+(void) getCotactListWithUserId:( NSString *) userId AndSessionId:(NSString*) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@ContactList/%@/%@", [super getUrl], userId, sessionId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_CONTACT_LIST Wait:wait Delegate:delegate];
    [request execute];
}
+(void) getCotactListWithoutSomeDataWithUserId:( NSString *) userId AndSessionId:(NSString*) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@MapContactList/%@/%@", [super getUrl], userId, sessionId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_SOME_DATAFRIEND Wait:wait Delegate:delegate];
    [request execute];
}

+(void) changePermissionStatusWithContactId: (NSString *) contactId AndPermission: (BOOL) permission Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@ChangeContactPermission", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[Service getUserId],
                              @"SessionId":[Service getSessionId],
                              @"ContacId": contactId,
                              @"Permission": @(permission)
                              };
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kCHANGE_PERMISSIONS Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];


}

@end
