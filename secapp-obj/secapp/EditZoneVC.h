//
//  EditZoneVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 29/04/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditZoneVC;
@class CircleOverlay;
@protocol EditZoneDelegate <NSObject>
@optional
- (void) editZone:(EditZoneVC*) editZone SaveZone:(id) zone;
- (void) editZone:(EditZoneVC*) editZone CancelZone:(id) zone;

@end

@interface EditZoneVC : UIViewController

@property id<EditZoneDelegate> delegate;
@property (weak, nonatomic) CircleOverlay *zone;
@property BOOL isEdtiting;
@property BOOL isShow;
- (void) drawInView:(UIView*) view;


@end

