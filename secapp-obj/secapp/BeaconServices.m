//
//  BeaconServices.m
//  secapp
//
//  Created by SECAPP on 29/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import "BeaconServices.h"

@implementation BeaconServices
+(void) connectBeaconWithBeaconUUID:(NSString *)uuid Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ConnectBeacon", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"BeaconId":uuid
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kCONNECT_BEACON Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) disconnectBeaconWithWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DisconnectBeacon", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId]
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kDISCONNECT_BEACON Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
@end
