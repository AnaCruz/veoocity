//
//  AlertService.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 21/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AlertService.h"

@implementation AlertService

+ (void)activeAlertWithLatitude:(NSNumber*) latitude AndLonguitude:(NSNumber*) longitude Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SendAlert", [super getUrl]];
    NSDictionary* params = @{
                              @"UserId":[super getUserId],
                              @"Latitude":latitude,
                              @"Longitude":longitude,
                              @"SessionId":[super getSessionId]
                              };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kSEND_ALERT Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}

+ (void) desactiveAlertWithAlertType:(NSNumber*) type Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@DisableAlert/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], type];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDISABLE_ALERT Wait:wait Delegate:delegate];
    [request execute];
}

+ (void)saveAlertCallWithPhoneNumber:(NSString*) phone AndCountryCode:(NSString*) code Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SaveAlertCallNumber", [super getUrl]];
    NSDictionary* params = @{
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId],
                             @"CountryCode":code,
                             @"Phone":phone
                             };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kSAVE_ALERT_PHONE Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}
+ (void) deleteAlertPhoneCallNumberWithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@DeleteAlertCallNumber/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_ALERT_PHONE Wait:wait Delegate:delegate];
    [request execute];
}



@end
