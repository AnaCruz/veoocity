//
//  CallAlertVC.m
//  secapp
//
//  Created by SECAPP on 08/03/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import "CallAlertVC.h"
#import "Profile.h"
#import "Alert/AlertView.h"
#import "Service.h"
#import "AlertService.h"
#import "AppDelegate.h"

@interface CallAlertVC () <UITextFieldDelegate, ResponseDelegate,AlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIView *viewCalls;
@property NSString * languaje;
@property CGRect originalPosition;
@property (nonatomic, strong) IBOutlet CountryPicker *objCountryPicker;
@property (weak, nonatomic) IBOutlet UIView *phoneViewWhite;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UITextField *followTextField;
@property (strong, nonatomic) NSDictionary * codeDictionary;
@end

@implementation CallAlertVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary * dic= @{
                            @"+93":@"AF",
                             @"+355":@"AL",
                             @"+213":@"DZ",
                             @"+1-684":@"AS",
                             @"+376":@"AD",
                             @"+244":@"AO",
                             @"+1-264":@"AI",
                             @"+672":@"AQ",
                             @"+1-268":@"AG",
                             @"+54":@"AR",
                             @"+374":@"AM",
                             @"+297":@"AW",
                             @"+61":@"AU",
                             @"+43":@"AT",
                             @"+994":@"AZ",
                             @"+358":@"AX",
                             @"+1-242":@"BS",
                             @"+973":@"BH",
                             @"+880":@"BD",
                             @"+1-246":@"BB",
                             @"+375":@"BY",
                             @"+32":@"BE",
                             @"+501":@"BZ",
                             @"+229":@"BJ",
                             @"+1-441":@"BM",
                             @"+975":@"BT",
                             @"+591":@"BO",
                             @"+387":@"BA",
                             @"+267":@"BW",
                             @"+47":@"BV",
                             @"+55":@"BR",
                             @"+246":@"IO",
                             @"+1-284":@"VG",
                             @"+673":@"BN",
                             @"+359":@"BG",
                             @"+226":@"BF",
                             @"+257":@"BI",
                             @"+855":@"KH",
                             @"+237":@"CM",
                             @"+1":@"CA",
                             @"+238":@"CV",
                             @"+599":@"BQ",
                             @"+1-345":@"KY",
                             @"+236":@"CF",
                             @"+235":@"TD",
                             @"+56":@"CL",
                             @"+86":@"CN",
                             @"+61":@"CX",
                             @"+61":@"CC",
                             @"+57":@"CO",
                             @"+269":@"KM",
                             @"+242":@"CG",
                             @"+243":@"CD",
                             @"+682":@"CK",
                             @"+506":@"CR",
                             @"+225":@"CI",
                             @"+385":@"HR",
                             @"+53":@"CU",
                             @"+599":@"CW",
                             @"+357":@"CY",
                             @"+420":@"CZ",
                             @"+45":@"DK",
                             @"+253":@"DJ",
                             @"+1-767":@"DM",
                             @"+1-809":@"DO",
                             @"+593":@"EC",
                             @"+20":@"EG",
                             @"+503":@"SV",
                             @"+240":@"GQ",
                             @"+291":@"ER",
                             @"+372":@"EE",
                             @"+251":@"ET",
                             @"+500":@"FK",
                             @"+298":@"FO",
                             @"+679":@"FJ",
                             @"+358":@"FI",
                             @"+33":@"FR",
                             @"+594":@"GF",
                             @"+689":@"PF",
                             @"+689":@"TF",
                             @"+241":@"GA",
                             @"+220":@"GM",
                             @"+995":@"GE",
                             @"+49":@"DE",
                             @"+233":@"GH",
                             @"+350":@"GI",
                             @"+30":@"GR",
                             @"+299":@"GL",
                             @"+1-473":@"GD",
                             @"+590":@"GP",
                             @"+1-671":@"GU",
                             @"+502":@"GT",
                             @"+44-1481":@"GG",
                             @"+224":@"GN",
                             @"+245":@"GW",
                             @"+592":@"GY",
                             @"+509":@"HT",
                             @"+672":@"HM",
                             @"+504":@"HN",
                             @"+852":@"HK",
                             @"+36":@"HU",
                             @"+354":@"IS",
                             @"+91":@"IN",
                             @"+62":@"ID",
                             @"+98":@"IR",
                             @"+964":@"IQ",
                             @"+353":@"IE",
                             @"+44-1624":@"IM",
                             @"+972":@"IL",
                             @"+39":@"IT",
                             @"+1-876":@"JM",
                             @"+81":@"JP",
                             @"+44-1534":@"JE",
                             @"+962":@"JO",
                             @"+7":@"KZ",
                             @"+254":@"KE",
                             @"+686":@"KI",
                             @"+965":@"KW",
                             @"+996":@"KG",
                             @"+856":@"LA",
                             @"+371":@"LV",
                             @"+961":@"LB",
                             @"+266":@"LS",
                             @"+231":@"LR",
                             @"+218":@"LY",
                             @"+423":@"LI",
                             @"+370":@"LT",
                             @"+352":@"LU",
                             @"+853":@"MO",
                             @"+389":@"MK",
                             @"+261":@"MG",
                             @"+265":@"MW",
                             @"+60":@"MY",
                             @"+960":@"MV",
                             @"+223":@"ML",
                             @"+356":@"MT",
                             @"+692":@"MH",
                             @"+596":@"MQ",
                             @"+222":@"MR",
                             @"+230":@"MU",
                             @"+262":@"YT",
                             @"+52":@"MX",
                             @"+691":@"FM",
                             @"+373":@"MD",
                             @"+377":@"MC",
                             @"+976":@"MN",
                             @"+382":@"ME",
                             @"+1-664":@"MS",
                             @"+212":@"MA",
                             @"+258":@"MZ",
                             @"+95":@"MM",
                             @"+264":@"NA",
                             @"+674":@"NR",
                             @"+977":@"NP",
                             @"+31":@"NL",
                             @"+687":@"NC",
                             @"+64":@"NZ",
                             @"+505":@"NI",
                             @"+227":@"NE",
                             @"+234":@"NG",
                             @"+683":@"NU",
                             @"+672":@"NF",
                             @"+850":@"KP",
                             @"+1-670":@"MP",
                             @"+47":@"NO",
                             @"+968":@"OM",
                             @"+92":@"PK",
                             @"+680":@"PW",
                             @"+970":@"PS",
                             @"+507":@"PA",
                             @"+675":@"PG",
                             @"+595":@"PY",
                             @"+51":@"PE",
                             @"+63":@"PH",
                             @"+64":@"PN",
                             @"+48":@"PL",
                             @"+351":@"PT",
                             @"+1-787":@"PR",
                             @"+974":@"QA",
                             @"+262":@"RE",
                             @"+40":@"RO",
                             @"+7":@"RU",
                             @"+250":@"RW",
                             @"+685":@"WS",
                             @"+378":@"SM",
                             @"+966":@"SA",
                             @"+239":@"ST",
                             @"+221":@"SN",
                             @"+381":@"RS",
                             @"+248":@"SC",
                             @"+232":@"SL",
                             @"+65":@"SG",
                             @"+1-721":@"SX",
                             @"+421":@"SK",
                             @"+386":@"SI",
                             @"+500":@"GS",
                             @"+677":@"SB",
                             @"+252":@"SO",
                             @"+27":@"ZA",
                             @"+82":@"KR",
                             @"+211":@"SS",
                             @"+34":@"ES",
                             @"+94":@"LK",
                             @"+590":@"BL",
                             @"+290":@"SH",
                             @"+1-869":@"KN",
                             @"+1-758":@"LC",
                             @"+590":@"MF",
                             @"+508":@"PM",
                             @"+1-784":@"VC",
                             @"+249":@"SD",
                             @"+597":@"SR",
                             @"+47":@"SJ",
                             @"+268":@"SZ",
                             @"+46":@"SE",
                             @"+41":@"CH",
                             @"+963":@"SY",
                             @"+886":@"TW",
                             @"+992":@"TJ",
                             @"+255":@"TZ",
                             @"+66":@"TH",
                             @"+670":@"TL",
                             @"+228":@"TG",
                             @"+690":@"TK",
                             @"+676":@"TO",
                             @"+1-868":@"TT",
                             @"+216":@"TN",
                             @"+90":@"TR",
                             @"+993":@"TM",
                             @"+1-649":@"TC",
                             @"+688":@"TV",
                             @"+246":@"UM",
                             @"+1-340":@"VI",
                             @"+256":@"UG",
                             @"+380":@"UA",
                             @"+971":@"AE",
                             @"+44":@"GB",
                             @"+1":@"US",
                             @"+598":@"UY",
                             @"+998":@"UZ",
                             @"+678":@"VU",
                             @"+379":@"VA",
                             @"+58":@"VE",
                             @"+84":@"VN",
                             @"+681":@"WF",
                             @"+212":@"EH",
                             @"+967":@"YE",
                             @"+260":@"ZM",
                             @"+263":@"ZW"
                            };
    self.codeDictionary = [dic copy];
     [self registerForKeyboardNotifications];
    self.originalPosition= self.viewCalls.frame;
    self.languaje= [Service getLanguaje];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    int radius = 5;
    CALayer *layer = self.phoneViewWhite.layer;
    layer.cornerRadius = radius;
    layer.borderWidth=3.0;
    [layer setBorderColor:[[UIColor whiteColor] CGColor]];
    layer.masksToBounds = YES;
     UIGraphicsBeginImageContext(self.phoneViewWhite.frame.size);

   Profile * user = [Profile findWithUserId:[Service getUserId]];
    self.phoneNumber.text= user.phoneAlertCalls;
    //self.ladaNumber.text= user.countryCode;
    if (user.phoneAlertCalls!=nil && user.countryCode!=nil) {
        self.clearButton.hidden=NO;
    }else{
        self.clearButton.hidden=YES;
    }
     [self.objCountryPicker setSelectedCountryCode:[self getCountryCode:user.countryCode] animated:YES];
    
}

-(NSString *)getCountryCode: (NSString *)lada{
    NSString *string = [NSString stringWithFormat:@"%@",[self.codeDictionary objectForKey:lada]];
    if (string != nil) {
        return string;
    }else{
        return @"";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)countryPicker:(__unused CountryPicker *)picker didSelectCountryWithName:(NSString *)name code:(NSString *)code
{
    self.ladaNumber.text = [self getCountryCodePhoneWithCountryCode:code];
   
    
}

-(NSString*)getCountryCodePhoneWithCountryCode:(NSString*)code{
      // The one we want to switch on
    
    NSDictionary *lada = [NSDictionary dictionaryWithObjectsAndKeys:
                          @"93", @"AF",
                           @"355", @"AL",
                           @"213", @"DZ",
                           @"1-684", @"AS",
                           @"376", @"AD",
                           @"244", @"AO",
                           @"1-264", @"AI",
                           @"672", @"AQ",
                           @"1-268", @"AG",
                           @"54", @"AR",
                           @"374", @"AM",
                           @"297", @"AW",
                           @"61", @"AU",
                           @"43", @"AT",
                           @"994", @"AZ",
                           @"358", @"AX",
                           @"1-242", @"BS",
                           @"973", @"BH",
                           @"880", @"BD",
                           @"1-246", @"BB",
                           @"375", @"BY",
                           @"32", @"BE",
                           @"501", @"BZ",
                           @"229", @"BJ",
                           @"1-441", @"BM",
                           @"975", @"BT",
                           @"591", @"BO",
                           @"387", @"BA",
                           @"267", @"BW",
                           @"47", @"BV",
                           @"55", @"BR",
                           @"246", @"IO",
                           @"1-284", @"VG",
                           @"673", @"BN",
                           @"359", @"BG",
                           @"226", @"BF",
                           @"257", @"BI",
                           @"855", @"KH",
                           @"237", @"CM",
                           @"1", @"CA",
                           @"238", @"CV",
                           @"599", @"BQ",
                           @"1-345", @"KY",
                           @"236", @"CF",
                           @"235", @"TD",
                           @"56", @"CL",
                           @"86", @"CN",
                           @"61", @"CX",
                           @"61", @"CC",
                           @"57", @"CO",
                           @"269", @"KM",
                           @"242", @"CG",
                           @"243", @"CD",
                           @"682", @"CK",
                           @"506", @"CR",
                           @"225", @"CI",
                           @"385", @"HR",
                           @"53", @"CU",
                           @"599", @"CW",
                           @"357", @"CY",
                           @"420", @"CZ",
                           @"45", @"DK",
                           @"253", @"DJ",
                           @"1-767", @"DM",
                           @"1-809", @"DO",
                           @"593", @"EC",
                           @"20", @"EG",
                           @"503", @"SV",
                           @"240", @"GQ",
                           @"291", @"ER",
                           @"372", @"EE",
                           @"251", @"ET",
                           @"500", @"FK",
                           @"298", @"FO",
                           @"679", @"FJ",
                           @"358", @"FI",
                           @"33", @"FR",
                           @"594", @"GF",
                           @"689", @"PF",
                           @"689", @"TF",
                           @"241", @"GA",
                           @"220", @"GM",
                           @"995", @"GE",
                           @"49", @"DE",
                           @"233", @"GH",
                           @"350", @"GI",
                           @"30", @"GR",
                           @"299", @"GL",
                           @"1-473", @"GD",
                           @"590", @"GP",
                           @"1-671", @"GU",
                           @"502", @"GT",
                           @"44-1481", @"GG",
                           @"224", @"GN",
                           @"245", @"GW",
                           @"592", @"GY",
                           @"509", @"HT",
                           @"672", @"HM",
                           @"504", @"HN",
                           @"852", @"HK",
                           @"36", @"HU",
                           @"354", @"IS",
                           @"91", @"IN",
                           @"62", @"ID",
                           @"98", @"IR",
                           @"964", @"IQ",
                           @"353", @"IE",
                           @"44-1624", @"IM",
                           @"972", @"IL",
                           @"39", @"IT",
                           @"1-876", @"JM",
                           @"81", @"JP",
                           @"44-1534", @"JE",
                           @"962", @"JO",
                           @"7", @"KZ",
                           @"254", @"KE",
                           @"686", @"KI",
                           @"965", @"KW",
                           @"996", @"KG",
                           @"856", @"LA",
                           @"371", @"LV",
                           @"961", @"LB",
                           @"266", @"LS",
                           @"231", @"LR",
                           @"218", @"LY",
                           @"423", @"LI",
                           @"370", @"LT",
                           @"352", @"LU",
                           @"853", @"MO",
                           @"389", @"MK",
                           @"261", @"MG",
                           @"265", @"MW",
                           @"60", @"MY",
                           @"960", @"MV",
                           @"223", @"ML",
                           @"356", @"MT",
                           @"692", @"MH",
                           @"596", @"MQ",
                           @"222", @"MR",
                           @"230", @"MU",
                           @"262", @"YT",
                           @"52", @"MX",
                           @"691", @"FM",
                           @"373", @"MD",
                           @"377", @"MC",
                           @"976", @"MN",
                           @"382", @"ME",
                           @"1-664", @"MS",
                           @"212", @"MA",
                           @"258", @"MZ",
                           @"95", @"MM",
                           @"264", @"NA",
                           @"674", @"NR",
                           @"977", @"NP",
                           @"31", @"NL",
                           @"687", @"NC",
                           @"64", @"NZ",
                           @"505", @"NI",
                           @"227", @"NE",
                           @"234", @"NG",
                           @"683", @"NU",
                           @"672", @"NF",
                           @"850", @"KP",
                           @"1-670", @"MP",
                           @"47", @"NO",
                           @"968", @"OM",
                           @"92", @"PK",
                           @"680", @"PW",
                           @"970", @"PS",
                           @"507", @"PA",
                           @"675", @"PG",
                           @"595", @"PY",
                           @"51", @"PE",
                           @"63", @"PH",
                           @"64", @"PN",
                           @"48", @"PL",
                           @"351", @"PT",
                           @"1-787", @"PR",
                           @"974", @"QA",
                           @"262", @"RE",
                           @"40", @"RO",
                           @"7", @"RU",
                           @"250", @"RW",
                           @"685", @"WS",
                           @"378", @"SM",
                           @"966", @"SA",
                           @"239", @"ST",
                           @"221", @"SN",
                           @"381", @"RS",
                           @"248", @"SC",
                           @"232", @"SL",
                           @"65", @"SG",
                           @"1-721", @"SX",
                           @"421", @"SK",
                           @"386", @"SI",
                           @"500", @"GS",
                           @"677", @"SB",
                           @"252", @"SO",
                           @"27", @"ZA",
                           @"82", @"KR",
                           @"211", @"SS",
                           @"34", @"ES",
                           @"94", @"LK",
                           @"590", @"BL",
                           @"290", @"SH",
                           @"1-869", @"KN",
                           @"1-758", @"LC",
                           @"590", @"MF",
                           @"508", @"PM",
                           @"1-784", @"VC",
                           @"249", @"SD",
                           @"597", @"SR",
                           @"47", @"SJ",
                           @"268", @"SZ",
                           @"46", @"SE",
                           @"41", @"CH",
                           @"963", @"SY",
                           @"886", @"TW",
                           @"992", @"TJ",
                           @"255", @"TZ",
                           @"66", @"TH",
                           @"670", @"TL",
                           @"228", @"TG",
                           @"690", @"TK",
                           @"676", @"TO",
                           @"1-868", @"TT",
                           @"216", @"TN",
                           @"90", @"TR",
                           @"993", @"TM",
                           @"1-649", @"TC",
                           @"688", @"TV",
                           @"246", @"UM",
                           @"1-340", @"VI",
                           @"256", @"UG",
                           @"380", @"UA",
                           @"971", @"AE",
                           @"44", @"GB",
                           @"1", @"US",
                           @"598", @"UY",
                           @"998", @"UZ",
                           @"678", @"VU",
                           @"379", @"VA",
                           @"58", @"VE",
                           @"84", @"VN",
                           @"681", @"WF",
                           @"212", @"EH",
                           @"967", @"YE",
                           @"260", @"ZM",
                          @"263", @"ZW", nil];
    
    NSString *string = [NSString stringWithFormat:@"+%@",[lada objectForKey:code]];
    if (string != nil) {
        return string;
    }else{
        return @"";
    }
}
- (IBAction)saveChanges:(id)sender {
    if (self.phoneNumber.text.length<=0 &&([self.ladaNumber.text isEqualToString:@"Código |"]||[self.ladaNumber.text isEqualToString:@"Code |"])) {
        NSLog(@"No hay numero regitrado");
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"alert.phone.without.phone.and.code", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        return;
    }
    if([self.phoneNumber.text isEqualToString:@""]||self.phoneNumber.text.length<10){
        NSLog(@"Falta el numero o esta muy corto");
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"alert.phone.with.phone.short.or.empty", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        return;
    }
    if([self.ladaNumber.text isEqualToString:@""]||self.ladaNumber.text==nil||self.ladaNumber.text.length<=0||[self.ladaNumber.text isEqualToString:@"Código |"]||[self.ladaNumber.text isEqualToString:@"Code |"]){
        NSLog(@"Falta el codigo de pais");
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"alert.phone.withoth.code", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        return;
    }
    NSString * numberForAlerts= [NSString stringWithFormat:@"%@%@",self.ladaNumber.text, self.phoneNumber.text];
    NSLog(@"Number for alert: %@", numberForAlerts);
     Profile * user = [Profile findWithUserId:[Service getUserId]];
    if ([self.phoneNumber.text isEqualToString:user.phoneAlertCalls]&&[self.ladaNumber.text isEqualToString:user.countryCode]) {
        return;
    }
    [AlertService saveAlertCallWithPhoneNumber:self.phoneNumber.text AndCountryCode:self.ladaNumber.text Wait:NO Delegate:self];
}

- (IBAction)unactiveCalls:(id)sender {
    self.phoneNumber.text= @"";
    self.ladaNumber.text=@"";
 [AlertService deleteAlertPhoneCallNumberWithWait:NO Delegate:self];
}

- (IBAction)goClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.originalPosition;
    aRect.origin.y = aRect.origin.y - (kbSize.height-aRect.size.height)+110;
    self.viewCalls.frame = aRect;
    self.instructionsLabel.hidden=YES;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.viewCalls.frame = self.originalPosition;
    self.instructionsLabel.hidden=NO;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=14)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:14];
    }
    return NO;
}


- (IBAction)followCoutryName:(UITextField *)sender {
    [self.objCountryPicker setSelectedCountryName:sender.text animated:YES];
}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    [self.view endEditing:YES];
}
-(void)successResponseWithService:(kService)service Response:(id)response{
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kSAVE_ALERT_PHONE:
            dictionary = [dictionary objectForKey:@"SaveAlertCallNumberResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Profile * user = [Profile findWithUserId:[Service getUserId]];
                user.countryCode= self.ladaNumber.text;
                user.phoneAlertCalls=self.phoneNumber.text;
                [user mergeObject];
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"alert.phone.save", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                self.clearButton.hidden=NO;
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            
            break;
        case kDELETE_ALERT_PHONE:
            dictionary = [dictionary objectForKey:@"DeleteAlertCallNumberResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Profile * user = [Profile findWithUserId:[Service getUserId]];
                user.countryCode= nil;
                 user.phoneAlertCalls= nil;
                [user mergeObject];
                self.phoneNumber.text= @"";
                if ([self.languaje isEqualToString:@"1"]) {
                  self.ladaNumber.text= @"Código |";
                }else{
                self.ladaNumber.text= @"Code |";
                }
                NSLog(@"Numero desactivado y eliminado");
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"alert.phone.deleted", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                self.clearButton.hidden=YES;
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
            
        default:
            break;
    }



}


@end
