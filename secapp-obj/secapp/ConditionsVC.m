 //
//  ConditionsVC.m
//  secapp
//
//  Created by SecApp on 05/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ConditionsVC.h"
#import  "Service.h"

@interface ConditionsVC ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation ConditionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    int languaje=[[Service getLanguaje]intValue];
    NSString * urls;
    if (languaje==1) {
      urls=@"https://secapp-co-mx.squarespace.com/terminos-y-condiciones";
    }else{
     urls=@"https://secapp-co-mx.squarespace.com/terms-and-conditions";
    }
    NSURL *url = [NSURL URLWithString:urls];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
   
}

- (IBAction)close:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
  
}

@end
