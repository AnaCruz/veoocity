//
//  EmergencyNumbersVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 24/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EmergencyNumbersVC.h"
#import "Alert/AlertView.h"


@interface EmergencyNumbersVC () <AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inName;
@property (weak, nonatomic) IBOutlet UITextField *inNumber;

@property (weak, nonatomic) IBOutlet UIButton *numberTwo;
@property (weak, nonatomic) IBOutlet UIButton *numberThree;
@property (weak, nonatomic) IBOutlet UIButton *numberFour;
@property (weak, nonatomic) IBOutlet UIButton *numberFive;
@property (weak, nonatomic) IBOutlet UIButton *numberSixe;

@property int numberSelect;
@end

@implementation EmergencyNumbersVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.numberSelect = -1;
    for (int i = 2; i <= 6; i++) {
        NSString* tmpKeyNumber = [NSString stringWithFormat:@"number%i", i];
        NSString* tmpKeyName = [NSString stringWithFormat:@"numberName%i", i];
        NSString* tmpNumber = [[NSUserDefaults standardUserDefaults] objectForKey:tmpKeyNumber];
        NSString* tmpName = [[NSUserDefaults standardUserDefaults] objectForKey:tmpKeyName];
        //  NSString* title = [NSString stringWithFormat:@"%@ - %@", tmpName, tmpNumber];
        NSString* title = [NSString stringWithFormat:@"%@", tmpName];
        if (tmpNumber) {
            switch (i) {
                case 2:
                    [self.numberTwo setTitle:title forState:UIControlStateNormal];
                    self.numberTwo.selected = YES;
                    break;
                case 3:
                    [self.numberThree setTitle:title forState:UIControlStateNormal];
                    self.numberThree.selected = YES;
                    break;
                case 4:
                    [self.numberFour setTitle:title forState:UIControlStateNormal];
                    self.numberFour.selected = YES;
                    break;
                case 5:
                    [self.numberFive setTitle:title forState:UIControlStateNormal];
                    self.numberFive.selected = YES;
                    break;
                case 6:
                    [self.numberSixe setTitle:title forState:UIControlStateNormal];
                    self.numberSixe.selected = YES;
                    break;
                default:
                    break;
            }
        }
    }
}

- (IBAction)numberSelected:(UIButton*) sender {
    if (sender.isSelected) {
        NSString* number = nil;
        if (sender.tag == 1) {
            number = @"066";
        } else {
            NSString* tmpKeyNumber = [NSString stringWithFormat:@"number%ld", (long)sender.tag];
            number = [[NSUserDefaults standardUserDefaults] objectForKey:tmpKeyNumber];
        }
        if (number) {
            [self callNumber:number];
            
        }
    }
}

- (void) callNumber:(NSString*) number {
    
    NSString* phoneCallNum = [NSString stringWithFormat:@"tel://%@",number];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
}

- (IBAction)saveNewNumber:(id)sender {
    if (![self.inName.text isEqualToString:@""] && ![self.inNumber.text isEqualToString:@""]) {
        if (self.numberTwo.isSelected &&
            self.numberThree.isSelected &&
            self.numberFour.isSelected &&
            self.numberFive.isSelected &&
            self.numberSixe.isSelected) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"espacios.ocupados", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
            return;
        }
        NSString* tmpKeyNumber = nil;
        NSString* tmpKeyName = nil;
//        NSString* title = [NSString stringWithFormat:@"%@ - %@", self.inName.text, self.inNumber.text];
        NSString* title = [NSString stringWithFormat:@"%@", self.inName.text];
        if (!self.numberTwo.isSelected) {
            tmpKeyNumber = [NSString stringWithFormat:@"number%i", 2];
            tmpKeyName = [NSString stringWithFormat:@"numberName%i", 2];
            [self.numberTwo setTitle:title forState:UIControlStateNormal];
            self.numberTwo.selected = YES;
        } else if (!self.numberThree.isSelected) {
            tmpKeyNumber = [NSString stringWithFormat:@"number%i", 3];
            tmpKeyName = [NSString stringWithFormat:@"numberName%i", 3];
            [self.numberThree setTitle:title forState:UIControlStateNormal];
            self.numberThree.selected = YES;
        } else if (!self.numberFour.isSelected) {
            tmpKeyNumber = [NSString stringWithFormat:@"number%i", 4];
            tmpKeyName = [NSString stringWithFormat:@"numberName%i", 4];
            [self.numberFour setTitle:title forState:UIControlStateNormal];
            self.numberFour.selected = YES;
        } else if (!self.numberFive.isSelected) {
            tmpKeyNumber = [NSString stringWithFormat:@"number%i", 5];
            tmpKeyName = [NSString stringWithFormat:@"numberName%i", 5];
            [self.numberFive setTitle:title forState:UIControlStateNormal];
            self.numberFive.selected = YES;
        } else if (!self.numberSixe.isSelected) {
            tmpKeyNumber = [NSString stringWithFormat:@"number%i", 6];
            tmpKeyName = [NSString stringWithFormat:@"numberName%i", 6];
            [self.numberSixe setTitle:title forState:UIControlStateNormal];
            self.numberSixe.selected = YES;
        }
        if (tmpKeyName && tmpKeyNumber) {
            [[NSUserDefaults standardUserDefaults] setObject:self.inNumber.text forKey:tmpKeyNumber];
            [[NSUserDefaults standardUserDefaults] setObject:self.inName.text forKey:tmpKeyName];
        }
        self.inNumber.text = @"";
        self.inName.text = @"";
        [self.view endEditing:YES];
        
    }
}

- (IBAction) deleteNumber:(UILongPressGestureRecognizer*) sender {
    if (sender.state == UIGestureRecognizerStateEnded) {
        self.numberSelect = (int)sender.view.tag;
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:@"" Message:NSLocalizedString(@"eliminar.numero.seleccionado", nil) Delegate:self CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
    }
}

- (IBAction)backView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark AlertView Delegate

-(void)buttonPressed:(UIButton *)sender {
    if (sender.tag == 1) {
        if (self.numberSelect > 0) {
            NSString* tmpKeyNumber = [NSString stringWithFormat:@"number%i", self.numberSelect];
            NSString* tmpKeyName = [NSString stringWithFormat:@"numberName%i", self.numberSelect];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:tmpKeyName];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:tmpKeyNumber];
            switch (self.numberSelect) {
                case 2:
                    [self.numberTwo setTitle:@"" forState:UIControlStateNormal];
                    [self.numberTwo setSelected:NO];
                    break;
                case 3:
                    [self.numberThree setTitle:@"" forState:UIControlStateNormal];
                    [self.numberThree setSelected:NO];
                    break;
                case 4:
                    [self.numberFour setTitle:@"" forState:UIControlStateNormal];
                    [self.numberFour setSelected:NO];
                    break;
                case 5:
                    [self.numberFive setTitle:@"" forState:UIControlStateNormal];
                    [self.numberFive setSelected:NO];
                    break;
                case 6:
                    [self.numberSixe setTitle:@"" forState:UIControlStateNormal];
                    [self.numberSixe setSelected:NO];
                    break;
                default:
                    break;
            }
            self.numberSelect = -1;
        }
    }
}
@end
