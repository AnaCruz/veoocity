//
//  OccurrenceTableViewCell.h
//  secapp
//
//  Created by SecApp on 04/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
// celda de comentarios
@interface OccurrenceTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *nameUser;
@property (strong, nonatomic) IBOutlet UILabel *comments;
@property (strong, nonatomic) IBOutlet UILabel *date;
@property (strong, nonatomic) IBOutlet UIButton *like;
@property (strong, nonatomic) IBOutlet UIButton *dislike;
@property (strong, nonatomic) IBOutlet UILabel *countLike;
@property (strong, nonatomic) IBOutlet UILabel *countDislike;


@end
