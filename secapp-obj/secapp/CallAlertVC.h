//
//  CallAlertVC.h
//  secapp
//
//  Created by SECAPP on 08/03/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CountryPicker.h"

@interface CallAlertVC : UIViewController <CountryPickerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *ladaNumber;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;

@end
