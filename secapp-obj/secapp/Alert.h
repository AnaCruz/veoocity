//
//  Alert.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 11/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Alert : NSObject

/**
 *  Show a alertview
 *
 *  @param title   Title to alert
 *  @param message Message to alert
 */
+ (void) showAlertWithTitle:(NSString*) title AndMessage:(NSString*) message;

/**
 *  Showw a alert in view controller
 *
 *  @param controller Controller of the alert to show.
 *  @param title      Title of Alert
 *  @param message    Message of Alert
 */
+ (void) showAlertInViewController:(UIViewController*) controller
                         WithTitle:(NSString*) title
                           Message:(NSString*) message;
@end
