//
//  MapSecurityZoneVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "MapSecurityZoneVC.h"
#import <CoreLocation/CoreLocation.h>
#import "MapSecApp.h"
#import "CircleOverlay.h"
#import "CircleRenderer.h"
#import "MapGesture.h"
#import "InfoZoneVC.h"
#import "EditZoneVC.h"
#import "SecurityZonesServices.h"
#import "SecurityZone.h"
#import "EditeInsecureZoneVC.h"
#import "InsecureZone.h"
#import "InsecureZoneServices.h"
#import "AlertView.h"
#import "AppDelegate.h"
#import "Profile.h"
#import "LocationManagement.h"

NSInteger const defaultRadius = 100;
NSInteger const defaultMinDistance = 30;
NSInteger const defaultMaxDistance = 10000;

@interface MapSecurityZoneVC ()<MKMapViewDelegate, UIGestureRecognizerDelegate, EditZoneDelegate, InfoZoneDelegate, ResponseDelegate, EditeInsecureZoneDelegate, AlertViewDelegate>

// Colores de los circulos
@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *strokeColor;

@property CircleOverlay* currentOverlay;
@property CircleOverlay* lastOverlay;
@property CircleRenderer* currentOverlayRenderer;
@property CircleRenderer* lastOverlayRenderer;

@property BOOL mapViewGestureEnabled;
@property MKMapPoint prevMapPoint;
@property CLLocationDistance prevRadius;
@property CGRect radiusTouchRect;
@property UIView *radiusTouchView;

@property BOOL overlaySelected;
@property BOOL isDB;
@property BOOL isSecure;
@property BOOL isOwner;

@property SecurityZone* zoneForDelete;
@property CircleOverlay* deletedZoneCircle;
@property (nonatomic, assign) CLLocationDistance circleRadius;
@property (nonatomic, assign) CLLocationDistance circleRadiusMin;
@property (nonatomic, assign) CLLocationDistance circleRadiusMax;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *tapMap;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *doubleTap;
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longTap;
@property (strong, nonatomic) IBOutlet UIButton *goToMyLocation;


@property InfoZoneVC* info;
@property EditZoneVC* edit;
@property EditeInsecureZoneVC* editIn;

// Bandera para controlar cuando se genera una nueva zona de seguridad
@property (nonatomic, getter=isNewZone) BOOL newZone;
@property NSArray* overlays;
//@property NSMutableArray * currentOverlaysArray;

@property BOOL movedOverlay;
@property (strong, nonatomic) IBOutlet UIButton *createSafeZone;
@property (strong, nonatomic) IBOutlet UIButton *createUnsafeZone;

@end

@implementation MapSecurityZoneVC


-(void) viewWillAppear:(BOOL)animated{
    self.mapView = [MapSecApp shareInstance];
    self.mapView.delegate = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.frame = self.view.frame;
        [self.view addSubview:self.mapView];
        [self.view sendSubviewToBack:self.mapView];
    });
    [self.mapView clearMap];
     self.mapView = [MapSecApp shareInstance];
    self.mapView.delegate = self;
    [self.mapView addGestureRecognizer:self.tapMap];
    [self.mapView addGestureRecognizer:self.doubleTap];
    [self.mapView addGestureRecognizer:self.longTap];
    [self.mapView addGestureRecognizer:[self selectorGestureRecognizer]];
    [self setNewZone:NO];
    self.movedOverlay = NO;
    NSArray * zones = [SecurityZone findAll];
    
    for (SecurityZone * zone in zones) {
//        self.isSecure=YES;
        [self addZonesToMapWithZoneData:zone];
        
    }
    NSArray * insecureZones = [InsecureZone findAll];
    
    for (InsecureZone * zone in insecureZones) {
//        self.isSecure=NO;
        
        [self addInsecureZonesToMapWithZoneData:zone];
        
    }
     _overlays = _mapView.overlays;
     self.currentOverlay=nil;
    self.lastOverlayRenderer=nil;
    self.lastOverlay=nil;
    self.currentOverlayRenderer=nil;
    self.overlaySelected=NO;
    self.isDB=NO;
    [self selectorSetDefaults];
    self.createSafeZone.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.createUnsafeZone.titleLabel.textAlignment = NSTextAlignmentCenter;

}
-(void) addZonesToMapWithZoneData: (SecurityZone *)zone{
    _currentOverlayRenderer = nil;
    _lastOverlay = nil;
    _lastOverlayRenderer = nil;
    CGFloat newLat = [zone.latitude floatValue];
    CGFloat newLon = [zone.longitude floatValue];
    CLLocationCoordinate2D coordinate ={newLat, newLon};
  
        self.currentOverlay = [[CircleOverlay alloc] initWithCenterCoordinate:coordinate Radius:[zone.radius doubleValue] Title:zone.name idZone:zone.securitiZoneId ZoneType:YES];

    
    self.overlaySelected = NO;
    self.circleRadius = [zone.radius doubleValue];
    self.currentOverlay.editingRadius=NO;
    self.currentOverlay.editingCoordinate = NO;
    self.currentOverlay.isOwner=YES;
    //[self.currentOverlaysArray addObject:self.currentOverlay];
        self.isDB= YES;
        [self.mapView addOverlay:self.currentOverlay];

}
-(void) addInsecureZonesToMapWithZoneData: (InsecureZone *)zone{
    _currentOverlayRenderer = nil;
    _lastOverlay = nil;
    _lastOverlayRenderer = nil;
    CGFloat newLat = [zone.latitude floatValue];
    CGFloat newLon = [zone.longitude floatValue];
    CLLocationCoordinate2D coordinate ={newLat, newLon};
    
    self.currentOverlay = [[CircleOverlay alloc] initWithCenterCoordinate:coordinate Radius:[zone.radius doubleValue] Title:zone.name idZone:zone.insecureZoneId ZoneType:NO];
    
    if ([zone.isOwner boolValue]==NO) {
        self.isOwner=NO;
        self.currentOverlay.isOwner=NO;
    }
    else{
        self.isOwner=YES;
        self.currentOverlay.isOwner=YES;
    }
    self.overlaySelected = NO;
    self.circleRadius = [zone.radius doubleValue];
    self.currentOverlay.editingRadius=NO;
    self.currentOverlay.editingCoordinate = NO;
    //[self.currentOverlaysArray addObject:self.currentOverlay];
    self.isDB= YES;
    [self.mapView addOverlay:self.currentOverlay];
    
}


-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
 
    _overlays = _mapView.overlays;
    [_mapView removeOverlays:_mapView.overlays];
    
    [self.mapView clearMap];
    _overlays=nil;
    if(self.edit.isShow) {
        [self.edit.view removeFromSuperview];
    }
    if(self.info.isShow) {
        [self.info.view removeFromSuperview];
        
    }
    if(self.editIn.isShow) {
        [self.editIn.view removeFromSuperview];
        
    }
    
}

-(void)viewDidLoad {
    [super viewDidLoad];
 dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.frame = self.view.frame;
        [self.view addSubview:self.mapView];
        [self.view sendSubviewToBack:self.mapView];
 });
    [self.mapView clearMap];
    self.mapView.delegate = self;
   // self.currentOverlaysArray =  [[NSMutableArray alloc] init];
        self.mapView = [MapSecApp shareInstance];
    [self selectorSetDefaults];
    self.mapViewGestureEnabled = YES;
    [self.longTap addTarget:self action:@selector(longTapped:)];
    self.longTap.delegate= self;
    [self.tapMap addTarget:self action:@selector(singleTap:)];
    [self.tapMap requireGestureRecognizerToFail:self.doubleTap];
    [self performSelector:@selector(recalculateRadiusTouchRect) withObject:nil afterDelay:0.2f];
    self.info = [[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoZoneVC"];
    [self addChildViewController:self.info];
    self.info.delegate = self;
    self.edit = [[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"EditZoneVC"];
    [self addChildViewController:self.edit];
    self.edit.delegate = self;
    self.editIn=[[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"EditInsecureZoneVC"];
    [self addChildViewController:self.editIn];
    self.editIn.delegate = self;

}

- (void)selectorSetDefaults {
    self.circleRadius = defaultRadius;
    self.circleRadiusMin = defaultMinDistance;
    self.circleRadiusMax = defaultMaxDistance;
}

- (void)longTapped:(UILongPressGestureRecognizer*)sender {
    if (self.currentOverlay && self.currentOverlayRenderer && ([self isNewZone] || self.currentOverlay.editingRadius)) {
        if (sender.state == UIGestureRecognizerStateChanged) {
            self.movedOverlay = YES;
            CGPoint newPoint = [sender locationInView:sender.view];
            CLLocationCoordinate2D newCoordinate = [self.mapView convertPoint:newPoint toCoordinateFromView:sender.view];
            self.currentOverlay.coordinate = newCoordinate;
            [self.mapView removeOverlay:self.currentOverlay];
            self.currentOverlay.editingCoordinate = YES;
            [self.mapView addOverlay:self.currentOverlay];
        }
        if (sender.state == UIGestureRecognizerStateEnded) {
            self.movedOverlay = NO;
            [self.mapView removeOverlay:self.currentOverlay];
            self.currentOverlay.editingCoordinate = NO;
            [self.mapView addOverlay:self.currentOverlay];
        }
    }
    
}

- (void)singleTap:(UITapGestureRecognizer*)sender {
    if (sender.numberOfTouches == 1) {
        CircleOverlay* newOverlay = nil;
        CLLocationCoordinate2D coord = [self.mapView convertPoint:[sender locationInView:self.view] toCoordinateFromView:self.view];
        for (CircleOverlay* overlay in self.mapView.overlays) {
            MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
            if (MKMapRectContainsPoint(overlay.boundingMapRect, mapPoint)) {
                newOverlay = overlay;
                break;
            }
        }
                
        if (newOverlay && ![self isNewZone]) {
           //chekar si el overlay es mio
            if (!self.currentOverlay) {
                
                self.currentOverlay = newOverlay;
                self.overlaySelected = YES;
                [self.mapView addOverlay:self.currentOverlay];
            } else if (![self.currentOverlay isEqual:newOverlay]) {
                
                // el seleccionado pasa a ser el ultimo
                self.lastOverlay = self.currentOverlay;
                
                // se remueve el seleccionado
                [self.mapView removeOverlay:self.currentOverlay];
                self.overlaySelected = NO;
                self.lastOverlay.editingRadius = NO;
                
                // se agrega el ultimo
                [self.mapView addOverlay:self.lastOverlay];
                
                // el seleccionado pasa a ser el nuevo y se dibuja
                self.currentOverlay = newOverlay;
                [self.mapView removeOverlay:self.currentOverlay];
                self.overlaySelected = YES;
                self.currentOverlay.editingRadius = NO;
                [self.mapView addOverlay:self.currentOverlay];
            }
            [self updateMapRegionForMapSelector];
            
        } else if (self.currentOverlay && self.currentOverlayRenderer && ![self isNewZone]) {
            // Deseleccionar un overlay
            
            self.lastOverlay = self.currentOverlay;
            [self.mapView removeOverlay:self.currentOverlay];
            self.overlaySelected = NO;
            self.currentOverlay.editingRadius=NO;
            [self.mapView addOverlay:self.lastOverlay];
            self.currentOverlay = nil;
            self.currentOverlayRenderer = nil;
        } else if ([self isNewZone]) {
            if (self.currentOverlay && self.currentOverlayRenderer) {
                return;
            }
            //agregar nuevo overlay
           
            self.currentOverlay = [[CircleOverlay alloc] initWithCenterCoordinate:coord Radius:defaultRadius Title:@"" idZone:@"" ZoneType:self.isSecure];
            self.currentOverlay.isOwner=YES;
            self.overlaySelected = YES;
            self.circleRadius = defaultRadius;
            self.currentOverlay.editingRadius=YES;
            self.currentOverlay.editingCoordinate = NO;
            [self.mapView addOverlay:self.currentOverlay];
        }
    }
}

- (void)setCircleRadius:(CLLocationDistance)circleRadius {
    if (_circleRadius!= MAX(MIN(circleRadius, _circleRadiusMax), _circleRadiusMin)) {
        _circleRadius = MAX(MIN(circleRadius, _circleRadiusMax), _circleRadiusMin);
        _currentOverlay.radius = _circleRadius;
    }
}

- (void)setCircleRadiusMax:(CLLocationDistance)circleRadiusMax {
    if (_circleRadiusMax != circleRadiusMax) {
        _circleRadiusMax = circleRadiusMax;
        _circleRadiusMin = MIN(_circleRadiusMin, _circleRadiusMax);
        self.circleRadius = _circleRadius;
    }
}

- (void)setCircleRadiusMin:(CLLocationDistance)circleRadiusMin {
    if (_circleRadiusMin != circleRadiusMin) {
        _circleRadiusMin = circleRadiusMin;
        _circleRadiusMax = MAX(_circleRadiusMax, _circleRadiusMin);
        self.circleRadius = _circleRadius;
    }
}

#pragma mark Recognizer

- (MapGesture *)selectorGestureRecognizer {
    
    __weak typeof(self)weakSelf = self;
    MapGesture *selectorGestureRecognizer = [[MapGesture alloc] init];
    
    selectorGestureRecognizer.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) {
        
        UITouch *touch = [touches anyObject];
        CGPoint touchPoint = [touch locationInView:weakSelf.mapView];
        
        CLLocationCoordinate2D coord = [weakSelf.mapView convertPoint:touchPoint toCoordinateFromView:weakSelf.mapView];
        MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
        
        if (CGRectContainsPoint(weakSelf.radiusTouchRect, touchPoint) && weakSelf.currentOverlay.editingRadius){
            __block int t = 0;
            dispatch_async(dispatch_get_main_queue(), ^{
                t = 1;
                weakSelf.mapView.scrollEnabled = NO;
                _mapViewGestureEnabled = NO;
            });
        } else {
            weakSelf.mapView.scrollEnabled = YES;
        }
        weakSelf.prevMapPoint = mapPoint;
        weakSelf.prevRadius = weakSelf.circleRadius;
    };
    
    selectorGestureRecognizer.touchesMovedCallback = ^(NSSet * touches, UIEvent * event) {
        if(!_mapViewGestureEnabled && [event allTouches].count == 1){
            UITouch *touch = [touches anyObject];
            CGPoint touchPoint = [touch locationInView:weakSelf.mapView];
            
            CLLocationCoordinate2D coord = [weakSelf.mapView convertPoint:touchPoint toCoordinateFromView:weakSelf.mapView];
            MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
            
            double meterDistance = (mapPoint.x - _prevMapPoint.x)/MKMapPointsPerMeterAtLatitude(self.mapView.centerCoordinate.latitude) + _prevRadius;
            weakSelf.circleRadius = MIN( MAX( meterDistance, weakSelf.circleRadiusMin ), weakSelf.circleRadiusMax );
           
        }
    };
    
    selectorGestureRecognizer.touchesEndedCallback = ^(NSSet * touches, UIEvent * event) {
        _mapViewGestureEnabled = YES;
        weakSelf.mapView.zoomEnabled = YES;
        weakSelf.mapView.scrollEnabled = YES;
        weakSelf.mapView.userInteractionEnabled = YES;
        
        if (_prevRadius != weakSelf.circleRadius) {
            [weakSelf recalculateRadiusTouchRect];
            if (((_prevRadius / weakSelf.circleRadius) >= 1.25f) || ((_prevRadius / weakSelf.circleRadius) <= .75f)) {
                [weakSelf updateMapRegionForMapSelector];
            }
        }
    };
    
    return selectorGestureRecognizer;
}

#pragma mark MKMapView Delegate

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {

    MKOverlayRenderer *overlayRenderer;
    if (self.movedOverlay) {
        return self.currentOverlayRenderer;
    }
    if ([overlay isKindOfClass:[CircleOverlay class]]) {
        CircleOverlay* over=(CircleOverlay *)overlay;
        
        if (self.overlaySelected) {
            self.currentOverlayRenderer = [[CircleRenderer alloc] initWithSelectorOverlay:(CircleOverlay*) overlay];
            if (over.zoneType==YES) {
           
            self.currentOverlayRenderer.fillColor = [UIColor colorWithRed:0.0f green:0.77f blue:0.8f alpha:0.3f];
                self.currentOverlayRenderer.strokeColor = [UIColor grayColor];

            }else{
                if (over.isOwner==NO) {
                    self.currentOverlayRenderer.fillColor = [UIColor colorWithRed:1.0f green:0.5f blue:0.0f alpha:0.3f];
                    self.currentOverlayRenderer.strokeColor = [UIColor orangeColor];
                }else{
                self.currentOverlayRenderer.fillColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.3f];
                self.currentOverlayRenderer.strokeColor = [UIColor redColor];
                }
                //
            }
            overlayRenderer = self.currentOverlayRenderer;
            [self updateMapRegionForMapSelector];
            
                if ([self isNewZone] || self.currentOverlay.editingRadius) {
                     // Agregar vista de edicion.
                    if (over.zoneType==YES) {
                        // Agregar vista de edicion zona segura.
                     self.edit.zone = (CircleOverlay*)overlay;
                    [self.edit drawInView:self.view];
                    }else{
                        // Agregar vista de edicion zona insegura.
                        self.editIn.zone = (CircleOverlay*)overlay;
                       
                        [self.editIn drawInView:self.view];
                    }
                } else  {
                    // Agregar vista de informacion.
                    self.info.isOwner=over.isOwner;
                    
                    self.info.zone = (CircleOverlay*)overlay;
                    [self.info drawInView:self.view];
                    
                    
                }
         
        } else {
            self.lastOverlayRenderer = [[CircleRenderer alloc] initWithSelectorOverlay:(CircleOverlay*) overlay];
            if (over.zoneType==YES) {
                
                self.lastOverlayRenderer.fillColor = [UIColor colorWithRed:0.0f green:0.77f blue:0.8f alpha:0.2f];
               self.lastOverlayRenderer.strokeColor = [UIColor whiteColor];
                
            }else{
                if (over.isOwner==NO) {
                    self.lastOverlayRenderer.fillColor = [UIColor colorWithRed:1.0f green:0.5f blue:0.0f alpha:0.2f];
                    self.lastOverlayRenderer.strokeColor = [UIColor whiteColor];
                }
                else{
                self.lastOverlayRenderer.fillColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.2f];
                 self.lastOverlayRenderer.strokeColor = [UIColor whiteColor];
                }
            }

            overlayRenderer = self.lastOverlayRenderer;
            if (!self.isDB) {
                if ([self isNewZone]) {
                    if (self.editIn.isShow) {
                        [self.editIn.view removeFromSuperview];
                    }
                    if (self.edit.isShow) {
                        [self.edit.view removeFromSuperview];
                    }
                } else if(self.info.isShow) {
                    [self.info.view removeFromSuperview];
                }
                
            }
            
        }
    } else if ([overlay isKindOfClass:[MKTileOverlay class]]) {
        overlayRenderer = [[MKTileOverlayRenderer alloc] initWithTileOverlay:overlay];
    }
   // self.isOwner=YES;
    return overlayRenderer;
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [self recalculateRadiusTouchRect];
}

- (IBAction)goToMyLocation:(id)sender {
    CGFloat newLat;
    CGFloat newLon;
    newLat =[[[LocationManagement sharedInstance] latitude] floatValue];
    newLon = [[[LocationManagement sharedInstance] longitude] floatValue];
    int intlat= newLat;
    int intlong= newLon;
    NSString *latString= [NSString stringWithFormat:@"%d", intlat];
    NSString *longString= [NSString stringWithFormat:@"%d",intlong];
    if ([latString isEqualToString:@"0"]&&[longString isEqualToString:@"0"]) {
        Profile * profile = [Profile findWithUserId:[Service getUserId]];
        newLat = [profile.lastLatitude floatValue];
        newLon =[profile.lastLongitude floatValue];
    }
    CLLocationCoordinate2D newCoord = {newLat, newLon};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, 50, 50);
    [self.mapView setRegion:region animated:YES];

    
}

- (void)recalculateRadiusTouchRect {
    MKMapRect selectorMapRect = self.currentOverlay.boundingMapRect;
    MKMapPoint selectorRadiusPoint = MKMapPointMake(MKMapRectGetMaxX(selectorMapRect), MKMapRectGetMidY(selectorMapRect));
    MKCoordinateRegion coordinateRegion = MKCoordinateRegionMakeWithDistance(MKCoordinateForMapPoint(selectorRadiusPoint), _circleRadius *.3f, _circleRadius *.3f);
    BOOL needDisplay = MKMapRectContainsPoint(self.mapView.visibleMapRect, selectorRadiusPoint);
    _radiusTouchRect = needDisplay ? [self.mapView convertRegion:coordinateRegion toRectToView:self.view] : CGRectZero;
    if (_radiusTouchRect.size.height < 34) {
        _radiusTouchRect.size.height = 34;
        _radiusTouchRect.size.width = 34;
    }
//    if ([self isNewZone]) {
//        if (self.isSecure==YES) {
//           self.edit.zone = self.currentOverlay;
//        }else{
//            self.editIn.zone = self.currentOverlay;
//        }
//    } else {
//        // Establecemos nuevos valores.
//        self.info.zone = self.currentOverlay;
//    }
    
#ifdef DEBUG
    _radiusTouchView.frame = _radiusTouchRect;
    _radiusTouchView.hidden = !needDisplay;
#endif
}

- (void)updateMapRegionForMapSelector {
    MKCoordinateRegion selectorRegion = MKCoordinateRegionForMapRect(self.currentOverlay.boundingMapRect);
    MKCoordinateRegion region;
    region.center = selectorRegion.center;
    region.span = MKCoordinateSpanMake(selectorRegion.span.latitudeDelta *1.5f, selectorRegion.span.longitudeDelta *1.5f);
    [self.mapView setRegion:region animated:YES];
}

#pragma mark Action new zone

- (IBAction) newZone:(id)sender {
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"crear.nueva.zona", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
    alert.delegate=self;
 
    [self setNewZone:YES];
    self.isSecure= YES;
    if(self.edit.isShow) {
        [self.edit.view removeFromSuperview];
    }
    if(self.info.isShow) {
        [self.info.view removeFromSuperview];
        
    }
    if(self.editIn.isShow) {
        [self.editIn.view removeFromSuperview];
        
    }
    
}
- (IBAction)newInsecureZone:(id)sender {
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"crear.nueva.zona", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
    alert.delegate=self;
    [self setNewZone:YES];
    self.isSecure= NO;
    if(self.edit.isShow) {
        [self.edit.view removeFromSuperview];
    }
    if(self.info.isShow) {
        [self.info.view removeFromSuperview];
        
    }
    if(self.editIn.isShow) {
        [self.editIn.view removeFromSuperview];
        
    }

}

- (void)setNewZone:(BOOL)newZone {
    _newZone = newZone;
    if (_newZone) {
        _overlaySelected = NO;
        _currentOverlay = nil;
        _currentOverlayRenderer = nil;
        _lastOverlay = nil;
        _lastOverlayRenderer = nil;
        _overlays = _mapView.overlays;
        [_mapView removeOverlays:_mapView.overlays];
        [self selectorSetDefaults];
    } else if (_overlays) {
        _overlaySelected = NO;
        [_mapView addOverlays:_overlays];
    }
}

#pragma mark EditZoneDelegate

-(void)editZone:(EditZoneVC *)editZone SaveZone:(id)zone {
    // Guardar en la BD la zona
    
    // Mostrar las demás zonas ocultas por la edición de la nueva zona.
    //self.currentOverlay=zone;
    [self.mapView removeOverlay:self.currentOverlay];
    self.lastOverlay=self.currentOverlay;
    self.currentOverlay=nil;
    self.currentOverlayRenderer=nil;
    self.lastOverlay.editingRadius=NO;
    self.overlaySelected= NO;
    self.lastOverlay.isOwner=YES;
    self.lastOverlay.editingCoordinate = NO;
    [self.mapView addOverlay:self.lastOverlay];
    [self setNewZone:NO];
    [editZone.view removeFromSuperview];
}

-(void)editZone:(EditZoneVC *)editZone CancelZone:(id)zone {
     // self.currentOverlay=zone;
    CircleOverlay* zone2=zone;
    if (editZone.isEdtiting) {
      
        [self.mapView removeOverlay:self.currentOverlay];
        NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
        [tmpOverlays removeObject:self.currentOverlay];
        zone2.isOwner=YES;
        [tmpOverlays addObject:zone2];
        self.overlays = [tmpOverlays copy];
       // self.currentOverlay = zone;///
    } else {
        [self.mapView removeOverlay:self.currentOverlay];
    }
    self.currentOverlay.editingRadius = NO;
    self.overlaySelected= NO;
    [self setNewZone:NO];
    self.currentOverlay = nil;
    self.currentOverlayRenderer=nil;
    editZone.isEdtiting = NO;
    [editZone.view removeFromSuperview];
}
#pragma mark EditInsecureZoneDelegate

-(void)editInsecureZone:(EditeInsecureZoneVC *)editZone SaveZone:(id)zone {
    // Guardar en la BD la zona
    
    // Mostrar las demás zonas ocultas por la edición de la nueva zona.
    [self.mapView removeOverlay:self.currentOverlay];
    self.lastOverlay=self.currentOverlay;
    self.currentOverlay=nil;
    self.currentOverlayRenderer=nil;
    self.lastOverlay.editingRadius=NO;
    self.overlaySelected= NO;
    self.lastOverlay.isOwner=YES;
    self.lastOverlay.editingCoordinate = NO;
    [self.mapView addOverlay:self.lastOverlay];
    [self setNewZone:NO];
    [editZone.view removeFromSuperview];
}

-(void)editInsecureZone:(EditeInsecureZoneVC *)editZone CancelZone:(id)zone {
    // self.currentOverlay = zone;
       CircleOverlay* zone2=zone;
    if (editZone.isEdtiting) {
      
        [self.mapView removeOverlay:self.currentOverlay];
        NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
        [tmpOverlays removeObject:self.currentOverlay];
        zone2.isOwner=YES;
        [tmpOverlays addObject:zone2];
        
        self.overlays = [tmpOverlays copy];
       // self.currentOverlay = zone;
    } else {
        [self.mapView removeOverlay:self.currentOverlay];
    }
    self.currentOverlay.editingRadius = NO;
    self.overlaySelected= NO;
    [self setNewZone:NO];
    self.currentOverlay = nil;
    self.currentOverlayRenderer=nil;
    editZone.isEdtiting = NO;
    [editZone.view removeFromSuperview];
}

#pragma mark InfoZoneDelegate

-(void)infoZone:(InfoZoneVC *)infoZone EditeZone:(id)zone {
    CircleOverlay* zoneSend=zone;
     CircleOverlay *tmp = self.currentOverlay;
   
    [self setNewZone:YES];
    if (zoneSend.zoneType==YES) {
        self.edit.isEdtiting = YES;
      self.edit.zone=zoneSend;
        [self.edit drawInView:self.view];
    }else{
        self.editIn.isEdtiting = YES;
       self.editIn.zone=zoneSend;
        [self.editIn drawInView:self.view];
    }
    [self.mapView removeOverlay:self.currentOverlay];
    self.currentOverlay = tmp;
    self.currentOverlay.editingRadius=YES;
    self.overlaySelected= YES;
    [self.mapView addOverlay:tmp];
  
}

-(void)infoZone:(InfoZoneVC *)infoZone DeleteZone:(id)zone {
    // Eliminar del servidor
    
   // CircleOverlay* zoned=zone;
    self.deletedZoneCircle= zone;
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"seguro.eliminar.zona", nil) Delegate:nil CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.parentViewController];
    alert.delegate=self;
}
-(void)buttonPressed:(UIButton *)sender{
    if (sender.tag==1) {
        
        if(self.deletedZoneCircle.zoneType==YES){
            self.zoneForDelete= [[SecurityZone findWithZoneId:self.deletedZoneCircle.idZone]objectAtIndex:0];
            [SecurityZonesServices deleteSecurityZonesWithIdZone:self.deletedZoneCircle.idZone Wait:NO Delegate:self];
        }
        else{
            self.zoneForDelete= [[InsecureZone findWithZoneId:self.deletedZoneCircle.idZone]objectAtIndex:0];
            [InsecureZoneServices deleteInsecurityZonesWithIdZone:self.deletedZoneCircle.idZone Wait:NO Delegate:self];
        }

    }
}
#pragma mark LocationUser

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    
    static NSString* AnnotationIdentifier = @"Annotation";
    MKAnnotationView *customPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    
    if (annotation == mapView.userLocation){
        customPinView.image = [UIImage imageNamed:@"icono-05-verde"];
    }
    
    customPinView.canShowCallout = YES;
    return customPinView;
    
  
}
-(void)successResponseWithService:(kService)service Response:(id)response{
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    
    switch (service) {
        case kDELETE_SECURITY_ZONE:
            dictionary = [dictionary objectForKey:@"DeleteSecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            
            if (responseCode == 200) {
             [self.zoneForDelete deleteObject];
                 self.zoneForDelete= nil;
                // Eliminar del mapa
                [self.mapView removeOverlay:self.deletedZoneCircle];
                NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
                [tmpOverlays removeObject:self.deletedZoneCircle];
                self.overlays = [tmpOverlays copy];
                _currentOverlay=nil;
                _currentOverlayRenderer=nil;
                _lastOverlay = nil;
                _lastOverlayRenderer = nil;
                self.deletedZoneCircle=nil;
                
            }
            if (responseCode == 404) {
                [self.zoneForDelete deleteObject];
                self.zoneForDelete= nil;
                // Eliminar del mapa
                [self.mapView removeOverlay:self.deletedZoneCircle];
                NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
                [tmpOverlays removeObject:self.deletedZoneCircle];
                self.overlays = [tmpOverlays copy];
                _currentOverlay=nil;
                _currentOverlayRenderer=nil;
                _lastOverlay = nil;
                _lastOverlayRenderer = nil;
                self.deletedZoneCircle=nil;
               
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
        case kDELETE_INSECURITY_ZONE:
            dictionary = [dictionary objectForKey:@"DeleteInsecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            
            if (responseCode == 200) {
               [self.zoneForDelete deleteObject];
                self.zoneForDelete= nil;
                // Eliminar del mapa
                [self.mapView removeOverlay:self.deletedZoneCircle];
                NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
                [tmpOverlays removeObject:self.deletedZoneCircle];
                self.overlays = [tmpOverlays copy];
                _currentOverlay=nil;
                _currentOverlayRenderer=nil;
                _lastOverlay = nil;
                _lastOverlayRenderer = nil;
                self.deletedZoneCircle=nil;
                             }
            if (responseCode == 404) {
                [self.zoneForDelete deleteObject];
                self.zoneForDelete= nil;
                // Eliminar del mapa
                [self.mapView removeOverlay:self.deletedZoneCircle];
                NSMutableArray* tmpOverlays = [self.overlays mutableCopy];
                [tmpOverlays removeObject:self.deletedZoneCircle];
                self.overlays = [tmpOverlays copy];
                _currentOverlay=nil;
                _currentOverlayRenderer=nil;
                _lastOverlay = nil;
                _lastOverlayRenderer = nil;
                self.deletedZoneCircle=nil;
            }

            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
            
        default:
            break;
    }
}

@end
