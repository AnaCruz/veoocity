//
//  InsecureZoneServices.h
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@interface InsecureZoneServices : Service

+(void) createSecurityZoneWithName: (NSString *)name CenterLatitude: (NSString * )latitude CenterLongitude:(NSString *)longitude Radius: (NSString *)radius FirstTime:(NSString *)firstTime LastTime:(NSString *)lastTime Wait: (BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) updateInsecurityZoneWithLatitude:(NSString*) latitude Longitude:(NSString*) longitude Radius:(NSString*) radius Name:(NSString*) name FisrtTime:(NSString*)firstTime LastTime:(NSString *)lastTime IdZone:(NSString*) idZone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
+(void) deleteInsecurityZonesWithIdZone:(NSString * )IdZone Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getUserInsecurityZonesWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
@end
