//
//  SessionServices.h
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Service.h"

@interface SessionServices : Service

/**
 * Login in the app using number phone.
 * @param data Data for the request
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (NSData*) loginWithData:(NSData*) data Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Login in the app using phone number.
 * @param phone Number phone.
 * @param pin Security pin.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (NSData*) loginWithPhone:(NSString*) phone Pin:(NSString*) pin Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Login in the app using mail.
 * @param mail Mail.
 * @param pin Security pin.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (NSData*) loginWithMail:(NSString*) mail Pin:(NSString*) pin Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Login in the app using Facebook.
 * @param idFacebook Id of the user from Facebook.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (NSData*) loginWithFacebook:(NSString*) idFacebook Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;


/**
 * Close the user session.
 * @param userId User idenfier.
 * @param sessionId Session identifier.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) logoutWithUserId: (NSString* ) userId SessionId:( NSString *) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
/**
 * Check if the session is active for a user.
 * @param userId User idenfier.
 * @param sessionId Session identifier.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) isSessionActiveWithUserId: (NSString* ) userId SessionId:( NSString *) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

@end
