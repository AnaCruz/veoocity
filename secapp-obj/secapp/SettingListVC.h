//
//  SettingListVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 20/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingListVC : UITableViewController

@property (weak, nonatomic) IBOutlet UISwitch *toggleZone;
@property (weak, nonatomic) IBOutlet UISwitch *toggleLocationHide;
@property (strong, nonatomic) IBOutlet UISwitch *toggleOccurrences;

/**
 * Block that notify when the user press a option in the list from menu.
 */
@property (nonatomic, copy) void (^callbackSwitch)(int tag, BOOL selected);
/**
 * Block that notify when the user press a option in the list from menu.
 */
@property (nonatomic, copy) void (^callback)(int tag);

@end
