//
//  InsecureZoneServices.m
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "InsecureZoneServices.h"


@implementation InsecureZoneServices

+(void) createSecurityZoneWithName: (NSString *)name CenterLatitude: (NSString * )latitude CenterLongitude:(NSString *)longitude Radius: (NSString *)radius FirstTime:(NSString *)firstTime LastTime:(NSString *)lastTime Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@CreateInsecureZone", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"Name":name,
                             @"Latitude":latitude,
                             @"Longitude":longitude,
                             @"Radius":radius,
                             @"FirstTime":firstTime,
                             @"LastTime":lastTime,
                             @"CreatedByUserId":[super getUserId],
                             @"UserIdTo":[super getUserId],
                             @"SessionId":[super getSessionId]
                             
                             };
    
 
    
    RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_INSECURE_ZONE Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) getUserInsecurityZonesWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@InsecureZonesByUser/%@/%@/%@", [super getUrl], [super getUserId], [super getUserId],[super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_INSECURITY_ZONES Wait:wait Delegate:delegate];
    
    [request execute];
    
}
+(void) deleteInsecurityZonesWithIdZone:(NSString * )IdZone Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DeleteInsecureZone/%@/%@/%@", [super getUrl], IdZone,[super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_INSECURITY_ZONE Wait:wait Delegate:delegate];
    [request execute];
    
    
    
}
+(void) updateInsecurityZoneWithLatitude:(NSString*) latitude Longitude:(NSString*) longitude Radius:(NSString*) radius Name:(NSString*) name FisrtTime:(NSString*)firstTime LastTime:(NSString *)lastTime IdZone:(NSString*) idZone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    
    NSString* uri = [NSString stringWithFormat:@"%@UpdateInsecureZone", [super getUrl]];
    
    NSDictionary* dataDic = @{
                              @"objZone":@{
                                      @"Latitude":latitude,
                                      @"Longitude":longitude,
                                      @"Radius":radius,
                                      @"Name":name,
                                      @"FirstTime":firstTime,
                                      @"LastTime":lastTime,
                                      @"idZone":idZone
                                      },
                              @"UserId":[Service getUserId],
                              @"SessionId":[Service getSessionId]
                              };
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_INSECURITY_ZONE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}

@end
