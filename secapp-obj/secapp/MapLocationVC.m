 //
//  MapLocationVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "MapLocationVC.h"
#import "Friend.h"
#import "MapSecApp.h"
#import <CoreLocation/CoreLocation.h>
#import "CustomAnnotation.h"
#import "InfoUserVC.h"
#import "ContactService.h"
#import "Email.h"
#import "Phone.h"
#import "Profile.h"
#import "SearchContactsVC.h"
#import "ConversationVC.h"
#import "ContainerVC.h"
#import "AppDelegate.h"
#import "Alert/AlertView.h"
#import "OcurrenceVC.h"
#import "Occurrence.h"
#import "OccurenceServices.h"
#import "DisplayOccurrenceVC.h"
#import "LocationManagement.h"
#import "TestViewController.h"
#import "ConnectionManagement.h"
#import "GetNotification.h"
#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface MapLocationVC  () <InfoUserDelegate, UIGestureRecognizerDelegate, ResponseDelegate, MKMapViewDelegate, UIActionSheetDelegate, AlertViewDelegate>

@property  CLLocationManager* locationManager;
@property NSTimer *timer;
@property int color;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *doubleTap;
//@property (weak, nonatomic) IBOutlet UIButton *transito;
@property Friend *currentFriend;
@property (weak, nonatomic) IBOutlet UIButton *occurrenceButton;
//tap para creacion de annotations
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *singleTap; //crear annotation
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longTap; //mover annotation de lugar
//@property BOOL mapViewGestureEnabled;
@property BOOL pin; // hay o no un pin dibujado en el mapa

@property CustomAnnotation * currentAnnOccurrence; //pin que se acaba de poner, para moverlo tiene q existir
//@property OccurrenceListVC * occurenceView;

@property TestViewController * testVC;
@property NSMutableArray *recordIncidences;
@property int languaje;
@end

@implementation MapLocationVC

-(void)viewDidAppear:(BOOL)animated {
    self.creatingOccurrence=NO;
    self.pin=NO;
  self.mapView.delegate=self;
    self.mapView = [MapSecApp shareInstance];
    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.frame = self.view.frame;
        [self.view addSubview:self.mapView];
        [self.view sendSubviewToBack:_mapView];
    });
    [self.mapView clearMap];
    self.mapView.delegate=self;
    self.mapView = [MapSecApp shareInstance];
   
    [self.mapView addGestureRecognizer:self.doubleTap]; //double tap zoom
    [self.mapView addGestureRecognizer:self.longTap]; //arrastar
    [self.mapView addGestureRecognizer:self.singleTap];
    [_mapView setShowsUserLocation:YES];
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:YES];
    [_mapView setScrollEnabled:YES];
    self.languaje=[[Service getLanguaje]intValue];
    [self drawPinsOnMap];
}

-(void)viewWillDisappear:(BOOL)animated{
    [self.timer invalidate];
    self.timer=nil;
    self.creatingOccurrence=NO;
    self.pin=NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.creatingOccurrence=NO;

    dispatch_async(dispatch_get_main_queue(), ^{
        self.mapView.frame = self.view.frame;
        [self.view addSubview:self.mapView];
 [self.view sendSubviewToBack:_mapView];
        
    });
    [self.mapView clearMap];
    self.mapView.delegate=self;
    self.mapView = [MapSecApp shareInstance];
    
    [self.longTap addTarget:self action:@selector(longTapped:)];
    self.longTap.delegate= self;
    [self.singleTap  addTarget:self action:@selector(singleTapp:)];
    self.singleTap.delegate = self;
    self.doubleTap.delegate=self;
    [self.singleTap requireGestureRecognizerToFail:self.doubleTap];
    _wasOn=NO;
    _locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        
        [self.locationManager requestAlwaysAuthorization];
        
    }
#endif
    [self.locationManager startUpdatingLocation];
    [_mapView setShowsUserLocation:YES];
    self.infoU =[[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoUserVC"];
    [self addChildViewController:self.infoU];
    self.infoU.delegate=self;
    if (!self.recordIncidences) {
          self.recordIncidences = [[NSMutableArray alloc] init];
    }
  [self drawPinsOnMap];
    
}

-(void)showView:(NSString *)friendSelected{

    self.conver = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationVC"];
     self.conver.userIdTo=friendSelected;
    [self.conver drawInView:self.view];
  
}

/**
 * Obtains the annotation selected in the map.
 * If the annotation is the user location send the user profile to the InfoUserVC class.
 * If the annotation is a friend send the annotation created that contains the friend data.
 * If the InfoUserVC is visible and the user select another annotation, the setUser method is called. Otherwise, the drawInView method is called.
 */
-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    [mapView deselectAnnotation:view.annotation animated:NO];
    if (self.creatingOccurrence) {
        //arrastar
        return;
    }
    
    CustomAnnotation * ann=(CustomAnnotation *)(view.annotation);
   if (view.annotation == mapView.userLocation){
      //limpiar mapa
//        self.infoU.me = [Profile findWithUserId:[Service getUserId]];
//        self.infoU.user = nil;
       [self.container showInfoBarWithMe];
    }
  else if(ann.friendInMap){
       [self.container showInfoBar:ann.friendInMap];
    }
   else {
       //ventana de info de incidencia
       NSString* ids=ann.occurrenceInMap.occurrenceId;
     if (ids) {
           if ([ann.occurrenceInMap.isSpecial boolValue]==true) {
               self.displayOccurrence= [[UIStoryboard storyboardWithName:@"Occurrence" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoSpeciallOccurrenceVC"];
               self.displayOccurrence.occ= ann.occurrenceInMap;
               [self.displayOccurrence drawInView:self.parentViewController.view];
               
           }else{
               self.displayOccurrence= [[UIStoryboard storyboardWithName:@"Occurrence" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoOccurrenceVC"];
               self.displayOccurrence.occ= ann.occurrenceInMap;
               [self.displayOccurrence drawInView:self.parentViewController.view];
           }
           return;
       }else{
           return;
       }
}
 
}

-(UIImage *)makeRoundedImage:(UIImage *) image;
{
    float radiu= image.size.height;
    CALayer *imageLayer = [CALayer layer];
    imageLayer.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    imageLayer.contents = (id) image.CGImage;
    
    imageLayer.masksToBounds = YES;
    imageLayer.cornerRadius = radiu/2;
    
    UIGraphicsBeginImageContext(image.size);
    [imageLayer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *roundedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return roundedImage;
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString* AnnotationIdentifier = @"Annotation";
    MKAnnotationView *customPinView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationIdentifier];
    
    CustomAnnotation * tmp = (CustomAnnotation *)annotation;
    UIImage *frame;
    UIImage *image;
    if (annotation == mapView.userLocation||[annotation isKindOfClass:[MKUserLocation class]]){
        customPinView.canShowCallout = NO;
        customPinView.image = [UIImage imageNamed:@"icono-05-verde"];
        frame = [UIImage imageNamed:@"icono-05-verde"];
        Profile *me  = [Profile findWithUserId:[Service getUserId]];
        NSLog(@"me for pin: %@",me.userId);
        if ([me.userId isEqualToString:[Service getUserId]]) {
            image =[self makeRoundedImage:[UIImage imageWithData:me.pictureProfile]];
        }else{
            NSData* pictureData = UIImagePNGRepresentation([UIImage imageNamed:@"avatar"]);
            image =[self makeRoundedImage:[UIImage imageWithData:pictureData]];
        }
        //validar que sea el nuevo perfil
        
        UIGraphicsBeginImageContextWithOptions(customPinView.image.size, NO, [UIScreen mainScreen].scale);
       
        [frame drawInRect:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [image drawInRect:CGRectMake(24, 4, 24, 24)];
        customPinView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return customPinView;
        
    } else if(tmp.friendInMap){
        
        switch ([tmp.friendInMap.status intValue]) {
            case 0:
                customPinView.image = [UIImage imageNamed:@"icono-03-azul"];
                frame = [UIImage imageNamed:@"icono-03-azul"];
                
                break;
            case 1:
                customPinView.image = [UIImage imageNamed:@"icono-02-rojo"];
                frame = [UIImage imageNamed:@"icono-02-rojo"];
                break;
            
            case 4:
                customPinView.image = [UIImage imageNamed:@"icono-04-gris"];
                frame = [UIImage imageNamed:@"icono-04-gris"];
                break;
            default:
                break;
        }
        image =[self makeRoundedImage:[UIImage imageWithData:tmp.friendInMap.pictureProfile]];
        UIGraphicsBeginImageContextWithOptions(customPinView.image.size, NO, [UIScreen mainScreen].scale);
        [frame drawInRect:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [image drawInRect:CGRectMake(24, 4, 24, 24)];
        customPinView.image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        customPinView.canShowCallout = YES;
        return customPinView;
    }
    else{
        
        switch ([tmp.title intValue]) {
            case 1:
                customPinView.image = [UIImage imageNamed:@"pin_1"];
                break;
            case 2:
                customPinView.image = [UIImage imageNamed:@"pin_2"];
                break;
            case 3:
                customPinView.image = [UIImage imageNamed:@"pin_3"];
                
                break;
            case 4:
                customPinView.image = [UIImage imageNamed:@"pin_4"];
                break;
            case 5:
                customPinView.image = [UIImage imageNamed:@"pin_5"];
                
                break;
            case 6:
                customPinView.image = [UIImage imageNamed:@"pin_6"];
                break;
            case 7:
                customPinView.image = [UIImage imageNamed:@"pin_7"];
                
                break;
            case 8:
                customPinView.image = [UIImage imageNamed:@"pin_8"];
                break;
            case 9:
                customPinView.image = [UIImage imageNamed:@"pin_9"];
                
                break;
            default:
                switch ([tmp.subtitle intValue]) {
                    case 3:
                        customPinView.image = [UIImage imageNamed:@"pin_cr"];
                        break;
                   default:
                        customPinView.image = [UIImage imageNamed:@"pin_sec"];
                        break;
                }
                
                break;
        }
        customPinView.canShowCallout = YES;
       return customPinView;
        
        
    }
    
    return customPinView;
    
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    CLLocationCoordinate2D loc = [userLocation coordinate];
      if (!_wasOn) {
       
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 250, 250);
        [_mapView setRegion:region animated:YES];
        _wasOn=YES;
     }
}
- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState{
  

    if (newState == MKAnnotationViewDragStateStarting)
    {
        annotationView.dragState = MKAnnotationViewDragStateDragging;
    }
    else if (newState == MKAnnotationViewDragStateEnding || newState == MKAnnotationViewDragStateCanceling)
    {
        annotationView.dragState = MKAnnotationViewDragStateNone;
    }
     CLLocationCoordinate2D droppedAt = annotationView.annotation.coordinate;
    
        self.occurrence.latitude= [NSString stringWithFormat:@"%f",droppedAt.latitude];
        self.occurrence.longitude=[NSString stringWithFormat:@"%f",droppedAt.longitude];
    
}



/**
 * Remove all annotations from the map and redraw only the user location.
 */
- (void)removeAllPinsButUserLocation
{
 id userLocation = [_mapView userLocation];
  
    [self.mapView removeAnnotations:[self.mapView annotations]];
    _currentFriend=nil;
    if ( userLocation != nil ) {
     [_mapView setShowsUserLocation:YES];
    
    }
}
- (void) addFriendAnnotationWithLocation:(Friend *)friend{
    CGFloat newLat = [friend.currentLatitude floatValue];
    CGFloat newLon = [friend.currentLongitude floatValue];
    NSString *subtitle;
    NSNumber * status=friend.status;
    _color=[friend.status intValue];
    CLLocationCoordinate2D newCoord = {newLat, newLon};
    switch ([status intValue]) {
        case 0:
            subtitle = @"Todo bien";
            break;
        case 1:
            subtitle = @"Esta en Alerta!";
            break;
        case 2:
            subtitle = @"Tiene reportes sin responder";
            break;
        case 4:
            subtitle = @"Tiene el GPS apagado";
            break;
        default:
            
            break;
    }
    CustomAnnotation *annotation = [[CustomAnnotation alloc]initWithTitle: friend.userName
                                                                 subtitle: subtitle
                                                            andCoordinate:newCoord];
    annotation.friendInMap = friend;
    
    dispatch_async(dispatch_get_main_queue(), ^{
       [self.mapView addAnnotation:annotation];
    });
}
- (void) addOccurrenceAnnotation:(Occurrence *)occurrence{
    CGFloat newLat = [occurrence.latitude floatValue];
    CGFloat newLon = [occurrence.longitude floatValue];
    NSString *subtitle= occurrence.createdUserId;
    NSString * type= [NSString stringWithFormat:@"%@",occurrence.type];
    
    CLLocationCoordinate2D newCoord = {newLat, newLon};
     CustomAnnotation *annotation = [[CustomAnnotation alloc]initWithTitle: type
                                                                 subtitle: subtitle
                                                            andCoordinate:newCoord];
    annotation.occurrenceInMap = occurrence;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView addAnnotation:annotation];
    });
}


/**
 * Call the getContactList service.
 */
-(void)updateContacts{
if (self.creatingOccurrence) {
        return;
    }
[self drawPinsOnMap];

    
}
-(void)drawPinsOnMap{
    if (self.creatingOccurrence) {
        return;
    }
    self.pin=NO;
    self.currentAnnOccurrence=nil;
   
    [self removeAllPinsButUserLocation];
    NSArray * result = [Friend findAll];
    for (Friend *friend in result) {
        [self addFriendAnnotationWithLocation:friend];
    }
    if(self.updateContactsCount){
        self.updateContactsCount();
    }
    NSArray * result2 = [Occurrence findAll];
    for (Occurrence *occur in result2) {
        [self addOccurrenceAnnotation:occur];
    }

}



#pragma mark - ResponseDelgate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kGET_CONTACT_LIST:
            dictionary = [dictionary objectForKey:@"ContactListResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayFriend = [dictionary objectForKey:@"UserList"];
                 NSArray * arrayFriendsInBD= [Friend findAll];
                for (Friend* friend in arrayFriendsInBD) {
                    [friend deleteObject];
                }
                for (NSDictionary* friendData in arrayFriend) {
                  [self saveFriend:friendData];
                 }
                [self drawPinsOnMap];
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            
            break;
        case kGET_ALL_OCCURRENCES:
            dictionary = [dictionary objectForKey:@"AllOccurrencesResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayOccurrence = [dictionary objectForKey:@"OccurrenceList"];
                NSArray * arrayOccurrenceInBD= [Occurrence findAll];
                for (Occurrence* occurrence in arrayOccurrenceInBD) {
                    [occurrence deleteObject];
                }
                for (NSDictionary* occurrenceData in arrayOccurrence) {
                    [self saveOccurrence:occurrenceData];
                }
                [self drawPinsOnMap];
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;

        default:
            break;
    }
}


/**
 * Save the friend data on the data base.
 */
- (void) saveFriend:(NSDictionary*)friendData {
    NSArray* result = [Friend findWithUserId:[friendData objectForKey:@"UserId"]];
    Friend* friend;
    if (result && [result count] > 0) {
        friend = [result objectAtIndex:0];
    } else {
        friend = [Friend createObject];
        [friend saveObject];
    }
    friend.userId = [friendData objectForKey:@"UserId"];
    friend.batteryLevel = [NSNumber numberWithFloat:[[friendData objectForKey:@"BatteryLevel"] floatValue]];
    friend.currentLatitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Latitude"] doubleValue]];
    friend.currentLongitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Longitude"] doubleValue]];
    NSNumber* gender = [NSNumber numberWithBool:[[friendData objectForKey:@"Gender"] isEqualToString:@"1"] ? YES : NO];
    friend.gender = gender;
     NSString * name;
     NSString * userName;
    NSString * lastname;
    if ([[friendData objectForKey:@"Name"] isEqualToString:@""]||[[friendData objectForKey:@"Name"]isEqual:[NSNull null]]) {
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]){
                if (self.languaje==1) {
                    name= @"Desconocido";
                    lastname = @"";
                }else{
                    name= @"Unknown";
                    lastname = @"";
                }
               
            }else{
                name=[friendData objectForKey:@"LastName"];
                userName= [friendData objectForKey:@"LastName"];
                lastname=@"";
            
            }
        } else{
      
        name=[friendData objectForKey:@"Username"];
        userName=[friendData objectForKey:@"Username"];
        }
    }else {
        name=[friendData objectForKey:@"Name"];
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            userName=name;
        }else{
            userName = [friendData objectForKey:@"Username"];
        }
    }
    
    if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]) {
       lastname = @"";
    }else{
        lastname=[friendData objectForKey:@"LastName"];
    }
    friend.lastName = lastname;
    friend.name = name;
    friend.status = [NSNumber numberWithInt:[[friendData objectForKey:@"Status"] intValue]];
    friend.permissions =[friendData objectForKey:@"Permission"];
    friend.positionDate =[NSDate dateWithTimeIntervalSince1970:[[friendData objectForKey:@"PositionDate"] doubleValue]/1000];
    friend.userName = userName;
    if ([friendData objectForKey:@"ProfileImageURL"] && ![[friendData objectForKey:@"ProfileImageURL"] isEqual:[NSNull null]]) {
       // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^ {
            NSData *img = [NSData dataWithContentsOfURL:[NSURL URLWithString:[friendData objectForKey:@"ProfileImageURL"]]];
            friend.pictureProfile = img;
       // });
    } else {
        NSData* pictureData = UIImagePNGRepresentation([UIImage imageNamed:@"avatar"]);
        friend.pictureProfile=pictureData;
       // friend.pictureProfile = nil; // error poner avatar
    }
    NSArray *emails = [friendData objectForKey:@"Email"];
    for (Email *email in friend.emails) {
        [email deleteObject];
    }
    [friend removeEmails:friend.emails];
    for (NSString *email in emails) {
        Email* oEmail = [Email createObject];
        [oEmail saveObject];
        oEmail.email = email;
        oEmail.people = friend;
        [friend addEmailsObject:oEmail];
    }
    for (Phone *phone in friend.phones) {
        [phone deleteObject];
    }
    NSArray *phones = [friendData objectForKey:@"Telephone"];
    [friend removePhones:friend.phones];
    for (NSString *phone in phones) {
        Phone* oPhone = [Phone createObject];
        [oPhone saveObject];
        oPhone.phone = phone;
        oPhone.people = friend;
        [friend addPhonesObject:oPhone];
    }
    
    [friend mergeObject];
    
}
-(void) saveOccurrence:(NSDictionary *) occurrenceData{
    NSArray* result = [Occurrence findWithOccurrenceId:[occurrenceData objectForKey:@"OccurrenceId"]];
    Occurrence * occurrence;
    if (result && [result count] > 0) {
        occurrence = [result objectAtIndex:0];
    } else {
        occurrence = [Occurrence createObject];
        [occurrence saveObject];
    }
    occurrence.occurrenceId = [NSString stringWithFormat:@"%@",[occurrenceData objectForKey:@"OccurrenceId"]];
    occurrence.latitude =[NSNumber numberWithDouble:[[occurrenceData objectForKey:@"Latitude"] doubleValue]];
    occurrence.longitude =[NSNumber numberWithDouble:[[occurrenceData objectForKey:@"Longitude"] doubleValue]];
    
    occurrence.type=[NSNumber numberWithInt:[[occurrenceData objectForKey:@"Type"] intValue]];

    [occurrence mergeObject];



}

#pragma mark Create occurrences
- (IBAction)newOccurrence:(id)sender {

    self.testVC = [[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"TestListVC"];
    
    MapLocationVC * mapa=self;
    self.testVC.map = mapa;
    [self.testVC drawInView:self.parentViewController.view];

    }


-(void)createOccurrence:(NSString *)type{
    CGFloat newLat;
    CGFloat newLon;
    newLat =[[[LocationManagement sharedInstance] latitude] floatValue];
    newLon = [[[LocationManagement sharedInstance] longitude] floatValue];
    if (newLat==0 ||newLon==0) {
        Profile * profile = [Profile findWithUserId:[Service getUserId]];
        newLat = [profile.lastLatitude floatValue];
        newLon =[profile.lastLongitude floatValue];
    }

    CLLocationCoordinate2D newCoord = {newLat, newLon};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, 50, 50);
    [self.mapView setRegion:region animated:YES];
    self.typeOccurrence=type;
 
    self.occurrence = [[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"OcurrenceVC"];
    self.occurrence.type=type;
    MapLocationVC *map=self;
    self.occurrence.map=map;
    self.occurrence.contView= self.container;
    [self.occurrence drawInView:self.view];
    self.creatingOccurrence=YES;
  
 }

- (void)longTapped:(UILongPressGestureRecognizer*)sender {
    
    
    if (self.creatingOccurrence && self.pin&& self.currentAnnOccurrence) {
                if (sender.state == UIGestureRecognizerStateChanged) {
                   CGPoint newPoint = [sender locationInView:sender.view];
                    CLLocationCoordinate2D newCoordinate = [self.mapView convertPoint:newPoint toCoordinateFromView:sender.view];
                    self.currentAnnOccurrence.coordinate = newCoordinate;
                    [self.mapView removeAnnotation:self.currentAnnOccurrence];
                     [self.mapView addAnnotation:self.currentAnnOccurrence];
                    self.occurrence.latitude=[NSString stringWithFormat:@"%f", newCoordinate.latitude];
                    self.occurrence.longitude=[NSString stringWithFormat:@"%f", newCoordinate.longitude];
                }
                if (sender.state == UIGestureRecognizerStateEnded) {
                   [self.mapView removeAnnotation:self.currentAnnOccurrence];
                    [self.mapView addAnnotation:self.currentAnnOccurrence];
                }
    }
   
}
- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
  
}
- (void)singleTapp:(UITapGestureRecognizer*)sender {
    if (sender.numberOfTouches == 1) {
        if (self.creatingOccurrence && !self.pin) {
            self.pin=YES;
            CLLocationCoordinate2D coord = [self.mapView convertPoint:[sender locationInView:self.view] toCoordinateFromView:self.view];
        CustomAnnotation *annotation = [[CustomAnnotation alloc]initWithTitle: self.typeOccurrence
                                                                     subtitle: @""
                                                                andCoordinate:coord];
            
            self.occurrence.latitude= [NSString stringWithFormat:@"%f",coord.latitude];
            self.occurrence.longitude=[NSString stringWithFormat:@"%f",coord.longitude];
            self.currentAnnOccurrence=annotation; //se acaba de crear esta anotacion
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.mapView addAnnotation:annotation];
        });
        }else{
           // tap en el mapa, cerrar vista
             [self.container clearSelection:nil];
            [self.container clearContainer];
        
        }
  }
}

@end
