//
//  PairBeaconInterfaceVC.m
//  secapp
//
//  Created by SECAPP on 27/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import "PairBeaconInterfaceVC.h"
#import "PairBeaconCentralManager.h"
#import "ContainerVC.h"
#import "Service.h"
#import "LocationManagement.h"
#import "AlertService.h"
#import "Profile.h"
#import "Alert/AlertView.h"

@interface PairBeaconInterfaceVC () <UITextFieldDelegate, PairBeaconCentralManagerDelegate, ResponseDelegate, AlertViewDelegate>
@property NSString * languaje;
@property CGRect originalPosition;
@property (weak, nonatomic) IBOutlet UIView *viewPairBeacon;

@property (weak, nonatomic) IBOutlet UIImageView *imageStatusBeacon;
@property (weak, nonatomic) IBOutlet UILabel *instructionsLabel;
@property (weak, nonatomic) IBOutlet UITextField *uuidTextField;
@property (weak, nonatomic) IBOutlet UIButton *pairButton;
@property (weak, nonatomic) IBOutlet UIButton *disconectButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property PairBeaconCentralManager* central;
@property NSString * uuidToPair;

@end

@implementation PairBeaconInterfaceVC
@synthesize status;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.originalPosition= self.viewPairBeacon.frame;
    [self registerForKeyboardNotifications];
     self.central = [PairBeaconCentralManager sharedInstance];
    self.central.delegate=self;
    self.container= (ContainerVC *)self.container;
    self.central.cont= self.container;
    self.languaje= [Service getLanguaje];
    
    
    
// Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    [self.central statusBeacon];
   self.container= (ContainerVC *)self.container;
}


-(IBAction)pair:(id)sender {
   // [self cleanup];
    if ([self.uuidTextField.text isEqualToString:@""]||[self.uuidTextField.text isEqualToString:@"XXXXXXXX"]) {
        return;
    }
    else{
       self.central.uuid= self.uuidTextField.text;
        if ([self.languaje isEqualToString:@"1"]) {
            self.statusBeaconLabel.text= @"Buscando...";
        }else{
             self.statusBeaconLabel.text= @"Searching...";
        
        }
       
        [self.central scan];
       
    }
}
- (IBAction)disconnect:(id)sender {
    [self.central disconnect];
}

-(void)updateViewDidConnect{
    if ([self.languaje isEqualToString:@"1"]) {
       self.statusBeaconLabel.text=@"Conectado";
    }else{
    self.statusBeaconLabel.text=@"Connected";
    }
    self.imageStatusBeacon.image= [UIImage imageNamed:@"beacon_act"];
    self.pairButton.hidden=YES;
    self.disconectButton.hidden=NO;
    self.uuidTextField.hidden=YES;
    self.instructionsLabel.hidden=YES;
    self.searchButton.hidden=NO;
    
    
}
- (IBAction)searchMyBeacon:(id)sender {
    [self.central searchMyBeacon];
}
-(void)updateViewDidDisconnect{
    if ([self.languaje isEqualToString:@"1"]) {
         self.statusBeaconLabel.text=@"Desconectado";
         self.instructionsLabel.text = @"Ingrese identificador de botón";
    }
    else{
     self.statusBeaconLabel.text=@"Disconnected";
        self.instructionsLabel.text = @"Enter your Beacon ID";
    }
   
    self.imageStatusBeacon.image= [UIImage imageNamed:@"beacon_pas"];
    self.pairButton.hidden=NO;
    self.disconectButton.hidden=YES;
    self.uuidTextField.hidden=NO;
    self.uuidTextField.placeholder= @"XXXXXXXX";
    self.uuidTextField.text= @"";
    self.instructionsLabel.hidden=NO;
    self.searchButton.hidden=YES;
   
}
-(void)alert{
    if ([self.languaje isEqualToString:@"1"]) {
      self.instructionsLabel.text=@"Alerta activada";
    }
    else{
    self.instructionsLabel.text=@"Alert activated";
    }
    self.instructionsLabel.hidden=NO;
   [self.container startAlertFromBeacon];
//generar alerta
}
-(void)isConnected{
[self updateViewDidConnect];
}
-(void)notConnected{
 [self updateViewDidDisconnect];
}
-(void)timeOutForConnect{
    if ([self.languaje isEqualToString:@"1"]) {
         self.statusBeaconLabel.text=@"Dispositivo no encontrado";
    }else{
     self.statusBeaconLabel.text=@"Peripheral not found";
    
    }
   AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"beacon.not.found", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];

}
-(void)uuidNotMatch{
    if ([self.languaje isEqualToString:@"1"]) {
       self.statusBeaconLabel.text=@"UUID no coincide";
    }else{
    self.statusBeaconLabel.text=@"UUID not match";
    }
    
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"beaconuuid.not.match", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];

}
- (IBAction)goClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.originalPosition;
    aRect.origin.y = aRect.origin.y - (kbSize.height-aRect.size.height)+60;
    self.viewPairBeacon.frame = aRect;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.viewPairBeacon.frame = self.originalPosition;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    [self.view endEditing:YES];
}
-(void)viewDidDisappear:(BOOL)animated{
    self.central.cont=nil;
    [self.container setContainerVCDelegateOfBeaconCentral]; 
}
@end
