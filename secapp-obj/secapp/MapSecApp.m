//
//  MapSecApp.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "MapSecApp.h"
#import "Friend.h"

@implementation MapSecApp

+ (instancetype) shareInstance {
    static id shared = nil;
    @synchronized(self) {
        if (shared == nil)
            shared = [[self alloc] init];
    }
    return shared;
}

- (void) drawFriendInTheMap:(Friend*)friend {
    MKPointAnnotation* annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake([friend.currentLatitude doubleValue], [friend.currentLongitude doubleValue]);
    annotation.title = friend.name;
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:friend.userId];
    UIImage *image = [UIImage imageNamed:@"logo"];
    annotationView.image = image;

    [self addAnnotation:annotation];
}

- (void) clearMap {
    [self removeOverlays:self.overlays];
    [self removeAnnotations:self.annotations];
    self.delegate = nil;
    for (id gesture in self.gestureRecognizers) {
        [self removeGestureRecognizer:gesture];
    }
}

@end
