//
//  BuyServices.h
//  secapp
//
//  Created by SecApp on 08/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@interface BuyServices : Service
+(void) buyPremiumVersion:(NSString*) aplicationType ReceiptData:(NSString *)receiptData WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
@end
