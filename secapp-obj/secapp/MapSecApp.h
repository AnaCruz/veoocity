//
//  MapSecApp.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface MapSecApp : MKMapView
+ (instancetype) shareInstance;
- (void) clearMap;
@end
