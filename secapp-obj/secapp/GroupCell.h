//
//  GroupCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameGroup;

@end
