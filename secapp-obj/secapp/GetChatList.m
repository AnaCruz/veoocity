//
//  GetChatList.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 24/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "GetChatList.h"
#import "ResponseDelegate.h"
#import "Message.h"
#import "MessagesService.h"
#import "AppDelegate.h"
#import "Friend.h"

@interface GetChatList () <ResponseDelegate>

@end

@implementation GetChatList

+ (GetChatList*) shareInstance {
    static id shared = nil;
    @synchronized(self) {
        if (shared == nil)
            shared = [[self alloc] init];
    }
    return shared;
}

- (void) updateData {
    [MessagesService getMessageListWithWait:NO Delegate:self];
}
-(void)callTheCallBackForIconUnreadMessage{
    if ( self.callback ) {
        self.callback();
    }
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
  switch (service) {
        case kGET_LAST_MESSAGE:
            dictionary = [dictionary objectForKey:@"LastMessageResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayMessage = [dictionary objectForKey:@"MessageList"];
                [self parseArrayMessages:arrayMessage];
                if ( self.callback ) {
                    self.callback();
                }
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        default:
            break;
    }
}

/**
 * Parsing the JSON to Messages Object
 * @param array JSON Object
 */
- (void) parseArrayMessages:(NSArray*) array {
    NSArray* messages = [Message findAll];
    for (Message* message in messages) {
        [message deleteObject];
    }
    for (NSDictionary* messageData in array) {
        NSArray * dataFriend = [Friend findWithUserId:[messageData objectForKey:@"UserIdFrom"]];
        NSArray * dataFriend2 = [Friend findWithUserId:[messageData objectForKey:@"UserIdTo"]];
        NSString *userFrom=[[messageData objectForKey:@"UserIdFrom"]stringValue];
        NSString *userTo=[[messageData objectForKey:@"UserIdTo"]stringValue];
        if (![userFrom isEqualToString:[Service getUserId]]) {
            if ((!dataFriend||[dataFriend count]<=0)) {
                if (![[messageData objectForKey:@"FromMonitor"] boolValue]&&![[messageData objectForKey:@"ToMonitor"]boolValue]) {
                    if ([[messageData objectForKey:@"GroupId"]intValue]==0) {
                        continue;
                    }
                }
            }
        }else if (![userTo isEqualToString:[Service getUserId]]) {
            if ((!dataFriend2||[dataFriend2 count]<=0)){
                if (![[messageData objectForKey:@"FromMonitor"] boolValue]&&![[messageData objectForKey:@"ToMonitor"]boolValue]) {
                    if ([[messageData objectForKey:@"GroupId"]intValue]==0) {
                        continue;
                    }
                }
            }
        }
        Message* message = [Message createObject];
        [message saveObject];
        message.userIdFrom = [messageData objectForKey:@"UserIdFrom"];
        message.userIdTo = [messageData objectForKey:@"UserIdTo"];
        if ([[message.userIdFrom stringValue]isEqualToString:[Service getUserId]]) {
            message.isReaded =@1;
        }else{
            message.isReaded = [messageData objectForKey:@"IsReaded"];
        }
        message.message = [messageData objectForKey:@"Message"];
        message.messageId = [messageData objectForKey:@"MessageId"];
        id date = [messageData objectForKey:@"SendDate"];
        message.sendDate =  [NSDate dateWithTimeIntervalSince1970:[date doubleValue]/1000];
        
        NSString * chatname= [messageData objectForKey:@"ChatName"];
        if (chatname == (id)[NSNull null] || chatname.length == 0 ) {
            message.groupName=@"";
        }
        else{
            message.groupName= [messageData objectForKey:@"ChatName"];
        }
        message.groupId= [[messageData objectForKey:@"GroupId"] stringValue];
        NSString * imageGroup= [messageData objectForKey:@"Image"];
        if (imageGroup == (id)[NSNull null] || imageGroup.length == 0 ) {
            message.imageGroup=@"";
        }
        else{
            message.imageGroup= [messageData objectForKey:@"Image"];
        }
        message.isOwner=[messageData objectForKey:@"IsOwner"];
        message.fromMonitor=[messageData objectForKey:@"FromMonitor"];
        message.toMonitor=[messageData objectForKey:@"ToMonitor"];
        [message mergeObject];
    }
}
@end
