//
//  PushLocalSended.m
//  secapp
//
//  Created by SecApp on 07/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import "PushLocalSended.h"
#import "AppDelegate.h"

@implementation PushLocalSended
@dynamic dateSend;
@dynamic distance;
@dynamic occId;

+ (instancetype)createObject {
    PushLocalSended * occurrence = [super createObjectWithNameEntity:@"PushLocalSended"];
    return occurrence;
}
+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PushLocalSended"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"occId" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"PushLocalOccurrences (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}
+ (NSArray*) findWithPushOccurrenceId:(NSString*) occurrenceId {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PushLocalSended"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"occId == '%@'", occurrenceId]];
    [request setPredicate:predicate];
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        NSLog(@"PushLocalOccurrences (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}


@end
