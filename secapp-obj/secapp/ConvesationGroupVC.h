//
//  ConvesationGroupVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CellConversationSample.h"
#import "Friend.h"
#import "ContainerVC.h"

@interface ConvesationGroupVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSString * groupID;
@property (nonatomic) NSDictionary * chatGroup;
@property Friend * friendSender;
@property BOOL isVisibleinMap;
- (void) drawInView:(UIView*) view;
- (IBAction)goBack:(id)sender;
@end
