//
//  NearIncidences.m
//  secapp
//
//  Created by SecApp on 07/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import "NearIncidences.h"
#import "AppDelegate.h"


@implementation NearIncidences

@dynamic type;
@dynamic latitude;
@dynamic longitude;
@dynamic occId;

+ (instancetype)createObject {
    NearIncidences * occurrence = [super createObjectWithNameEntity:@"NearIncidences"];
    return occurrence;
}
+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NearIncidences"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"occId" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"NearIncidences (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}
+ (NSArray*) findWithNearOccurrenceId:(NSString*) occurrenceId {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"NearIncidences"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"occId == '%@'", occurrenceId]];
    [request setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"NearIncidences (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}


@end
