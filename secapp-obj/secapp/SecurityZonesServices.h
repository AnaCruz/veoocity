//
//  SecurityZonesServices.h
//  secapp
//
//  Created by SecApp on 03/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Service.h"

@interface SecurityZonesServices : Service

+(void) createSecurityZoneWithName: (NSString *)name CenterLatitude: (NSString * )latitude CenterLongitude:(NSString *)longitude Radius: (NSString *)radius Wait: (BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) getUserSecurityZonesWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) deleteSecurityZonesWithIdZone:(NSString * )IdZone Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) updateSecurityZoneWithLatitude:(NSString*) latitude Longitude:(NSString*) longitude Radius:(NSString*) radius Name:(NSString*) name IdZone:(NSString*) idZone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
+(void) showNotificationsSecureZonesWithPermission: (BOOL)permission Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
@end
