//
//  EditZoneVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 29/04/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EditZoneVC.h"
#import "CircleOverlay.h"
#import "SecurityZonesServices.h"
#import "SecurityZone.h"
#import "AlertView.h"
#import "AppDelegate.h"


@interface EditZoneVC () <UITextFieldDelegate, ResponseDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fieldName;
@property CircleOverlay *tmpZone;
@property CGRect originalPosition;
@end

@implementation EditZoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self registerForKeyboardNotifications];
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.isShow=YES;
    if ([self.zone.titleCircle isEqualToString:@""]) {
        self.fieldName.text = @"";
    }
    else {
        self.fieldName.text = self.zone.titleCircle;
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterForKeyboardNotifications];
    self.isShow=NO;
    self.isEdtiting=NO;
    self.tmpZone = nil;
}

- (void) drawInView:(UIView*) view {
    CGRect frame = self.view.frame;
    frame.size.height = 90;
    frame.origin.y = (view.frame.size.height - frame.size.height);
    
    frame.size.width= view.frame.size.width;
    self.view.frame = frame;
    self.originalPosition = frame;
    [view addSubview:self.view];
}

- (void)setZone:(CircleOverlay *)zone {
    _zone = zone;
    if (_zone) {

        if (!_tmpZone) {
            _tmpZone = [self.zone copy];
            
        }
    }
  
}
- (IBAction)saveZone:(id)sender {
    [self.view endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(editZone:SaveZone:)]) {
        if ([self.fieldName.text isEqualToString:@""]) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"nombre.zona.vacio", nil)  Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;

            return;
        }
        self.zone.titleCircle =self.fieldName.text;
        self.fieldName.text=@"";
   if (self.isEdtiting) {
       
            [SecurityZonesServices updateSecurityZoneWithLatitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.longitude] Radius:[NSString stringWithFormat:@"%f",self.zone.radius] Name:self.zone.titleCircle IdZone:self.zone.idZone Wait:YES Delegate:self];
            
        }
        else{
            [SecurityZonesServices createSecurityZoneWithName:self.zone.titleCircle CenterLatitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.latitude] CenterLongitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.longitude] Radius:[NSString stringWithFormat:@"%f",self.zone.radius] Wait:YES Delegate:self];
        }
    }
  
}


- (IBAction)cancelZone:(id)sender {
    [self.view removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(editZone:CancelZone:)]) {
        self.tmpZone.zoneType= self.zone.zoneType;
        
        if (self.isEdtiting) {
            [self.delegate editZone:self CancelZone:self.tmpZone];
        } else {
            [self.delegate editZone:self CancelZone:nil];
        }
        [self.view removeFromSuperview];
    }
    self.tmpZone = nil;
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.originalPosition;
    aRect.origin.y = aRect.origin.y - (kbSize.height-aRect.size.height)+31;
    self.view.frame = aRect;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.view.frame = self.originalPosition;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kCREATE_SECURITY_ZONE:
            dictionary = [dictionary objectForKey:@"CreateSecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               self.zone.idZone= [[dictionary objectForKey:@"ID"] stringValue];
               
                SecurityZone* zoneSave = [SecurityZone createObject];
                zoneSave.name= self.zone.titleCircle;
                NSString* radius =[NSString  stringWithFormat:@"%f", self.zone.radius];
                zoneSave.radius= [NSNumber numberWithDouble:[radius doubleValue]];
                NSString* latitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.latitude];
                zoneSave.latitude=[NSNumber numberWithDouble:[latitude doubleValue]];
                 NSString* longitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.longitude];
                zoneSave.longitude= [NSNumber numberWithDouble:[longitude doubleValue]];
                zoneSave.securitiZoneId= self.zone.idZone;
                
                [zoneSave saveObject];
                self.tmpZone = nil;
                [self.delegate editZone:self SaveZone:self.zone];
                
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            if(responseCode==410){
                //
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"not.premium.version", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                [self.delegate editZone:self CancelZone:self.zone];
            }
            if(responseCode==411){
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"premium.version.expired", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                [self.delegate editZone:self CancelZone:self.zone];
            }

            break;
        case kUPDATE_SECURITY_ZONE:
            dictionary = [dictionary objectForKey:@"UpdateSecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               
                SecurityZone *zoneSave = [[SecurityZone findWithZoneId:self.zone.idZone]objectAtIndex:0];
                zoneSave.name= self.zone.titleCircle;
                NSString* radius =[NSString  stringWithFormat:@"%f", self.zone.radius];
                zoneSave.radius= [NSNumber numberWithDouble:[radius doubleValue]];
                NSString* latitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.latitude];
                zoneSave.latitude=[NSNumber numberWithDouble:[latitude doubleValue]];
                NSString* longitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.longitude];
                zoneSave.longitude= [NSNumber numberWithDouble:[longitude doubleValue]];
                zoneSave.securitiZoneId= self.zone.idZone;
                [zoneSave mergeObject];
                self.tmpZone = nil;
                [self.delegate editZone:self SaveZone:self.zone];
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
            
        default:
            break;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
   
        if ([textField.text length]<=59)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:59];
        }
        
        return NO;
   
        
        
   
    
}



@end
