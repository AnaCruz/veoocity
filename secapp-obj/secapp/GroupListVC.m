//
//  GroupListVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "GroupListVC.h"
#import "SimpleTableViewCell.h"
#import "EditGroupVC.h"
#import "MessagesService.h"
#import "ChatContainerVC.h"
#import "AddFriendsToChatGroupVC.h"
#import "EditChatGroupExistingVC.h"
#import "ConvesationGroupVC.h"
#import "Message.h"
#import "AlertView.h"
#import "AppDelegate.h"
@interface GroupListVC () <ResponseDelegate, UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *chatGroupListArray;
@property int indexTemp;
@property Message *chatGroupToDelete;
@property NSString * groupIdTemp;
@end

@implementation GroupListVC

- (void)viewDidLoad {
    [super viewDidLoad];

    
       // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
[super viewDidAppear:animated];
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.chatGroupListArray = [[NSMutableArray alloc] init];
    [MessagesService getChatGroupListWait:NO Delegate:self];


}
-(void)viewDidDisappear:(BOOL)animated{
   self.chatGroupListArray=nil;
}
-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kGET_CHATGROUPLIST:
            dictionary = [dictionary objectForKey:@"ChatGroupsResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayGroupList = [dictionary objectForKey:@"ChatGroupList"];
                self.chatGroupListArray = [arrayGroupList mutableCopy];
                [self.tableView reloadData];
           }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        case kDELETE_CHATGROUP:
            dictionary = [dictionary objectForKey:@"DeleteChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [self.chatGroupListArray removeObjectAtIndex:self.indexTemp];
                [self.chatGroupToDelete deleteObject];
                self.chatGroupToDelete=nil;
                 [self.tableView reloadData];
                [MessagesService getChatGroupListWait:NO Delegate:self];
               
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }

            break;

                default:
            break;
    }
}


- (IBAction)createNewGroup:(UIButton *)sender {
    if([[Friend findAll]count]<2){
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"numero.de.amigos.min", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        alert.delegate=self;
        return;
    }
    
 
    EditGroupVC * edit = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"AddValuesToGroupVC"];
    edit.isNew=YES;

    if (self.delegate && [self.delegate respondsToSelector:@selector(showViewController:)]) {
        [self.delegate showViewController:edit];
    }
}

#pragma mark UITableView Delegate and Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
     SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"GroupListCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    NSDictionary* data = [self.chatGroupListArray objectAtIndex:indexPath.row];
    cell.nameLabel.text = [data objectForKey:@"Name"];
    cell.buttonChat.tag= indexPath.row;
     cell.buttonEdit.tag= indexPath.row;
     cell.buttonDelete.tag= indexPath.row;
    [cell.buttonEdit addTarget:self action:@selector(buttonEditClicked:) forControlEvents:UIControlEventTouchUpInside];
     [cell.buttonDelete addTarget:self action:@selector(buttonDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
     [cell.buttonChat addTarget:self action:@selector(buttonChatClicked:) forControlEvents:UIControlEventTouchUpInside];
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        NSString *uri= [data objectForKey:@"Image"];
        if (uri && ![uri isEqual: [NSNull null]])  {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
                dispatch_async(dispatch_get_main_queue(), ^{
                  cell.profileImageView.image = [UIImage imageWithData:data];
                });
                
            });
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
               cell.profileImageView.image = [UIImage imageNamed:@"chat-29"];
            });
        }
    
    });
    
    return cell;
}

-(void)buttonEditClicked:(UIButton *)sender{
    NSDictionary * dic = [self.chatGroupListArray objectAtIndex:sender.tag];
    EditChatGroupExistingVC * edit = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"EditGrop"];
    edit.groupID=[dic objectForKey:@"IdChatGroup"];
    edit.nameGroup=[dic objectForKey:@"Name"];
    edit.imageGroup=[dic objectForKey:@"Image"];
//    [self.navigationController pushViewController:edit animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(showViewController:)]) {
        [self.delegate showViewController:edit];
    }


}
-(void)buttonDeleteClicked:(UIButton *)sender{
      NSDictionary * dic = [self.chatGroupListArray objectAtIndex:sender.tag];
    NSString * groupId= [dic objectForKey:@"IdChatGroup"];
    self.groupIdTemp=groupId;
    self.indexTemp=(int)sender.tag;
    self.chatGroupToDelete =[Message findWithGroupId:groupId];
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"desea.eliminar.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
    alert.delegate=self;
   
  
}
-(void)buttonPressed:(UIButton *)sender{
    if (sender.tag==1) {
        [MessagesService deleteChatGroupWithGroupID:self.groupIdTemp Wait:NO Delegate:self];
        self.groupIdTemp=nil;
    }

}
-(void)buttonChatClicked:(UIButton *)sender{
     //NSDictionary * dic = [self.chatGroupListArray objectAtIndex:sender.tag];
    ConvesationGroupVC * conversationGroup = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationGrupalVC"];
    conversationGroup.chatGroup=[self.chatGroupListArray objectAtIndex:sender.tag];
//   [self.navigationController pushViewController:conversationGroup animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(showViewController:)]) {
        [self.delegate showViewController:conversationGroup];
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.chatGroupListArray count];
}





@end
