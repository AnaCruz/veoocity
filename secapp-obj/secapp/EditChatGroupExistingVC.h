//
//  EditChatGroupExistingVC.h
//  secapp
//
//  Created by SecApp on 21/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EditChatGroupExistingVC : UIViewController
@property NSString * groupID;
@property NSString * nameGroup;
@property NSString * imageGroup;
@end
