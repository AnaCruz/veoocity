//
//  ConfirmNewGroupVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

//#import "ConfirmNewGroupVC.h"
//#import "Friend.h"
//#import "Group.h"
//#import "UserInGroupCell.h"
//#import "GroupService.h"
//#include <MessageUI/MessageUI.h>
//
//@interface ConfirmNewGroupVC () <UITableViewDelegate, UITableViewDataSource, ResponseDelegate, MFMessageComposeViewControllerDelegate>
//
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (weak, nonatomic) IBOutlet UILabel *nameGroup;
//@property NSArray* friends;
//@property NSArray* phones;
//@property NSArray* emails;
//@property NSString* groupId;
//@property NSArray* ids;
//@end
//
//@implementation ConfirmNewGroupVC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.groupId = @"";
//    if (!self.usersInGroupList) {
//        self.usersInGroupList = [NSArray array];
//    }
//    self.nameGroup.text = self.name;
//}
//
//- (IBAction)saveGroup:(id)sender {
//    if (!(self.usersInGroupList && [self.usersInGroupList count] > 0)) {
//        // TODO Alert Debe de tener al menos q contacto para crear el grupo
//        return;
//    }
//    NSMutableArray* tmpFriends = [NSMutableArray array];
//    NSMutableArray* tmpPhones = [NSMutableArray array];
//    NSMutableArray* tmpEmails = [NSMutableArray array];
//    
//    // Separo por tipo la lista del usuarios en el grupo
//    
//    for (id friend in self.usersInGroupList) {
//        if ([friend isKindOfClass:Friend.class]) {
//            [tmpFriends addObject:friend];
//        } else {
//            NSDictionary* data = friend;
//            if ([[data objectForKey:@"type"] intValue] == 1) {
//                [tmpEmails addObject:friend];
//            } else {
//                [tmpPhones addObject:friend];
//            }
//        }
//    }
//    
//    // Los guardo en memoria
//    self.friends = [tmpFriends copy];
//    self.phones = [tmpPhones copy];
//    self.emails = [tmpEmails copy];
//    
//    // Filtro los datos para guararlos en su tipo y eliminar el diccionario de datos (emails y telefonos).
//    [self filterData];
//    
//    // Crear Grupo y empieza a guardar los contactos
//    [GroupService createGroupWithGroupName:self.name Wait:YES Delegate:self];
//}
//
//- (void) filterData {
//    NSMutableArray* tmpFriends = [NSMutableArray array];
//    for (Friend* friend in self.friends) {
//        [tmpFriends addObject:friend.userId];
//    }
//    // Emails
//    NSMutableArray* tmpEmails = [NSMutableArray array];
//    for (NSDictionary* friend in self.emails) {
//        if ([[friend objectForKey:@"type"] intValue] == 1) {
//            [tmpEmails addObject:[friend objectForKey:@"data"]];
//        }
//    }
//    // Phones
//    NSMutableArray* tmpPhones = [NSMutableArray array];
//    for (NSDictionary* friend in self.phones) {
//        if ([[friend objectForKey:@"type"] intValue] == 2) {
//            [tmpPhones addObject:[friend objectForKey:@"data"]];
//        }
//    }
//    self.friends = [tmpFriends copy];
//    self.phones = [tmpPhones copy];
//    self.emails = [tmpEmails copy];
//}
//
//- (IBAction)backToView:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//#pragma mark - Table delegete and datasource
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return [self.usersInGroupList count];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString* identifier = @"UserInGroup";
//    UserInGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UserInGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    id friend = [self.usersInGroupList objectAtIndex:indexPath.row];
//    if ([friend isKindOfClass:Friend.class]) {
//        Friend *data = friend;
//        cell.nameUser.text = [NSString stringWithFormat:@"%@ %@", data.name, data.lastName];
//        cell.pictureUser.image = [UIImage imageWithData:data.pictureProfile];
//    } else {
//        NSDictionary *data = friend;
//        cell.nameUser.text = [data objectForKey:@"data"];
//    }
//    cell.pictureUser.clipsToBounds = YES;
//    cell.pictureUser.layer.cornerRadius = 22;
//    cell.pictureUser.layer.borderWidth = 3;
//    cell.pictureUser.layer.borderColor = [[UIColor whiteColor] CGColor];
//    cell.deleteUser.tag = indexPath.row;
//    [cell.deleteUser addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
//    return cell;
//}
//
//-(void) deleteUser:(UIButton*) sender {
//    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    
//    id friend = [self.usersInGroupList objectAtIndex:indexPath.row];
//    NSMutableArray* usersInGroup = [self.usersInGroupList mutableCopy];
//    
//    [usersInGroup removeObject:friend];
//    self.usersInGroupList = [usersInGroup copy];
//    
//    [indexPathsToDelete addObject:indexPath];
//    
//    [self.tableView beginUpdates];
//    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView endUpdates];
//}
//
//- (void)successResponseWithService:(kService)service Response:(id)response {
//    NSError *parsingError = nil;
//    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
//    if (parsingError) {
//        NSLog(@"Error: %@", parsingError.description);
//        return;
//    }
//    int respondeCode = -1;
//    switch (service) {
//        case kCREATE_GROUP:
//            dictionary = [dictionary objectForKey:@"CreateGroupResult"];
//            respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (respondeCode == 200) {
//                self.groupId =[dictionary objectForKey:@"ID"];
//                //guardar grupos
//                Group *group = [Group createObject];
//                group.name = self.name;
//                group.groupId = [NSString stringWithFormat:@"%@",self.groupId];
//                [group saveObject];
////                for (Friend* friend in self.usersInGroupList) {
////                    [group addFriendsObject:friend];
////                    [friend mergeObject];
////                    [group mergeObject];
////                }
//
//                [self saveContacts];
//            }
//            break;
//        case kINVITATION_EXISTS_USERS:
//            dictionary = [dictionary objectForKey:@"SendInvitationExistsUsersResult"];
//            respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (respondeCode == 200) {
//                // Emails
//                if ([self.emails count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.groupId Data:self.emails IsPhone:NO Wait:YES Delegate:self Extra:nil];
//                } else if ([self.phones count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.groupId Data:self.phones IsPhone:NO Wait:YES Delegate:self Extra:self.phones];
//                } else {
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//            }
//            break;
//        case kINVITATION:
//            dictionary = [dictionary objectForKey:@"SendInvitationResult"];
//            respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (respondeCode == 200) {
//                // Phones
//                if ([self.phones count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.groupId Data:self.phones IsPhone:NO Wait:YES Delegate:self Extra:self.phones];
//                } else {
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//            }
//            break;
//        default:
//            break;
//    }
//}
//
//-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
//    NSError *parsingError = nil;
//    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
//    if (parsingError) {
//        NSLog(@"Error: %@", parsingError.description);
//        return;
//    }
//    int respondeCode = -1;
//    switch (service) {
//        case kINVITATION:
//            dictionary = [dictionary objectForKey:@"SendInvitationResult"];
//            respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (respondeCode == 200) {
//                NSMutableArray* ids = [NSMutableArray array];
//                for (NSDictionary* data in (NSArray*)[dictionary objectForKey:@"Ids"]) {
//                    [ids addObject:[data objectForKey:@"Id"]];
//                }
//                self.ids = [ids copy];
//                [self sendMessages];
//            }
//            break;
//        default:
//            break;
//    }
//}
//
//- (void) saveContacts {
//    if ([self.friends count] > 0) {
//        // Friends
//        [GroupService sendInvitationExistsUsersWithGroupId:self.groupId UsersId:self.friends Wait:YES Delegate:self];
//    } else if ([self.emails count] > 0) {
//        // Emails
//        [GroupService sendInvitationWithGroupId:self.groupId Data:self.emails IsPhone:NO Wait:YES Delegate:self Extra:nil];
//    } else if ([self.phones count]>0) {
//        // Phones
//        [GroupService sendInvitationWithGroupId:self.groupId Data:self.phones IsPhone:NO Wait:YES Delegate:self Extra:self.phones];
//    }
//}
//
//- (void) sendMessages {
//    if(![MFMessageComposeViewController canSendText]) {
//        return;
//    }
//    NSMutableArray* tmpPhones = [self.phones mutableCopy];
//    NSMutableArray* tmpIds = [self.ids mutableCopy];
//    
//    NSArray *recipents = @[[tmpPhones objectAtIndex:0]];
//    NSString *message = [NSString stringWithFormat:@"Baja SecApp aqui http://goo.gl/0XM8ew y unete a mi red de seguridad ingresando el siguiente código: %@", [tmpIds objectAtIndex:0]];
//    
//    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
//    messageController.messageComposeDelegate = self;
//    [messageController setRecipients:recipents];
//    [messageController setBody:message];
// 
//    [tmpPhones removeObject:[tmpPhones objectAtIndex:0]];
//    [tmpIds removeObject:[tmpIds objectAtIndex:0]];
//    
//    self.phones = [tmpPhones copy];
//    self.ids = [tmpIds copy];
//    
//    // Present message view controller on screen
//    [self presentViewController:messageController animated:YES completion:nil];
//    
//}
//
//- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
//    switch (result) {
//        case MessageComposeResultCancelled:
//            break;
//        case MessageComposeResultFailed:
//            break;
//        case MessageComposeResultSent:
//            break;
//        default:
//            break;
//    }
//    [controller dismissViewControllerAnimated:YES completion:^{
//        if ([self.phones count] > 0 && [self.ids count] > 0) {
//            [self sendMessages];
//        } else {
//            [self.navigationController popToRootViewControllerAnimated:YES];
//        }
//    }];
//}
//
//@end
