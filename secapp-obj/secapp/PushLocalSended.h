//
//  PushLocalSended.h
//  secapp
//
//  Created by SecApp on 07/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@interface PushLocalSended : GenericObject
@property (nonatomic, retain) NSDate * dateSend; //shipping date
@property (nonatomic, retain) NSString * distance; //Distance between the user and the incidence
@property (nonatomic, retain) NSString * occId;

@end
@interface PushLocalSended (CoreDataGeneratedAccessors)
/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;
/**
 * Find a Push local sended across a occurrenceId in the data base.
 * @param occurrenceId Occurrence Identifier.
 * @return array Array with occurrence data.
 */
+ (NSArray*) findWithPushOccurrenceId:(NSString*) occurrenceId;
/**
 * Find all occurrrences reported in the data base
 * @return array Array with ocurrences data.
 */
+ (NSArray*) findAll;
@end