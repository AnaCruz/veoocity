//
//  UserNotifications.h
//  secapp
//
//  Created by SecApp on 12/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Service.h"

@interface UserNotifications : Service

/**
 * Get user notifications using user ID and session ID.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getNotificationsWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

+ (void) deleteNotification:(NSString*) notificationId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra;
+ (void) deleteNotificationWith:(NSString*) notificationId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+ (void) deleteAllNotificationWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
@end
