//
//  FriendsListVC.m
//  secapp
//
//  Created by SecApp on 14/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "FriendsListVC.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "ContactService.h"
#import "Friend.h"
#import "AppDelegate.h"
#import "Service.h"
#import "AddNewFriendsVC.h"
#import "AlertViewSingleInput.h"
#import "AlertView.h"
#import "AppDelegate.h"
#import "GroupService.h"
#import "MapLocationVC.h"
#import "ContainerVC.h"

@interface FriendsListVC () <ResponseDelegate, UINavigationControllerDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextField *searchField;
@property BOOL isSearch;
@property NSMutableArray *contactsArray;
@property NSArray *sortedArray;
@property NSNumber * x;
@property NSIndexPath * tmpIndexPath;
@property  NSMutableArray *indexPathsToDelete;
@property  NSMutableArray *indexPathsToAdd;
@property NSString * tmpUserID;
@property MapLocationVC* mapLocationView;



@end

@implementation FriendsListVC
-(void)viewWillAppear:(BOOL)animated{
    
    self.isSearch = NO;
    self.contactsArray = [[Friend findAll] mutableCopy];
    if (!self.contactsArray) {
        self.contactsArray=[NSMutableArray array];
    }
    self.sortedArray = [[NSMutableArray alloc] init];
    self.searchField = [[UITextField alloc] init];
    self.searchField.delegate = self;
    [self.view addSubview: self.searchField];
    
    [self.tableView reloadData];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    self.contactsArray = nil;
    self.isSearch = NO;
    self.searchField.text=@"";
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.mapLocationView = [[UIStoryboard storyboardWithName:@"Map" bundle:nil] instantiateViewControllerWithIdentifier:@"MapLocationVC"];
    
}
- (IBAction)textFieldDidChange:(UITextField *)sender {
    NSString * match = sender.text;
    if ([match isEqualToString:@""]) {
        self.isSearch = NO;
        [self.tableView reloadData];
        return;
    }
    NSArray *listFiles = [[NSMutableArray alloc]  init];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:
                             @"name CONTAINS[cd] %@", match];
    listFiles = [NSMutableArray arrayWithArray:[self.contactsArray
                                                filteredArrayUsingPredicate:predicate]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"name"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.sortedArray = [listFiles sortedArrayUsingDescriptors:sortDescriptors];
    self.isSearch = YES;
    [self.tableView reloadData];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Friend * friend;
    if (self.isSearch) {
        friend =[self.sortedArray objectAtIndex:indexPath.row];
       
    } else {
        friend =[self.contactsArray objectAtIndex:indexPath.row];
   }
}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) {
        return [self.sortedArray count];
    }
    return [self.contactsArray count];
    }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ShowFriendsList"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ShowFriendsList" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (self.isSearch) {
        Friend *friendsSearch =[self.sortedArray objectAtIndex:indexPath.row];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",friendsSearch.name, friendsSearch.lastName];
        cell.buttonDelete.tag= [friendsSearch.userId integerValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            if([friendsSearch.pictureProfile isEqual:@""]|| friendsSearch.pictureProfile==nil){
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                cell.profileImageView.image = [UIImage imageWithData:friendsSearch.pictureProfile];
                }
         });
        if ([friendsSearch.permissions boolValue]) {
            cell.buttonWithoutPermissions.hidden=YES;
            cell.buttonWithPermissions.hidden=NO;
        }
        else{
            cell.buttonWithPermissions.hidden=YES;
            cell.buttonWithoutPermissions.hidden=NO;
        }
        cell.buttonWithPermissions.tag= [friendsSearch.userId integerValue];
        cell.buttonWithoutPermissions.tag= [friendsSearch.userId integerValue];
        [cell.buttonDelete addTarget:self action:@selector(buttonDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.buttonWithoutPermissions addTarget:self action:@selector(buttonWithoutPermissionClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.buttonWithPermissions addTarget:self action:@selector(buttonWithPermissionClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    Friend * friends=[self.contactsArray objectAtIndex:indexPath.row];
    
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",friends.name, friends.lastName];
    
    if ([friends.permissions boolValue]) {
        cell.buttonWithoutPermissions.hidden=YES;
        cell.buttonWithPermissions.hidden=NO;
       
    }
    else{
        cell.buttonWithPermissions.hidden=YES;
        cell.buttonWithoutPermissions.hidden=NO;
        
    }
     cell.buttonWithPermissions.tag= [friends.userId integerValue];
     cell.buttonWithoutPermissions.tag= [friends.userId integerValue];
    
   cell.buttonDelete.tag= [friends.userId integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        if([friends.pictureProfile isEqual:@""]|| friends.pictureProfile==nil){
            
            cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
        }
        else{
            cell.profileImageView.image = [UIImage imageWithData:friends.pictureProfile];
            }
        
    });
    
    [cell.buttonDelete addTarget:self action:@selector(buttonDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
      [cell.buttonWithoutPermissions addTarget:self action:@selector(buttonWithoutPermissionClicked:) forControlEvents:UIControlEventTouchUpInside];
      [cell.buttonWithPermissions addTarget:self action:@selector(buttonWithPermissionClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)buttonDeleteClicked:(UIButton *)sender{
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"eliminar.contacto.definitivo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
    alert.delegate=self;
    self.tmpUserID= [NSString stringWithFormat:@"%ld",(long)sender.tag];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.tmpIndexPath =  [self.tableView indexPathForRowAtPoint:buttonPosition];
    //[GroupService removeUserFromGroupWithUserIdRemove:self.tmpUserID Wait:YES Delegate:self];
    
  //delete user from contact list, update map
}
-(void)buttonPressed:(UIButton *)sender{
    if (sender.tag==1) {
       [GroupService removeUserFromGroupWithUserIdRemove:self.tmpUserID Wait:YES Delegate:self];
         }
    else{
        self.tmpIndexPath=nil;
        self.tmpUserID=nil;
    }
    
}

-(void)buttonWithPermissionClicked:(UIButton *)sender{
    //remove permissions
      self.x=@(0);
   self.tmpUserID= [NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
     self.tmpIndexPath =  [self.tableView indexPathForRowAtPoint:buttonPosition];
   
    [ContactService changePermissionStatusWithContactId:self.tmpUserID AndPermission:NO Wait:YES Delegate:self];
    
    
   
    
}
-(void)buttonWithoutPermissionClicked:(UIButton *)sender{
    // add permissions
    
     self.x=@(1);
    self.tmpUserID= [NSString stringWithFormat:@"%ld",(long)sender.tag];
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.tmpIndexPath =  [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    [ContactService changePermissionStatusWithContactId:self.tmpUserID AndPermission:YES Wait:YES Delegate:self];

}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
       [self.view endEditing:YES];
}

- (IBAction)addNewFriends:(id)sender {
        //enviar a nueva vista AddNewFriends
    [self.view endEditing:YES];
    AddNewFriendsVC* add = [[UIStoryboard storyboardWithName:@"AddFriends" bundle:nil] instantiateViewControllerWithIdentifier:@"AddNewFriends"];
       [self.navigationController pushViewController:add animated:YES];

}

- (IBAction)insertInvitationCode:(id)sender {
    //ingresar codigo de invitacion
    AlertViewSingleInput* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertViewSingleInput"];
    alert.textField.keyboardType = UIKeyboardTypeNumberPad;
    
    [alert.textField setKeyboardType:UIKeyboardTypeNumberPad];
   __block FriendsListVC* bSelsf = self;
    alert.callbackOk =  ^(NSString* text){
        if (![text isEqualToString:@""]) {
            [GroupService addUserToGroupWithAccessCode:text WithWait:YES Delegate:bSelsf];
        }
    };
    [alert showWithTitle:NSLocalizedString(@"codigo.invitacion", nil) Message:NSLocalizedString(@"introduce.codigo.invitación", nil) CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];

}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    
    switch (service) {
        case kADD_USER_TO_GROUP:
            dictionary = [dictionary objectForKey:@"AddUserToGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                //alert usuario agregado correctamente
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"agregado.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.container updateContacts];
                    NSArray *arra=[Friend findAll];
                    [self.container showUsersCount:(int)[arra count]];
                });
                
                //actualizar mapa y numero de usuarios
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            if (responseCode == 407) {
                //el codigo de acceso esta caducado o ya fue usado
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"codigo.invalido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                
            }
            break;
        case kCHANGE_PERMISSIONS:
            dictionary = [dictionary objectForKey:@"ChangeContactPermissionResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                //permiso cambiado
                Friend * friend = [[Friend findWithUserId:self.tmpUserID] objectAtIndex:0];
                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
                NSMutableArray *indexPathsToAdd = [NSMutableArray new];
                if (!self.tmpIndexPath) {
                    return;
                }
                [indexPathsToDelete addObject:self.tmpIndexPath];
                [indexPathsToAdd addObject:self.tmpIndexPath];
                if (self.isSearch) {
                    NSMutableArray *arrayTmp= [self.sortedArray mutableCopy];
                    friend.permissions=self.x;
                    [friend mergeObject];
                    int y =0;
                    for (Friend *friendB in arrayTmp) {
                        if ([friendB isEqual:friend]) {
                            break;
                        }
                        y++;
                    }
                    [arrayTmp replaceObjectAtIndex:y withObject:friend];
                    self.sortedArray = [arrayTmp copy];
                }else{
                    
                    NSMutableArray *arrayTmp= [self.contactsArray mutableCopy];
                    friend.permissions=self.x;
                    [friend mergeObject];
                    int y =0;
                    for (Friend *friendB in arrayTmp) {
                        if ([friendB isEqual:friend]) {
                            break;
                        }
                        y++;
                    }
                    [arrayTmp replaceObjectAtIndex:y withObject:friend];
                    
                    self.contactsArray = [arrayTmp copy];
                }
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
                 [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView endUpdates];
                self.x=nil;
                self.tmpUserID=nil;
                self.tmpIndexPath=nil;
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            
            break;
        case kREMOVE_USER_FROM_GROUP:
            dictionary = [dictionary objectForKey:@"RemoveUserFromGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
              
                Friend * friend = [[Friend findWithUserId:self.tmpUserID] objectAtIndex:0];
                [friend deleteObject];
                NSArray * array2 = [Friend findAll];
                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
                
                [indexPathsToDelete addObject:self.tmpIndexPath];
                if (self.isSearch) {
                    NSMutableArray *arrayTmp= [self.sortedArray mutableCopy];
                    [arrayTmp removeObject:friend];
                    self.sortedArray = [arrayTmp copy];
                }else{
                    
                    NSMutableArray *arrayTmp= [self.contactsArray mutableCopy];
                    [arrayTmp removeObject:friend];
                    self.contactsArray = [arrayTmp copy];
                }
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView endUpdates];
                self.tmpUserID=nil;
                self.tmpIndexPath=nil;
                
                dispatch_async(dispatch_get_main_queue(), ^{
                [self.container updateContacts];
                [self.container showUsersCount:(int)[array2 count]];
                    });

            }
            if (responseCode==403) {
                           }
            break;
            
            
        default:
            break;
    }
}


@end
