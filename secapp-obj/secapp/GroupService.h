//
//  GroupService.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 06/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Service.h"

@interface GroupService : Service

/**
 * Create Group
 {
    "CreateGroupResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int),
        "ID":Id group was created (long)
    }
 }
 * @param groupName Name of a group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) createGroupWithGroupName:(NSString*) groupName Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Update the name of a group.
 {
    "UpdateGroupNameResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param gruopId id of a group.
 * @param groupName Name of a group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) updateGroupWithGroupId:(NSString*) groupId GroupName:(NSString*) groupName Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Add user to Group
 {
    "AddUserToGroupResult": {
        "ErrorMessage":"Mensaje de error",
        "ResponseCode":(int,codigo de error)
    }
 }
 * @param userId Id of the user to add.
 * @param accessCode Code of the group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra;
/**
 * Remove User From Group
 {
    "RemoveUserFromGroupResult": {
        "ErrorMessage": "Error message if needed",
        "ResponseCode": (int)
    }
 }
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) removeUserFromGroupWithUserIdRemove:(NSString*) userIdRemove Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * User Groups
 {
    "GroupsByUserResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int),
        "GroupList":[
            {
                "GroupId":"Group Id in long (string)",
                "GroupName":"Group Name (string)"
            },...
        ]
 }
 * @param type Type of groups, the direferent types are: 1. Groups where user is a guest 2. Groups where user is administrator but is not Owner 3. Groups where user is Owner
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) groupsByUserWithType:(int) type Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Users in Group
 * @param groupId Group id.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 * @param extra Object to recive in response (preferens type "Group").
 */
+ (void) UsersInGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra;

/**
 * Add Administrator To Group
 {
    "AddAdministratorToGroupResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param userId Id of the user to set as administrator.
 * @param groupId Id of the group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) addAddministrator:(NSString*) userId ToGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Remove Administrator from Group
 {
    "RemoveAdministratorFromGroupResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param userId Id of the user to remove as administrator.
 * @param groupId Id of the group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) removeAddministrator:(NSString*) userId FromGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Change Ownership
 {
    "ChangeOwnershipResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param userId Id of the user to set as Ownership.
 * @param groupId Id of the group.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) changeOwnership:(NSString*) userId ToGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/*
 * Allow leave a group
 {
    "LeaveGroupResult": {
        "ErrorMessage": "Error message if needed",
        "ResponseCode": (int)
    }
 }
 * @param groupId Id of the group to leave.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) leaveGroupsWithGroupId:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/*
 * Delete a group
 {
    "SendInvitationResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param groupId Id of the group to delete.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) deleteGroupsWithGroupId:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Send invitation a user for join to a group
 {
    "SendInvitationResult": {
        "ErrorMessage": "Empty if correct or if the client doesn't need message",
        "ResponseCode": (int)
    }
 }
 * @param groupId Id of the group to delete.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
//+ (void) sendInvitationWithGroupId:(NSString*) groupId Data:(NSArray*) data IsPhone:(BOOL) isPhone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra;

/**
 * Send invitations to a the users of secapp.
 */
//+ (void) sendInvitationExistsUsersWithGroupId:(NSString*) groupId UsersId:(NSArray*) usersArray Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

+ (void) sendInvitationWithData:(NSArray*) data IsPhone:(BOOL) isPhone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra;

+(void) advanceSearchContactsWithData:(NSString *)data Wait:(BOOL) wait Delegate: (id<ResponseDelegate>)delegate;
+ (void) sendInvitationExistsUsersWithUsersId:(NSArray*) usersArray Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

@end
