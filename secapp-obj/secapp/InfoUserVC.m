//
//  InfoUserVC.m
//  secapp
//
//  Created by SecApp on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "InfoUserVC.h"
#import "CustomAnnotation.h"
#import "ContainerVC.h"
#import "MapLocationVC.h"
#import "Friend.h"
#import "Profile.h"
#import "Service.h"
#import "ConversationVC.h"
#import "Email.h"
#import "Phone.h"
#import "StringUtils.h"

@interface InfoUserVC() <UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property UIView* mainView;
@property (weak, nonatomic) IBOutlet UILabel *battery;
@property (weak, nonatomic) IBOutlet UIImageView *batteryImage;
@property (weak, nonatomic) IBOutlet UIButton *messajesButton;
@property (weak, nonatomic) IBOutlet UIButton *callButton;

@property (weak, nonatomic) IBOutlet UILabel *ubicacionActual;
@property UIViewController* controller;
@property ContainerVC * contView;
@property int languaje;
@property (weak, nonatomic) IBOutlet UILabel *lastConection;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;


@end

@implementation InfoUserVC
-(void)viewWillAppear:(BOOL)animated{
     [self setUser];
    
}

-(void) viewDidLoad{
    [super viewDidLoad];
       self.languaje=[[Service getLanguaje]intValue];
    self.contView = (ContainerVC *)self.controller;

}

- (IBAction)closeView:(id)sender {
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    [self.mainView addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        self.isVisible = NO;
        [self.view removeFromSuperview];
    }];
    self.me=nil;
    self.user=nil;
    self.friendSelected=nil;
}
- (IBAction)message:(id)sender {
    if(self.friendSelected){
 [((ContainerVC*)self.parentViewController) showView:self.friendSelected.userId IsMonitor:NO];
    [self closeView:nil];
    }
    
    
}
- (IBAction)call:(UIButton *)sender {
    if(self.friendSelected){
       Friend * friend= self.friendSelected;
            if (friend && [[friend.phones allObjects] count] > 0) {
            Phone *phone = [[friend.phones allObjects] objectAtIndex:0];
            NSString* phoneCallNum = [NSString stringWithFormat:@"tel://%@",phone.phone];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
               
        }
    
    }
    
    

}


- (void) setUser{
   if (self.me) {
        dispatch_async(dispatch_get_main_queue(), ^{
//            UIFont *arialBIFont = [UIFont fontWithName:@"Arial-BoldItalicMT" size:16.0];
//            NSDictionary *arialBIDict = [NSDictionary dictionaryWithObject: arialBIFont forKey:NSFontAttributeName];
//            NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:self.me.name attributes: arialBIDict];
//            self.userName.attributedText= aBIAttrString;
//            self.battery.text= [NSString stringWithFormat:@"%@%%", [[Service getBatteryLevel] stringValue]];
//            int batteryInt = [[Service getBatteryLevel] intValue];
//            if(batteryInt<6) {
//                self.batteryImage.image = [UIImage imageNamed:@"bateria4"];
//            }else if(batteryInt<38){
//                self.batteryImage.image = [UIImage imageNamed:@"bateria3"];
//            }else if (batteryInt<70){
//                self.batteryImage.image = [UIImage imageNamed:@"bateria2"];
//            }else if (batteryInt<=100){
//                self.batteryImage.image = [UIImage imageNamed:@"bateria1"];
//            }
            UIFont *arialBIFont = [UIFont fontWithName:@"Arial-BoldItalicMT" size:18.0];
            NSDictionary *arialBIDict = [NSDictionary dictionaryWithObject: arialBIFont forKey:NSFontAttributeName];
          
            if(self.languaje==1){
            NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:@"Ubicación actual" attributes: arialBIDict];
                 self.ubicacionActual.attributedText=aBIAttrString;
            }else{
            NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:@"Current location" attributes: arialBIDict];
                self.ubicacionActual.attributedText=aBIAttrString;
            }
            
            self.ubicacionActual.hidden=NO;
             // self.ubicacionActual.attributedText=aBIAttrString;
            self.userName.hidden=YES;
            self.battery.hidden=YES;
            self.batteryImage.hidden=YES;
            self.messajesButton.hidden=YES;
            self.callButton.hidden=YES;
            self.lastConection.hidden=YES;
            self.emailLabel.hidden=YES;
            
        });
        
   }
   else{
       
        if(self.user){
               self.friendSelected= self.user.friendInMap;
        }
       if(self.friendSelected){
        self.userName.hidden=NO;
        self.ubicacionActual.hidden=YES;
        self.battery.hidden=NO;
        self.batteryImage.hidden=NO;
        self.messajesButton.hidden=NO;
        self.callButton.hidden=NO;
           self.emailLabel.hidden=NO;
           
           
       self.lastConection.hidden=NO;
          
               if(self.languaje==1){
                   self.lastConection.text=[NSString stringWithFormat:@"Conectado desde: %@", [StringUtils formattedDateWithDate:self.friendSelected.positionDate]];
                 
               }else{
                   self.lastConection.text=[NSString stringWithFormat:@"Connect since: %@", [StringUtils formattedDateWithDate:self.friendSelected.positionDate]];
               }

        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *statusString;
            switch ([self.friendSelected.status intValue]) {
                case 0:
                    
                    if(self.languaje==1){
                    statusString = @"Todo bien";
                    }else{
                    statusString = @"It´s ok";
                    }
                    break;
                case 1:
                    
                    if(self.languaje==1){
                        statusString = @"Esta en Alerta!";
                    }else{
                        statusString = @"Is in danger!";
                    }
                    break;
                case 2:
                   
                    if(self.languaje==1){
                        statusString = @"Con reportes sin responder";
                    }else{
                        statusString = @"With reports unanswered";
                    }
                    break;
                case 4:
                    
                    if(self.languaje==1){
                        statusString = @"GPS apagado";
                    }else{
                        statusString = @"GPS off";
                    }
                    break;
                default:
                    
                    break;
            }
            
            UIFont *arialBIFont = [UIFont fontWithName:@"Arial-BoldItalicMT" size:16.0];
            NSDictionary *arialBIDict = [NSDictionary dictionaryWithObject: arialBIFont forKey:NSFontAttributeName];
            NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ ", self.friendSelected.name] attributes: arialBIDict];
            
            UIFont *arialFont = [UIFont fontWithName:@"Arial-ItalicMT" size:16.0];
            NSDictionary *arialDict = [NSDictionary dictionaryWithObject:arialFont forKey:NSFontAttributeName];
            NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc]initWithString: statusString attributes:arialDict];
            if ([self.friendSelected.status intValue]==1) {
                [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.35 green:0.78 blue:0.82 alpha:1.0] range:(NSMakeRange([statusString length]-7, 7))];
            }
            
            [aBIAttrString appendAttributedString:aAttrString];
            self.userName.attributedText =aBIAttrString;
            self.battery.text= [NSString stringWithFormat:@"%@ %%", [self.friendSelected.batteryLevel stringValue]];
            int batteryInt = [self.friendSelected.batteryLevel intValue];
            NSArray *arrayMails= [self.friendSelected.emails allObjects];
           NSArray * arrayPhones = [self.friendSelected.phones allObjects];
          if ([arrayMails count]>0&&[arrayPhones count]) {
               NSString *email = ((Email*)[arrayMails objectAtIndex:0]).email;
                 NSString *phone=((Phone*)[arrayPhones objectAtIndex:0]).phone;
               if (self.languaje==1) {
                   self.emailLabel.text =[NSString stringWithFormat:@"%@\rTel:%@",email, phone];
               }else{
                   self.emailLabel.text =[NSString stringWithFormat:@"%@\rPhone:%@",email, phone];
               }
           }
            
            if(batteryInt<6){
                self.batteryImage.image = [UIImage imageNamed:@"bateria4"];
            }else if(batteryInt<38){
                self.batteryImage.image = [UIImage imageNamed:@"bateria3"];
            }else if (batteryInt<70){
                self.batteryImage.image = [UIImage imageNamed:@"bateria2"];
            }else if (batteryInt<=100){
                self.batteryImage.image = [UIImage imageNamed:@"bateria1"];
            }
        });
        }
    }
}

- (void) drawInView:(UIView*) view {

    int height = 150;
    self.mainView=view;
    CGRect frame = CGRectMake(0, view.frame.size.height, view.frame.size.width, height);
    self.view.frame = frame;
    frame.origin.y -= height;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        self.isVisible = YES;
    }];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    self.isSearch=NO;
}
//- (NSString*) formattedDate:(NSDate*) dateM{
//    // NSCalendar* calendar = [NSCalendar currentCalendar];
//    NSDate* now = [NSDate date];
//    NSLog(@"now %@", now);
//    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    
//    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit fromDate:now toDate:dateM options:0];
//    int totalDays = (int)components.day;
////    int differenceInDays =
////    [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:dateM] -
////    [calendar ordinalityOfUnit:NSDayCalendarUnit inUnit:NSEraCalendarUnit forDate:now];
//    NSDateFormatter *format = [[NSDateFormatter alloc] init];
//   [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
////    NSString *createdDateStr = [format stringFromDate:dateM];
////    NSDate *formattedDate = [format dateFromString:createdDateStr];
//    NSString *dateStr = @"";
//    switch (totalDays) {
//        case -1:
//            //[format setDateFormat:@"hh:mm a"];
//            [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            if(self.languaje ==1){
//                
//                NSLog(@"dateString: %@  dateM: %@",dateStr, dateM);
//                dateStr = [NSString stringWithFormat:@"Ayer %@",[format stringFromDate:dateM]];
//            }else{
//                 NSLog(@"dateString: %@  dateM: %@",dateStr, dateM);
//                dateStr = [NSString stringWithFormat:@"Yesterday %@",[format stringFromDate:dateM]];
//            }
//            break;
//        case 0:
//            //[format setDateFormat:@"hh:mm a"];
//           [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//            if(self.languaje ==1){
//                 NSLog(@"dateString: %@  dateM: %@",dateStr, dateM);
//                dateStr = [NSString stringWithFormat:@"Hoy %@",[format stringFromDate:dateM]];
//            }else{
//                 NSLog(@"dateString: %@  dateM: %@",dateStr, dateM);
//                dateStr = [NSString stringWithFormat:@"Today %@",[format stringFromDate:dateM]];
//            }
//            
//            break;
//            
//        default: {
//            [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
////            if(self.languaje ==1){
////            [format setDateFormat:@"dd MMM yyyy, hh:mm a"];
////            }else{
////            [format setDateFormat:@"MMM dd yyyy, hh:mm a"];
////            }
//            dateStr = [format stringFromDate:dateM];
//            break;
//        }
//    }
//    return dateStr;
//}
@end
