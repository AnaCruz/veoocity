//
//  MapLocationVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import "InfoUserVC.h"
#import "MapSecApp.h"
#import "ConversationVC.h"
#import "OcurrenceVC.h"
#import "CreateOccurrence.h"
#import "DisplayOccurrenceVC.h"

@class Friend;
@class ContainerVC;
@class OcurrenceVC;
@class CreateOccurrence;
@class MapSecApp;

@interface MapLocationVC : UIViewController <CLLocationManagerDelegate>
@property InfoUserVC *infoU;
@property  (strong, nonatomic) MapSecApp *mapView;
@property ConversationVC * conver;
@property OcurrenceVC *occurrence;
@property CreateOccurrence * createOccView;
//@property DisplayOccurrenceVC * displayOccView;
@property BOOL creatingOccurrence;
@property BOOL wasOn;
@property ContainerVC * container;
@property NSString * typeOccurrence;
@property (nonatomic, copy) void (^updateContactsCount)(void);
@property DisplayOccurrenceVC * displayOccurrence;
/**
 * Show the user location on the map when the user start session.
 * @param userLocation User location according to GPS.
 */
- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation;

/**
 * Draw the annotations on the mapView according to the contact list.
 * Set the pin color according to the friend status. 
 * Green: user; Red: alert; Yellow: unanswered reports; Gray: GPS off.
 */
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation;

/**
 * Create and Add the annotations on the mapView. 
 * @param friend Object type Friend with all its properties.
 */
- (void) addFriendAnnotationWithLocation:(Friend *)friend;
-(void)updateContacts;
-(void)createOccurrence:(NSString *)type;
-(void)drawPinsOnMap;

@end
