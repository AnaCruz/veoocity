//
//  GroupService.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 06/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "GroupService.h"

@implementation GroupService

//+ (void) createGroupWithGroupName:(NSString*) groupName Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@CreateGroup", [super getUrl]];
//    NSDictionary* params = @{
//                            @"ManagerUserId":[super getUserId],
//                            @"GroupName":groupName
//                            };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_GROUP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}

//+ (void) updateGroupWithGroupId:(NSString*) groupId GroupName:(NSString*) groupName Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@UpdateGroupName", [super getUrl]];
//    NSDictionary* params = @{
//                             @"GroupId":groupId,
//                             @"Name":groupName,
//                             @"UserId":[super getUserId]
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_GROUP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}

//+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@AddUserToGroup", [super getUrl]];
//    NSDictionary* params = @{
//                             @"AccessCode":accessCode,
//                             @"UserId":[super getUserId]
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_USER_TO_GROUP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}
//
//+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra {
//    NSString* uri = [NSString stringWithFormat:@"%@AddUserToGroup", [super getUrl]];
//    NSDictionary* params = @{
//                             @"AccessCode":accessCode,
//                             @"UserId":[super getUserId]
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_USER_TO_GROUP Wait:wait Delegate:delegate];
//    if (extra) {
//        request.extra = extra;
//    }
//    request.body = paramsData;
//    [request execute];
//}

//+ (void) removeUserFromGroupWithUserIdRemove:(NSString*) userIdRemove Groupid:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    // "/UserId(long)/UserIdToRemove(long)/GroupId(long)"
//    NSString* uri = [NSString stringWithFormat:@"%@RemoveUserFromGroup/%@/%@/%@", [super getUrl], [super getUserId], userIdRemove, groupId];
//    
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kREMOVE_USER_FROM_GROUP Wait:wait Delegate:delegate];
//    [request execute];
//}

//+ (void) groupsByUserWithType:(int) type Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    if (type < 1 || type > 3) {
//        NSLog(@"GroupService (ERROR: type not definded)");
//        return;
//    }
//    NSString* uri = [NSString stringWithFormat:@"%@GroupsByUser/%@/%d", [super getUrl], [super getUserId], type];
//    
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGROUP_BY_USER Wait:wait Delegate:delegate];
//    [request execute];
//}

+ (void) UsersInGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra {
    NSString* uri = [NSString stringWithFormat:@"%@UsersInGroup/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], groupId];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kUSERS_IN_GROUP Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    [request execute];
}

//+ (void) addAddministrator:(NSString*) userId ToGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate{
//    NSString* uri = [NSString stringWithFormat:@"%@AddAdministratorToGroup", [super getUrl]];
//    NSDictionary* params = @{
//                             @"UserId":[super getUserId],
//                             @"GroupId":groupId,
//                             @"UserIdToAdd":userId
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_ADMINISTRATOR_TO_GROUP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}

//+ (void) removeAddministrator:(NSString*) userId FromGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    // "/UserId(long)/UserIdToRemove(long)/GroupId(long)"
//    NSString* uri = [NSString stringWithFormat:@"%@RemoveAdministratorFromGroup", [super getUrl]];
//    NSDictionary* params = @{
//                             @"UserId":[super getUserId],
//                             @"GroupId":groupId,
//                             @"UserIdToRemove":userId
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kREMOVE_ADMINISTRATOR_FROM_GROUP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}

//+ (void) changeOwnership:(NSString*) userId ToGroup:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@ChangeOwnership", [super getUrl]];
//    NSDictionary* params = @{
//                             @"UserId":[super getUserId],
//                             @"GroupId":groupId,
//                             @"UserIdToOwner":userId
//                             };
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kCHANGE_OWNERSHIP Wait:wait Delegate:delegate];
//    request.body = paramsData;
//    [request execute];
//}
//
//+ (void) leaveGroupsWithGroupId:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@LeaveGroup/%@/%@", [super getUrl], [super getUserId], groupId];
//    
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kLEAVE_GROUP Wait:wait Delegate:delegate];
//    [request execute];
//}

//+ (void) deleteGroupsWithGroupId:(NSString*) groupId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
//    NSString* uri = [NSString stringWithFormat:@"%@DeleteGroup/%@/%@", [super getUrl], groupId, [super getUserId]];
//    
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_GROUP Wait:wait Delegate:delegate];
//    [request execute];
//}

//+ (void) sendInvitationWithGroupId:(NSString*) groupId Data:(NSArray*) data IsPhone:(BOOL) isPhone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra {
//    NSString* uri = [NSString stringWithFormat:@"%@SendInvitation", [super getUrl]];
//    NSDictionary* params = @{
//                             @"GroupId":groupId,
//                             @"Data":data,
//                             @"IsPhone":isPhone ? @"true": @"false",
//                             @"SessionId":[super getSessionId],
//                             @"UserId":[super getUserId]
//                             };
//    
//    NSData* paramsData = [super parsingWithObject:params];
//    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kINVITATION Wait:wait Delegate:delegate];
//    if (extra) {
//        request.extra = extra;
//    }
//    request.body = paramsData;
//    [request execute];
//}

/**
 * Send invitations to a the users of secapp.
 */
+ (void) sendInvitationExistsUsersWithGroupId:(NSString*) groupId UsersId:(NSArray*) usersArray Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SendInvitationExistsUsers", [super getUrl]];
    NSDictionary* params = @{
                            @"GroupId":groupId,
                            @"UserId":[super getUserId],
                            @"SessionId":[super getSessionId],
                            @"IdsToInvite":usersArray
                            };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kINVITATION_EXISTS_USERS Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@AddUserToGroup", [super getUrl]];
    NSDictionary* params = @{
                             @"AccessCode":accessCode,
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId]
                             };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_USER_TO_GROUP Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}

+ (void) addUserToGroupWithAccessCode:(NSString*) accessCode WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra {
    NSString* uri = [NSString stringWithFormat:@"%@AddUserToGroup", [super getUrl]];
    NSDictionary* params = @{
                             @"AccessCode":accessCode,
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId]
                             };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADD_USER_TO_GROUP Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    request.body = paramsData;
    [request execute];
}
+ (void) removeUserFromGroupWithUserIdRemove:(NSString*) userIdRemove Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    // "/UserId(long)/UserIdToRemove(long)/GroupId(long)"
    NSString* uri = [NSString stringWithFormat:@"%@RemoveUserFromGroup/%@/%@/%@", [super getUrl], [super getUserId], userIdRemove, [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kREMOVE_USER_FROM_GROUP Wait:wait Delegate:delegate];
    [request execute];
}
+ (void) sendInvitationWithData:(NSArray*) data IsPhone:(BOOL) isPhone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate Extra:(id) extra {
    NSString* uri = [NSString stringWithFormat:@"%@SendInvitation", [super getUrl]];
    NSDictionary* params = @{
                             
                             @"Data":data,
                             @"IsPhone":isPhone ? @"true": @"false",
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId]
                             };
    
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kINVITATION Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    request.body = paramsData;
    [request execute];
}
+(void) advanceSearchContactsWithData:(NSString *)data Wait:(BOOL) wait Delegate: (id<ResponseDelegate>)delegate{

    NSString* uri = [NSString stringWithFormat:@"%@AdvanceSearchContacts", [super getUrl]];
    NSDictionary* params = @{
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId],
                             @"Data":data,
                             };
    
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kADVANCE_SEARCH_CONTACTS Wait:wait Delegate:delegate];
   
    request.body = paramsData;
    [request execute];


}
+ (void) sendInvitationExistsUsersWithUsersId:(NSArray*) usersArray Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SendInvitationExistsUsers", [super getUrl]];
    NSDictionary* params = @{
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId],
                             @"IdsToInvite":usersArray
                             };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kINVITATION_EXISTS_USERS Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}


@end
