//
//  LocationUser.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 17/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "LocationUser.h"
#import "Service.h"
#import "ConnectionManagement.h"
#import "LocationManagement.h"
#import "Location.h"
#import "PositionsServices.h"
#import "AppDelegate.h"
#import "Profile.h"
#import  "Service.h"
#import "OccurenceServices.h"
#import "GetNotification.h"
#import "GetChatList.h"
#import "Occurrence.h"
#import "NearIncidences.h"
#import "PushLocalSended.h"

@interface LocationUser() <ResponseDelegate, LocationDelegate>
@property NSTimer *timer;
@property int count;
@property BOOL gpsOFF;
@end

@implementation LocationUser

+ (LocationUser*) shareInstance {
    static LocationUser *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc]init];
    });
    return shared;
}

-(instancetype)init {
    self = [super init];
    if (self) {
        _count=-60;
    }
    return self;
}

- (void) startMonitoring {
    _timer = [NSTimer scheduledTimerWithTimeInterval: 60.0 target: self
                                            selector:@selector(locationSave:)
                                            userInfo: nil repeats:YES];
}

- (void) stopMonitoring {
    [_timer invalidate];
    _timer = nil;
}
/**
 * Save position in a Core Data object.
 *
 */
- (void) locationSave:(id)sender {
    _count+=60;
    if ([[LocationManagement sharedInstance] isGPSActive]) {
        if (self.gpsOFF==YES) {
            [self sendGPSstatus:NO];
        }
        // Se agrega la posicion al array de posiciones para enviarlos cuando pasen 5 minutos.
        Location *location = [Location createObject];
        location.latitude=[[LocationManagement sharedInstance] latitude];
        location.longitude=[[LocationManagement sharedInstance] longitude];
        location.dateLocation = [NSDate date];
        //nsnumber no funciona con ==
        
        NSString *latString= [NSString stringWithFormat:@"%d", [location.latitude intValue]];
        NSString *longString= [NSString stringWithFormat:@"%d", [location.longitude intValue]];
        if (![latString isEqualToString:@"0"]&&![longString isEqualToString:@"0"]) {
            [location saveObject];
            NSLog(@"New location added: %@", location);
        }else{
            NSLog(@"posicion en ceros!! y se supone el GPS esta encendido");
            LocationManagement * locationManager= [LocationManagement sharedInstance];
            [locationManager stopMonitoring];
            [locationManager stopLocation];
            [locationManager startLocation];
            [locationManager startMonitoring];
        }
        self.gpsOFF=NO;
        if (_count==300) {
            //revisar conexion para enviar posicion mas este record de posiciones
           [self locationMonitoring];
            _count=-60;
        }
    }else{
        if (self.gpsOFF==NO) {
            // Indicar que el GPS esta desactivado.
            [self sendGPSstatus:YES];
            self.gpsOFF=YES;
            return;
        }
    }
}
/**
 * Send my position and the positions record
 * If the connection isn`t available save the position
 */
- (void) locationMonitoring {
    // si si hay conexion envia
    if ([ConnectionManagement isAvailableConnection]) {
        if ([[LocationManagement sharedInstance] isGPSActive]) {
           // Ejecutar el servicio de enviar posicion.
            [self sendPosition];
            // Si existen datos guardados de posiciones, se enviaran tambien las posiciones.
            if ( [[Location findAllOrderbyDateLocation]count]>0) {
                [self sendAllPositions];
            }
          self.gpsOFF=NO;
        } else {
            // Indicar que el GPS esta desactivado.
            if (self.gpsOFF==NO) {
                [self sendGPSstatus:YES];
                self.gpsOFF=YES;
          }
        }
    }
}

- (void) sendPosition {
    NSNumber* latitude = [[LocationManagement sharedInstance] latitude];
    NSNumber* longitude = [[LocationManagement sharedInstance] longitude];
    
    NSLog(@"Send position latitude %@ longitude %@", latitude,longitude);
    
    NSString *latString= [NSString stringWithFormat:@"%d", [latitude intValue]];
    NSString *longString= [NSString stringWithFormat:@"%d", [longitude intValue]];
    if (![latString isEqualToString:@"0"]&&![longString isEqualToString:@"0"]) {
        [PositionsServices sendMyPositionWithLatitude:latitude Longitude:longitude Wait:NO Delegate:self];
        Profile * user= [Profile findWithUserId:[Service getUserId]];
        user.lastLatitude= [NSString stringWithFormat:@"%@",latitude];
        user.lastLongitude=[NSString stringWithFormat:@"%@",longitude];
        [user mergeObject];
    }else{
        NSLog(@"posicion en ceros hacer algo!!");
    }
}

- (void) sendAllPositions {
    NSArray* listLocations = [Location findAllOrderbyDateLocation];
    if (listLocations && [listLocations count] > 0) {
        [PositionsServices sendAllPosition:listLocations WithWait:NO Delegate:self];
    }
}
-(void)sendGPSstatus:(BOOL)status{
    [PositionsServices sendGPSstatus:status Wait:NO Delegate:self];
}

#pragma mark - Methods of ResponseDelegate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kPOSITION:
            responseCode = [[[dictionary objectForKey:@"PositionResult"] objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                //gps activado
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kUPDATE_GPS_STATUS:
            responseCode = [[[dictionary objectForKey:@"ToggleGPSResult"] objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kFIND_NEAR_OCCURRENCES:
            dictionary = [dictionary objectForKey:@"FindOccurrencesResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
           break;

        default:
            break;
    }
}

-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        // Si el envio de las posiciones fue satisfactorio limpiar el array.
        case kUPDATE_POSITIONS_RECORD:
             responseCode = [[[dictionary objectForKey:@"UpdatePositionsRecordResult"] objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                // Eliminar posiciones de extra.
                NSArray* listPositions =[Location findAllOrderbyDateLocation];
                for (Location* location in listPositions) {
                    [location deleteObject];
                }
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        default:
            break;
    }
}

- (void)errorResponseWithService:(kService)service Error:(NSError *)error {
    // Si ocurrio algun problema con el elvio de las posiciones, dejar el array con las posiciones que ya tenia para enviarlos posteriormente.
    NSLog(@"Error services: %@", error);
}
@end
