        //
//  SettingsVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 20/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SettingsVC.h"
#import "SettingListVC.h"
#import "AppDelegate.h"
#import "PositionsServices.h"
#import "SecurityZonesServices.h"
#import "OccurenceServices.h"
#import "LocationManagement.h"
#import "AlertView.h"
#import "Tutorial2VC.h"
#import "ContainerVC.h"
#import "ConditionsVC.h"

#import "Profile.h"


#import "PairBeaconInterfaceVC.h"

@interface SettingsVC () <ResponseDelegate, AlertViewDelegate>

@property SettingListVC* listVC;

@property (strong) NSArray *Products;
@property BOOL isPremiumBuy;
@property BOOL flag;
@property BOOL isCloseSesion;
@property BOOL isHiddenPosition;
@property BOOL isShowOccNoti;
@property BOOL isShowZonesNoti;
@property NSString * reciptData;
@property  Profile * user;

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
  
  
    self.isCloseSesion=NO;
     self.user = [Profile findWithUserId:[Service getUserId]];

}
-(void)viewDidAppear:(BOOL)animated{
self.user = [Profile findWithUserId:[Service getUserId]];
    self.contView= (ContainerVC *)self.navigationController.parentViewController;
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    self.listVC = segue.destinationViewController;

    __block SettingsVC* blockSelf = self;
    self.listVC.callback = ^(int tag){
        [blockSelf selectMenu:tag];
    };
    self.listVC.callbackSwitch = ^(int tag, BOOL isSelect){
        [blockSelf selectOption:tag select:isSelect];
    };
}

- (void) closeSession {
    
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"cerrar.sesion", nil) Delegate:nil CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
    alert.delegate=self;
    self.isCloseSesion=YES;

  
}
-(void)buttonPressed:(UIButton *)sender{
    if (sender.tag==1) {
        if(self.isCloseSesion==YES){
        AppDelegate* app = [[UIApplication sharedApplication] delegate];
          [app closeSession];
        }
    }
    else if (sender.tag ==0){
       
        self.isCloseSesion=NO;
    }
}

- (void) showEmergencyNumbers {
    UIViewController* emergencyVC = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"EmergencyNumbersVC"];
    [self.navigationController pushViewController:emergencyVC animated:YES];
}
- (void) configBeacon {
    UIViewController* beacons = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"PairBeacon"];
    [self.navigationController pushViewController:beacons animated:YES];
     ((PairBeaconInterfaceVC*)beacons).container = self.contView;
}
- (void) configCallAlert {
    UIViewController* callAlerts = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"CallAlert"];
    [self.navigationController pushViewController:callAlerts animated:YES];
    //((PairBeaconInterfaceVC*)beacons).container = self.contView;
}

- (void) showConditions {
    
    UIViewController* conditions = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"ConditionsVC"];

    [self.navigationController pushViewController:conditions animated:YES];
    
}
- (void) showPrivacy{
   
    UIViewController* privacy = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"PrivacyVC"];
    
    [self.navigationController pushViewController:privacy animated:YES];
    
}
- (void) showTutorial {
    UIViewController* tuto = [[UIStoryboard storyboardWithName:@"Start" bundle:nil] instantiateViewControllerWithIdentifier:@"Turtorial2VC"];
     [((Tutorial2VC * )tuto) drawInView:self.navigationController.parentViewController.view];
  
}

- (void) selectMenu:(int) tag {
    switch (tag) {
        case 3: // Enlazar beacon
            [self configBeacon];
            break;
        case 4: // Emergency numbers
            [self showEmergencyNumbers];
            break;
        case 5: // Set alert calls
            [self configCallAlert];
            break;
        case 6:
            [self showTutorial];
            // un tutorial bien chafa
            break;
        case 7:
            [self showConditions];
            //hacer clase de terminos y condiciones
            break;
        case 8:
            [self showPrivacy];
            //hacer clase de aviso de privacidad
            break;
        case 9: // Close session
            [self closeSession];
            break;
       
        default:
            break;
    }
   
}

- (void) hiddenPositions:(BOOL) hidden {
    NSNumber* latitude = [LocationManagement sharedInstance].latitude;
    NSNumber* longitude = [LocationManagement sharedInstance].longitude;
    [PositionsServices changePositionWithHide:hidden Latitude:latitude Longitude:longitude Wait:YES Delegate:self];
}
-(void) showNotifOccurrences:(BOOL)sender{
    
    [OccurenceServices showNotificationsOccurrencesWithPermission:sender Wait:YES Delegate:self];

}
-(void) showNotifSecureZones:(BOOL)sender{
  
    [SecurityZonesServices showNotificationsSecureZonesWithPermission:sender Wait:YES Delegate:self]
    ;
    
}

- (void) selectOption:(int) tag select:(BOOL) isSelect {
  //  NSString * value;
    switch (tag) {
        case  0:
                         self.isShowZonesNoti=isSelect;
            [self showNotifSecureZones:isSelect];
            break;
        case 1:
            
            self.isShowOccNoti=isSelect;
            [self showNotifOccurrences:isSelect];
            break;
        case 2:
            if (isSelect) {
                isSelect=NO;
                
            }else{
                isSelect=YES;
                
            }
            self.isHiddenPosition=isSelect;
            [self hiddenPositions:isSelect];
            break;

        default:
            break;
    }
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kCHANGE_POSITIONS_HIDE:
            dictionary = [dictionary objectForKey:@"ChangePositionHiddenStatusResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode != 200) {
                self.listVC.toggleLocationHide.selected = !self.listVC.toggleLocationHide.selected;
            }
            if (responseCode == 200) {
                self.user.hiddenPosition=[NSNumber numberWithBool:self.isHiddenPosition];
                [self.user mergeObject];
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            
            break;
        case kSHOW_NOTIFICATIONS_OCCURRENNCES:
            dictionary = [dictionary objectForKey:@"ShowOccurrencesNotificationsResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode != 200) {
                self.listVC.toggleOccurrences.selected = !self.listVC.toggleOccurrences.selected;
            }
            if (responseCode == 200) {
                self.user.showOccurrenceNotifications=[NSNumber numberWithBool:self.isShowOccNoti];
                [self.user mergeObject];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
           break;
        case kSHOW_NOTIFICATIONS_ZONES:
            dictionary = [dictionary objectForKey:@"ShowSecureZonesNotificationsResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode != 200) {
                self.listVC.toggleZone.selected = !self.listVC.toggleZone.selected;
            }
            if (responseCode == 200) {
                self.user.showSecureZonesNotifications=[NSNumber numberWithBool:self.isShowZonesNoti];
                [self.user mergeObject];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
           break;
        
                default:
            break;
    }
}

@end
