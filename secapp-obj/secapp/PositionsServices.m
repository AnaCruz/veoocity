//
//  PositionsServices.m
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "PositionsServices.h"
#import "Location.h"

@implementation PositionsServices

+ (void) sendMyPositionWithLatitude:(NSNumber*) latitude Longitude:(NSNumber*) longitude Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@Position", [super getUrl]];
    NSDictionary* params = @{@"Coordinates":@{
                                     @"UserId":[self getUserId],
                                     @"SessionId":[self getSessionId],
                                     @"Latitude":latitude,
                                     @"Longitude":longitude,
                                     @"Battery":[self getBatteryLevel]
                                     }
                             };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kPOSITION Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];
}

+ (void) sendAllPosition:(NSArray*) positions WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@UpdatePositionsRecord", [super getUrl]];
    NSMutableArray* positionsMutable = [NSMutableArray array];
    for (Location* location in positions) {
        // NSTimeInterval milisecondedDate = ([location.dateLocation timeIntervalSinceNow] * 1000);
        
        NSNumber* dateTime = [NSNumber numberWithLongLong:([location.dateLocation timeIntervalSince1970]*1000)];
        
        NSDictionary* position = @{@"Latitude":location.latitude,
                                   @"Longitude":location.longitude,
                                   @"Time":dateTime
                                   };
        [positionsMutable addObject:position];
    }
    NSDictionary* params = @{@"UserId":[self getUserId],
                             @"SessionId":[self getSessionId],
                             @"Positions":[positionsMutable copy]
                             };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kUPDATE_POSITIONS_RECORD Wait:wait Delegate:delegate];
    request.body = paramsData;
    request.extra = positions;
    [request execute];
}

+(void) changePositionWithHide:(BOOL) hide Latitude:(NSNumber*) latitude Longitude:(NSNumber*) longitude Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@ChangePositionHiddenStatus", [super getUrl]];
   
    NSDictionary* params = @{@"UserId":[self getUserId],
                             @"SessionId":[self getSessionId],
                             @"Hide": @(hide),
                             @"Latitude":latitude,
                             @"Longitude":longitude
                             };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kCHANGE_POSITIONS_HIDE Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];


}
+(void)sendGPSstatus:(BOOL) gpsStatus Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ToggleGPS", [super getUrl]];
    NSDictionary* params = @{@"UserId":[self getUserId],
                             @"SessionId":[self getSessionId],
                             @"GPSOff": gpsStatus ? @"TRUE": @"FALSE"
                            
                             };
    NSData* paramsData = [self parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_GPS_STATUS Wait:wait Delegate:delegate];
    request.body = paramsData;
    [request execute];

    
    


}

@end
