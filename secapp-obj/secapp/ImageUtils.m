//
//  ImageUtils.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ImageUtils.h"
#import <UIKit/UIKit.h>

@implementation ImageUtils

+(NSString*) encodingImageToBase64:(UIImage*)image {
   // NSData *imageData = UIImagePNGRepresentation(image);
     NSData *imageData = UIImageJPEGRepresentation(image,0.5);
    NSString* image64 =[imageData base64EncodedStringWithOptions:0];
    return image64;
}

+(UIImage*) decodingBase64ToImage:(NSString*)imageStr {
    NSData *imageData64 = [imageStr dataUsingEncoding:NSUTF8StringEncoding];
    imageData64 = [imageData64 initWithBase64EncodedData:imageData64 options:0];
    UIImage* image = [UIImage imageWithData:imageData64];
    return image;
}

+(UIImage *)resizeImageFor72DPI:(UIImage*)image
{
    CGRect newRect= CGRectZero;
     CGSize newSize=CGSizeMake(200, 200);
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]
        && [[UIScreen mainScreen] scale] == 2.0)
    {
        //For retina image
        newSize=CGSizeMake(newSize.width/2.0,newSize.height/2.0);
    }
    newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    
    CGImageRef imageRef = image.CGImage;
    
    UIGraphicsBeginImageContextWithOptions(newSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Set the quality level to use when rescaling
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, newSize.height);
    
    CGContextConcatCTM(context, flipVertical);
    // Draw into the context; this scales the image
    CGContextDrawImage(context, newRect, imageRef);
    
    // Get the resized image from the context and a UIImage
    CGImageRef newImageRef = CGBitmapContextCreateImage(context);
    UIImage *newImage = [UIImage imageWithCGImage:newImageRef];
    
    CGImageRelease(newImageRef);
    UIGraphicsEndImageContext();
    
    return newImage;
}


@end
