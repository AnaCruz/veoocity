//
//  AlertService.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 21/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"

@interface AlertService : Service

/*
 */
+ (void) activeAlertWithLatitude:(NSNumber*) latitude AndLonguitude:(NSNumber*) longitude Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/*
 */
+ (void) desactiveAlertWithAlertType:(NSNumber*) type Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;


+ (void) deleteAlertPhoneCallNumberWithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

+ (void)saveAlertCallWithPhoneNumber:(NSString*) phone AndCountryCode:(NSString*) code Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
@end
