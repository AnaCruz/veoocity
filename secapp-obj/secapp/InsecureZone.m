//
//  InsecureZone.m
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "InsecureZone.h"
#import "AppDelegate.h"
#import "UserServices.h"
#import "SecurityZonesServices.h"


@implementation InsecureZone
@dynamic radius;
@dynamic latitude;
@dynamic longitude;
@dynamic name;
@dynamic insecureZoneId;
@dynamic timeFinal;
@dynamic timeInitial;
@dynamic isOwner;


+(instancetype)createObject {
    InsecureZone *insecureZone= [super createObjectWithNameEntity:@"InsecureZone"];
    return insecureZone;
}

+(NSArray *) findWithZoneId: (NSString *)zoneId{
    AppDelegate * app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"InsecureZone"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"insecureZoneId == '%@'", zoneId]];
    [request setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"InsecureZone (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

+ (NSArray*) findAll {
    return [super findAllWithNameEntity:@"InsecureZone"];
}

@end
