//
//  ErrorResult.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 11/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorResult : NSObject

+ (BOOL) errorResultWithErrorCode:(int) errorCode AndErrorMessage:(NSString*) message;

@end
