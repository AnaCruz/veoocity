//
//  SecurityZonesServices.m
//  secapp
//
//  Created by SecApp on 03/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SecurityZonesServices.h"



@implementation SecurityZonesServices

+(void) createSecurityZoneWithName: (NSString *)name CenterLatitude: (NSString * )latitude CenterLongitude:(NSString *)longitude Radius: (NSString *)radius Wait: (BOOL)wait Delegate:(id<ResponseDelegate>)delegate{

    NSString * uri = [NSString stringWithFormat:@"%@CreateSecureZone", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"Name":name,
                             @"CenterLatitude":latitude,
                             @"CenterLongitude":longitude,
                             @"Radius":radius,
                             @"CreatedByUserId":[super getUserId],
                             @"UserIdTo":[super getUserId],
                             @"SessionId":[super getSessionId]
                             
                             };
   RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_SECURITY_ZONE Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
 }
+(void) getUserSecurityZonesWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{

    NSString* uri = [NSString stringWithFormat:@"%@SecureZonesByUser/%@/%@", [super getUrl], [super getUserId], [super getUserId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_SECURITY_ZONES Wait:wait Delegate:delegate];
   
    [request execute];

}
+(void) deleteSecurityZonesWithIdZone:(NSString * )IdZone Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DeleteSecureZone/%@/%@/%@", [super getUrl], IdZone,[super getUserId], [super getSessionId]];
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_SECURITY_ZONE Wait:wait Delegate:delegate];
    [request execute];



}
+(void) updateSecurityZoneWithLatitude:(NSString*) latitude Longitude:(NSString*) longitude Radius:(NSString*) radius Name:(NSString*) name IdZone:(NSString*) idZone Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    
    NSString* uri = [NSString stringWithFormat:@"%@UpdateSecureZone", [super getUrl]];
    
    NSDictionary* dataDic = @{
                              @"objZone":@{
                                      @"Latitude":latitude,
                                      @"Longitude":longitude,
                                      @"Radius":radius,
                                      @"Name":name,
                                      @"idZone":idZone
                                      },
                              @"UserId":[Service getUserId],
                              @"SessionId":[Service getSessionId]
                              };
   
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_SECURITY_ZONE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}

+(void) showNotificationsSecureZonesWithPermission: (BOOL)permission Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ShowSecureZonesNotifications", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"Permission":permission ? @"TRUE": @"FALSE"
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kSHOW_NOTIFICATIONS_ZONES Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}


@end
