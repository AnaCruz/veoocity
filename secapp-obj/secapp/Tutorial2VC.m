//
//  Tutorial2VC.m
//  secapp
//
//  Created by SecApp on 04/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Tutorial2VC.h"

@interface Tutorial2VC ()<UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

// Timer for change a the next picture.
@property NSTimer *timer;
@property int indexPicture;
@property UIView* mainView;
@end

@implementation Tutorial2VC

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.frame.size.width*5, self.scrollView.frame.size.height)];
    });
    self.indexPicture = 0;
    self.timer = [NSTimer scheduledTimerWithTimeInterval: 5.0
                                                  target: self
                                                selector:@selector(changePicture:)
                                                userInfo: nil repeats:YES];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.timer invalidate];
    self.timer = nil;
}

// Change the next picture.
- (void) changePicture:(id)sender {
    self.indexPicture ++;
    if (self.indexPicture > 4 ) {
        self.indexPicture = 0;
    }
    [self.scrollView setContentOffset:CGPointMake(self.indexPicture*self.scrollView.frame.size.width, 0) animated:YES];
}


- (IBAction)changePage:(UIPageControl*)sender {
    [self.scrollView setContentOffset:CGPointMake(sender.currentPage*self.scrollView.frame.size.width, 0) animated:YES];
}

#pragma Mark - Method of UISCrollDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    int page = (int)lround(fractionalPage);
    self.indexPicture = page;
    self.pageControl.currentPage = page; // you need to have a **iVar** with getter for pageControl
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
- (void) drawInView:(UIView*) view {
    CGRect frame =view.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.view.frame = frame;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}

- (IBAction)goBack:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
}



@end
