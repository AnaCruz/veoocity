//
//  GetNotification.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 23/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Notification;

@interface GetNotification : NSObject

/**
 * Block that notify when the "notifications" was dowload
 */
@property (nonatomic, copy) void (^updatePNotification)(void);
/**
 * Count for show the notifications number
 */
@property int cont;
/**
 * Block that notify when there are unread messages
 */
@property (nonatomic, copy) void (^newMessageChat)(Notification* notificacion);

/**
 * Download and save all the notifications
 */
- (void) updateData;
/**
 * Save local notifications
 */
-(void) addOccurrencesLocalNotification:(NSDictionary *) apn;

/**
 * Singleton mehotd
 */
+ (GetNotification*) shareInstance;

@end
