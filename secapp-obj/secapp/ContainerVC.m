//
//  ContainerVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 09/04/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ContainerVC.h"
#import "MapLocationVC.h"
#import "MapSecurityZoneVC.h"
#import "NotificationVC.h"
#import "EditProfileVC.h"
#import "SearchContactsVC.h"
#import "ChatVC.h"
#import "Profile.h"
#import "LocationManagement.h"
#import "AlertService.h"
#import "Friend.h"
#import "Notification.h"
#import "GetChatList.h"
#import "GetNotification.h"
#import "Message.h"
#import "ConversationVC.h"
#import "Alert/AlertView.h"
#import "ChatContainerVC.h"
#import "SecurityZonesServices.h"
#import "SecurityZone.h"
#import "InsecureZone.h"
#import "InsecureZoneServices.h"
#import "GroupService.h"
#import "GroupsVC.h"
#import "InfoUserVC.h"
#import "Location.h"
#import "DisableAlertView.h"
#import "CheckPin.h"

#import "ReviewVC.h"
#import "Service.h"
#import "UserNotifications.h"

#import "AppDelegate.h"

#import "ConvesationGroupVC.h"
#import "FriendsListVC.h"
#import "UserServices.h"
#import "Phone.h"
#import "Email.h"
#import "ContactService.h"

#import "ConnectionManagement.h"
#import "Occurrence.h"
#import "OccurenceServices.h"
#import "PairBeaconInterfaceVC.h"
#import "SettingsVC.h"
#import "PairBeaconCentralManager.h"
#import "PositionsServices.h"

@interface ContainerVC () <ResponseDelegate, InfoUserDelegate, LocationDelegate, AlertViewDelegate,PairBeaconCentralManagerDelegate>

@property (weak, nonatomic) IBOutlet UIView *container;
@property UIViewController *mapZoneSecurityView;
@property MapLocationVC* mapLocationView;
@property  ConversationVC * conversation;
@property ConvesationGroupVC * groupConversation;
@property UIViewController* notificationView;
@property UIViewController* editProfileView;
@property UIViewController* searchFriendView;
@property UIViewController* chatListView;
@property UIViewController* infoUserView;
@property UIViewController* conversationView;
@property NSTimer *timer;
@property NSTimer *timer2;
@property UIViewController* settingsView;
@property UIViewController* groupView;//quitar
@property UIViewController* friendsListView; //nueva ventana de amigos

@property UIView *containerChat;
@property int count; //nivel de zoom de acuerdo al contador
@property int countUpdateContactsAllData; //contador que llega a 2 horas y se reinicia
@property int countUpdateContactsSomeData; //contador que llega a 3 miutos y se reinicia
@property int languaje;
@property BOOL occurrencesUpdated;
@property BOOL friendsUpdated;
@property BOOL friendCompleteUpdate;
@property BOOL isChat; //cuando tiene notificaciones de chat
@property BOOL isAlert;// para cuando tiene notificaciones de alerta activa o inactiva y de agregar contactos nuevos
@property BOOL isSpecialIncidence;
@property int countBeacon; //para cuando el tiempo de conexion automatica caduco
@property (weak, nonatomic) IBOutlet UIButton *btnEditProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnAddFriend;
@property (weak, nonatomic) IBOutlet UIButton *btnSecurityZone;
@property (weak, nonatomic) IBOutlet UIButton *btnChatList;
@property (weak, nonatomic) IBOutlet UIButton *btnSetting;
@property (weak, nonatomic) IBOutlet UIButton *btnNotification;
@property (weak, nonatomic) IBOutlet UIButton *btnUsers;

@property PairBeaconCentralManager * pairCentral;
@end

@implementation ContainerVC

-(instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     [Profile updateProfile];
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
    NSString * userId=[userDefault objectForKey:@"userId"];
    NSString *sessionId = [userDefault objectForKey:@"sessionId"];

    self.countUpdateContactsAllData=-300;
   self.countUpdateContactsSomeData=-300;

    [self.btnAlert setBackgroundImage:[UIImage imageNamed:NSLocalizedString(@"btnAlerta", nil)]
                             forState:UIControlStateNormal];
    [self.btnAlert setBackgroundImage:[UIImage imageNamed:NSLocalizedString(@"btnAlertaSelected", nil)]
                             forState:UIControlStateSelected];
    [self showMapLocation];
    [self showUsersCount:0];
    [[LocationManagement sharedInstance] startLocation];
    [[LocationManagement sharedInstance] startMonitoring];
    GetNotification* notification = [GetNotification shareInstance];
    notification.cont=0;
    notification.updatePNotification = ^{
        // Indica cuando se actualizan las notificaciones
        NSMutableArray* arrayResult = [[Notification findAll] mutableCopy];
        [self showNotificationsCount:(int)[arrayResult count]];
        //mensaje de calificar app
        if (self.btnNotification.selected==YES&& self.notificationView) {
            NotificationVC * notiView = (NotificationVC *)self.notificationView;
            [notiView updateView];
        }
        self.isAlert=NO;
       self.isSpecialIncidence= NO;
        for (Notification *notification in arrayResult) {
            if ([notification.typeId intValue] >5  && [notification.typeId intValue]<12) {
                if (self.isChat==false&&self.countUpdateContactsSomeData!=-300) {
                    [[GetChatList shareInstance]updateData];
//                    ChatContainerVC * chatList= (ChatContainerVC *)self.chatListView;
//                    [chatList notification]; //update lastMessage View
                    self.isChat=YES;
                    continue;
                }
            }
            if([notification.typeId isEqual:@1]||[notification.typeId isEqual:@2]){
                if (self.isAlert==false&&self.countUpdateContactsSomeData!=-300) {
                  
                    [ContactService getCotactListWithoutSomeDataWithUserId:userId AndSessionId:sessionId Wait:NO Delegate:self];
                    self.isAlert=YES;
//                    self.friendCompleteUpdate
//                    =YES;
                    self.occurrencesUpdated=YES;
                    continue;
                }
            }
            if ([notification.typeId isEqual:@3]&&self.countUpdateContactsSomeData!=-300){
                [ContactService getCotactListWithUserId:userId AndSessionId:sessionId Wait:NO Delegate:self];
                continue;
            }
            if ([notification.typeId isEqual:@27]||[notification.typeId isEqual:@23]) {
                if (self.isSpecialIncidence==false&&self.countUpdateContactsSomeData!=-300) {
                    [self updateOccurrences];
                    self.isSpecialIncidence=YES;
                    continue;
                    
                }
                
            }
        }
    };
    [notification updateData];
    
    GetChatList* chatList = [GetChatList shareInstance];
    chatList.callback = ^{
        [self checkTheChatList];
        self.isChat=YES;
        ChatContainerVC * chatList= (ChatContainerVC *)self.chatListView;
        [chatList notification]; // update lastMessageView
    };
    //[chatList updateData];
    [self getSecurityZones];
    NSArray* friendsSaved= [Friend findAll];
    for (Friend* friend in friendsSaved) {
        [friend deleteObject];
    }
   [self updateContacts];
   [self startTimer];
  
   
}
-(void)setContainerVCDelegateOfBeaconCentral{
    self.pairCentral= [PairBeaconCentralManager sharedInstance];
    self.pairCentral.delegate=self;
}

-(void)viewDidAppear:(BOOL)animated{
     self.languaje=[[Service getLanguaje]intValue];
    Profile * user = [Profile findWithUserId:[Service getUserId]];
    NSLog(@"profile latitude: %@ profile status: %@", user.lastLatitude, user.status);
    if (user==nil||user.lastLatitude==nil||[user.lastLatitude isEqualToString:@""]||[user.lastLatitude isEqual:[NSNull null]]) {
        [Profile updateProfile];
        user = [Profile findWithUserId:[Service getUserId]];
    }
    int status=-1;
    status=[user.status intValue];
    if (status==1) {
        self.btnAlert.selected= YES;
      
    }
    else{
        self.btnAlert.selected= NO;
    }
   
    if (user.beaconId!=nil && ![user.beaconId isEqual:[NSNull null]]) {
        //[self checkStatusBeacon];
        self.pairCentral= [PairBeaconCentralManager sharedInstance];
        self.pairCentral.delegate=self;
         [self.pairCentral statusBeacon];
    }
    
}
-(void)isConnected{}

-(void)notConnected{
    Profile * user = [Profile findWithUserId:[Service getUserId]];
    if (user.beaconId && ![user.beaconId isEqual:[NSNull null]]) {
        self.pairCentral.uuid=user.beaconId;
        [self.pairCentral scan];
    }
}
-(void)alert{
[self startAlertFromBeacon];
}
-(void)updateViewDidConnect{}
-(void)updateViewDidDisconnect{
   
}
-(void)uuidNotMatch{}
-(void)timeOutForConnect{
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"enlace.perdido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
    [self.pairCentral disconnectAutomatic];
    // no pudo conectar
}


/**
 * Timer for update the contact list on the mapView
 */
-(void) startTimer{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:(300.0)
                                                      target: self
                                                    selector:@selector(updateDataContacts)
                                                    userInfo: nil repeats: YES];
}

/**
 * update only status, location, date, of the contacts
 */
-(void)updateDataContacts{
    self.countUpdateContactsAllData+=300;
   self.countUpdateContactsSomeData+=300;
    if (self.countUpdateContactsAllData==7200) {
        [self updateContacts];
       [[GetNotification shareInstance] updateData]; //quitar esto si consume muchos datos
        self.countUpdateContactsAllData=-300;
    }
    if (self.countUpdateContactsSomeData==300) {
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString * userId=[userDefault objectForKey:@"userId"];
        NSString *sessionId = [userDefault objectForKey:@"sessionId"];
        NSArray * friends= [Friend findAll];
        for (Friend *friend in friends) {
            if (friend && [[friend.phones allObjects] count]==0) {
                [self updateContacts];
                return;
            }
        }
        [ContactService getCotactListWithoutSomeDataWithUserId:userId AndSessionId:sessionId Wait:NO Delegate:self];
        [OccurenceServices getAllOccurrencesWithWait:NO Delegate:self];
        self.countUpdateContactsSomeData=0;
        
       
    }
   
    
    
    
}
-(void)updateOccurrences{
 [OccurenceServices getAllOccurrencesWithWait:NO Delegate:self];
}
/**
 * Call the getContactList service.
 */
-(void)updateContacts{
    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString * userId=[userDefault objectForKey:@"userId"];
        NSString *sessionId = [userDefault objectForKey:@"sessionId"];
       [ContactService getCotactListWithUserId:userId AndSessionId:sessionId Wait:NO Delegate:self];
        [OccurenceServices getAllOccurrencesWithWait:NO Delegate:self];
}


/**
 * Check if the user have unreaded message
 */
-(void) checkTheChatList {
    NSArray* array = [Message findUnreadMessage];
   
    if ([array count]>0) {
        for (Message *mesage in array) {
            NSLog(@"Message unread: %@", mesage.message);
        }
       [self.btnChatList setImage:[UIImage imageNamed:@"chat_mensaje_activo"] forState:UIControlStateNormal];
        [self.btnChatList setImage:[UIImage imageNamed:@"chat_mensaje"] forState:UIControlStateSelected];
    } else {
        [self.btnChatList setImage:[UIImage imageNamed:@"icono5_a"] forState:UIControlStateNormal];
        [self.btnChatList setImage:[UIImage imageNamed:@"icono5"] forState:UIControlStateSelected];
    }
}

-(void) showUsersCount: (int)count{
    UIFont *arialBIFont = [UIFont fontWithName:@"Arial-BoldMT" size:22.0];
    NSDictionary *arialBIDict = [NSDictionary dictionaryWithObject: arialBIFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%u\n",count] attributes: arialBIDict];
    
    UIFont *arialFont = [UIFont fontWithName:@"Arial" size:9.0];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject:arialFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc]initWithString: NSLocalizedString(@"amigos", nil) attributes:arialDict];
    [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:(NSMakeRange(0,[aAttrString length]))];
    [aBIAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:(NSMakeRange(0,[aBIAttrString length]))];
    
    [aBIAttrString appendAttributedString:aAttrString];
    self.btnUsers.titleLabel.numberOfLines=2;
    self.btnUsers.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btnUsers.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.btnUsers setAttributedTitle:aBIAttrString forState:UIControlStateNormal];
    
}


-(void) showNotificationsCount: (int)count{
    if (count==20) {
    [Service hasCommentWithWait:NO Delegate:self];
    }
    UIFont *arialBIFont = [UIFont fontWithName:@"Arial-BoldMT" size:22.0];
    NSDictionary *arialBIDict = [NSDictionary dictionaryWithObject: arialBIFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aBIAttrString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%u\n",count] attributes: arialBIDict];
    
    UIFont *arialFont = [UIFont fontWithName:@"Arial" size:9.0];
    NSDictionary *arialDict = [NSDictionary dictionaryWithObject:arialFont forKey:NSFontAttributeName];
    NSMutableAttributedString *aAttrString = [[NSMutableAttributedString alloc]initWithString: NSLocalizedString(@"notificaciones", nil) attributes:arialDict];
    [aAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:(NSMakeRange(0,[aAttrString length]))];
    [aBIAttrString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:(NSMakeRange(0,[aBIAttrString length]))];
    
    [aBIAttrString appendAttributedString:aAttrString];
    self.btnNotification.titleLabel.numberOfLines=2;
    self.btnNotification.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.btnNotification.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [self.btnNotification setAttributedTitle:aBIAttrString forState:UIControlStateNormal];
}

- (void) showMapLocation {
    self.mapLocationView = [[UIStoryboard storyboardWithName:@"Map" bundle:nil] instantiateViewControllerWithIdentifier:@"MapLocationVC"];
        [self addChildViewController:self.mapLocationView];
        ContainerVC *_self=self;
        self.mapLocationView.updateContactsCount=^{
            NSMutableArray* arrayResult = [[Friend findAll] mutableCopy];
            [_self showUsersCount:(int)[arrayResult count]];
            
        };
    self.mapLocationView.container= self;
   [self clearContainer];
    CGRect frame = self.container.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.mapLocationView.wasOn=NO;
    self.mapLocationView.view.frame = frame;
    [self.container addSubview:self.mapLocationView.view];
}

- (void) showMapSecurity {
    self.mapZoneSecurityView= [[UIStoryboard storyboardWithName:@"Map" bundle:nil] instantiateViewControllerWithIdentifier:@"MapSecurityZoneVC"];
        [self addChildViewController:self.mapZoneSecurityView];
   [self clearContainer];
    CGRect frame = self.container.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.mapZoneSecurityView.view.frame = frame;
  [self.container addSubview:self.mapZoneSecurityView.view];
}


- (void) showNotifications {
  [self clearSelection:self.btnNotification];
    [self showMapLocation];
    if (!self.notificationView) {
        self.notificationView= [[UIStoryboard storyboardWithName:@"Notification" bundle:nil] instantiateViewControllerWithIdentifier:@"NotificationVC"];
        [self addChildViewController:self.notificationView];
        ((NotificationVC*)self.notificationView).container = self;
    }
  int height = 300;
    CGRect frame = CGRectMake(0, self.container.frame.size.height,
                              self.container.frame.size.width, height);
    self.notificationView.view.frame = frame;
    frame.origin.y -= height;
    [self.container addSubview:self.notificationView.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.notificationView.view.frame = frame;
    } completion:^(BOOL finished) {

    }];
    self.btnNotification.selected = YES;
}


- (void) hideNotifications {
    if (!self.notificationView) {
        return;
    }
    CGRect frame = self.notificationView.view.frame;
    frame.origin.y += frame.size.height;
    [UIView animateWithDuration:0.5 animations:^{
        self.notificationView.view.frame = frame;
    } completion:^(BOOL finished) {
        
        [self.notificationView.view removeFromSuperview];
    }];
    self.btnNotification.selected = NO;
}

- (void) hideSearching {
    if (!self.searchFriendView) {
        return;
    }
    CGRect frame = self.searchFriendView.view.frame;
    frame = self.searchFriendView.view.frame;
    frame.origin.x = -self.container.frame.size.width;
    [UIView animateWithDuration:0.5 animations:^{
        self.searchFriendView.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.searchFriendView.view removeFromSuperview];
    }];
    self.btnSearchFriend.selected = NO;
}

-(void)showInfoBar:(Friend *)friendInfoBar{
    if (!self.infoU) {
        self.infoU =[[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoUserVC"];
        [self addChildViewController:self.infoU];
        self.infoU.delegate=self;
    }
    if (friendInfoBar==nil) {
        return;
    }
   self.infoU.friendSelected = friendInfoBar;
    self.infoU.isSearch=YES;
    self.infoU.me=nil;
    self.infoU.user=nil;
    if (self.infoU.isVisible==YES) {
        [self.infoU setUser];    }
    else{
        [self.infoU drawInView:self.container];//chkar cambio en mapa
    }
}
-(void)showInfoBarWithMe{
    if (!self.infoU) {
        self.infoU =[[UIStoryboard storyboardWithName:@"Robot" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoUserVC"];
        [self addChildViewController:self.infoU];
        self.infoU.delegate=self;
    }
    self.infoU.me = [Profile findWithUserId:[Service getUserId]];
    self.infoU.user = nil;
    self.infoU.isSearch=NO;
        if (self.infoU.isVisible==YES) {
        [self.infoU setUser];    }
    else{
        [self.infoU drawInView:self.container];//chkar cambio en mapa
    }
}

-(void)closeInfoUser{
    [self.infoU closeView:nil];
    [self showMapLocation];
}
-(void)showView:(NSString *)friendSelected IsMonitor:(BOOL)isMonitor{
    //container si se usa
    self.conversationView=[[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationVC"];
    self.conversation = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationVC"];
    self.conversation.userIdTo=friendSelected;
    self.conversation.isVisibleInMapView=YES;
    self.conversation.isMonitor=isMonitor;
   [self.conversation drawInView:self.mapLocationView.view];
    
}
-(void)showViewChatGroup:(NSDictionary *)datadic GroupId:(NSString *)groupId {
    //conversacion grupal desde notificacion
    self.groupConversation=[[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationGrupalVC"];
   self.groupConversation.chatGroup=datadic;
    self.groupConversation.groupID=groupId;
    self.groupConversation.isVisibleinMap=YES;
   [self.groupConversation drawInView:self.mapLocationView.view];
    
}
// show view para conversacion grupal y otro para monitor

- (void) clearContainer {
    if (self.btnNotification.selected) {
        [self hideNotifications];
    }
    if (self.mapLocationView.infoU.isVisible==YES) {
        [self.mapLocationView.infoU closeView:nil];
    }
    if (self.infoU.isVisible==YES) {
        [self.infoU closeView:nil];
    }
}

- (void) clearSelection:(UIButton*) sender {
  
  if (self.btnAddFriend.tag != sender.tag && self.btnAddFriend.selected) {
        [self showGroups:self.btnAddFriend];
        self.btnAddFriend.selected = NO;
    }
    if (self.btnChatList.tag != sender.tag && self.btnChatList.selected) {
        [self showChatList:self.btnChatList];
        self.btnChatList.selected = NO;
    }
    if (self.btnEditProfile.tag != sender.tag && self.btnEditProfile.selected) {
        [self showEditProfile:self.btnEditProfile];
        self.btnEditProfile.selected = NO;
    }
    if (self.btnSearchFriend.tag != sender.tag && self.btnSearchFriend.selected) {
        [self showSearchFriend:self.btnSearchFriend];
        self.btnSearchFriend.selected = NO;
    }
    if (self.btnSecurityZone.tag != sender.tag && self.btnSecurityZone.isSelected) {
        [self showMapLocation];
        self.btnSecurityZone.selected = NO;
    }
    if (self.btnSetting.tag != sender.tag && self.btnSetting.selected) {
        [self showSettings:self.btnSetting];
        self.btnSetting.selected = NO;
    }
     if (self.mapLocationView.infoU.isVisible==YES) {
        [self.mapLocationView.infoU closeView:nil];
    }
    if (self.mapLocationView.conver.isVisibleInMapView==YES) {
        [self.mapLocationView.conver goBack:nil];
    }
    if (self.conversation.isVisibleInMapView) {
        [self.conversation goBack:nil];
    }
    if (self.groupConversation.isVisibleinMap) {
        [self.groupConversation goBack:nil];
    }
    if (self.mapLocationView.occurrence.isVisible) {
        [self.mapLocationView.occurrence closeView:nil];
    }
    if (self.mapLocationView.occurrence.createOccurrenceView.isShowInMap) {
         [self.mapLocationView.occurrence.createOccurrenceView cancel:nil];
    }
    if (self.mapLocationView.displayOccurrence.isShowInMap) {
        [self.mapLocationView.displayOccurrence closeView:nil];
    }
}

- (void) showEditProfile:(UIButton*) sender {
    if (!self.editProfileView) {
        self.editProfileView = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"EditProfileVC"];
        ((EditProfileVC*) self.editProfileView).mainContainer = self;
        UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:self.editProfileView];
        nav.navigationBarHidden = YES;
        [self addChildViewController:self.editProfileView.parentViewController];
    }
    if (sender.isSelected) {
        [self hideEditProfile];
        return;
    }
    [((UINavigationController*)self.editProfileView.parentViewController) popToRootViewControllerAnimated:NO];
    CGRect frame = CGRectMake(-self.container.frame.size.width, 0,
                              self.container.frame.size.width, 359);
    self.editProfileView.parentViewController.view.frame = frame;
    [self.view insertSubview:self.editProfileView.parentViewController.view aboveSubview:self.container];
    frame.origin.x = self.container.frame.origin.x;
    [UIView animateWithDuration:0.5 animations:^{
        self.editProfileView.parentViewController.view.frame = frame;
    }];
}

-(void) hideEditProfile {
    CGRect frame = self.editProfileView.parentViewController.view.frame;
    frame.origin.x = -self.container.frame.size.width;
    [UIView animateWithDuration:0.5 animations:^{
        self.editProfileView.parentViewController.view.frame = frame;
    } completion:^(BOOL finished) {
        [self.editProfileView.parentViewController.view removeFromSuperview];
        self.btnEditProfile.selected = NO;
    }];
}

- (void) showSearchFriend:(UIButton*) sender {
    if (!self.searchFriendView) {
        self.searchFriendView = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"SearchContactsVC"];
        [self addChildViewController:self.searchFriendView];
        ((SearchContactsVC*)self.searchFriendView).container = self;
    }
    CGRect frame;
    if (sender.isSelected) {
        frame = self.searchFriendView.view.frame;
        frame.origin.x = -self.container.frame.size.width;
        [UIView animateWithDuration:0.5 animations:^{
            self.searchFriendView.view.frame = frame;
        } completion:^(BOOL finished) {
            [self.searchFriendView.view removeFromSuperview];
        }];
        return;
    }
    frame = CGRectMake(-self.container.frame.size.width, sender.frame.origin.y,
                       self.container.frame.size.width, self.container.frame.size.height-sender.frame.origin.y);
    self.searchFriendView.view.frame = frame;
    [self.view insertSubview:self.searchFriendView.view aboveSubview:self.container];
    frame.origin.x = self.container.frame.origin.x;
    [UIView animateWithDuration:0.5 animations:^{
        self.searchFriendView.view.frame = frame;
    }];
}

- (void) showChatList:(UIButton*) sender {
        if (!self.chatListView) {
        self.chatListView = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatContainerVC"];
        UINavigationController * nav=[[UINavigationController alloc]initWithRootViewController:self.chatListView];
        nav.navigationBarHidden=YES;
        [self addChildViewController:self.chatListView.parentViewController];
    }
    CGRect frame;
    if (sender.isSelected) {
        frame = self.chatListView.parentViewController.view.frame;
        frame.origin.x = -self.container.frame.size.width;
        [UIView animateWithDuration:0.5 animations:^{
            self.chatListView.parentViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [self.chatListView.parentViewController.view removeFromSuperview];
        }];
        return;
    }
    [((UINavigationController *)self.chatListView.parentViewController)popToRootViewControllerAnimated:NO];
    float y = 0;
    frame = CGRectMake(15, y,
                       self.container.frame.size.width, self.container.frame.size.height);
    self.chatListView.parentViewController.view.frame = frame;
    [self.view insertSubview:self.chatListView.parentViewController.view aboveSubview:self.container];
    frame.origin.x = self.container.frame.origin.x;
    [UIView animateWithDuration:0.5 animations:^{
        self.chatListView.parentViewController.view.frame = frame;
    }];
}

- (void) showSettings:(UIButton*) sender {
    if (!self.settingsView) {
        self.settingsView = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"SettingsVC"];
        UINavigationController * nav=[[UINavigationController alloc]initWithRootViewController:self.settingsView];
        nav.navigationBarHidden=YES;
        [self addChildViewController:self.settingsView.parentViewController];
         ((SettingsVC*)self.settingsView).contView = self;
    }
    CGRect frame;
    if (sender.isSelected) {
        frame = self.settingsView.parentViewController.view.frame;
        frame.origin.x = -self.container.frame.size.width;
        [UIView animateWithDuration:0.5 animations:^{
            self.settingsView.parentViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [self.settingsView.parentViewController.view removeFromSuperview];
        }];
        return;
    }
    [((UINavigationController *)self.settingsView.parentViewController)popToRootViewControllerAnimated:NO];
//    float height = 359;
//    float y = (self.container.frame.size.height) - height;
//    frame = CGRectMake(-self.container.frame.size.width, y,
//                       self.container.frame.size.width, height);
    float y = 0;
    frame = CGRectMake(15, y,
                       self.container.frame.size.width, self.container.frame.size.height);

    self.settingsView.parentViewController.view.frame = frame;
    [self.view insertSubview:self.settingsView.parentViewController.view aboveSubview:self.container];
    frame.origin.x = self.container.frame.origin.x;
    [UIView animateWithDuration:0.5 animations:^{
        self.settingsView.parentViewController.view.frame = frame;
    }];
}

- (void) showGroups:(UIButton*)sender {
    
    if (!self.friendsListView) {
        self.friendsListView = [[UIStoryboard storyboardWithName:@"AddFriends" bundle:nil] instantiateViewControllerWithIdentifier:@"MainFriendsList"];
        UINavigationController * nav=[[UINavigationController alloc]initWithRootViewController:self.friendsListView];
        nav.navigationBarHidden=YES;
        [self addChildViewController:self.friendsListView.parentViewController];
         ((FriendsListVC*)self.friendsListView).container = self;
    }
    CGRect frame;
    if (sender.isSelected) {
        frame = self.friendsListView.parentViewController.view.frame;
        frame.origin.x = -self.container.frame.size.width;
        [UIView animateWithDuration:0.5 animations:^{
            self.friendsListView.parentViewController.view.frame = frame;
        } completion:^(BOOL finished) {
            [self.friendsListView.parentViewController.view removeFromSuperview];
        }];
        return;
    }
    [((UINavigationController *)self.friendsListView.parentViewController)popToRootViewControllerAnimated:NO];
    frame = CGRectMake(-self.container.frame.size.width, 0,
                       self.container.frame.size.width, self.container.frame.size.height);
    self.friendsListView.parentViewController.view.frame = frame;
    [self.view insertSubview:self.friendsListView.parentViewController.view aboveSubview:self.container];
    frame.origin.x = self.container.frame.origin.x;
    [UIView animateWithDuration:0.5 animations:^{
        self.friendsListView.parentViewController.view.frame = frame;
    }];
}

#pragma mark IBAction

- (IBAction)menuSelect:(UIButton*)sender {

    
    if (self.btnNotification.isSelected) {
        [self hideNotifications];
    }
    if (self.infoU.isVisible==YES) {
        [self.infoU closeView:nil];
    }
    switch (sender.tag) {
        case 101: //Editar perfil {
            [self showEditProfile:sender];
                       break;
        case 102: // Localizar amigos
            [self showSearchFriend:sender];
            

            break;
        case 103: // Agregar amigos
            [self showGroups:sender];
                       break;
        case 104: // Zonas Seguras
            if (sender.isSelected) {
                [self showMapLocation];
                            }
            else {
                [self showMapSecurity];
                            }
            break;
        case 105: // Chat
              [self showChatList:sender];
            break;
        case 106: // Ajustes
        
            [self showSettings:sender];
            break;
        default:
            break;
    }
    [sender setSelected:!sender.isSelected];
    [self clearSelection:sender];
}

- (IBAction)locationTapped:(id)sender {
 
    if (self.count==3) {
        self.count=1;
    }else{
        self.count++;
    }
    int zoom;
    switch (self.count) {
        case 1:
            zoom=630000;
      
            break;
        case 2:
            zoom=12500;
                       break;
        case 3:
            zoom=50;
                       break;
        default:
            break;
    }
    
    CGFloat newLat;
    CGFloat newLon;
    newLat =[[[LocationManagement sharedInstance] latitude] floatValue];
    newLon = [[[LocationManagement sharedInstance] longitude] floatValue];
    int intlat= newLat;
    int intlong= newLon;
    NSString *latString= [NSString stringWithFormat:@"%d", intlat];
    NSString *longString= [NSString stringWithFormat:@"%d",intlong];
    
    if ([latString isEqualToString:@"0"]&&[longString isEqualToString:@"0"]) {
        Profile * profile = [Profile findWithUserId:[Service getUserId]];
        newLat = [profile.lastLatitude floatValue];
        newLon =[profile.lastLongitude floatValue];

    }
    CLLocationCoordinate2D newCoord = {newLat, newLon};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, zoom, zoom);
    [self.mapLocationView.mapView setRegion:region animated:YES];
    [self clearSelection:sender];
    [self clearContainer];
}

- (IBAction)newsTapped:(UIButton*)sender {
     if (!sender.selected) {
        [self showNotifications];
    } else {
        [self hideNotifications];
    }
 
}
-(void)disableAlertWithAlertType:(int)alertType{
[AlertService desactiveAlertWithAlertType:[NSNumber numberWithInt:alertType] Wait:NO Delegate:self];
}
-(void)startAlertFromBeacon{
    NSNumber* latitude = [[LocationManagement sharedInstance] latitude];
    NSNumber* longitude = [[LocationManagement sharedInstance] longitude];
    [AlertService activeAlertWithLatitude:latitude AndLonguitude:longitude Wait:NO Delegate:self];
    Profile* user = [Profile findWithUserId:[UserServices getUserId]];
    if (user.phoneAlertCalls!=nil&&user.countryCode!=nil) {
        NSString* phoneCallNum = [NSString stringWithFormat:@"tel://%@%@", user.countryCode,user.phoneAlertCalls];
        NSLog(@"marcar al %@", phoneCallNum);
         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
    }

}
- (IBAction)startAndStopAlert:(UIButton *)sender {
    [self clearSelection:sender];
    if (self.btnAlert.isSelected) {
        ///desactivar alerta pedir pin de seguridad y motivo de alerta
        CheckPin* checkPin = [[CheckPin alloc] init];
        [checkPin checkPinWithController:self Callback:^(BOOL complete) {
            
            if (complete) {
                //
                DisableAlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"DisableAlert"];
                ContainerVC *_self=self;
                alert.container=_self;
                [alert showOptionsForDisableAlertInController:self];
               
            }else {
               [self.btnAlert setSelected:YES];
            }
            
        }];
       return;
       
    }
    
    NSLog(@"LatLong %@ - %@", [[LocationManagement sharedInstance] latitude], [[LocationManagement sharedInstance] longitude]);
    NSNumber* latitude = [[LocationManagement sharedInstance] latitude];
    NSNumber* longitude = [[LocationManagement sharedInstance] longitude];
    
    Profile* user = [Profile findWithUserId:[UserServices getUserId]];
    if (user.phoneAlertCalls!=nil&&user.countryCode!=nil) {
        NSString* phoneCallNum = [NSString stringWithFormat:@"tel://%@%@", user.countryCode,user.phoneAlertCalls];
        NSLog(@"marcar al %@", phoneCallNum);
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
    }

    [AlertService activeAlertWithLatitude:latitude AndLonguitude:longitude Wait:NO Delegate:self];

}



-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    NSDictionary *dicResult;
    int responseCode = -1;
    switch (service) {
        case kSEND_ALERT:
            dicResult = [dictionary objectForKey:@"SendAlertResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            switch (responseCode) {
                case 200: {
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:1];
                    [profile mergeObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.btnAlert setSelected:YES];
                        self.isDisableAlert=NO;
                    });
                }
                    break;
                case 409: { // Ya existe una alerta activa
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:1];
                    [profile mergeObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                    [self.btnAlert setSelected:YES];
                        self.isDisableAlert=NO;
                    });
                    
                }
                    break;
                case 206: {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                        [alert showWithTitle:nil Message:NSLocalizedString(@"alerta.activa.sin.contactos", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                    });
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:1];
                    [profile mergeObject];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.btnAlert setSelected:YES];
                        self.isDisableAlert=NO;
                    });
                  
                    
                }
                    break;
                case 405:{
                   dispatch_async(dispatch_get_main_queue(), ^{
                   AppDelegate* app = [[UIApplication sharedApplication] delegate];
                        [app forceCloseSession];
                        });
                        //enviar a login
                }
                    break;
                
                default:
                    break;
            }
            break;
        case kDISABLE_ALERT:
            dicResult = [dictionary objectForKey:@"DisableAlertResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            switch (responseCode) {
                case 200: {
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:0];
                    [profile mergeObject];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.btnAlert setSelected:NO];
                        self.isDisableAlert=YES;
                                    });
                }
                    break;
                case 205: {
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:0];
                    [profile mergeObject];
                   
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.btnAlert setSelected:NO];
                        self.isDisableAlert=YES;
                    });
                    dispatch_async(dispatch_get_main_queue(), ^{
                        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                        [alert showWithTitle:nil Message:NSLocalizedString(@"alerta.desactiva.sin.contactos", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                                               [self.btnAlert setSelected:NO];
                    });
                } case 404: {
                    Profile *profile = [Profile findWithUserId:[Service getUserId]];
                    profile.status =[NSNumber numberWithInt:0];
                    [profile mergeObject];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.btnAlert setSelected:NO];
                        self.isDisableAlert=YES;
                    });
                }
                    break;
                case 405:{
                    
                    AppDelegate* app = [[UIApplication sharedApplication] delegate];
                        [app closeSession];
                  
                    //enviar a login
                }
                    break;
                default:
                    break;
            }
            break;
        
        case kGET_SECURITY_ZONES:
            dictionary = [dictionary objectForKey:@"SecureZonesByUserResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayZones = [dictionary objectForKey:@"CriticalZones"];
                NSArray * arrayZonesInBD= [SecurityZone findAll];
                for (SecurityZone* zone in arrayZonesInBD) {
                    [zone deleteObject];
                }
                for (NSDictionary* zoneData in arrayZones) {
                    [self saveZone:zoneData];
                }
            }
            if (responseCode==405) {
            AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kGET_INSECURITY_ZONES:
            
            dictionary = [dictionary objectForKey:@"InsecureZonesByUserResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayZones = [dictionary objectForKey:@"InsecureZones"];
                NSArray * arrayZonesInBD= [InsecureZone findAll];
                for (InsecureZone* zone in arrayZonesInBD) {
                    [zone deleteObject];
                }
                for (NSDictionary* zoneData in arrayZones) {
                    [self saveInsecureZone:zoneData];
                }
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;

        case kHAS_COMMENT:
            dicResult = [dictionary objectForKey:@"HasCommentedResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];

            if (responseCode==407) {
               //que comente
                ReviewVC* review =[ReviewVC shareInstance];
                [review plusCount:20];
                
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kGET_CONTACT_LIST:
            dictionary = [dictionary objectForKey:@"ContactListResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayFriend = [dictionary objectForKey:@"UserList"];
                NSArray * arrayFriendsInBD= [Friend findAll];
                for (Friend* friend in arrayFriendsInBD) {
                    [friend deleteObject];
                }
                for (NSDictionary* friendData in arrayFriend) {
                    [self saveFriend:friendData];
                }
                self.friendCompleteUpdate=YES;
                
               [self updatepins];
               
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kGET_ALL_OCCURRENCES:
            dictionary = [dictionary objectForKey:@"AllOccurrencesResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayOccurrence = [dictionary objectForKey:@"OccurrenceList"];
                NSArray * arrayOccurrenceInBD= [Occurrence findAll];
                for (Occurrence* occurrence in arrayOccurrenceInBD) {
                    [occurrence deleteObject];
                }
                for (NSDictionary* occurrenceData in arrayOccurrence) {
                    [self saveOccurrence:occurrenceData];
                }
                self.occurrencesUpdated=YES;
                [self updatepins];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kGET_SOME_DATAFRIEND:
            dictionary = [dictionary objectForKey:@"MapContactListResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSArray* arrayFriend = [dictionary objectForKey:@"UserList"];
                for (NSDictionary* friendData in arrayFriend) {
                    [self saveFriendDataWithoutPhoto:friendData];
                }
                self.friendsUpdated=YES;
                [self updatepins];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
          break;
    
        default:
            break;
    }
}
-(void)updatepins{
    if (self.friendsUpdated&&self.occurrencesUpdated) {
         [self.mapLocationView updateContacts];
        self.friendsUpdated=NO;
        self.occurrencesUpdated=NO;
        NSArray* messages= [Message findAll];
        if ([messages count]<=0) {
            [[GetChatList shareInstance] updateData];
        }
    }else if (self.friendCompleteUpdate){
        [self.mapLocationView updateContacts];
        self.friendCompleteUpdate=NO;
         NSArray* messages= [Message findAll];
        if ([messages count]<=0) {
            [[GetChatList shareInstance] updateData];
        }
    }

}
-(void)saveFriendDataWithoutPhoto:(NSDictionary*)friendData{
    NSArray* result = [Friend findWithUserId:[friendData objectForKey:@"UserId"]];
    Friend* friend;
    if (result && [result count] > 0) {
        friend = [result objectAtIndex:0];
    }else{
        friend = [Friend createObject];
        [friend saveObject];
       
        //return;
    }
     friend.userId=[friendData objectForKey:@"UserId"];
    friend.batteryLevel = [NSNumber numberWithFloat:[[friendData objectForKey:@"BatteryLevel"] floatValue]];
    friend.currentLatitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Latitude"] doubleValue]];
    friend.currentLongitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Longitude"] doubleValue]];
     friend.status = [NSNumber numberWithInt:[[friendData objectForKey:@"Status"] intValue]];
    friend.positionDate =[NSDate dateWithTimeIntervalSince1970:[[friendData objectForKey:@"PositionDate"] doubleValue]/1000];
    NSString * name;
    NSString * userName;
    NSString * lastname;
    if ([[friendData objectForKey:@"Name"] isEqualToString:@""]||[[friendData objectForKey:@"Name"]isEqual:[NSNull null]]) {
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]){
                if (self.languaje==1) {
                    name= @"Desconocido";
                    lastname = @"";
                }else{
                    name= @"Unknown";
                    lastname = @"";
                }
                
            }else{
                name=[friendData objectForKey:@"LastName"];
                userName= [friendData objectForKey:@"LastName"];
                lastname=@"";
                
            }
        } else{
            
            name=[friendData objectForKey:@"Username"];
            userName=[friendData objectForKey:@"Username"];
        }
    }else {
        name=[friendData objectForKey:@"Name"];
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            userName=name;
        }else{
            userName = [friendData objectForKey:@"Username"];
        }
    }
    
    if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]) {
        lastname = @"";
    }else{
        lastname=[friendData objectForKey:@"LastName"];
    }
    friend.lastName = lastname;
    friend.name = name;
    friend.userName = userName;
    
      [friend mergeObject];
 
}

/**
 * Save the friend data on the data base.
 */
- (void) saveFriend:(NSDictionary*)friendData {
    NSArray* result = [Friend findWithUserId:[friendData objectForKey:@"UserId"]];
    Friend* friend;
    if (result && [result count] > 0) {
        friend = [result objectAtIndex:0];
    } else {
        friend = [Friend createObject];
        [friend saveObject];
    }
    friend.userId = [friendData objectForKey:@"UserId"];
    friend.batteryLevel = [NSNumber numberWithFloat:[[friendData objectForKey:@"BatteryLevel"] floatValue]];
    friend.currentLatitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Latitude"] doubleValue]];
    friend.currentLongitude = [NSNumber numberWithDouble:[[friendData objectForKey:@"Longitude"] doubleValue]];
    NSNumber* gender = [NSNumber numberWithBool:[[friendData objectForKey:@"Gender"] isEqualToString:@"1"] ? YES : NO];
    friend.gender = gender;
    NSString * name;
    NSString * userName;
    NSString * lastname;
    if ([[friendData objectForKey:@"Name"] isEqualToString:@""]||[[friendData objectForKey:@"Name"]isEqual:[NSNull null]]) {
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]){
                if (self.languaje==1) {
                    name= @"Desconocido";
                    lastname = @"";
                }else{
                    name= @"Unknown";
                    lastname = @"";
                }
                
            }else{
                name=[friendData objectForKey:@"LastName"];
                userName= [friendData objectForKey:@"LastName"];
                lastname=@"";
            }
        } else{
            name=[friendData objectForKey:@"Username"];
            userName=[friendData objectForKey:@"Username"];
        }
    }else {
        name=[friendData objectForKey:@"Name"];
        if([[friendData objectForKey:@"Username"] isEqualToString:@""]||[[friendData objectForKey:@"Username"] isEqual:[NSNull null]]){
            userName=name;
        }else{
            userName = [friendData objectForKey:@"Username"];
        }
    }
    
    if ([[friendData objectForKey:@"LastName"] isEqualToString:@""]||[[friendData objectForKey:@"LastName"] isEqual:[NSNull null]]) {
        lastname = @"";
    }else{
        lastname=[friendData objectForKey:@"LastName"];
    }
    friend.lastName = lastname;
    friend.name = name;
    friend.userName = userName;
    friend.status = [NSNumber numberWithInt:[[friendData objectForKey:@"Status"] intValue]];
    friend.permissions =[friendData objectForKey:@"Permission"];
    friend.positionDate =[NSDate dateWithTimeIntervalSince1970:[[friendData objectForKey:@"PositionDate"] doubleValue]/1000];
    
    if ([friendData objectForKey:@"ProfileImageURL"] && ![[friendData objectForKey:@"ProfileImageURL"] isEqual:[NSNull null]]) {
        // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^ {
        NSData *img = [NSData dataWithContentsOfURL:[NSURL URLWithString:[friendData objectForKey:@"ProfileImageURL"]]];
        friend.pictureProfile = img;
        // });
    } else {
      NSData* pictureData = UIImageJPEGRepresentation([UIImage imageNamed:@"avatar"], 0.5);
        friend.pictureProfile=pictureData;
    }
    NSArray *emails = [friendData objectForKey:@"Email"];
    for (Email *email in friend.emails) {
        [email deleteObject];
    }
    [friend removeEmails:friend.emails];
    for (NSString *email in emails) {
        Email* oEmail = [Email createObject];
      
        oEmail.email = email;
        oEmail.people = friend;
          [oEmail saveObject];
        [friend addEmailsObject:oEmail];
    }
    for (Phone *phone in friend.phones) {
        [phone deleteObject];
    }
    NSArray *phones = [friendData objectForKey:@"Telephone"];
    [friend removePhones:friend.phones];
    for (NSString *phone in phones) {
        Phone* oPhone = [Phone createObject];
      
        oPhone.phone = phone;
        oPhone.people = friend;
          [oPhone saveObject];
        [friend addPhonesObject:oPhone];
    }
   [friend mergeObject];
}

-(void) saveOccurrence:(NSDictionary *) occurrenceData{
    NSArray* result = [Occurrence findWithOccurrenceId:[occurrenceData objectForKey:@"OccurrenceId"]];
    Occurrence * occurrence;
    if (result!=nil && [result count] > 0) {
        occurrence = [result objectAtIndex:0];
    } else {
        occurrence = [Occurrence createObject];
        [occurrence saveObject];
    }
    occurrence.occurrenceId = [NSString stringWithFormat:@"%@",[occurrenceData objectForKey:@"OccurrenceId"]];
    occurrence.latitude =[NSNumber numberWithDouble:[[occurrenceData objectForKey:@"Latitude"] doubleValue]];
    occurrence.longitude =[NSNumber numberWithDouble:[[occurrenceData objectForKey:@"Longitude"] doubleValue]];
    
    occurrence.type=[NSNumber numberWithInt:[[occurrenceData objectForKey:@"Type"] intValue]];
    occurrence.isSpecial= [occurrenceData objectForKey:@"isSpecial"];
    occurrence.createdUserId=[NSString stringWithFormat:@"%@",[occurrenceData objectForKey:@"CreatedUserId"]];
    [occurrence mergeObject];
    
}

-(void)getSecurityZones{
   
    [SecurityZonesServices getUserSecurityZonesWait:NO Delegate:self];
    [InsecureZoneServices getUserInsecurityZonesWait:NO Delegate:self];
    
}
- (void) saveZone:(NSDictionary*) dictionary {
    NSArray * result = [SecurityZone findWithZoneId:[dictionary objectForKey:@"idZone"]];
    SecurityZone* zone;
    if (result && [result count] > 0) {
        zone = [result objectAtIndex:0];
    } else {
        zone = [SecurityZone createObject];
        [zone saveObject];
    }
    
    zone.name = [dictionary objectForKey:@"Name"];
    
    NSString* radius =[dictionary objectForKey:@"Radius"];
    zone.radius = [NSNumber numberWithDouble:[radius doubleValue]];
    
    NSString* latitude =[dictionary objectForKey:@"Latitude"];
    zone.latitude =[NSNumber numberWithDouble:[latitude doubleValue]];
    
    NSString* longitude =[dictionary objectForKey:@"Longitude"];
    zone.longitude = [NSNumber numberWithDouble:[longitude doubleValue]];
    
    zone.securitiZoneId = [[dictionary objectForKey:@"idZone"] stringValue];
    [zone mergeObject];
}
- (void) saveInsecureZone:(NSDictionary*) dictionary {
    NSArray * result = [InsecureZone findWithZoneId:[dictionary objectForKey:@"idZone"]];
    InsecureZone* zone;
    if (result && [result count] > 0) {
        zone = [result objectAtIndex:0];
    } else {
        zone = [InsecureZone createObject];
        [zone saveObject];
    }
    
    zone.name = [dictionary objectForKey:@"Name"];
    
    NSString* radius =[dictionary objectForKey:@"Radius"];
    zone.radius = [NSNumber numberWithDouble:[radius doubleValue]];
    
    NSString* latitude =[dictionary objectForKey:@"Latitude"];
    zone.latitude =[NSNumber numberWithDouble:[latitude doubleValue]];
    
    NSString* longitude =[dictionary objectForKey:@"Longitude"];
    zone.longitude = [NSNumber numberWithDouble:[longitude doubleValue]];
    
    zone.insecureZoneId = [[dictionary objectForKey:@"idZone"] stringValue];
    double diference=[[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
    int hourInitial= [[[dictionary objectForKey:@"FirstTime"] substringToIndex:2] intValue];
    int minuteInitial= [[[dictionary objectForKey:@"FirstTime"] substringFromIndex:MAX((int)[[dictionary objectForKey:@"FirstTime"]length]-2, 0)] intValue];
    int hourFinal= [[[dictionary objectForKey:@"LastTime"] substringToIndex:2] intValue];
    int minuteFinal= [[[dictionary objectForKey:@"LastTime"] substringFromIndex:MAX((int)[[dictionary objectForKey:@"FirstTime"]length]-2, 0)] intValue];
   
    hourInitial= hourInitial+(diference);
    if (hourInitial<0) {
        hourInitial=hourInitial+24;
    }
    hourFinal=hourFinal+(diference);
    if (hourFinal<0) {
        hourFinal=hourFinal+24;
    }
    NSString * timeHrFirst;
    NSString * timeMinFirst;
    NSString * timeHrFinal;
    NSString * timeMinFinal;
    if (hourInitial<10) {
        timeHrFirst=[NSString stringWithFormat:@"0%d",hourInitial];
    }
    else{
        timeHrFirst= [NSString stringWithFormat:@"%d",hourInitial];
    }
    if (minuteInitial<10) {
        timeMinFirst=[NSString stringWithFormat:@"0%d",minuteInitial];
    }
    else{
        timeMinFirst= [NSString stringWithFormat:@"%d",minuteInitial];
    }
    if (hourFinal<10) {
        timeHrFinal=[NSString stringWithFormat:@"0%d",hourFinal];
    }
    else{
        timeHrFinal= [NSString stringWithFormat:@"%d",hourFinal];
    }
    if (minuteFinal<10) {
        timeMinFinal=[NSString stringWithFormat:@"0%d",minuteFinal];
    }
    else{
        timeMinFinal=[NSString stringWithFormat:@"%d",minuteFinal];
    }
    zone.timeInitial = [NSString stringWithFormat:@"%@:%@",timeHrFirst,timeMinFirst];
    zone.timeFinal = [NSString stringWithFormat:@"%@:%@",timeHrFinal,timeMinFinal];
    zone.isOwner=[dictionary objectForKey:@"IsOwner"];
    [zone mergeObject];
}


- (void)errorResponseWithService:(kService)service Error:(NSError *)error {
    // Si ocurrio algun problema con el elvio de las posiciones, dejar el array con las posiciones que ya tenia para enviarlos posteriormente.
    NSLog(@"Error services: %@", error);
}



@end
