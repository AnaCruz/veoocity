//
//  Service.m
//  secapp
//
//  Created by iOS on 29/12/14.
//  Copyright (c) 2014 SecApp. All rights reserved.
//

#import "Service.h"
#import <UIKit/UIKit.h>
#import "Location.h"
//
////#define URLService @"https://api.secapp.co/WebServices/APIv2.svc/" //produccion con certificado
//#define URLService @"https://testing.secapp.co/WebServices/APIv2.svc/" //testing con certificado

//#define URLService @"http://secappapi.cloudapp.net/WebServices/APIv2.svc/" //produccion sin certificado
#define URLService @"http://14871167ed6f457e90b658a1542fdf98.cloudapp.net/WebServices/APIv2.svc/" //testing sin certificado
#define kAppVersion [NSBundle.mainBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"]
#define kBatteryLevel ([UIDevice currentDevice].batteryLevel * 100)

@implementation Service
#pragma mark Create Comment Methods
+ (void) sendComment:(NSString*) comment WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@SendComment", URLService];
    NSDictionary* dataDic = @{
                              @"UserId":[self getUserId],
                              @"SessionId":[self getSessionId],
                              @"comment":comment
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kSEND_COMMENT Wait:wait Delegate:delegate];
    NSData* paramsData = [self parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}

+ (void) hasCommentWithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@HasCommented/%@/%@", URLService, [self getUserId],[self getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kHAS_COMMENT Wait:wait Delegate:delegate];
    [request execute];
}

#pragma mark Utils Service

/**
 * parse Object to JSON
 * @param object Object to parse a JSON.
 * @return Data with the JSON.
 */
+ (NSData*) parsingWithObject:(id) object {
    NSError *parsingError = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:&parsingError];
    if (parsingError) {
        NSLog(@"Service: Error de parse de datos./n%@", [parsingError localizedDescription]);
        return nil;
    }
    return data;
}

+ (NSNumber*) getBatteryLevel {
    if (![[UIDevice currentDevice] isBatteryMonitoringEnabled]) {
        [[UIDevice currentDevice] setBatteryMonitoringEnabled:YES];
    }
    NSNumber* level = [NSNumber numberWithFloat:kBatteryLevel];
    if (level && [level floatValue] <0) {
        level = [NSNumber numberWithFloat:0];
    }
    return level;
}

+ (NSString*) getUserId { return [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"]; }
+ (NSString*) getSessionId { return [[NSUserDefaults standardUserDefaults] objectForKey:@"sessionId"]; }
+ (NSString*) getUrl {return URLService;}
+ (NSString*) getAppVersion{ return kAppVersion;}
+ (NSString*) getLanguaje{
    NSString*identifier;
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    if ([language isEqualToString:@"es"]) {
        identifier= @"1";
    }
   else if ([language isEqualToString:@"en"]) {
        identifier= @"2";
    }
   else{
        identifier= @"2";
   }
    return identifier;
}

@end
