//
//  CustomAnnotation.m
//  secapp
//
//  Created by SecApp on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "CustomAnnotation.h"

@implementation CustomAnnotation


@synthesize title, subtitle, coordinate;


- (id)initWithTitle:(NSString *)aTitle subtitle:(NSString*)aSubtitle andCoordinate:(CLLocationCoordinate2D)coord
{
    self = [super init];
    title = aTitle;
    subtitle = aSubtitle;
    coordinate = coord;
    return self;
}
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate {
    coordinate = newCoordinate;
}

-(CLLocationCoordinate2D)coord
{
    return coordinate;
}
@end
