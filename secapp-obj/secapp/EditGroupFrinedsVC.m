////
////  EditGroupFrinedsVC.m
////  secapp
////
////  Created by Marco Antonio Navarro Montoya on 05/08/15.
////  Copyright (c) 2015 SecApp. All rights reserved.
////
//
//#import "EditGroupFrinedsVC.h"
//#import "GroupService.h"
//#import "Group.h"
//#import "Friend.h"
//#import "UserInGroupCell.h"
//#import "UserGroupCell.h"
//#import "StringUtils.h"
//#import "AlertView.h"
//#include <MessageUI/MessageUI.h>
//#import "AppDelegate.h"
//#import "GAI.h"
//#import "GAIFields.h"
//#import "GAITracker.h"
//#import "GAIDictionaryBuilder.h"
//
//@interface EditGroupFrinedsVC () <UITableViewDataSource, UITableViewDelegate, ResponseDelegate, AlertViewDelegate, MFMessageComposeViewControllerDelegate> {
//    
//}
//
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property (weak, nonatomic) IBOutlet UITextField *inNameGroup;
//@property (weak, nonatomic) IBOutlet UITextField *inUser;
//
//@property (weak, nonatomic) IBOutlet UIButton *backGroup;
//
//@property NSArray* friends;
//@property NSArray* users;
//@property NSArray* usersInGroup;
//// Temporal para saber que usuarios invitar
//@property NSArray* tmpUsers;
//
//// Temporal
//@property NSArray* tmpEmails;
//@property NSArray* tmpPhones;
//@property NSArray* tmpFriends;
//@property NSArray* ids;
//@property NSIndexPath * tmpIndexPath;
//@property Friend * tmpFriend;
//@end
//
//@implementation EditGroupFrinedsVC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.friends = [Friend findAll];
//    if (!self.friends) {
//        self.friends = [NSArray array];
//    }
//    self.users = [self.friends copy];
//    self.usersInGroup = [self.group.friends allObjects];
//
//    self.usersInGroup = self.usersInGroup ? self.usersInGroup :[NSArray array];
//    
//    [self filterUsers];
//    [self.tableView reloadData];
//    [self.backGroup setTitle:self.group.name forState:UIControlStateNormal];
//}
//
//- (void) filterUsers {
//    NSMutableArray* tmpUsers = [self.users mutableCopy];
//    for (id friend in self.usersInGroup) {
//        [tmpUsers removeObject:friend];
//    }
//    self.users = [tmpUsers copy];
//}
//
//- (IBAction)saveGroup:(id)sender {
//    if(![self.inNameGroup.text isEqualToString:@""] && ![self.inNameGroup.text isEqualToString:self.group.name]) {
//        [GroupService updateGroupWithGroupId:self.group.groupId GroupName:self.inNameGroup.text Wait:YES Delegate:self];
//    } else {
//        [self saveFriends];
//    }
//}
//
//- (void) saveFriends {
//    self.tmpUsers = [self.usersInGroup copy];
//    NSMutableArray* tmpUsers = [self.tmpUsers mutableCopy];
//    for (id friend in [self.group.friends allObjects]) {
//        [tmpUsers removeObject:friend];
//    }
//    self.tmpUsers = [tmpUsers copy];
//    
//    
//    NSMutableArray* tmpFriends = [NSMutableArray array];
//    NSMutableArray* tmpPhones = [NSMutableArray array];
//    NSMutableArray* tmpEmails = [NSMutableArray array];
//    
//    // Separo por tipo la lista del usuarios en el grupo
//    
//    for (id friend in self.tmpUsers) {
//        if ([friend isKindOfClass:Friend.class]) {
//            [tmpFriends addObject:friend];
//        } else {
//            NSDictionary* data = friend;
//            if ([[data objectForKey:@"type"] intValue] == 1) {
//                [tmpEmails addObject:friend];
//            } else {
//                [tmpPhones addObject:friend];
//            }
//        }
//    }
//    
//    self.tmpPhones = [tmpPhones copy];
//    self.tmpEmails = [tmpEmails copy];
//    self.tmpFriends = [tmpFriends copy];
//    
//    [self filterData];
//    
//    [self saveContacts];
//}
//
//- (void) saveContacts {
//    if ([self.tmpFriends count] > 0) {
//        // Friends
//        [GroupService sendInvitationExistsUsersWithGroupId:self.group.groupId UsersId:self.tmpFriends Wait:YES Delegate:self];
//    } else if ([self.tmpEmails count] > 0) {
//        // Emails
//        [GroupService sendInvitationWithGroupId:self.self.group.groupId Data:self.tmpEmails IsPhone:NO Wait:YES Delegate:self Extra:nil];
//    } else if ([self.tmpFriends count]>0) {
//        // Phones
//        [GroupService sendInvitationWithGroupId:self.group.groupId Data:self.tmpPhones IsPhone:NO Wait:YES Delegate:self Extra:self.tmpPhones];
//    } else {
//        [self showAlertSaveChanges];
//    }
//}
//
//- (void) filterData {
//    NSMutableArray* tmpFriends = [NSMutableArray array];
//    for (Friend* friend in self.tmpFriends) {
//        [tmpFriends addObject:friend.userId];
//    }
//    // Emails
//    NSMutableArray* tmpEmails = [NSMutableArray array];
//    for (NSDictionary* friend in self.tmpEmails) {
//        if ([[friend objectForKey:@"type"] intValue] == 1) {
//            [tmpEmails addObject:[friend objectForKey:@"data"]];
//        }
//    }
//    // Phones
//    NSMutableArray* tmpPhones = [NSMutableArray array];
//    for (NSDictionary* friend in self.tmpPhones) {
//        if ([[friend objectForKey:@"type"] intValue] == 2) {
//            [tmpPhones addObject:[friend objectForKey:@"data"]];
//        }
//    }
//    self.tmpFriends = [tmpFriends copy];
//    self.tmpPhones = [tmpPhones copy];
//    self.tmpEmails = [tmpEmails copy];
//}
//
//- (IBAction)addUserInGroup:(id)sender {
//    int type = -1;
//    if ([StringUtils validateEmail:self.inUser.text]) {
//        type = 1;
//    } else if ([StringUtils validateNumberPhone:self.inUser.text]) {
//        type = 2;
//    } else {
//        return;
//    }
//    
//    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//    
//    NSDictionary* data = @{@"data": self.inUser.text,
//                           @"type": @(type)};
//    
//    NSMutableArray* usersInGroup = [self.usersInGroup mutableCopy];
//    
//    [usersInGroup addObject:data];
//    
//    self.usersInGroup = [usersInGroup copy];
//    
//    int newRow = (int)[self.usersInGroup count];
//    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:0]];
//    self.inUser.text=@"";
//    [self.tableView beginUpdates];
//    [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView endUpdates];
//}
//
//- (IBAction)deleteGroup:(id)sender {
//    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//    [alert showWithTitle:nil Message:NSLocalizedString(@"eliminar.grupo", nil) Delegate:self CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
//}
//
//- (IBAction)cancelUpdates:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (IBAction)goBackView:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (IBAction)searchFriend:(UITextField*)sender {
//    if ([sender.text isEqualToString:@""]) {
//        self.users = self.friends;
//        NSMutableArray* tmp = [self.users mutableCopy];
//        for (Friend* f in self.usersInGroup) {
//            [tmp removeObject:f];
//        }
//        self.users = [tmp copy];
//        [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
//        return;
//    }
//    NSString* data = sender.text;
//    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name CONTAINS '%@'", data]];
//    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"lastName CONTAINS '%@'", data]];
//    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(SUBQUERY(emails, $emails, $emails.email CONTAINS '%@').@count) > 0",data]];
//    NSPredicate *predicate4 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(SUBQUERY(phones, $phones, $phones.phone CONTAINS '%@').@count) > 0",data]];
//    NSPredicate *predicate5 = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate, predicate2, predicate3, predicate4]];
//    self.users = [self.friends filteredArrayUsingPredicate:predicate5];
//    
//    NSMutableArray* tmp = [self.users mutableCopy];
//    for (Friend* f in self.usersInGroup) {
//        [tmp removeObject:f];
//    }
//    self.users = [tmp copy];
//    
//    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
//}
//
//- (void)successResponseWithService:(kService)service Response:(id)response {
//    NSError *parsingError = nil;
//    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
//    if (parsingError) {
//        NSLog(@"Error: %@", parsingError.description);
//        return;
//    }
//    int responseCode = -1;
//    switch (service) {
//        case kDELETE_GROUP:
//            dictionary = [dictionary objectForKey:@"DeleteGroupResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                [self.group deleteObject];
//                [self.navigationController popViewControllerAnimated:YES];
//            }
//            break;
//        case kUPDATE_GROUP:
//            dictionary = [dictionary objectForKey:@"UpdateGroupNameResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                self.group.name = self.inNameGroup.text;
//                [self.group mergeObject];
//                [self.backGroup setTitle:self.group.name forState:UIControlStateNormal];
//                [self saveFriends];
//                [self showAlertSaveChanges];
//            }
//            break;
//        case kINVITATION_EXISTS_USERS:
//            dictionary = [dictionary objectForKey:@"SendInvitationExistsUsersResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Viral"
//                                                                                                  action:@"TapInvitarUsuarioSecapp"
//                                                                                                   label:@"Invita usuarios de Secapp"
//                                                                                                   value:nil] build]];
//                // Emails
//                if ([self.tmpEmails count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.group.groupId Data:self.tmpEmails IsPhone:NO Wait:YES Delegate:self Extra:nil];
//                } else if ([self.tmpPhones count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.group.groupId Data:self.tmpPhones IsPhone:NO Wait:YES Delegate:self Extra:self.tmpPhones];
//                } else {
//                    [self showAlertInvitationSended];
//                }
//            }
//            if (responseCode==405) {
//              AppDelegate* app = [[UIApplication sharedApplication] delegate];
//                [app forceCloseSession];
//                //enviar a login
//                
//            }
//            if (responseCode==409) {
//                //sesion cerrada
//                
//                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//                [alert showWithTitle:nil Message:NSLocalizedString(@"usuario.ya.esta.en.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
//                alert.delegate=self;
//             
//                
//            }
//
//
//            break;
//        case kINVITATION:
//            dictionary = [dictionary objectForKey:@"SendInvitationResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                [[GAI sharedInstance].defaultTracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Viral"
//                                                                                                  action:@"TapInvitarUsuarioNuevo"
//                                                                                                   label:@"Invita usuarios nuevos"
//                                                                                                   value:nil] build]];
//                // Phones
//                if ([self.tmpPhones count] > 0) {
//                    [GroupService sendInvitationWithGroupId:self.group.groupId Data:self.tmpPhones IsPhone:NO Wait:YES Delegate:self Extra:self.tmpPhones];
//                } else {
//                    [self showAlertInvitationSended];
//                }
//            }
//            if (responseCode==405) {
//             AppDelegate* app = [[UIApplication sharedApplication] delegate];
//                [app forceCloseSession];
//                //enviar a login
//                
//            }
//            if (responseCode==409) {
//               
//                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//                [alert showWithTitle:nil Message:NSLocalizedString(@"usuario.ya.esta.en.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
//                alert.delegate=self;
//             
//            }
//
//            break;
//        case kREMOVE_USER_FROM_GROUP:
//            dictionary = [dictionary objectForKey:@"RemoveUserFromGroupResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                NSLog(@"Contacto eliminado");
//                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//                NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//                NSMutableArray* users = [self.users mutableCopy];
//                NSMutableArray* usersInGroup = [self.usersInGroup mutableCopy];
//                
//                [usersInGroup removeObject:self.tmpFriend];
//                if ([self.tmpFriend isKindOfClass:Friend.class]) {
//                    [users addObject:self.tmpFriend];
//                    self.users = [users copy];
//                    int newRow = (int)[self.users count];
//                    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:1]];
//                }
//                self.usersInGroup = [usersInGroup copy];
//                
//                [indexPathsToDelete addObject:self.tmpIndexPath];
//                
//                [self.tableView beginUpdates];
//                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView endUpdates];
//            }
//            if (responseCode==403) {
////                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
////                [alert showWithTitle:nil Message:@"El contacto que desea eliminar no ha aceptado unirse al grupo" Delegate:self CancelTitle:@"Aceptar" OkTitle:nil InController:self.navigationController.parentViewController];
//                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//                NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//                NSMutableArray* users = [self.users mutableCopy];
//                NSMutableArray* usersInGroup = [self.usersInGroup mutableCopy];
//                
//                [usersInGroup removeObject:self.tmpFriend];
//                if ([self.tmpFriend isKindOfClass:Friend.class]) {
//                    [users addObject:self.tmpFriend];
//                    self.users = [users copy];
//                    int newRow = (int)[self.users count];
//                    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:1]];
//                }
//                self.usersInGroup = [usersInGroup copy];
//                
//                [indexPathsToDelete addObject:self.tmpIndexPath];
//                
//                [self.tableView beginUpdates];
//                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//                [self.tableView endUpdates];
//
//            }
//                  break;
//
//        default:
//            break;
//    }
//}
//
//-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
//    NSError *parsingError = nil;
//    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
//    if (parsingError) {
//        NSLog(@"Error: %@", parsingError.description);
//        return;
//    }
//    int responseCode = -1;
//    switch (service) {
//        case kINVITATION:
//            dictionary = [dictionary objectForKey:@"SendInvitationResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                NSMutableArray* ids = [NSMutableArray array];
//                for (NSDictionary* data in (NSArray*)[dictionary objectForKey:@"Ids"]) {
//                    [ids addObject:[data objectForKey:@"Id"]];
//                }
//                self.ids = [ids copy];
//                [self sendMessages];
//            }
//            if (responseCode==405) {
//              AppDelegate* app = [[UIApplication sharedApplication] delegate];
//                [app forceCloseSession];
//                //enviar a login
//                
//            }
//            if (responseCode==409) {
//                
//                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//                [alert showWithTitle:nil Message:NSLocalizedString(@"usuario.ya.esta.en.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
//                alert.delegate=self;
//                
//            }
//
//            break;
//               default:
//            break;
//    }
//}
//
//- (void) sendMessages {
//    if(![MFMessageComposeViewController canSendText]) {
//        return;
//    }
//    NSMutableArray* tmpPhones = [self.tmpPhones mutableCopy];
//    NSMutableArray* tmpIds = [self.ids mutableCopy];
//    
//    NSArray *recipents = @[[tmpPhones objectAtIndex:0]];
//    int languaje= [[Service getLanguaje]intValue];
//    NSString *message;
//    if (languaje==1) {
//    message = [NSString stringWithFormat:@"Baja SecApp aqui http://goo.gl/0XM8ew y unete a mi red de seguridad ingresando el siguiente código: %@", [tmpIds objectAtIndex:0]];
//    }else{
//    message = [NSString stringWithFormat:@"Download SecApp here http://goo.gl/0XM8ew and join in my security network entering the next code: %@", [tmpIds objectAtIndex:0]];
//               }
//    
//    
//    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
//    messageController.messageComposeDelegate = self;
//    [messageController setRecipients:recipents];
//    [messageController setBody:message];
//    
//    [tmpPhones removeObject:[tmpPhones objectAtIndex:0]];
//    [tmpIds removeObject:[tmpIds objectAtIndex:0]];
//    
//    self.tmpPhones = [tmpPhones copy];
//    self.ids = [tmpIds copy];
//    
//    // Present message view controller on screen
//    [self presentViewController:messageController animated:YES completion:nil];
//    
//}
//
//- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
//    switch (result) {
//        case MessageComposeResultCancelled:
//            break;
//        case MessageComposeResultFailed:
//            break;
//        case MessageComposeResultSent:
//            break;
//        default:
//            break;
//    }
//    [controller dismissViewControllerAnimated:YES completion:^{
//        if ([self.tmpPhones count] > 0 && [self.ids count] > 0) {
//            [self sendMessages];
//        } else {
//            [self showAlertSaveChanges];
//        }
//    }];
//}
//
//- (void) showAlertSaveChanges {
//    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//    [alert showWithTitle:nil Message:NSLocalizedString(@"datos.guardaron.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
//}
//- (void) showAlertInvitationSended {
//    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//    [alert showWithTitle:nil Message:NSLocalizedString(@"invitacion.enviada.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
//}
//
//
//#pragma mark - Table delegate and datasource
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 2;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 1) {
//        return [self.users count];
//    }
//    return [self.usersInGroup count];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell* cell = nil;
//    if (indexPath.section == 0) {
//        id friend = [self.usersInGroup objectAtIndex:indexPath.row];
//        cell = [self tableView:tableView UserInGroup:friend InRowAtIndexPath:indexPath];
//    } else {
//        Friend* friend = [self.users objectAtIndex:indexPath.row];
//        cell = [self tableView:tableView User:friend InRowAtIndexPath:indexPath];
//    }
//    return cell;
//}
//
//- (IBAction)closeKeyboard:(id)sender {
//    [self.view endEditing:YES];
//}
//
//
//
//#pragma mark - Custom cells
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return;
//    }
//    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//    
//    Friend* friend = [self.users objectAtIndex:indexPath.row];
//    NSMutableArray* users = [self.users mutableCopy];
//    NSMutableArray* usersInGroup = [self.usersInGroup mutableCopy];
//    
//    [users removeObject:friend];
//    [usersInGroup addObject:friend];
//    
//    self.users = [users copy];
//    self.usersInGroup = [usersInGroup copy];
//    
//    [indexPathsToDelete addObject:indexPath];
//    
//    int newRow = (int) [self.usersInGroup count];
//    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:0]];
//    
//    [tableView beginUpdates];
//    [tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView endUpdates];
//}
//
//- (UITableViewCell*) tableView:(UITableView*) tableView User:(Friend*) friend InRowAtIndexPath:(NSIndexPath*) indexPath {
//    static NSString* identifier = @"User";
//    UserInGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UserInGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    cell.nameUser.text = [NSString stringWithFormat:@"%@ %@", friend.name, friend.lastName];
//    cell.pictureUser.image = [UIImage imageWithData:friend.pictureProfile];
//    cell.pictureUser.clipsToBounds = YES;
//    cell.pictureUser.layer.cornerRadius = 22;
//    cell.pictureUser.layer.borderWidth = 3;
//    cell.pictureUser.layer.borderColor = [[UIColor whiteColor] CGColor];
//    return cell;
//}
//
//- (UITableViewCell*) tableView:(UITableView*) tableView UserInGroup:(id) friend InRowAtIndexPath:(NSIndexPath*) indexPath {
//    static NSString* identifier = @"UserInGroup";
//    UserInGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UserInGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    if ([friend isKindOfClass:Friend.class]) {
//        Friend *data = friend;
//        cell.nameUser.text = [NSString stringWithFormat:@"%@ %@", data.name, data.lastName];
//        cell.pictureUser.image = [UIImage imageWithData:data.pictureProfile];
//    } else {
//        NSDictionary *data = friend;
//        cell.nameUser.text = [data objectForKey:@"data"];
//    }
//    cell.pictureUser.clipsToBounds = YES;
//    cell.pictureUser.layer.cornerRadius = 22;
//    cell.pictureUser.layer.borderWidth = 3;
//    cell.pictureUser.layer.borderColor = [[UIColor whiteColor] CGColor];
//    cell.deleteUser.tag = indexPath.row;
//    [cell.deleteUser addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
//    return cell;
//}
//
//-(void) deleteUser:(UIButton*) sender {
////    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
////    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//    self.tmpIndexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    
//   self.tmpFriend = [self.usersInGroup objectAtIndex:self.tmpIndexPath.row];
//   
//   // [GroupService removeUserFromGroupWithUserIdRemove:self.tmpFriend.userId Groupid:self.group.groupId Wait:YES Delegate:self];
//    //
//
////    NSMutableArray* users = [self.users mutableCopy];
////    NSMutableArray* usersInGroup = [self.usersInGroup mutableCopy];
////    
////    [usersInGroup removeObject:self.tmpFriend];
////    if ([self.tmpFriend isKindOfClass:Friend.class]) {
////        [users addObject:self.tmpFriend];
////        self.users = [users copy];
////        int newRow = (int)[self.users count];
////        [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:1]];
////    }
////    self.usersInGroup = [usersInGroup copy];
////    
////    [indexPathsToDelete addObject:self.tmpIndexPath];
////    
////    [self.tableView beginUpdates];
////    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
////    [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
////    [self.tableView endUpdates];
//    //borrar usuarios del grupo
//   }
//
//#pragma mark AlertView Delegate
//
//- (void)buttonPressed:(UIButton *)sender {
//    if (sender.tag == 1) {
//        [GroupService deleteGroupsWithGroupId:self.group.groupId Wait:YES Delegate:self];
//        
//    }
//}
//
//@end
