//
//  AppDelegate.h
//  secapp
//
//  Created by iOS on 23/01/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

- (void) goToStart;
- (void) goToSignUp;
- (void) goToLogin;
- (void) goToMainView;
- (void) pushNotifications;

- (void) closeSession;
-(void) forceCloseSession;

@end

