//
//  OccurenceServices.m
//  secapp
//
//  Created by SecApp on 02/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "OccurenceServices.h"

@implementation OccurenceServices
+(void) createOccurrenceType: (NSString *)type Details: (NSString * )details Latitude:(NSString *)latitude Longitude: (NSString *)longitude Image:(NSString *)image Anonymous:(BOOL)anonynous HappenedDay:(NSString *)happenedDay Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@CreateOccurrence", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"OccurrenceInfo": @{
                                 @"Type":type,
                                 @"Description": details,
                                 @"Latitude": latitude,
                                 @"Longitude":longitude,
                                 @"Image": image,
                                 @"Anonymous": @(anonynous),
                                 @"HappenedDay":happenedDay }
                             };
 
 RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_OCCURRENCE Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) getAllOccurrencesWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@AllOccurrences/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_ALL_OCCURRENCES Wait:wait Delegate:delegate];
    [request execute];
}

+(void) findNearOccurrenceLatitude:(NSString *)latitude Longitude: (NSString *)longitude Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@FindOccurrences", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"Latitude": latitude,
                             @"Longitude":longitude,
                            };
  RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kFIND_NEAR_OCCURRENCES Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) getOccurrenceInformationWithOccurrenceId:(NSString*)occurrenceId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@OccurrenceInformation/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], occurrenceId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_OCCURRENCE_INFO Wait:wait Delegate:delegate];
    [request execute];
}
+(void) likeOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@LikeOccurrence", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"OccurrenceId":occurrenceId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kLIKE_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) dislikeOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DislikeOccurrence", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"OccurrenceId":occurrenceId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kDISLIKE_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}

+(void) commentOccurrencesWithOccurrenceId:(NSString *)occurrenceId Comment: (NSString *)comment Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@CommentOccurrence", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"OccurrenceId": occurrenceId,
                             @"Comment":comment
                             };
   RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCOMMENT_OCCURRENCES Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}

+(void) likeCommentOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@LikeComment", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"CommentId":commentId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kLIKE_COMMENT_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) dislikeCommentOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@DislikeComment", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"CommentId":commentId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kDISLIKE_COMMENT_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) tagCommentOccurrencesWithOccurrenceId:(NSString *)occurrenceId UserIdCommented: (NSString *)userIdCommented Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@TagComment", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"OccurrenceId": occurrenceId,
                             @"UserIdCommented":userIdCommented,
                             };
   RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kTAG_COMMENT_OCCURRENCES Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) showNotificationsOccurrencesWithPermission: (BOOL)permission Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ShowOccurrencesNotifications", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"Permission":permission ? @"TRUE": @"FALSE"
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kSHOW_NOTIFICATIONS_OCCURRENNCES Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}


+(void) followOccurrencesWithOccurrenceId: (NSString *)occurrenceId Follow: (BOOL)follow Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@FollowOccurrence", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"OccurrenceId": occurrenceId,
                             @"Follow":follow ? @"TRUE": @"FALSE"
                             };
  RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kFOLLOW_OR_UNFOLLOW_OCCURRENCES Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];

}
//occurrences speacial
/**
 *Get icon and name ocurrence.
 */
+(void) getTypesSpecialOccurrencesWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
        
        NSString* uri = [NSString stringWithFormat:@"%@SpecialOccurrencesType/%@/%@/%ld", [super getUrl], [super getUserId], [super getSessionId], (long)[[super getLanguaje] intValue]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_TYPE_SPECIAL_OCCURRENCE Wait:wait Delegate:delegate];
        [request execute];
   

}

+(void) commentSpecialOccurrencesWithOccurrenceId:(NSString *)occurrenceId Comment: (NSString *)comment Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@CommentSpecialOccurrences", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"Comment":comment,
                             @"OccurrenceId": occurrenceId
                             };
    RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCOMMENT_SPECIAL_OCCURRENCES Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
+(void) getSpecialOccurrenceInformationWithOccurrenceId:(NSString*)occurrenceId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@SpecialOccurrenceInformation/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], occurrenceId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_SPECIAL_OCCURRENCE_INFO Wait:wait Delegate:delegate];
    [request execute];
}
+(void) followUser: (NSString *)userIdToFollow Follow: (BOOL)follow Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    
    NSString * uri = [NSString stringWithFormat:@"%@FollowUser", [super getUrl]];
    NSDictionary* dataDic =@{
                             @"UserId": [super getUserId],
                             @"SessionId": [super getSessionId],
                             @"UserIdtoFollow": userIdToFollow,
                             @"Follow":follow ? @"TRUE": @"FALSE"
                             };
    RESTClient * request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kFOLLOW_OR_UNFOLLOW_USER Wait:wait Delegate:delegate];
    NSData * paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) likeCommentSpecialOccurrenceWithCommentId: (NSString *)commentId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@LikeSpecialOccurrenceComment", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"CommentId":commentId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kLIKE_COMMENT_SPECIAL_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}
+(void) likeSpecialOccurrenceWithOccurrenceId: (NSString *)occurrenceId Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate{
    NSString* uri = [NSString stringWithFormat:@"%@LikeSpecialOccurrence", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"OccurrenceId":occurrenceId
                              
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kLIKE_SPECIAL_OCCURRENCE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    
}


@end
