//
//  DisplayCertifiedIncidencesVC.h
//  secapp
//
//  Created by SECAPP on 15/02/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Occurrence.h"

@interface DisplayCertifiedIncidencesVC : UIViewController

@property (strong) Occurrence * occ;
@property (strong) NSString * occurrenceiD;
@property (strong) NSNumber * typeOccSpecial;
@property BOOL isShowInMap;
- (void) drawInView:(UIView*) view;
- (IBAction)closeView:(id)sender;

@end
