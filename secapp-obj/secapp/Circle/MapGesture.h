//
//  MapGesture.h
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 27/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^TouchEventBlock)(NSSet* touches, UIEvent* event);

@interface MapGesture : UIGestureRecognizer {
    TouchEventBlock touchesBeganCallback;
}

@property(copy) TouchEventBlock touchesBeganCallback;
@property(copy) TouchEventBlock touchesMovedCallback;
@property(copy) TouchEventBlock touchesEndedCallback;

@end
