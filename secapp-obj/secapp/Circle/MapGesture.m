//
//  MapGesture.m
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 27/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import "MapGesture.h"



@implementation MapGesture

@synthesize touchesBeganCallback;
@synthesize touchesEndedCallback;
@synthesize touchesMovedCallback;

-(instancetype)init {
    self = [super init];
    if (self) {
        self.cancelsTouchesInView = NO;
    }
    return self;
}

- (void) touchesBegan:(NSSet*) touches withEvent:(UIEvent*)event {
    if (touchesBeganCallback) {
        touchesBeganCallback(touches,event);
    }
}

- (void) touchesCancelled:(NSSet*) touches withEvent:(UIEvent*)event {
}

- (void) touchesEnded:(NSSet*) touches withEvent:(UIEvent*)event {
    if (touchesEndedCallback) {
        touchesEndedCallback(touches,event);
    }
}

- (void) touchesMoved:(NSSet*) touches withEvent:(UIEvent*)event {
    if (touchesMovedCallback) {
        touchesMovedCallback(touches,event);
    }
}

- (BOOL) canBePreventedByGestureRecognizer:(UIGestureRecognizer*) preventingGestureRecognizer {
    return NO;
}

-(BOOL) canPreventGestureRecognizer:(UIGestureRecognizer*)preventedGestureRecognizer {
    return NO;
}
@end
