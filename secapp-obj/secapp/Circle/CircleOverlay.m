//
//  CircleOverlay.m
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 27/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import "CircleOverlay.h"

@implementation CircleOverlay

@synthesize boundingMapRect = _boundingMapRect;

-(id) initWithCenterCoordinate:(CLLocationCoordinate2D) coordinate Radius:(CLLocationDistance) radius Title:(NSString*) title idZone:(NSString* )idZone ZoneType:(BOOL)zoneType{
    self = [super init];
    if (self) {
        self.coordinate = coordinate;
        self.radius = radius;
        self.titleCircle = title;
        self.idZone= idZone;
        self.zoneType=zoneType;
        _boundingMapRect = [self MKMapRectForCoordinate:self.coordinate Radius:self.radius];
        self.editingCoordinate = NO;
        self.editingRadius = NO;

    }
    return self;
}

-(void)setCoordinate:(CLLocationCoordinate2D)coordinate {
    if (_coordinate.latitude != coordinate.latitude || _coordinate.longitude != coordinate.longitude) {
        _coordinate = coordinate;
        _boundingMapRect = [self MKMapRectForCoordinate:_coordinate Radius:_radius];
    }
}

-(void)setRadius:(CLLocationDistance)radius {
    if (_radius != radius) {
        _radius = radius;
        _boundingMapRect = [self MKMapRectForCoordinate:_coordinate Radius:_radius];
    }
}


- (MKMapRect) MKMapRectForCoordinate:(CLLocationCoordinate2D) coordinate Radius:(CLLocationDistance) radius {
    MKCoordinateRegion r = MKCoordinateRegionMakeWithDistance(coordinate, radius * 2.f, radius * 2.f);
    MKMapPoint a = MKMapPointForCoordinate(CLLocationCoordinate2DMake(r.center.latitude + r.span.latitudeDelta * .5f,
                                                                      r.center.longitude - r.span.longitudeDelta * .5f));
    MKMapPoint b = MKMapPointForCoordinate(CLLocationCoordinate2DMake(r.center.latitude - r.span.latitudeDelta * .5f,
                                                                      r.center.longitude + r.span.longitudeDelta * .5f));
    return MKMapRectMake(MIN(a.x, b.x), MIN(a.y, b.y),
                         ABS(a.x - b.x), ABS(a.y-b.y));
}

-(id)copyWithZone:(NSZone *)zone {
    CircleOverlay *copy = [[CircleOverlay allocWithZone: zone] init];
    copy.coordinate = self.coordinate;
    copy.radius = self.radius;
    copy.editingCoordinate = self.editingCoordinate;
    copy.editingRadius = self.editingRadius;
    copy.titleCircle = self.titleCircle;
    copy.idZone= self.idZone;
    return copy;
}
@end
