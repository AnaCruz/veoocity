//
//  CircleOverlay.h
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 27/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CircleOverlay : NSObject<MKOverlay, NSCopying>

@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) CLLocationDistance radius;
@property (nonatomic, assign) BOOL editingCoordinate;
@property (nonatomic, assign) BOOL editingRadius;


@property NSString* titleCircle;
@property NSString * idZone;
@property BOOL zoneType;
@property BOOL isOwner;

-(id) initWithCenterCoordinate:(CLLocationCoordinate2D) coordinate Radius:(CLLocationDistance) radius Title:(NSString*) title idZone:(NSString* )idZone ZoneType:(BOOL)zoneType;

@end
