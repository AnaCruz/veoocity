//
//  CircleRenderer.h
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 27/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import <MapKit/MapKit.h>
@class CircleOverlay;

@interface CircleRenderer : MKCircleRenderer
- (instancetype)initWithSelectorOverlay:(CircleOverlay *)selectorOverlay;

@end
