//
//  NoticeOfPrivacyVC.m
//  secapp
//
//  Created by SecApp on 07/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "NoticeOfPrivacyVC.h"
#import  "Service.h"

@interface NoticeOfPrivacyVC ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@end

@implementation NoticeOfPrivacyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    int languaje=[[Service getLanguaje]intValue];
    NSString * urls;
    if (languaje==1) {
        urls=@"https://secapp-co-mx.squarespace.com/aviso-de-privacidad";
    }else{
        urls=@"https://secapp-co-mx.squarespace.com/privacy-notice";
    }

    NSURL *url = [NSURL URLWithString:urls];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:requestObj];
}
- (IBAction)close:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
    // [self dismissViewControllerAnimated:YES completion:nil];
}
@end
