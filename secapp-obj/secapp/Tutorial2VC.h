//
//  Tutorial2VC.h
//  secapp
//
//  Created by SecApp on 04/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Tutorial2VC : UIViewController
- (void) drawInView:(UIView*) view;
@end
