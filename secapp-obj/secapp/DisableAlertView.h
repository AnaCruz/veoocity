//
//  DisableAlertView.h
//  secapp
//
//  Created by SecApp on 18/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
@class ContainerVC;

@interface DisableAlertView : UIViewController
@property ContainerVC* container;
- (void) showOptionsForDisableAlertInController:(UIViewController*) controller;
@end
