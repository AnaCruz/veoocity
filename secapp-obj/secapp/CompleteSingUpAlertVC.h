//
//  CompleteSingUpAlertVC.h
//  secapp
//
//  Created by SecApp on 28/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SignUpVC;

@interface CompleteSingUpAlertVC : UIViewController

@property UIViewController* controller;

- (void) showOptionsForCompleteSignUp:(UIViewController*) controller;
@end
