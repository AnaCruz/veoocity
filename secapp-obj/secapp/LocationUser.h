//
//  LocationUser.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 17/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationUser : NSObject

+ (LocationUser*) shareInstance;
- (void) startMonitoring;
- (void) stopMonitoring;

@end
