//
//  CheckPin.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 13/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "CheckPin.h"
#import "AlertViewSingleInput.h"
#import "UserServices.h"
#import "AlertView.h"
#import "AppDelegate.h"

@interface CheckPin() <ResponseDelegate, AlertViewDelegate>

@property (copy) void (^callback)(BOOL complete);
@property AlertViewSingleInput* alert;
@property UIViewController* controller;

@end

@implementation CheckPin

- (void) checkPinWithController:(UIViewController*) controller Callback:(void (^)(BOOL complete)) callback {
    self.callback = callback;
    self.controller = controller;
    self.alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertViewSingleInput"];
    id _tempSelf = self;
    self.alert.callbackOk = ^(NSString* text){
        if ([text isEqualToString:@""]) {
            return;
        }
        [UserServices confirmPassword:text WithWait:YES Delegate:_tempSelf];
    };
    [self.alert showWithTitle:NSLocalizedString(@"pin.seguridad", nil) Message:NSLocalizedString(@"introduce.pin.seguridad", nil) CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:controller];
    [self.alert numberToCharacter:4];
    [self.alert.textField setKeyboardType:UIKeyboardTypeNumberPad];
    [self.alert.textField setSecureTextEntry:YES];
    [self.alert.textField setPlaceholder:NSLocalizedString(@"pin.seguridad", nil)];
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        self.callback(NO);
        return;
    }
    int respondeCode = -1;
    
    if (service == kCONFIRM_PASSWORD) {
        dictionary = [dictionary objectForKey:@"ConfirmPasswordResult"];
        respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
        if (respondeCode == 200) {
            if (self.callback) {
                self.callback(YES);
            }
        } else if (respondeCode == 404 ||respondeCode == 403 ) {
            [self.alert showWithTitle:NSLocalizedString(@"pin.seguridad", nil) Message:NSLocalizedString(@"introduce.pin.seguridad", nil) CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.controller];
        }
        else if (respondeCode == 405) {
          AppDelegate* app = [[UIApplication sharedApplication] delegate];
            [app forceCloseSession];
            
            
        }
    }
}

@end
