//
//  BuyServices.m
//  secapp
//
//  Created by SecApp on 08/09/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "BuyServices.h"



@implementation BuyServices


+(void) buyPremiumVersion:(NSString*) aplicationType ReceiptData:(NSString *)receiptData WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@ApplicationBoughtIOS", [super getUrl]];
    NSDictionary* dataDic = @{
                              @"UserId":[self getUserId],
                              @"SessionId":[self getSessionId],
                              @"ApplicationType": aplicationType,
                              @"ReceiptData":receiptData
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kBUY_APP Wait:wait Delegate:delegate];
    NSData* paramsData = [self parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}
@end
