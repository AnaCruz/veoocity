 //
//  NotificationVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Phone.h"
#import "NotificationVC.h"
#import "Notification.h"
#import "UserNotifications.h"
#import "AlertCell.h"
#import "NewMessageCell.h"
#import "ContainerVC.h"
#import "NotificationCell.h"
#import "InvitationCell.h"
#import "ConversationVC.h"
#import <CoreLocation/CoreLocation.h>
#import "MapSecApp.h"
#import "InfoUserVC.h"
#import "UserNotifications.h"
#import "GroupService.h"
#import "MessagesService.h"
#import "Message.h"
#import "ConvesationGroupVC.h"
#import "ContainerVC.h"
#import "Friend.h"
#import "UserServices.h"
#import "AlertView.h"
#import "AppDelegate.h"
#import "MapLocationVC.h"
#import "DisplayOccurrenceVC.h"
#import "DisplayCertifiedIncidencesVC.h"
#import "StringUtils.h"

@interface NotificationVC () <ResponseDelegate, UITableViewDataSource, UITableViewDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray* listNotification;
@property int languaje;
@property DisplayOccurrenceVC * displayOccurrence;
@property DisplayCertifiedIncidencesVC * displaySpecialOcurrence;


@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.estimatedRowHeight = 120;//the estimatedRowHeight but if is more this autoincremented with autolayout
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    [self.tableView setNeedsLayout];
    [self.tableView layoutIfNeeded];
}
-(void)updateView{
    self.listNotification = [Notification findAll];
    if (!self.listNotification) {
        self.listNotification = [NSArray array];
    }
    if ([self.listNotification count]>0) {
        self.deleteButton.enabled=YES;
    }else{
        self.deleteButton.enabled=NO;
    }
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [self updateView];
    self.languaje=[[Service getLanguaje]intValue];
   }

- (IBAction)closeView:(id)sender {
    if (self.container) {
        [self.container hideNotifications];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.listNotification count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = [self.listNotification objectAtIndex:indexPath.row];
    UITableViewCell * cell = nil;
    switch ([notification.typeId intValue]) {
        case 1: // MaximaAlerta ***
            cell = [self tableView:tableView AlertUser:notification InRowAtIndexPath:indexPath];
            break;
        case 2: //Alerta desactivada ***
            cell = [self tableView:tableView DisableAlert:notification InRowAtIndexPath:indexPath];
            break;
        case 3: // NuevoUsuarioEnGrupo
            cell = [self tableView:tableView InfoToInvitation:notification InRowAtIndexPath:indexPath];;
            break;
        case 4: // Grupo eliminado
            cell = [self tableView:tableView DeleteToGroup:notification InRowAtIndexPath:indexPath];
            break;
        case 5: // Invitacion ***
            cell = [self tableView:tableView Invitation:notification InRowAtIndexPath:indexPath];
            break;
        case 6: // Mensaje nuevo ***
            cell = [self tableView:tableView NewMessage:notification InRowAtIndexPath:indexPath];
            break;
        case 7: // Chat Grupal Eliminado
            cell = [self tableView:tableView DeleteToGroupToChat:notification InRowAtIndexPath:indexPath];
            break;
        case 8: // ***** Agregado a chat grupal
            cell = [self tableView:tableView YouAddToChatGroup:notification InRowAtIndexPath:indexPath];
              //  cell = [self tableView:tableView Generic:notification InRowAtIndexPath:indexPath];
            break;
        case 9: // Nombre de chat grupal Actualizado
            cell = [self tableView:tableView ChangeGroupName:notification InRowAtIndexPath:indexPath];
            break;
        case 10: // Eliminado de chat grupal
            cell = [self tableView:tableView DeleteToGroupToChat:notification InRowAtIndexPath:indexPath];
            break;
        case 11: // Mensaje de monitor
            cell = [self tableView:tableView MessageToMonitor:notification InRowAtIndexPath:indexPath];
            break;
        case 12: // Usuario fuera zona segura
            cell = [self tableView:tableView OutSecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 13: // Usuario entró a zona segura
            cell = [self tableView:tableView IntoSecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 14: // Usuario dentro de zona insegura
            cell = [self tableView:tableView IntoInsecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 15: // Usuario salio de zona insegura
            cell = [self tableView:tableView OutInecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 16: // Zona insegura activada
            cell = [self tableView:tableView ActiveInsecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 17: // Zona insegura desactivada
            cell = [self tableView:tableView DisableInecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 18: // GPS Apagado
            cell = [self tableView:tableView Generic:notification InRowAtIndexPath:indexPath];
            break;
        case 19: // GPS encendio
            cell = [self tableView:tableView Generic:notification InRowAtIndexPath:indexPath];
            break;
        case 20: // Invitacion a ser monitoreado
            cell = [self tableView:tableView Invitation:notification InRowAtIndexPath:indexPath];
            break;
        case 21: // Entro en tu zona insegura
           cell = [self tableView:tableView IntoInsecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 22: // Salio de tu zona insegura
             cell = [self tableView:tableView OutInecurityZone:notification InRowAtIndexPath:indexPath];
            break;
        case 23: // Nueva ocurrencia creada
            cell = [self tableView:tableView OccurrenceNotifications:notification InRowAtIndexPath:indexPath];
            break;
        case 24: // Etiquetado
            cell = [self tableView:tableView OccurrenceNotifications:notification InRowAtIndexPath:indexPath];
            break;
        case 25: // A mts de una incidencia
            cell = [self tableView:tableView OccurrenceNotifications:notification InRowAtIndexPath:indexPath];
            break;
        case 26: // La incidencia que estas siguiendo
            cell = [self tableView:tableView OccurrenceNotifications:notification InRowAtIndexPath:indexPath];
            break;
        case 27: // Incidencia especial del usuario que estas siguiendo
            cell = [self tableView:tableView SpecialOccurrenceNotifications:notification InRowAtIndexPath:indexPath];
            break;
        case 28: // Nuevas notificaciones de Tips
            cell = [self tableView:tableView TipsNotifications:notification InRowAtIndexPath:indexPath];
            break;
        default: // Generic
            cell = [self tableView:tableView Generic:notification InRowAtIndexPath:indexPath];
            break;
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Notification *notification = [self.listNotification objectAtIndex:indexPath.row];
    switch ([notification.typeId intValue]) {
        case 1:
            //maxima alerta no hacer nada
            break;
        case 6: // NuevoMensaje
            [self showChat:notification];
            [UserNotifications deleteNotification:notification.notificationId Wait:NO Delegate:self Extra:notification];
            break;
        case 11: // NuevoMensaje de monitor
            [self showChat:notification];
            [UserNotifications deleteNotification:notification.notificationId Wait:NO Delegate:self Extra:notification];
            break;
            //incidencias
        case 23:
            [self goToOccurrenceLocation:notification];
            
            break;
        case 24:
            [self goToOccurrenceLocation:notification];
            
            break;
        case 25:
            [self goToOccurrenceLocation:notification];
            
            break;
        case 26:
            [self goToOccurrenceLocation:notification];
            
            break;
        case 27:
            [self goToOccurrenceLocation:notification];
            break;
        case 28:
            //celda de tips no hacer nada
            break;
        default: // Generic
            [UserNotifications deleteNotification:notification.notificationId Wait:NO Delegate:self Extra:notification];
            break;
    }
}

-(void)goToOccurrenceLocation:(Notification*)noti{
    CGFloat newLat= [noti.latitude floatValue];
    CGFloat newLon=[noti.longitude floatValue];
    CLLocationCoordinate2D newCoord= {newLat, newLon};
   MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, 30, 30);
    [[MapSecApp shareInstance] setRegion:region animated:YES];
    
    if ([noti.typeId isEqual:@27]) {

        self.displaySpecialOcurrence= [[UIStoryboard storyboardWithName:@"Occurrence" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoSpeciallOccurrenceVC"];
        self.displaySpecialOcurrence.occurrenceiD=noti.invitationId;
         self.displaySpecialOcurrence.typeOccSpecial=[NSNumber numberWithInt:[noti.groupId intValue]];
        [self.displaySpecialOcurrence drawInView:self.parentViewController.view];
    }else{
        self.displayOccurrence= [[UIStoryboard storyboardWithName:@"Occurrence" bundle:nil] instantiateViewControllerWithIdentifier:@"InfoOccurrenceVC"];
        self.displayOccurrence.occurrenceiD=noti.invitationId;
        [self.displayOccurrence drawInView:self.parentViewController.view];
    }
   
    if ([noti.notificationId hasPrefix:@"l"]) {
        
        NSMutableArray *tmp = [self.listNotification mutableCopy];
        [tmp removeObject:noti];
        self.listNotification = [tmp copy];
        [self.tableView reloadData];
        [noti deleteObject];
         [self.container showNotificationsCount:(int)[self.listNotification count]];

    }else{
        [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
    }
    [self.container hideNotifications];

}
- (IBAction)deleteAllNotifications:(id)sender {
    [UserNotifications deleteAllNotificationWait:YES Delegate:self];
    
}

- (void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   switch (service) {
        case kREADED_NOTIFICATIONS:
            dictionary = [dictionary objectForKey:@"NotificationReadedResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Notification* noti = extra;
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                [tmp removeObject:noti];
                self.listNotification = [tmp copy];
                [self.tableView reloadData];
                [noti deleteObject];
               // NSMutableArray* arrayResult = [[Notification findAll] mutableCopy];
                [self.container showNotificationsCount:(int)[self.listNotification count]];
                //[self.container hideNotifications];
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
           break;
        case kADD_USER_TO_GROUP:
            dictionary = [dictionary objectForKey:@"AddUserToGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200 || responseCode == 404) {
                Notification* noti = extra;
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                [tmp removeObject:noti];
                self.listNotification = [tmp copy];
                [self.tableView reloadData];
                [noti deleteObject];
              
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [self.container updateContacts];
                    NSArray * array= [Friend findAll];
                    [self.container showUsersCount:(int)[array count]];
                });
                
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            if (responseCode==409) {
                Notification* noti = extra;
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                [tmp removeObject:noti];
                self.listNotification = [tmp copy];
                [self.tableView reloadData];
                [noti deleteObject];
            }

            break;
        case kACEPT_TO_BE_MONITORED:
            dictionary = [dictionary objectForKey:@"AcceptInvitationFromMonitorResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
           
            if (responseCode == 200) {
                //agregado exitosa mente
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"agregado.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;

           
            }
            if (responseCode == 403) {
                            //The user is in another monitor
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"ya.pertenece.a.monitor", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;
               
            }
            if (responseCode==404) {
                //la invitacion ya no existe
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"codigo.invalido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;
            }
            if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            if (responseCode==406) {
                //The user cannot accept the invitation because the monitor account have reached the user limit
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"monitor.account.limit.reach", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;
            }
            if (responseCode==407) {
                //The user is not monitor of the account
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"monitor.account.exipired", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;
            }
            if (responseCode==200||responseCode == 403||responseCode==404||responseCode==406||responseCode==407) {
                Notification* noti = extra;
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                [tmp removeObject:noti];
                self.listNotification = [tmp copy];
                [self.tableView reloadData];
                [noti deleteObject];
                
            }
        
            
            break;
        case kREJECT_TO_BE_MONITORED:
            dictionary = [dictionary objectForKey:@"RejectInvitationFromMonitorResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            
            if (responseCode == 200) {
                //agregado exitosa mente
            }
            if (responseCode==404) {
                //la invitacion ya no existe
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"codigo.invalido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController];
                alert.delegate=self;
            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            if (responseCode==200||responseCode==404) {
                Notification* noti = extra;
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                [tmp removeObject:noti];
                self.listNotification = [tmp copy];
                [self.tableView reloadData];
                [noti deleteObject];
                
            }
            
            
            break;

          default:
            break;
    }
}
- (void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   switch (service) {
        case kDELETE_ALL_NOTIFICATIONS:
            dictionary = [dictionary objectForKey:@"DeleteAllNotificationsResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               
                NSMutableArray *tmp = [self.listNotification mutableCopy];
                for (Notification *noti in tmp) {
                    [noti deleteObject];
                }
                self.listNotification=[Notification findAll];
                [self.container showNotificationsCount:(int)[self.listNotification count]];
                self.deleteButton.enabled=NO;
                [self.tableView reloadData];
               
  
            }
            if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
               default:
            break;
    }
}

#pragma mark - Cell Incidences

- (UITableViewCell*) tableView:(UITableView*) tableView OccurrenceNotifications:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"Occurrences";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (self.languaje==1) {
        cell.titleNotification.text= @"Incidencia";
         cell.messageNotification.text = notification.messege;
    }else{
        cell.titleNotification.text= @"Incidence";
              NSArray * array;
        array= [NSArray array];
        array = @[@"Robbery", @"Suspicious activity", @"Crime", @"Missing children",  @"Corruption", @"Car Robbery", @"Pet Loss", @"Animal abuse", @"Feeling good"];
        NSString * occurrenceName;
   
    switch ([notification.typeId intValue]) {
        case 23:
           // [((ContainerVC*)self.parentViewController) updateOccurrences];
            if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ created an incidence", user.name];
                }
            else{
                cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) was created an incidence"];
            }
            break;
        case 24:
            if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ username was tag you in a comment", user.name];
                }
            else{
                cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) username was tag you in a comment"];
            }
            break;
        case 25:
         
             occurrenceName = [array objectAtIndex:[notification.chatGroupName intValue]-1];
            cell.messageNotification.text= [NSString stringWithFormat:@"You are %@ meters from an incidence %@", notification.groupName, occurrenceName];
          
            break;
        case 26:
            occurrenceName = [array objectAtIndex:[notification.chatGroupName intValue]-1];
            cell.messageNotification.text= [NSString stringWithFormat:@"Has comment on the incidence %@  you follow", occurrenceName];
            break;
         default:
            break;
    }
}
    // cell.dateNotification.text =[self formattedDate:notification.date];
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Special Incidences
- (UITableViewCell*) tableView:(UITableView*) tableView SpecialOccurrenceNotifications:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"OccurrencesSpecial";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (self.languaje==1) {
        cell.titleNotification.text= @"Incidencia Especial";
        cell.messageNotification.text = notification.messege;
    }else{
        cell.titleNotification.text= @"Special Incidence";
        cell.messageNotification.text= [NSString stringWithFormat:@"The user %@ was created an special incidence", notification.groupName];
       
    }
    
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}
#pragma mark - Cell Tips
- (UITableViewCell*) tableView:(UITableView*) tableView TipsNotifications:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"TipsCell";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
//    if (self.languaje==1) {
        cell.titleNotification.text= @"Tips";
        cell.messageNotification.text = notification.messege;
//    }else{
//        cell.titleNotification.text= @"Tips";
//        cell.messageNotification.text= [NSString stringWithFormat:@"The user %@ was created an special incidence", notification.groupName];
//        
//    }
 cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}



#pragma mark - Cell Into Security zone

- (UITableViewCell*) tableView:(UITableView*) tableView IntoSecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"IntoSecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
      switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ is inside of her/his safe zone", user.name];
            }
            else{
                cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is inside of her/his safe zone"];}
            break;
            
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Out Security zone

- (UITableViewCell*) tableView:(UITableView*) tableView OutSecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"OutSecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
       switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            if ([[Friend findWithUserId:notification.userIdFrom]count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ is out of her/his safe zone", user.name];
            }else{
                cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is out of her/his safe zone"];}
            break;
            
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Into Insecurity zone

- (UITableViewCell*) tableView:(UITableView*) tableView IntoInsecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"IntoInsecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
  
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            if ([notification.typeId intValue] ==21) {
                if ([[Friend findWithUserId:notification.userIdFrom]count]>0) {
                    Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                    cell.messageNotification.text= [NSString stringWithFormat:@"%@ is inside of your unsafe zone %@", user.name, notification.groupName];
                }else {
                    cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is inside of your unsafe zone %@", notification.groupName];}
            }else{
                if ([[Friend findWithUserId:notification.userIdFrom]count]>0) {
                    Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                    cell.messageNotification.text= [NSString stringWithFormat:@"%@ is inside of her/his unsafe zone", user.name];
                }else {
                    cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is inside of her/his unsafe zone"];}
            
            }
           
            break;
            
        default:
            break;
    }
   
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Out Security zone

- (UITableViewCell*) tableView:(UITableView*) tableView OutInecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"OutInsecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            if ([notification.typeId intValue]==22) {
                if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                    Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                    cell.messageNotification.text= [NSString stringWithFormat:@"%@ is out of your unsafe zone %@", user.name, notification.groupName];
                }else{
                    cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is out of your unsafe zone %@", notification.groupName];
                }
                
                
            }else{
                if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                    Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                    cell.messageNotification.text= [NSString stringWithFormat:@"%@ is out of her/his unsafe zone", user.name];
                }else{
                    cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) is out of her/his unsafe zone"];
                }
                
                
            }
            break;
            
        default:
            break;
    }
    
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Active Insecurity zone

- (UITableViewCell*) tableView:(UITableView*) tableView ActiveInsecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"ActiveInsecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
     switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
             cell.messageNotification.text= [NSString stringWithFormat:@"Your unsafe zone was enabled"];
            break;
        default:
            break;
    }
 
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Disable Security zone

- (UITableViewCell*) tableView:(UITableView*) tableView DisableInecurityZone:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"DisableInsecurityZone";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
           
            cell.messageNotification.text= [NSString stringWithFormat:@"Your unsafe zone was disabled"];
            break;
            
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Disable Alert

- (UITableViewCell*) tableView:(UITableView*) tableView DisableAlert:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"DisableAlert";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            
            if ([[Friend findWithUserId:notification.userIdFrom]count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ has desactived his/her alert", user.name];
            }
            else{
                cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) has desactived his/her alert"];
            }
            
            break;
            
        default:
            break;
    }
    
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell Change Group name

- (UITableViewCell*) tableView:(UITableView*) tableView ChangeGroupName:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"ChangeGroupName";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    switch (self.languaje) {
        case 1:
            
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
           cell.messageNotification.text= [NSString stringWithFormat:@"The chat group name was updated as %@ ", notification.chatGroupName];
            break;
            
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
  
    return cell;
}

#pragma mark - Cell Delete to group to chat

- (UITableViewCell*) tableView:(UITableView*) tableView DeleteToGroupToChat:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"DeleteToGroupToChat";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
   
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            break;
        case 2:
            cell.messageNotification.text= [NSString stringWithFormat:@"You were removed from chatGroup %@",notification.chatGroupName];
            break;
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell info to invitation

- (UITableViewCell*) tableView:(UITableView*) tableView InfoToInvitation:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"InfoToInvitation";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    switch (self.languaje) {
        case 1:
           
            cell.messageNotification.text = notification.messege;
            
            break;
        case 2:
            if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                cell.messageNotification.text= [NSString stringWithFormat:@"%@ was added to %@", user.name, notification.groupName];
            }else{
            cell.messageNotification.text= [NSString stringWithFormat:@"(User not found) was added to %@", notification.groupName];
            }
            break;
        default:
            break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
//    [self.container updateContacts];
//    NSArray * array = [Friend findAll];
//    [self.container showUsersCount:(int)[array count]];
    return cell;
}
- (UITableViewCell*) tableView:(UITableView*) tableView YouAddToChatGroup:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"InfoToInvitation";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    switch (self.languaje) {
        case 1:
            cell.messageNotification.text = notification.messege;
            break;
        case 2:
           cell.messageNotification.text= [NSString stringWithFormat:@"You was added to chat group %@", notification.chatGroupName];
         break;
       default:
         break;
    }

    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell message to monitor

- (UITableViewCell*) tableView:(UITableView*) tableView MessageToMonitor:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"MessageToMonitor";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.messageNotification.text = notification.messege;
   cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
    return cell;
}

#pragma mark - Cell delete to group

- (UITableViewCell*) tableView:(UITableView*) tableView DeleteToGroup:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"DeleteToGroup";
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NotificationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.messageNotification.text = notification.messege;
    cell.dateNotification.text =[StringUtils formattedDateWithDate:notification.date];
 return cell;
}

#pragma mark - Cell Alert User

- (UITableViewCell*) tableView:(UITableView*) tableView AlertUser:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"MaximaAlerta";
    AlertCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[AlertCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.chat.tag = indexPath.row;
    cell.call.tag = indexPath.row;
    cell.location.tag = indexPath.row;

    switch (self.languaje) {
        case 1:
            cell.name.text = notification.messege;
            break;
        case 2:
            if ([[Friend findWithUserId:notification.userIdFrom] count]>0) {
                Friend *user = [[Friend findWithUserId:notification.userIdFrom]objectAtIndex:0];
                 cell.name.text= [NSString stringWithFormat:@"%@ is in danger", user.name];
                            }
            else{
             cell.name.text= [NSString stringWithFormat:@"(User not found) is in danger"];
             }
           break;
        default:
            break;
    }
    [cell.chat addTarget:self action:@selector(goToChat:) forControlEvents:UIControlEventTouchUpInside];
    [cell.call addTarget:self action:@selector(goToCall:) forControlEvents:UIControlEventTouchUpInside];
    [cell.location addTarget:self action:@selector(goToLocation:) forControlEvents:UIControlEventTouchUpInside];

    cell.dateAlert.text = [StringUtils formattedDateWithDate:notification.date];
    return cell;
}

- (void) goToChat:(UIButton*) sender {
    
    Notification* noti = [self.listNotification objectAtIndex:sender.tag];
    if ([[Friend findWithUserId:noti.userIdFrom] count]>0) {
        [((ContainerVC*)self.parentViewController) showView:noti.userIdFrom IsMonitor:NO];
       

        [self.container hideNotifications];
    }
    else{
    [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
    
    }
 }

- (void) goToCall:(UIButton*) sender {
    Notification* noti = [self.listNotification objectAtIndex:sender.tag];
    if ([[Friend findWithUserId:noti.userIdFrom] count]>0) {
    Friend* friend = [[Friend findWithUserId:noti.userIdFrom] objectAtIndex:0];
    if (friend && [[friend.phones allObjects] count] > 0) {
        Phone *phone = [[friend.phones allObjects] objectAtIndex:0];
        NSString* phoneCallNum = [NSString stringWithFormat:@"tel://%@",phone.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneCallNum]];
        
    }
      [self.container hideNotifications];
    }else{
        [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
 
    }
}




- (void) goToLocation:(UIButton*) sender {
    Notification* noti = [self.listNotification objectAtIndex:sender.tag];
     if ([[Friend findWithUserId:noti.userIdFrom] count]>0) {
    Friend* friend = [[Friend findWithUserId:noti.userIdFrom] objectAtIndex:0];
    CGFloat newLat = [noti.latitude floatValue];
    CGFloat newLon = [noti.longitude floatValue];
    CLLocationCoordinate2D newCoord = {newLat, newLon};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, 250, 250);
    [[MapSecApp shareInstance] setRegion:region animated:YES];
    [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
    [self.container hideNotifications];
    [self.container showInfoBar:friend];
         
     }
     else{
         [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];

     }
 
}

#pragma mark - Cell New Message

- (UITableViewCell*) tableView:(UITableView*) tableView NewMessage:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"NuevoMensaje";
    NewMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (NewMessageCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    switch (self.languaje) {
        case 1:
             if ([notification.chatGroupId isEqualToString:@""]) {
            cell.titleCell.text= [NSString stringWithFormat:@"Tienes un nuevo mensaje"];
                 cell.messageCell.text = notification.messege;}
             else{
                 cell.titleCell.text=notification.messege;
             }
            break;
        case 2:
            if ([notification.chatGroupId isEqualToString:@""]) {
                cell.titleCell.text =[NSString stringWithFormat:@"You have a new message"];
                cell.messageCell.text = notification.messege;
            }else {
           cell.titleCell.text= [NSString stringWithFormat:@"You have a new message in group %@",notification.chatGroupName];
               
            }
           
            break;
            
        default:
            break;
    }
  
    cell.dateMessage.text= [StringUtils formattedDateWithDate:notification.date];
        return cell;
}

- (void) showChat:(Notification *) notification {
    
    //conversacion grupal o individual o monitor?
    if (![notification.chatGroupId isEqualToString:@""]) {
        //abrir ventana de chat grupal
        
       
        NSArray *arrayLastMessages= [Message findAll];
        for (Message* message in arrayLastMessages) {
            if ([message.groupId isEqualToString:notification.chatGroupId]) {

                     NSDictionary* dataDic = @{
                                          @"Image":message.imageGroup,
                                          @"IdChatGroup": message.groupId,
                                          @"Name":message.groupName
                                          
                                          };

                [((ContainerVC*)self.parentViewController) showViewChatGroup:dataDic GroupId:message.groupId];
            }
        }
        
    }else if([notification.typeId intValue]==11){
        //abrir ventana de chat con monitor

         [((ContainerVC*)self.parentViewController) showView:notification.userIdFrom IsMonitor:YES];
    }
    else{
        //abrir ventana de chat individual

        [((ContainerVC*)self.parentViewController) showView:notification.userIdFrom IsMonitor:NO];
    }

    [self.container hideNotifications];
}



#pragma mark - Cell Invitation

- (UITableViewCell*) tableView:(UITableView*) tableView Invitation:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"Invitation";
    //groupname nombre del monitor
    InvitationCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = (InvitationCell*)[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.okButton.tag = indexPath.row;
    cell.cancelButton.tag = indexPath.row;
       switch (self.languaje) {
        case 1:
            cell.message.text = notification.messege;
            break;
        case 2:
               if([notification.typeId intValue]==20){
                   cell.message.text= [NSString stringWithFormat:@"The monitor %@ want to add you in her/his network of monitored users",notification.groupName];
               }else{
                    cell.message.text= [NSString stringWithFormat:@"%@ has invited you to join his/her contact´s list",notification.userIdFrom];
               }
           
           break;
            
        default:
            break;
    }
    
    [cell.okButton addTarget:self action:@selector(acceptInvitation:) forControlEvents:UIControlEventTouchUpInside];
    [cell.cancelButton addTarget:self action:@selector(cancelInvitation:) forControlEvents:UIControlEventTouchUpInside];
    cell.date.text= [StringUtils formattedDateWithDate:notification.date];
    return cell;
}

- (void) acceptInvitation:(UIButton*) sender {
    Notification* noti = [self.listNotification objectAtIndex:sender.tag];
    if ([noti.typeId intValue]==20) {
        //nuevo servicio
        
        [UserServices aceptToBeMonitoredWithInvitationId:noti.invitationId WithWait:YES Delegate:self Extra:noti];
    }else{
        
        [GroupService addUserToGroupWithAccessCode:noti.invitationId WithWait:YES Delegate:self Extra:noti];
    }
    [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
}

- (void) cancelInvitation:(UIButton*) sender {
    Notification* noti = [self.listNotification objectAtIndex:sender.tag];
    if ([noti.typeId intValue]==20) {
        //nuevo servicio de cancelacion
        [UserServices rejectToBeMonitoredWithInvitationId:noti.invitationId WithWait:YES Delegate:self Extra:noti];
    }
    [UserNotifications deleteNotification:noti.notificationId Wait:NO Delegate:self Extra:noti];
}

#pragma mark - Cell Generic

- (UITableViewCell*) tableView:(UITableView*) tableView Generic:(Notification*) notification InRowAtIndexPath:(NSIndexPath*) indexPath {
    static NSString* identifier = @"Generic";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    if (self.languaje==1) {
        cell.textLabel.text = notification.messege;
    }else{
        if ([notification.typeId intValue]==8) {
            cell.textLabel.text= [NSString stringWithFormat:@"You was added to chat group %@ ", notification.chatGroupName];
        }else {
            cell.textLabel.text = notification.messege;
        }
    }
    return cell;
}
@end
