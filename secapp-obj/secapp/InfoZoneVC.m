//
//  InfoZoneVC.m
//  Test Circle
//
//  Created by Marco Antonio Navarro Montoya on 29/04/15.
//  Copyright (c) 2015 Marco Antonio Navarro Montoya. All rights reserved.
//

#import "InfoZoneVC.h"
#import "CircleOverlay.h"
#import "InsecureZone.h"
#import "InsecureZoneServices.h"

@interface InfoZoneVC ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *radius;
@property (weak, nonatomic) IBOutlet UILabel *labelMonitorZone;


@end

@implementation InfoZoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    int languaje=[[Service getLanguaje]intValue];
    
    if (_zone) {
        self.name.text = _zone.titleCircle;
        if (_zone.zoneType==YES) {
            if(languaje==1){
            self.radius.text = [NSString stringWithFormat:@"Radio: %@",[self radiusStr:_zone.radius]];
            }else{
                
            self.radius.text = [NSString stringWithFormat:@"Radius: %@",[self radiusStr:_zone.radius]];
            }
        }else{
            InsecureZone *zone= [[InsecureZone findWithZoneId:_zone.idZone]objectAtIndex:0];
            if(languaje==1){
                self.radius.text = [NSString stringWithFormat:@"Activa de %@ a %@ hrs.",zone.timeInitial,zone.timeFinal];
            }else {
              self.radius.text = [NSString stringWithFormat:@"Active from %@ to %@ hrs.",zone.timeInitial,zone.timeFinal];
            }
           
            
        }
    }

}
-(void)viewWillAppear:(BOOL)animated{

self.isShow=YES;
    if (self.isOwner==NO) {
        self.buttonDelete.hidden=YES;
        self.buttonEdit.hidden=YES;
        self.labelMonitorZone.hidden=NO;
    }
    else{
        self.buttonDelete.hidden=NO;
        self.buttonEdit.hidden=NO;
        self.labelMonitorZone.hidden=YES;
    }
}
- (void)setZone:(CircleOverlay *)zone {
    _zone = zone;
      int languaje=[[Service getLanguaje]intValue];
    if (_zone) {
        self.name.text = _zone.titleCircle;
        if (_zone.zoneType==YES) {
            if(languaje==1){
                self.radius.text = [NSString stringWithFormat:@"Radio: %@",[self radiusStr:_zone.radius]];
            }else{
                
                self.radius.text = [NSString stringWithFormat:@"Radius: %@",[self radiusStr:_zone.radius]];
            }
        }else{
            InsecureZone *zone= [[InsecureZone findWithZoneId:_zone.idZone]objectAtIndex:0];
            if(languaje==1){
                self.radius.text = [NSString stringWithFormat:@"Activa de %@ a %@ hrs.",zone.timeInitial,zone.timeFinal];
            }else {
                self.radius.text = [NSString stringWithFormat:@"Active from %@ to %@ hrs.",zone.timeInitial,zone.timeFinal];
            }
            
            
        }
    }
    
}

- (void) drawInView:(UIView*) view {
    CGRect frame = self.view.frame;
    frame.size.height = 120;
    frame.origin.y = view.frame.size.height - frame.size.height;
    frame.size.width= view.frame.size.width;

    self.view.frame = frame;
    [view addSubview:self.view];
}

- (IBAction)edit:(id)sender {
    if (self.delegate) {
       [self.delegate infoZone:self EditeZone:self.zone];
       [self.view removeFromSuperview];
    }
}

- (IBAction)remove:(id)sender {
    
    if (self.delegate) {
        
        [self.delegate infoZone:self DeleteZone:self.zone];
        
        [self.view removeFromSuperview];
    }
}

- (NSString*) radiusStr:(double)radius {
    NSString *radiusStr;
    if (radius >= 1000) {
        NSString *diatanceOfKmStr = [NSString stringWithFormat:@"%.1f", radius * .001f];
        radiusStr = [NSString stringWithFormat:@"%@ km", diatanceOfKmStr];
    } else {
        NSString *diatanceOfMeterStr = [NSString stringWithFormat:@"%.0f", radius];
        radiusStr = [NSString stringWithFormat:@"%@ m", diatanceOfMeterStr];
    }
    return radiusStr;
}
-(void)viewDidDisappear:(BOOL)animated{
    self.isShow=NO;
}

@end
