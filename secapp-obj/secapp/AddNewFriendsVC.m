//
//  AddNewFriendsVC.m
//  secapp
//
//  Created by SecApp on 14/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "AddNewFriendsVC.h"
#import "GroupService.h"
#import "Friend.h"
#import "StringUtils.h"
#import "AlertView.h"
#include <MessageUI/MessageUI.h>
#import "AppDelegate.h"

#import "SimpleTableViewCell.h"


@interface AddNewFriendsVC () <UITableViewDataSource, UITableViewDelegate, ResponseDelegate, AlertViewDelegate, MFMessageComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property NSArray* friends;
@property NSArray *friendsSecApp;
@property (weak, nonatomic) IBOutlet UITextField *inUser;
@property NSArray* tmpEmails;
@property NSArray* tmpPhones;
@property NSArray* ids;
@property NSArray* tmpUserId;
@property (weak, nonatomic) IBOutlet UITextField *searchFriendsSecapp;
@property BOOL isSearch;
@property (weak, nonatomic) IBOutlet UIButton *cancelSearch;


@end

@implementation AddNewFriendsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    self.friends= [NSArray array];
    self.inUser.text=@"";
    self.cancelSearch.hidden=YES;
}
- (IBAction)backView:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}
- (IBAction)cancelSearch:(id)sender {
    self.isSearch=NO;
    [self.tableView reloadData];
     self.cancelSearch.hidden=YES;
    
}

- (IBAction)addInvitation:(id)sender {
    [self.view endEditing:YES];
        self.isSearch=NO;
       // self.searchFriendsSecapp.text=@"";
        [self.tableView reloadData];
    int type = -1;
    if ([StringUtils validateEmail:self.inUser.text]) {
        type = 1;
    } else if ([StringUtils validateNumberPhone:self.inUser.text]) {
        type = 2;
    } else {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"formato.icorrecto.usuario", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        return;
    }
    
    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
    
    NSDictionary* data = @{@"data": self.inUser.text,
                           @"name": @"",
                           @"image": @"",
                           @"type": @(type),
                           @"userId": @""
                           };
    
    NSMutableArray* usersInGroup = [self.friends mutableCopy];
    //////
    for (NSDictionary * user in usersInGroup) {
     if ([[user objectForKey:@"data"]isEqualToString:[data objectForKey:@"data"]]) {
           self.inUser.text=@"";
            return;
        }
    }

    [usersInGroup addObject:data];
    
    self.friends = [usersInGroup copy];

    
    int newRow = (int)[self.friends count];
//    if (newRow<2) {
//        [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow) inSection:0]];
//    }else{
     [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:0]];
   // }
   
    
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearch) {
        return [self.friendsSecApp count];
    }
    return [self.friends count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddFriendCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddFriendCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (self.isSearch) {
         NSDictionary* data = [self.friendsSecApp objectAtIndex:indexPath.row];
        cell.nameLabel.text = [data objectForKey:@"Name"];
        cell.buttonDelete.hidden=YES;
        cell.buttonDelete.enabled=NO;
        cell.buttonAdd.enabled=YES;
        cell.buttonAdd.hidden=NO;
        cell.buttonAdd.tag= indexPath.row;
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            if([[data objectForKey:@"ImageURL"] isEqual:@""]||[[data objectForKey:@"ImageURL"] isEqual:[NSNull null]]|| [data objectForKey:@"ImageURL"]==nil){
                
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                
                NSData *img = [NSData dataWithContentsOfURL:[NSURL URLWithString:[data objectForKey:@"ImageURL"]]];
                cell.profileImageView.image = [UIImage imageWithData:img];
                
            }
            
        });

        [cell.buttonAdd addTarget:self action:@selector(buttonAddClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.inUser.text=@"";
        return cell;

    }else{
        NSDictionary* data = [self.friends objectAtIndex:indexPath.row];
        if ([[data objectForKey:@"data"] isEqualToString:@""]) {
            cell.nameLabel.text = [data objectForKey:@"name"];
        }
        else{
            cell.nameLabel.text = [data objectForKey:@"data"];
        }
        
        cell.buttonDelete.hidden=NO;
        cell.buttonAdd.hidden=YES;
        cell.buttonDelete.enabled=YES;
        cell.buttonAdd.enabled=NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            if([[data objectForKey:@"image"] isEqual:@""]||[[data objectForKey:@"image"] isEqual:[NSNull null]]|| [data objectForKey:@"image"]==nil){
                
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                
                NSData *img = [NSData dataWithContentsOfURL:[NSURL URLWithString:[data objectForKey:@"image"]]];
                cell.profileImageView.image = [UIImage imageWithData:img];
                
            }

           // cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            
        });
        cell.buttonDelete.tag= indexPath.row;
        [cell.buttonDelete addTarget:self action:@selector(buttonDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        self.inUser.text=@"";
       
        return cell;
    }
 
    
}

-(void)buttonDeleteClicked:(UIButton *)sender{
  
    NSDictionary * dic = [self.friends objectAtIndex:sender.tag];
    NSMutableArray *arrayTmp= [self.friends mutableCopy];
    [arrayTmp removeObject:dic];
    self.friends = [arrayTmp copy];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath * tmpIndexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    
    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
     [indexPathsToDelete addObject:tmpIndexPath];
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView endUpdates];

  //borrar de la lista y recargar la tabla
    //[self.friends deleteObject:dic];
  }
- (IBAction)sendInvitations:(id)sender {
    [self.view endEditing:YES];
    
    NSMutableArray* tmpPhones = [NSMutableArray array];
    NSMutableArray* tmpEmails = [NSMutableArray array];
    NSMutableArray* tmpUsers = [self.friends mutableCopy];
    NSMutableArray* tmpSecappUserId = [NSMutableArray array];
    for (NSDictionary* friend in tmpUsers) {
        
            if ([[friend objectForKey:@"type"] intValue] == 1) {
                [tmpEmails addObject:[friend objectForKey:@"data"]];
            } else if([[friend objectForKey:@"type"] intValue] == 2){
                [tmpPhones addObject:[friend objectForKey:@"data"]];
            }
            else if ([[friend objectForKey:@"type"] intValue] == 3){
                [tmpSecappUserId addObject:[friend objectForKey:@"userId"]];
            }
        
    }
        self.tmpPhones = [tmpPhones copy];
        self.tmpEmails = [tmpEmails copy];
    self.tmpUserId= [tmpSecappUserId copy];

    
    if ([self.tmpUserId count]>0){
        //exists users
        [GroupService sendInvitationExistsUsersWithUsersId:self.tmpUserId Wait:YES Delegate:self];    }
    else if ([self.tmpEmails count] > 0) {
        // Emails
        [GroupService sendInvitationWithData:self.tmpEmails IsPhone:NO Wait:YES Delegate:self Extra:nil];
    }
    else if ([self.tmpPhones count]>0) {
        // Phones
        [GroupService sendInvitationWithData:self.tmpPhones IsPhone:YES Wait:YES Delegate:self Extra:self.tmpPhones];
    }


}
-(void) buttonAddClicked:(UIButton *)sender{
    [self.view endEditing:YES];
    self.cancelSearch.hidden=YES;
    NSDictionary * dic = [self.friendsSecApp objectAtIndex:sender.tag];
    NSDictionary* data = @{@"data": @"",
                           @"name": [dic objectForKey:@"Name"],
                           @"image": [dic objectForKey:@"ImageURL"],
                           @"type": @(3),
                           @"userId": [dic objectForKey:@"UserId"]
                           };

    
    NSMutableArray* usersInGroup = [self.friends mutableCopy];
    for (NSDictionary * user in usersInGroup) {
        
        if ([[user objectForKey:@"userId"]isEqualToString:[data objectForKey:@"userId"]]) {
            self.isSearch=NO;
            self.searchFriendsSecapp.text=@"";
            
            [self.tableView reloadData];

            return;
        }
    }
    
    [usersInGroup addObject:data];
    
    self.friends = [usersInGroup copy];
    //cambiar celda e icono de palomita por icono de basura para permitir que agregue mas contactos
    //revisar una sola logica
    self.isSearch=NO;
    self.searchFriendsSecapp.text=@"";
    
    [self.tableView reloadData];

}
 
- (void) showAlertInvitationSended {
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"invitacion.enviada.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
}
-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kINVITATION:
            dictionary = [dictionary objectForKey:@"SendInvitationResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                
                // invitations phones ids received
                NSMutableArray* ids = [NSMutableArray array];
                for (NSDictionary* data in (NSArray*)[dictionary objectForKey:@"Ids"]) {
                    [ids addObject:[data objectForKey:@"Id"]];
                }
                self.ids = [ids copy];
                [self sendMessages];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            if (responseCode==409) {
                
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"usuario.ya.esta.en.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                alert.delegate=self;
                
            }
            
            break;
        default:
            break;
    }
}
-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kINVITATION_EXISTS_USERS:
            dictionary = [dictionary objectForKey:@"SendInvitationExistsUsersResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                               // invitations users sended
                //ask for emails
                if ([self.tmpEmails count]>0) {
                   [GroupService sendInvitationWithData:self.tmpEmails IsPhone:NO Wait:YES Delegate:self Extra:nil];
                }else if ([self.tmpPhones count]>0){
                    //ask for phones
                    [GroupService sendInvitationWithData:self.tmpPhones IsPhone:YES Wait:YES Delegate:self Extra:self.tmpPhones];
                }else{
                    //close view
                  [self showAlertSaveChanges];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                
                }
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            if (responseCode==409) {
                
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"usuario.ya.esta.en.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                alert.delegate=self;
                
            }
            
            break;
        case kADVANCE_SEARCH_CONTACTS:
            dictionary = [dictionary objectForKey:@"AdvanceSearchContactsResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                
                self.friendsSecApp = [[dictionary objectForKey:@"BasicUserList"] copy];
                self.searchFriendsSecapp.text=@"";
                
                [self.tableView reloadData];
                self.cancelSearch.hidden=NO;
                
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            if (responseCode == 407) {
                //el codigo de acceso esta caducado o ya fue usado
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"no.encontraron.coincidencias", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                alert.delegate=self;
                self.searchFriendsSecapp.text=@"";
                self.isSearch=NO;
                [self.tableView reloadData];
                
            }
            break;
        case kINVITATION:
                dictionary = [dictionary objectForKey:@"SendInvitationResult"];
                responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
                    if (responseCode == 200) {
                        //invitations for mail sended
                      
                          //  ask for Phones
                            if ([self.tmpPhones count] > 0) {
                                [GroupService sendInvitationWithData:self.tmpPhones IsPhone:YES Wait:YES Delegate:self Extra:self.tmpPhones];
                                //ask for userID
                            } else{
                                //close view
                                [self showAlertSaveChanges];
                                [self.navigationController popToRootViewControllerAnimated:YES];
                            }
                        }
                        break;


        default:
            break;
    }
}

- (void) sendMessages {
    if(![MFMessageComposeViewController canSendText]) {
        return;
    }
    NSMutableArray* tmpPhones = [self.tmpPhones mutableCopy];
    NSMutableArray* tmpIds = [self.ids mutableCopy];
    
    NSArray *recipents = @[[tmpPhones objectAtIndex:0]];
    int languaje= [[Service getLanguaje]intValue];
    NSString *message;
    if (languaje==1) {
        message = [NSString stringWithFormat:@"Baja SecApp aqui http://goo.gl/0XM8ew y unete a mi red de seguridad ingresando el siguiente código: %@", [tmpIds objectAtIndex:0]];
    }else{
        message = [NSString stringWithFormat:@"Download SecApp here http://goo.gl/0XM8ew and join in my security network entering the next code: %@", [tmpIds objectAtIndex:0]];
    }
    
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setRecipients:recipents];
    [messageController setBody:message];
    
    [tmpPhones removeObject:[tmpPhones objectAtIndex:0]];
    [tmpIds removeObject:[tmpIds objectAtIndex:0]];
    
    self.tmpPhones = [tmpPhones copy];
    self.ids = [tmpIds copy];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result {
    switch (result) {
        case MessageComposeResultCancelled:
            break;
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultSent:
            break;
        default:
            break;
    }
    [controller dismissViewControllerAnimated:YES completion:^{
        if ([self.tmpPhones count] > 0 && [self.ids count] > 0) {
            [self sendMessages];
            //invitations for sms sended
        } else{
            //close view
            [self showAlertSaveChanges];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }];
}

- (void) showAlertSaveChanges {
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"invitacion.enviada.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
}


- (IBAction)searchSecappFriends:(id)sender {
    [self.view endEditing:YES];
    self.isSearch=YES;
    if([self.searchFriendsSecapp.text isEqualToString:@""]){
        return;
    }
     NSString *firstLetter = [self.searchFriendsSecapp.text substringToIndex:1];
    NSString *lastLetter =[self.searchFriendsSecapp.text substringFromIndex:[self.searchFriendsSecapp.text length]-1];
    NSString * stringForSearch;
    if ([firstLetter isEqualToString:@" "]) {
        stringForSearch = [self.searchFriendsSecapp.text substringFromIndex:1];
        if ([lastLetter isEqualToString:@" "]) {
            stringForSearch = [stringForSearch substringToIndex:[stringForSearch length]-1];
        }
    }
    else if ([lastLetter isEqualToString:@" "]) {
         stringForSearch = [self.searchFriendsSecapp.text substringToIndex:[self.searchFriendsSecapp.text length]-1];
    }else{
        stringForSearch = self.searchFriendsSecapp.text;
    }
    [GroupService advanceSearchContactsWithData:stringForSearch Wait:YES Delegate:self];
    


}


@end
