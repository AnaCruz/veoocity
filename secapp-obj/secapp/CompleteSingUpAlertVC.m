//
//  CompleteSingUpAlertVC.m
//  secapp
//
//  Created by SecApp on 28/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "CompleteSingUpAlertVC.h"
#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "Alert/AlertView.h"
#import "SignUpVC.h"
#import "StringUtils.h"
#import "Service.h"

@interface CompleteSingUpAlertVC () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *phoneField;
@property (weak, nonatomic) IBOutlet UITextField *pinField;
@property (weak, nonatomic) IBOutlet UIView *viewAlert;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;
@property SignUpVC *container;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerView;
@property (strong, nonatomic) IBOutlet UIButton *termsAndConditionsButtonText;
@property NSString * languaje;
@end

@implementation CompleteSingUpAlertVC
-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.errorLabel.hidden=YES;
    [self registerForKeyboardNotifications];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [UIView animateWithDuration:0.15f
                              delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 100.0, 100.0);
                         }
                         completion:^(BOOL finished) {
                             
                             [self.viewAlert updateConstraints];
                         } ];
    });
//
    self.languaje= [Service getLanguaje];
    self.container = (SignUpVC *)self.controller;
    self.btnCancel.layer.cornerRadius = 5;
    self.btnOk.layer.cornerRadius = 5;
    self.viewAlert.layer.cornerRadius=5;
    CGAffineTransform trans = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
    self.viewAlert.transform = trans;
    UIColor *color = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1.0];
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSString * text;
    
    if([self.languaje isEqualToString:@"1"]){
        text= @"Si usted continúa acepta los términos y condiciones";
    }else{
        text= @"If you continue you are accepting terms and conditions";
    }
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    // using text on button
    [self.termsAndConditionsButtonText setAttributedTitle: titleString forState:UIControlStateNormal];

   
}
- (void) showOptionsForCompleteSignUp:(UIViewController*) controller {
//    [controller addChildViewController:self];
//    self.view.frame=self.controller.view.frame;
//    [self.controller.view addSubview:self.view];
}

- (IBAction)buttonPressed:(UIButton *)sender {
    if (sender.tag==1) {
        self.errorLabel.hidden=YES;
        if ([self.emailField.text isEqualToString:@""]
               || [self.emailField.text length] >= 50) {
            //
            self.errorLabel.hidden=NO;
            if ([self.languaje isEqualToString:@"1"]) {
                 self.errorLabel.text=@"Correo no válido o vacio";
            }else{
                 self.errorLabel.text=@"Invalid or empty email";
            }
           return;
        }
        if ([self.phoneField.text isEqualToString:@""] || [self.phoneField.text length] > 15) {
            //
            self.errorLabel.hidden=NO;
            if ([self.languaje isEqualToString:@"1"]) {
                self.errorLabel.text=@"Teléfono no válido o vacio";
            }else{
                self.errorLabel.text=@"Invalid or empty phone";
            }
            return;
        }
        if ([self.pinField.text length]!=4) {
            //
            self.errorLabel.hidden=NO;
            if ([self.languaje isEqualToString:@"1"]) {
                self.errorLabel.text=@"Ingrese un Pin de 4 dígitos";
            }else{
             self.errorLabel.text=@"Enter your 4 digit security pin";
            }
            return;
        }
        if ([self.pinField.text isEqualToString:@""]) {
            //
            self.errorLabel.hidden=NO;
            if ([self.languaje isEqualToString:@"1"]) {
            self.errorLabel.text=@"Pin vacio";
            }
            else{
             self.errorLabel.text=@"Empty PIN";
            }
            return;
        }
        
        if ([StringUtils validateEmail:self.emailField.text]&&[StringUtils validateNumberPhone:self.phoneField.text]&&([self.pinField.text length]==4)) {
    NSDictionary * dic= @{ @"Email": self.emailField.text,
                               @"Phone": self.phoneField.text,
                               @"Pin": self.pinField.text
                               };
            
            [self.container signUpComplete:dic];
        }
        
//

    }
    [UIView animateWithDuration:0.15f
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }];

    
    
}
//-(void)viewDidAppear:(BOOL)animated {
//     self.errorLabel.hidden=YES;
//    [self registerForKeyboardNotifications];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        
//        [UIView animateWithDuration:0.15f
//                              delay:0.0 options:UIViewAnimationOptionCurveEaseIn
//                         animations:^{
//                             self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 100.0, 100.0);
//                         }
//                         completion:^(BOOL finished) {
//                             
//                             [self.viewAlert updateConstraints];
//                         } ];
//    });
//}

-(void) animationStopped {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}
//Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.viewAlert.frame;
    
    if ((aRect.origin.y+aRect.size.height) >= (self.view.frame.size.height - kbSize.height)) {
        //        aRect.origin.y = self.view.frame.size.height - kbSize.height - aRect.size.height;
        [UIView animateWithDuration:0.15f
                              delay:0.0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.constant = 80;
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.15f
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.centerView.constant = 0;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    //nil dictionary
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=4);
    }
    if (textField.tag == 200) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=15);
    }
    if (textField.tag == 300) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=50);
    }
    return YES;
}
- (IBAction)showTermsAndConditions:(id)sender {
    UIViewController* conditions = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"ConditionsVC"];
    
    [self.parentViewController presentViewController:conditions animated:YES completion:nil];

    
}


@end
