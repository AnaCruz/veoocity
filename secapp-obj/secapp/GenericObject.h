//
//  GenericObject.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 26/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface GenericObject : NSManagedObject

/**
 * Create a object using the name of the entity.
 * @param nameEntity Name to entity.
 */
+ (instancetype) createObjectWithNameEntity:(NSString*) nameEntity;

/**
 * Save the object in the context
 */
- (void) saveObject;

/**
 * Refresh the object in the context.
 */
- (void) mergeObject;

/**
 * Delete the object from the context
 */
- (void) deleteObject;

/**
 * Find all data of the table.
 * @param nameEntity Name to entity.
 */
+ (NSArray*) findAllWithNameEntity:(NSString*) nameEntity;

@end
