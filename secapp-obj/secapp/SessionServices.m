//
//  SessionServices.m
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SessionServices.h"

@implementation SessionServices
/**
 * Login in the app using the Social Media or User of SecApp
 * @param params Dictionary with the needs params for the login.
 * @param delegate In this object will notifies response of service.
 */
+ (NSData*) loginWithParam:(id) params Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    
    NSString* uri = [NSString stringWithFormat:@"%@Login", [super getUrl]];
    NSDictionary *dataDic = @{@"InfoLogin":params};
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kLOGIN Wait:wait Delegate:delegate];
    NSData* paramsData = [params isKindOfClass:NSData.class] ? params : [self parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
    return paramsData;
}


+ (NSData*) loginWithData:(NSData*) data Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSError *parsingError = nil;
    NSMutableDictionary *dictionary = [[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&parsingError] mutableCopy];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return nil;
    }
    dictionary[@"InfoLogin"] = [[dictionary objectForKey:@"InfoLogin"] mutableCopy];
    [dictionary[@"InfoLogin"] setObject:@"true" forKey:@"ForceClose"];
    dictionary[@"InfoLogin"] = [dictionary[@"InfoLogin"] copy];
    data = [[self parsingWithObject:dictionary] copy];
    return [self loginWithParam:data Wait:wait Delegate:delegate];
}

+ (NSData*) loginWithPhone:(NSString*) phone Pin:(NSString*) pin Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSDictionary *dataDic = @{@"Data":phone,
                              @"LoginType": @"1",
                              @"PIN":pin,
                              @"AppVersion":[super getAppVersion],
                              @"Device":@"11",
                              @"Language": [super getLanguaje]};
    return [self loginWithParam:dataDic Wait:wait Delegate:delegate];
}

+ (NSData*) loginWithMail:(NSString*) mail Pin:(NSString*) pin Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSDictionary *dataDic = @{@"Data":mail,
                              @"LoginType": @"2",
                              @"PIN":pin,
                              @"AppVersion":[super getAppVersion],
                              @"Device":@"11",
                              @"Language": [super getLanguaje]};
    return [self loginWithParam:dataDic Wait:wait Delegate:delegate];
}

+ (NSData*) loginWithFacebook:(NSString*) idFacebook Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    NSDictionary *dataDic = @{@"Data":idFacebook,
                              @"LoginType": @"3",
                              @"AppVersion":[super getAppVersion],
                              @"Device":@"11",
                              @"Language": [super getLanguaje]};
    return [self loginWithParam:dataDic Wait:wait Delegate:delegate];
}


+(void) logoutWithUserId: (NSString* ) userId SessionId:( NSString *) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@Logout/%@/%@", [super getUrl], userId, sessionId ];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kLOGOUT Wait:wait Delegate:delegate];
    [request execute];
}

+(void) isSessionActiveWithUserId: (NSString* ) userId SessionId:( NSString *) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@IsSessionActive/%@/%@", [super getUrl], userId, sessionId ];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kSESSIONACTIVE Wait:wait Delegate:delegate];
    [request execute];
}
@end
