//
//  ConnectionManagement.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 16/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ConnectionManagement.h"
#import "Reachability.h"

@implementation ConnectionManagement

+ (BOOL) isAvailableConnection {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    return YES;
}

+ (BOOL) isAvailableConnectionWiFi {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWiFi) {
        return YES;
    }
    return NO;
}

+ (BOOL) isAvailableConnectionWWAN {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == ReachableViaWWAN) {
        return YES;
    }
    return NO;
}

@end
