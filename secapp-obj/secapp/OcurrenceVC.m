//
//  OcurrenceVC.m
//  secapp
//
//  Created by SecApp on 28/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "OcurrenceVC.h"
#import "CustomAnnotation.h"
#import "ContainerVC.h"
#import "MapLocationVC.h"
#import "Friend.h"
#import "Profile.h"
#import "Service.h"
#import "CreateOccurrence.h"
#import "AlertView.h"


@interface OcurrenceVC () <UINavigationControllerDelegate, AlertViewDelegate>
@property UIView* mainView;



@end

@implementation OcurrenceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void) drawInView:(UIView*) view {
    
    int height = 150;
   self.mainView=view;
    CGRect frame = CGRectMake(0, view.frame.size.height, view.frame.size.width, height);
    self.view.frame = frame;
    frame.origin.y -= height;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        self.isVisible = YES;
    }];
    
}
- (IBAction)closeView:(id)sender {
    self.map.creatingOccurrence = NO;
    [self.map drawPinsOnMap];
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    [self.mainView addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        self.isVisible = NO;
        [self.view removeFromSuperview];
    }];
    
   
}
- (void)close {
    self.map.creatingOccurrence = NO;
   
    CGRect frame = self.view.frame;
    frame.origin.y += frame.size.height;
    [self.mainView addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        self.isVisible = NO;
        [self.view removeFromSuperview];
    }];
    
    
}
- (IBAction)nextStep:(id)sender {
    if(self.latitude && self.longitude){
    self.map.creatingOccurrence = NO;
       self.createOccurrenceView = [[UIStoryboard storyboardWithName:@"Occurrence" bundle:nil] instantiateViewControllerWithIdentifier:@"CreateOccurrenceVC"];
    self.createOccurrenceView.typeOccurrence=self.type;
    self.createOccurrenceView.latitude = self.latitude;
    self.createOccurrenceView.longitude=self.longitude;
    self.createOccurrenceView.map=self.map;
    [self.createOccurrenceView drawInView:self.contView.view]; //cambiar mainview por container
    [self close];
           }
    else{
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"sin.punto.en.mapa", nil) Delegate:nil CancelTitle:NSLocalizedString(@"Aceptar", nil) OkTitle:nil InController:self.map.parentViewController];
        alert.delegate=self;
        return;
    }
    
}


@end
