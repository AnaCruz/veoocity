//
//  PositionsServices.h
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Service.h"

@interface PositionsServices : Service


/**
 * Update the position of the user.
 * @param latitude Latitude.
 * @param longitude Longitude.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) sendMyPositionWithLatitude:(NSNumber*) latitude Longitude:(NSNumber*) longitude Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Update the user positions record.
 * @param positions Array of positions of type Location.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+ (void) sendAllPosition:(NSArray*) positions WithWait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

/**
 * Update the user position in hiden status.
 * @param hide status of the user, active or inactive
 * @param latitude Latitude.
 * @param longitude Longitude.
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) changePositionWithHide:(BOOL) hide Latitude:(NSNumber*) latitude Longitude:(NSNumber*) longitude Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;
+(void)sendGPSstatus:(BOOL) gpsStatus Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate;

@end
