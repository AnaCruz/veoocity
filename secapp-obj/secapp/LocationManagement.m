////
//  LocationManagement.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 16/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "LocationManagement.h"
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>
#import "LocationUser.h"
#import "DeviceUtils.h"
#import "Profile.h"
#import "Service.h"
#import "NearIncidences.h"
#import "PushLocalSended.h"
#import "GetNotification.h"
#import "Occurrence.h"

@interface LocationManagement () <CLLocationManagerDelegate>

@property CLLocationManager* locationManager;
@property LocationUser* locationUser;
@property int languaje;
@property int numIncicencesInit; //numero de incidencias inicial que contiene groupIncidencesNear
@property (strong, nonatomic) CLLocation * lastLocation;
@end

@implementation LocationManagement

+ (LocationManagement*) sharedInstance {
    static LocationManagement *shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shared = [[self alloc]init];
    });
    return shared;
}

/**
 * Builder the class.
 * @return Instance of a class.
 */
- (instancetype) init {
    self = [super init];
    if (self) {
        _delegates = [NSArray array];
        _longitude = [NSNumber numberWithDouble:0.0];
        _latitude = [NSNumber numberWithDouble:0.0];
        if (!self.locationManager) {
            self.locationManager = [[CLLocationManager alloc] init];
        }
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation; // Use the highest possible accuracy and combine it with additional sensor data. Declarado para el servicio 1
        self.locationManager.distanceFilter=100 ; //kCLDistanceFilterNone All movements are reported. Declarado para el servicio 1
        if ([DeviceUtils deviceVersion] >= 8) {
            [_locationManager requestAlwaysAuthorization];
        }
        if ([DeviceUtils deviceVersion] >= 9) {
            [self.locationManager setAllowsBackgroundLocationUpdates:YES];
        }
        //        if ([CLLocationManager locationServicesEnabled]) {
        [_locationManager startUpdatingLocation]; //servicio estandar (1)
        // [_locationManager startMonitoringSignificantLocationChanges]; //servicio de cambios de ubicacion significates (2)
        //        }
        self.languaje=[[Service getLanguaje]intValue];
        self.locationManager.pausesLocationUpdatesAutomatically=NO; //pausar los servicios en caso de que solo se use start updatingLocation
        self.locationManager.activityType= CLActivityTypeOther;
    }
    return self;
}

/**
 * Add a delegate to notify the update of location.
 */
- (void) addDelegate:(id<LocationDelegate>) delegate {
    if (delegate) {
        NSMutableArray* tmpDelegates = [self.delegates mutableCopy];
        [tmpDelegates addObject:delegate];
        self.delegates = [tmpDelegates copy];
    }
}

/**
 * Remove a delegate to notify the update of location.
 */
- (void) removeDelegate:(id<LocationDelegate>) delegate {
    if (delegate) {
        NSMutableArray* tmpDelegates = [self.delegates mutableCopy];
        [tmpDelegates removeObject:delegate];
        self.delegates = [tmpDelegates copy];
    }
}

- (void) startLocation {
    [self.locationManager startUpdatingLocation];
}

- (void) stopLocation {
    [self.locationManager stopUpdatingLocation];
}

- (void) startMonitoringSignificantLocationChanges {
    [self.locationManager startMonitoringSignificantLocationChanges];
}
- (void) stopMonitoringSignificantLocationChanges {
    [self.locationManager stopMonitoringSignificantLocationChanges];
}

- (void) startMonitoring {
    [[LocationUser shareInstance] startMonitoring];
}

- (void) stopMonitoring {
    [[LocationUser shareInstance] stopMonitoring];
}

- (BOOL) isGPSActive {
    if([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        return YES;
    }
    return NO;
}

#pragma mark Delegate "CLLocationManagerDelegate"

/**
 * Update the location.
 */
- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation* location = [locations lastObject];
    NSDate* eventDate = location.timestamp;
    NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
    if (fabs(howRecent) < 10.0) {
        // If the event is recent, do something with it.
               // La posición de latitud y longitud debe de ser diferente de 0.
        if (location && (location.coordinate.longitude != 0.0 && location.coordinate.latitude != 0.0)) {
                 NSLog(@"latitude %+.6f, longitude %+.6f\n,accurancy %+.6f\n ", location.coordinate.latitude,location.coordinate.longitude, location.horizontalAccuracy);
            int intlat =location.coordinate.latitude;
            int intlong= location.coordinate.longitude;
            NSString *latString= [NSString stringWithFormat:@"%d",intlat];
            NSString *longString= [NSString stringWithFormat:@"%d", intlong];
            if ([latString isEqualToString:@"0"]&&[longString isEqualToString:@"0"]) {
                return;
            }
                self.longitude = [NSNumber numberWithDouble:location.coordinate.longitude];
                self.latitude = [NSNumber numberWithDouble:location.coordinate.latitude];
                self.lastLocation = location;
                [self chekLocationOccurrences];
                for (id<LocationDelegate> delegate in self.delegates) {
                    if ([delegate respondsToSelector:@selector(onLocationChange:)]) {
                        [delegate onLocationChange:location];
                    }
                }
       }
    }
}
-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager{
    NSLog(@"servicio de ubicacion pausado!!!");
}
-(void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager{
    NSLog(@"servicio de ubicacion reanudado");
}

-(void) chekLocationOccurrences{
    //revisar si el usuario tiene encendidas las incidencias cercanas
    // revisar si no existe el array de incidencias cercanas detectadas. si no existe realizar un barrido para detectar incidencias cercanas y trabajar sobre ellas
 // Si si existe continuar con el proceso de identificacion de movimiento en
    // base a las incidencias cercanas detectadas.
   Profile * profile = [Profile findWithUserId:[Service getUserId]];
    BOOL occ= [profile.showOccurrenceNotifications boolValue];
    if (occ==true) {
        NSArray* incidencesNear2= [NearIncidences findAll];
        if ([incidencesNear2 count]<=0) {
            //metodo que recorre todas las incidencias y determina las cercanas
            incidencesNear2= [[self findNearOccurrencesOnAllMapWithLocation:self.lastLocation]copy];
            if (incidencesNear2!=nil && [incidencesNear2 count]>0) {
                self.numIncicencesInit=(int)[incidencesNear2 count];
                //empezar a revisar las incidencias detectadas para mandar push
                NSLog(@"Incidences near new %@", incidencesNear2);
                [self roamArrayGroupIncidencesNearWithArray:incidencesNear2];
            }
        }else if (incidencesNear2!=nil && [incidencesNear2 count]>0) {
                //metodo para seguir monitoreando incidencias detectadas
                self.numIncicencesInit=(int)[incidencesNear2 count];
            NSLog(@"Incidences near %@", incidencesNear2);
                [self roamArrayGroupIncidencesNearWithArray:incidencesNear2];
        }
   }
}
-(NSArray *)findNearOccurrencesOnAllMapWithLocation:(CLLocation *)locationUser{
    NSLog(@"Find near incidences");
    NSArray * result = [Occurrence findAll];
    NSMutableArray* arrayTmp = [[NSMutableArray alloc] init];
    for (Occurrence *occ in result) {
        CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:[occ.latitude floatValue] longitude:[occ.longitude floatValue]];
        CLLocationDistance distance = [locationUser distanceFromLocation:endLocation];
        if (distance<=40) {
            NearIncidences *incidence = [NearIncidences createObject];
            incidence.latitude=occ.latitude;
            incidence.longitude=occ.longitude;
            incidence.type = occ.type;
            incidence.occId=occ.occurrenceId;
            [incidence saveObject];
          [arrayTmp addObject:occ];
        }
    }
    NSArray* array = [NearIncidences findAll];
    return array;
    
}
-(void)roamArrayGroupIncidencesNearWithArray:(NSArray*)arrayIncidencesCopy{
 
    for (NearIncidences *occ in arrayIncidencesCopy) {
        
        CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:[occ.latitude floatValue] longitude:[occ.longitude floatValue]];
        CLLocationDistance distance = [self.lastLocation distanceFromLocation:endLocation];
        NSDate * currentTime= [NSDate date];
        if (distance<=40) { // if distance
           NSArray *arrayPushSended = [PushLocalSended findAll];
           if (arrayPushSended && [arrayPushSended count]>0) { //arrayPush
                //revisar si la incidencia s encuentra dentro de las push ya enviadas
               if ([[PushLocalSended findWithPushOccurrenceId:occ.occId]count]>0) {//1
                    PushLocalSended * pushFound =[[PushLocalSended findWithPushOccurrenceId:occ.occId]objectAtIndex:0];
                    NSLog(@"Incidence founded %@", pushFound);
                    //la incidencia ya se habia notificado, revisar la diferencia de la distancia para notificar de nuevo
                    CGFloat difDist= distance - [pushFound.distance floatValue];
                   
                   if (fabs(difDist)>=20) {//3
                       NSLog(@" dist %f", fabs(difDist));
                        //generar push y actualizar campo  de fecha y distancia en el objecto
                        pushFound.dateSend=currentTime;
                        pushFound.distance=[NSString stringWithFormat:@"%f",distance];
                        [pushFound mergeObject];
                        [self generatePushLocalWithDistanceMeters:distance Latitude:[NSString stringWithFormat:@"%@", occ.latitude] Longitude:[NSString stringWithFormat:@"%@", occ.longitude] AndTypeOccurrence:occ.type OccurrenceId:occ.occId];
                    }
                        else{//3
                     //   diferencia de horas
                        NSTimeInterval distanceBetweenDates = [currentTime timeIntervalSinceDate:pushFound.dateSend];
                        double secondsInAnHour = 3600;
                        NSInteger hoursBetweenDates = distanceBetweenDates / secondsInAnHour;
                        
                            if (hoursBetweenDates>=2) {//4
                            //si han pasado 2 horas generar push y actualizar el campo de fecha y distancia enviadas
                            NSLog(@"hours %ld", (long)hoursBetweenDates);
                            pushFound.dateSend=currentTime;
                            pushFound.distance=[NSString stringWithFormat:@"%f",distance];
                            [pushFound mergeObject];
                            [self generatePushLocalWithDistanceMeters:distance Latitude:[NSString stringWithFormat:@"%@", occ.latitude] Longitude:[NSString stringWithFormat:@"%@", occ.longitude] AndTypeOccurrence:occ.type OccurrenceId:occ.occId];
                        }//4
                    }//3 else
                }//1
                else { //1
                    PushLocalSended *pushNew= [PushLocalSended createObject];
                    pushNew.occId=occ.occId;
                    pushNew.dateSend=currentTime;
                    pushNew.distance=[NSString stringWithFormat:@"%f",distance];
                    [pushNew saveObject];
                    [self generatePushLocalWithDistanceMeters:distance Latitude:[NSString stringWithFormat:@"%@", occ.latitude] Longitude:[NSString stringWithFormat:@"%@", occ.longitude] AndTypeOccurrence:occ.type OccurrenceId:occ.occId];
                }//else 1
            }
            else{ //arrayPush
                //generar push
                PushLocalSended *push= [PushLocalSended createObject];
                push.occId=occ.occId;
                push.dateSend=currentTime;
                push.distance=[NSString stringWithFormat:@"%f",distance];
                [push saveObject];
                [self generatePushLocalWithDistanceMeters:distance Latitude:[NSString stringWithFormat:@"%@", occ.latitude] Longitude:[NSString stringWithFormat:@"%@", occ.longitude] AndTypeOccurrence:occ.type OccurrenceId:occ.occId];
            } //array push
            
            
        }else{ // if distance
            NSMutableArray* arrayTmp2= [[NSMutableArray alloc] init];
            arrayTmp2= [arrayIncidencesCopy mutableCopy];
            NearIncidences * incidence= [[NearIncidences findWithNearOccurrenceId:occ.occId]objectAtIndex:0];
            [incidence deleteObject];
            [arrayTmp2 removeObject:occ];
            arrayIncidencesCopy= [arrayTmp2 copy];
            int percent = (((int)[arrayIncidencesCopy count]) *100)/self.numIncicencesInit;
            if (percent <=30) {
               NSArray* pushArray =[PushLocalSended findAll];
                for (PushLocalSended* push in pushArray) {
                    [push deleteObject];
                }
                NSArray* nearArray =[NearIncidences findAll];
                for (NearIncidences* near in nearArray) {
                    [near deleteObject];
                }
                 NSLog(@"Push local: %@", [PushLocalSended findAll]);
                NSLog(@"Near incidences: %@", [NearIncidences findAll]);
                break;
            }
        } //else Distance
    }//for
}

-(void)generatePushLocalWithDistanceMeters: (CGFloat)meters Latitude:(NSString *)latitude Longitude:(NSString *)longitude AndTypeOccurrence:(NSNumber *)type OccurrenceId:(NSString*)occId{
    //generar push notification estas a n metros de una incidencia de tipo en ingles o español
    
    NSString * nameOccurrence;
    switch ([type intValue]) {
        case 1:
            if (self.languaje==1) {
                nameOccurrence= @"ROBO";
            }
            else{
                nameOccurrence= @"ROBBERY";
            }
            break;
        case 2:
            if (self.languaje==1) {
                nameOccurrence= @"ACTIVIDAD SOSPECHOSA";
            }else{
                nameOccurrence= @"SUSPICIOUS ACTIVITY";
            }
            break;
        case 3:
            if (self.languaje==1) {
                nameOccurrence= @"CRIMEN";
            }else{
                nameOccurrence= @"CRIME";
            }
            break;
        case 4:
            if (self.languaje==1) {
                nameOccurrence= @"DESAPARICION DE NIÑOS";
            }else{
                nameOccurrence= @"MISSING CHILDREN";
            }
            break;
        case 5:
            if (self.languaje==1) {
                nameOccurrence= @"CORRUPCION";
            }else{
                nameOccurrence= @"CORRUPTION";
            }
            break;
        case 6:
            if (self.languaje==1) {
                nameOccurrence= @"ATRACO DE AUTOS";
            }else{
                nameOccurrence= @"CAR ROBBERY";
            }
            break;
        case 7:
            if (self.languaje==1) {
                nameOccurrence= @"PERDIDA DE MASCOTAS";
            }else{
                nameOccurrence= @"PET LOSS";
            }
            break;
        case 8:
            if (self.languaje==1) {
                nameOccurrence= @"MALTRATO ANIMAL";
            }else {
                nameOccurrence= @"ANIMAL ABUSE";
            }
            break;
        case 9:
            if (self.languaje==1) {
                nameOccurrence= @"ME SIENTO BIEN";
            }else{
                nameOccurrence= @"FEELING GOOD";
            }
            break;
      default:
            if (self.languaje==1) {
                nameOccurrence= @"ESPECIAL";
            }else{
                nameOccurrence= @"SPECIAL";
            }
      break;
    }
    
    NSLog(@"te estas acercando a la incidencia %@. Estas a %f metros, id: %@ ", nameOccurrence, meters ,occId);
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.maximumFractionDigits = 0;
    formatter.roundingMode = NSNumberFormatterRoundHalfUp;
    NSString *numberString = [formatter stringFromNumber:@(meters)];
    
    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:60];
    if (self.languaje==1) {
        localNotification.alertBody = [NSString stringWithFormat:@"Estas a %@ metros de una incidencia %@", numberString, nameOccurrence];
        localNotification.alertAction = @"Ver la incidencia";
    }else{
        localNotification.alertBody = [NSString stringWithFormat:@"You are %@ meters from an incidence %@", numberString, nameOccurrence];
        localNotification.alertAction = @"Show me the incidence";
    }
    localNotification.timeZone = [NSTimeZone defaultTimeZone];
    localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    //agregar la notificacion a la lista
    NSDictionary * dicLocalNoti= @{
                                   @"ChatGroupName":[NSString stringWithFormat:@"%@", type],
                                   @"Date":[NSDate date],
                                   @"GroupName": numberString,
                                   @"InvitationId": occId,
                                   @"Latitude": latitude,
                                   @"Longitude": longitude,
                                   @"Message" : [NSString stringWithFormat:@"Estas a %@ metros de una incidencia %@", numberString, nameOccurrence],
                                   @"TypeId":@"NearOccurrence",
                                   @"UserIdFrom": [Service getUserId]
                                   };
    GetNotification* notification = [GetNotification shareInstance];
    [notification addOccurrencesLocalNotification:dicLocalNoti];
}

@end
