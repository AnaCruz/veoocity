//
//  ChatRomCell.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 15/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ChatRomCell.h"

@implementation ChatRomCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self layoutIfNeeded];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
