//
//  DisplayOccurrenceVC.h
//  secapp
//
//  Created by SecApp on 03/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Occurrence.h"
@class Occurrence;

@interface DisplayOccurrenceVC : UIViewController
@property (strong) Occurrence * occ;
@property (strong) NSString * occurrenceiD;
@property BOOL isShowInMap;
- (void) drawInView:(UIView*) view;
- (IBAction)closeView:(id)sender;
@end
