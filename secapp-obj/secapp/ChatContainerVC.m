//
//  ChatContainerVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ChatContainerVC.h"
#import "FriendListVC.h"
#import "ConversationVC.h"
#import "EditGroupVC.h"
#import "ChatVC.h"
#import "GetNotification.h"
#import "GetChatList.h"
#import "Notification.h"
#import "GroupListVC.h"
#import "Friend.h"
#import "AlertView.h"

@interface ChatContainerVC () <UINavigationControllerDelegate, ChatDelegate, ContactDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIButton *lastMessageButton;
@property (weak, nonatomic) IBOutlet UIButton *contactListButton;
@property (weak, nonatomic) IBOutlet UIButton *groupListButton;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *mainContainer;


@property UIViewController * contactListView;
@property UIViewController * conversationIndivView;
@property UIViewController * listGroupView;
@property UIViewController * editGroupView;
@property UIViewController * lastMessageView;

@end

@implementation ChatContainerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showLastMessage:self.lastMessageButton];
}

-(void)viewWillAppear:(BOOL)animated{
    if ([[Friend findAll]count]==0) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        
        [alert showWithTitle:nil Message:NSLocalizedString(@"no.amigos", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil)  OkTitle:nil InController:self.navigationController.parentViewController];
        alert.delegate=self;
 
    }
}
-(void)notification{
    ChatVC * chat= (ChatVC*)self.lastMessageView;
    [chat updateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)menu:(UIButton *)sender {
    switch (sender.tag) {
        case 1:
           [self showLastMessage:sender];
            break;
        case 2:
            [self showContactList:sender];
            break;
        case 3:
            [self showGroupList:sender];
            break;
        default:
            break;
    }
     [sender setSelected:!sender.isSelected];
     [self clearSelection:sender];
}
- (void) clearSelection:(UIButton*) sender {
    if (self.lastMessageButton.tag != sender.tag && self.lastMessageButton.selected) {
        [self showLastMessage:self.lastMessageButton];
        self.lastMessageButton.selected = NO;
    }
    if (self.contactListButton.tag != sender.tag && self.contactListButton.selected) {
        [self showContactList:self.contactListButton];
        self.contactListButton.selected = NO;
    }
    if (self.groupListButton.tag != sender.tag && self.groupListButton.selected) {
        [self showGroupList:self.groupListButton];
        self.groupListButton.selected = NO;
    }
   
}

- (void) showContactList:(UIButton*) sender {
    if (sender.isSelected) {
        return;
    }
    if (!self.contactListView) {
        self.contactListView = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatContactsVC"];
        [self addChildViewController:self.contactListView];
        ((FriendListVC*) self.contactListView).delegate = self;
    }
    [self clearViewContainer];
    CGRect frame = CGRectMake( 0, 0,
                       self.containerView.frame.size.width, self.containerView.frame.size.height);
    self.contactListView.view.frame = frame;
    [self.containerView addSubview:self.contactListView.view];

}



-(void) showGroupList:(UIButton *) sender{
    if (sender.isSelected) {
        return;
    }
    if (!self.listGroupView) {
        self.listGroupView = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatGroupsVC"];
        [self addChildViewController:self.listGroupView];
        ((GroupListVC*) self.listGroupView).delegate = self;
    }
    
    [self clearViewContainer];
    CGRect frame = CGRectMake(0, 0,
                       self.containerView.frame.size.width, self.containerView.frame.size.height);
    self.listGroupView.view.frame = frame;
    [self.containerView addSubview:self.listGroupView.view];

}

-(void) showLastMessage:(UIButton *)sender{
    if (sender.isSelected) {
        return;
    }
    if (!self.lastMessageView) {
        self.lastMessageView = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ChatRecentVC"];
        [self addChildViewController:self.lastMessageView];
        ((ChatVC*)self.lastMessageView).delegate = self;
    }
    [self clearViewContainer];
    CGRect frame = CGRectMake( 0, 0,
                       self.containerView.frame.size.width,
                        self.containerView.frame.size.height);
    self.lastMessageView.view.frame = frame;
    [self.containerView addSubview:self.lastMessageView.view];

}

- (void) clearViewContainer {
    for (UIView* view in self.containerView.subviews) {
        [view removeFromSuperview];
    }
}

-(void)showIndividualChat:(UIViewController *)chatVC {
    [self.navigationController pushViewController:chatVC animated:YES];
}

-(void)showViewController:(UIViewController *)viewController {
    [self.navigationController pushViewController:viewController animated:YES];
}

@end
