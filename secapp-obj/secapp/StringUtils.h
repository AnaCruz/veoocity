//
//  StringUtils.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 03/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringUtils : NSObject

/**
 * Validate that a string have more to a characters.
 * @param string String to validate.
 */
+ (BOOL) isEmpty:(NSString*) string;

/**
 * Validate that a array of string have more to a characters.
 * @param strings Array of string to validate.
 */
+ (BOOL) isEmptyStringsInArray:(NSArray*) strings;

/**
 * Validate that a string is a email.
 * @param mail String to validate.
 */
+ (BOOL) validateEmail:(NSString*) mail;

/**
 * Validate that a string is a number phone.
 * @param phone String to validate.
 */
+ (BOOL) validateNumberPhone: (NSString *) phone;

+(NSString *)formattedDateWithDate: (NSDate *)date;
@end
