//
//  AlertCell.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 14/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;

@property (weak, nonatomic) IBOutlet UIButton *chat;
@property (weak, nonatomic) IBOutlet UIButton *call;

@property (weak, nonatomic) IBOutlet UIButton *location;
@property (weak, nonatomic) IBOutlet UILabel *dateAlert;

@end
