//
//  GenericObject.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 26/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "GenericObject.h"
#import "AppDelegate.h"

@implementation GenericObject

+ (instancetype) createObjectWithNameEntity:(NSString*) nameEntity {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription entityForName:nameEntity inManagedObjectContext:context];
    
    id object = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:context];
    return object;
}

+ (NSArray*) findAllWithNameEntity:(NSString*) nameEntity {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:nameEntity];
    NSError *error=nil;
    NSArray *array = [context executeFetchRequest:request error:&error];
    if (error) {
        // Controlamos los posibles errores
        NSLog(@"GenericObject (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}

- (void) saveObject {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSError * error= nil;
    if ([context save:&error]==NO) {
        NSAssert(NO, @"Error Salvando objeto a CoreData: %@\n%@", [error localizedDescription], [error userInfo]);
    }
  // [context insertObject:self]; // esta se quito
}

- (void) mergeObject {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    [context refreshObject:self mergeChanges:YES];
}

- (void) deleteObject {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    [context deleteObject:self];
}



@end
