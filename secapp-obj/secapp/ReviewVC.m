//
//  ReviewVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 18/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ReviewVC.h"
#import "AppDelegate.h"
#import "Service.h"
#import "AlertView.h"

//posfijo ud = UserDefault
#define udHAS_COMMENT @"udHAS_COMMENT"
#define udCOMMENT_COUNT @"udCOMMENT_COUNT"

@interface ReviewVC () <ResponseDelegate, AlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *comment;
@property int count;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property BOOL isVisibleKeyboard;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerY;
@end

@implementation ReviewVC

+ (instancetype) shareInstance {
    static id shared = nil;
    @synchronized(self) {
        if (shared == nil)
            shared = [[UIStoryboard storyboardWithName:@"Review" bundle:nil] instantiateViewControllerWithIdentifier:@"ReviewVC"];
    }
    return shared;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _count = 0;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        if ([ud objectForKey:udCOMMENT_COUNT]) {
            _count = [[ud objectForKey:udCOMMENT_COUNT] intValue];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.comment.layer.cornerRadius = 6;
}

- (void) showNow {
    if ([self hasComment]) {
        return;
    }
    self.isVisibleKeyboard = NO;
    [self.view endEditing:YES];
    UIWindow *window = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).window;
    UIViewController* vc = [self viewController:window.rootViewController];
    self.view.frame = vc.view.frame;
    [vc.view addSubview:self.view];
    [vc addChildViewController:self];
    // Call this method somewhere in your view controller setup code.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    self.isVisibleKeyboard = YES;
   
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    self.centerY.constant = self.centerY.constant + (kbSize.height/3);
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    self.isVisibleKeyboard = NO;
    self.centerY.constant = 0;
}
-(void)viewDidDisappear:(BOOL)animated{
    self.isVisibleKeyboard = NO;
    self.centerY.constant = 0;
}
- (IBAction)closeView:(id)sender {
    if ([sender isKindOfClass:UITapGestureRecognizer.class]) {
        if (self.isVisibleKeyboard) {
            [self.view endEditing:YES];
            return;
        }
        UITapGestureRecognizer* tg = (UITapGestureRecognizer*)sender;
        UIView* view = tg.view;
        CGPoint loc = [tg locationInView:view];
        UIView* subview = [view hitTest:loc withEvent:nil];
        if(subview == self.viewContainer){
            return;
        }
    }
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void) plusCount:(int)plus {
    if ([self hasComment]) {
        return;
    }
    self.count += plus;
    if (self.count >= 20) {
        self.count = 0;
        [self showNow];
    }
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:self.count] forKey:udCOMMENT_COUNT];
}

-(UIViewController *)viewController:(UIViewController *)rootViewController {
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self viewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self viewController:presentedViewController];
}

- (BOOL) hasComment {
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([ud boolForKey:udHAS_COMMENT]) {
        return YES;
    }
    return NO;
}

#pragma mark - IBActions

- (IBAction)sendComment:(id)sender {
    NSString* comment = [self.comment.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if ([comment isEqualToString:@""]) {
       
        return;
    }
    
    [Service sendComment:comment WithWait:YES Delegate:self];
}

- (IBAction)goToAppStore:(id)sender {
    NSString *str = @"http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=636813818&mt=8";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udHAS_COMMENT];
}

#pragma mark - Method of ResponseDelagate

- (void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    NSDictionary *dicResult;
    int responseCode = -1;
    switch (service) {
        case kSEND_COMMENT:
            dicResult = [dictionary objectForKey:@"SendCommentResult"];
            responseCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udHAS_COMMENT];
                [self closeView:nil];
            }
            if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        case kHAS_COMMENT:
            responseCode = [[[response objectForKey:@"HasCommentedResult"] objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:udHAS_COMMENT];
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }

            if (responseCode==407) {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:udHAS_COMMENT];
                
            }
            break;
        default:
            break;
    }
}


@end
