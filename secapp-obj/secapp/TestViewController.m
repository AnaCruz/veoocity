//
//  TestViewController.m
//  secapp
//
//  Created by SecApp on 05/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "TestViewController.h"
#import "OccurrenceViewCell.h"
#import "MapLocationVC.h"
#import "Service.h"


@interface TestViewController () < UINavigationControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;
@property UIView * mainView;
@property NSString * typeOccurrence;
@property NSArray * list;
@property int languaje;


@end

@implementation TestViewController
- (void) drawInView:(UIView*) view {
    
    CGRect frame =view.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.view.frame = frame;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}
- (IBAction)closeView:(id)sender {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
    
}
- (void)buttonTap:(UIButton*)sender {
        switch (sender.tag) {
        case 1:
           
            self.typeOccurrence= @"1";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 2:
         

            self.typeOccurrence= @"2";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 3:
           

           self.typeOccurrence= @"3";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 4:
           
            self.typeOccurrence= @"4";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 5:
                       self.typeOccurrence= @"5";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 6:
           
            self.typeOccurrence= @"6";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 7:
                     self.typeOccurrence= @"7";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 8:
                       self.typeOccurrence= @"8";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
        case 9:
          
            self.typeOccurrence= @"9";
            [self.map createOccurrence:self.typeOccurrence];
            [self closeView:nil];
            break;
            
        default:
            break;
    }
    
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background_menuOcc"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
    self.collectionView.delegate=self;
    self.collectionView.dataSource=self;
     self.languaje=[[Service getLanguaje]intValue];
    
        self.list =  [NSArray array];
        NSDictionary * dic1 = @{
                                @"image":[UIImage imageNamed:@"1"],
                                @"text": @"ROBO",
                                @"textEn": @"ROBBERY",
                                @"type": @"1"
                                };
        NSDictionary * dic2 = @{
                                @"image":[UIImage imageNamed:@"2"],
                                @"text": @"ACTIVIDAD SOSPECHOSA",
                                @"textEn": @"SUSPICIOUS ACTIVITY",
                                @"type": @"2"
                                };
        NSDictionary * dic3 = @{
                                @"image":[UIImage imageNamed:@"3"],
                                @"text": @"CRIMEN",
                                @"textEn": @"CRIME",
                                @"type": @"3"
                                };
    
        NSDictionary * dic4 = @{
                                @"image":[UIImage imageNamed:@"4"],
                                @"text": @"DESAPARICION DE NIÑOS",
                                @"textEn": @"MISSING CHILDREN",
                                @"type": @"4"
                                };
        NSDictionary * dic5 = @{
                                @"image":[UIImage imageNamed:@"5"],
                                @"text": @"CORRUPCION",
                                @"textEn": @"CORRUPTION",
                                @"type": @"5"
                                };
        NSDictionary * dic6 = @{
                                @"image":[UIImage imageNamed:@"6"],
                                @"text": @"ATRACO DE AUTOS",
                                @"textEn": @"CAR ROBBERY",
                                @"type": @"6"
                                };
        NSDictionary * dic7 = @{
                                @"image":[UIImage imageNamed:@"7"],
                                @"text": @"PERDIDA DE MASCOTAS",
                                @"textEn": @"PET LOSS",
                                @"type": @"7"
                                };
        NSDictionary * dic8 = @{
                                @"image":[UIImage imageNamed:@"8"],
                                @"text": @"MALTRATO ANIMAL",
                                @"textEn": @"ANIMAL ABUSE",
                                @"type": @"8"
                                };
        NSDictionary * dic9 = @{
                                @"image":[UIImage imageNamed:@"9"],
                                @"text": @"ME SIENTO BIEN",
                                @"textEn": @"FEELING GOOD",
                                @"type": @"9"
                                };
        
        
        self.list = @[dic1, dic2, dic3, dic4, dic5, dic6, dic7, dic8, dic9];
     
       
        //  [self.occurrenceView reloadData];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    //#warning Incomplete implementation, return the number of sections
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    //#warning Incomplete implementation, return the number of items
    return 9;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    OccurrenceViewCell *cell = (OccurrenceViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
     NSDictionary *dic =[self.list objectAtIndex:indexPath.row];
     if (self.languaje==1) {
         cell.textLabel.text = [dic objectForKey:@"text"];
     
     }else{
         cell.textLabel.text = [dic objectForKey:@"textEn"];

     }
    [cell.iconButton setBackgroundImage:[UIImage imageNamed:[dic objectForKey:@"type"]] forState:UIControlStateNormal];
    cell.iconButton.tag=[[dic objectForKey:@"type"]intValue];

    [cell.iconButton addTarget:self action:@selector(buttonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    float width = self.collectionView.frame.size.width;
     float height = self.collectionView.frame.size.height;
    width = (width - 40)  / 3; // calcular el ancho de la vista
     height = (height - 20)  / 3; // calcular el ancho de la vista
    return CGSizeMake(width, height);
}


@end
