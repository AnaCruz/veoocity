//
//  EditeInsecureZoneVC.m
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EditeInsecureZoneVC.h"
#import "CircleOverlay.h"
#import "InsecureZoneServices.h"
#import "InsecureZone.h"
#import "AlertView.h"
#import "AppDelegate.h"


@interface EditeInsecureZoneVC ()  <UITextFieldDelegate, ResponseDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *fieldName;
@property CircleOverlay *tmpZone;
@property CGRect originalPosition;

@property (weak, nonatomic) IBOutlet UITextField *textTimeHrInitial;
@property (weak, nonatomic) IBOutlet UITextField *textTimeMinInitial;
@property (weak, nonatomic) IBOutlet UITextField *textTimeHrFinal;
@property (weak, nonatomic) IBOutlet UITextField *textTimeMinFinal;

//@property NSString * timeHrFirst;
//@property NSString * timeMinFirst;
//@property NSString * timeHrFinal;
//@property NSString * timeMinFinal;


@end

@implementation EditeInsecureZoneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self registerForKeyboardNotifications];
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.isShow=YES;
    if ([self.zone.titleCircle isEqualToString:@""]) {
        self.fieldName.text = @"";
    }
    else {
        self.fieldName.text = self.zone.titleCircle;
       
    }

        if (![self.zone.idZone isEqualToString:@""]) {
            InsecureZone* zoneInsecure=[[InsecureZone findWithZoneId:self.zone.idZone]objectAtIndex:0];
            if (zoneInsecure.timeInitial && [zoneInsecure.timeInitial isEqualToString:@""]) {
                self.textTimeHrInitial.text=@"";
                self.textTimeMinInitial.text=@"";
                self.textTimeHrFinal.text=@"";
                self.textTimeMinFinal.text=@"";
            }else{
                self.textTimeHrInitial.text=[zoneInsecure.timeInitial substringToIndex:2];
                self.textTimeMinInitial.text=[zoneInsecure.timeInitial substringFromIndex:MAX((int)[zoneInsecure.timeInitial length]-2, 0)];
                self.textTimeHrFinal.text=[zoneInsecure.timeFinal substringToIndex:2];
                self.textTimeMinFinal.text=[zoneInsecure.timeFinal substringFromIndex:MAX((int)[zoneInsecure.timeFinal length]-2, 0)];
            }
        }
    
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self unregisterForKeyboardNotifications];
    self.isShow=NO;
    self.isEdtiting=NO;
     self.tmpZone = nil;
}

- (void) drawInView:(UIView*) view {
    CGRect frame = self.view.frame;
    frame.size.height = 130;
    frame.origin.y = view.frame.size.height - frame.size.height;
    
    frame.size.width= view.frame.size.width;
    self.view.frame = frame;
    self.originalPosition = frame;
    [view addSubview:self.view];
}

- (void)setZone:(CircleOverlay *)zone {
    _zone = zone;
    if (_zone) {
        
        if (!_tmpZone) {
            _tmpZone = [self.zone copy];
            
        }
    }
    
}
- (IBAction)saveZone:(id)sender {
      [self.view endEditing:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(editInsecureZone:SaveZone:)]) {
        if ([self.fieldName.text isEqualToString:@""]) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"nombre.zona.insegura.vacio", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;
            
            return;
        }
        if ([self.textTimeHrInitial.text isEqualToString:@""]||[self.textTimeHrFinal.text isEqualToString:@""]) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"campo.hora.vacio", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;
            
            return;
        }
        if ([self.textTimeMinInitial.text isEqualToString:@""]||[self.textTimeMinFinal.text isEqualToString:@""]) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"campo.minuto.vacio", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;
            
            return;
        }//verificar que el valor entero no sea igual
        if ([self.textTimeHrInitial.text isEqualToString:self.textTimeHrFinal.text]&&[self.textTimeMinInitial.text isEqualToString:self.textTimeMinFinal.text]) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"sin.rango.horario", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;

            return;
        }
        self.zone.titleCircle =self.fieldName.text;
        self.fieldName.text=@"";

       double diference=[[NSTimeZone localTimeZone] secondsFromGMT] / 3600;
   
          int hourInitial= [self.textTimeHrInitial.text intValue];
          int minuteInitial= [self.textTimeMinInitial.text intValue];
          int hourFinal= [self.textTimeHrFinal.text intValue];
          int minuteFinal= [self.textTimeMinFinal.text intValue];
        if(hourInitial>23 || hourFinal>23 || minuteInitial>59 || minuteFinal>59 ){
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"horario.incorrecto", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;
           
            return;

        }
        if (hourInitial==hourFinal && minuteInitial==minuteFinal) {
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"sin.rango.horario", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
            alert.delegate=self;
            
            return;
        }

        NSString * timeHrFirst;
        NSString * timeMinFirst;
        NSString * timeHrFinal;
        NSString * timeMinFinal;
        hourInitial= hourInitial-(diference);
        if (hourInitial>23) {
            hourInitial=hourInitial-24;
        }
        hourFinal=hourFinal-(diference);
        if (hourFinal>23) {
            hourFinal=hourFinal-24;
        }
        
        
        if (hourInitial<10) {
           timeHrFirst=[NSString stringWithFormat:@"0%d",hourInitial];
        }
        else{
           timeHrFirst= [NSString stringWithFormat:@"%d",hourInitial];
        }
        if (minuteInitial<10) {
            timeMinFirst=[NSString stringWithFormat:@"0%d",minuteInitial];
        }
        else{
            timeMinFirst= self.textTimeMinInitial.text;
        }
        if (hourFinal<10) {
            timeHrFinal=[NSString stringWithFormat:@"0%d",hourFinal];
        }
        else{
            timeHrFinal= [NSString stringWithFormat:@"%d",hourFinal];
        }
        if (minuteFinal<10) {
            timeMinFinal=[NSString stringWithFormat:@"0%d",minuteFinal];
        }
        else{
           timeMinFinal= self.textTimeMinFinal.text;
        }

        
        if (self.isEdtiting) {
         
            [InsecureZoneServices updateInsecurityZoneWithLatitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.latitude]
                                                         Longitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.longitude]
                                                            Radius:[NSString stringWithFormat:@"%f",self.zone.radius]
                                                              Name:self.zone.titleCircle
                                                          FisrtTime:[NSString stringWithFormat:@"%@:%@",timeHrFirst,timeMinFirst]
                                                          LastTime:[NSString stringWithFormat:@"%@:%@",timeHrFinal,timeMinFinal]
                                                            IdZone:self.zone.idZone
                                                              Wait:YES
                                                          Delegate:self];
        }
        else{
            [InsecureZoneServices createSecurityZoneWithName:self.zone.titleCircle
                                              CenterLatitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.latitude]
                                              CenterLongitude:[NSString stringWithFormat:@"%f",self.zone.coordinate.longitude]
                                              Radius:[NSString stringWithFormat:@"%f",self.zone.radius]
                                              FirstTime:[NSString stringWithFormat:@"%@:%@",timeHrFirst,timeMinFirst]
                                              LastTime:[NSString stringWithFormat:@"%@:%@",timeHrFinal,timeMinFinal]
                                              Wait:YES
                                              Delegate:self];
        }
    }
 
    
    
}


- (IBAction)cancelZone:(id)sender {
    [self.view removeFromSuperview];
    if (self.delegate && [self.delegate respondsToSelector:@selector(editInsecureZone:CancelZone:)]) {
        if (self.isEdtiting) {
            [self.delegate editInsecureZone:self CancelZone:self.tmpZone];
        } else {
            [self.delegate editInsecureZone:self CancelZone:nil];
        }
        [self.view removeFromSuperview];
    }
    self.tmpZone = nil;
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.originalPosition;
    aRect.origin.y = aRect.origin.y - (kbSize.height-aRect.size.height)-5;
    self.view.frame = aRect;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.view.frame = self.originalPosition;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kCREATE_INSECURE_ZONE:
            dictionary = [dictionary objectForKey:@"CreateInsecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                self.zone.idZone= [[dictionary objectForKey:@"ID"] stringValue];
               
                InsecureZone* zoneSave = [InsecureZone createObject];
                zoneSave.name= self.zone.titleCircle;
                NSString* radius =[NSString  stringWithFormat:@"%f", self.zone.radius];
                zoneSave.radius= [NSNumber numberWithDouble:[radius doubleValue]];
                NSString* latitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.latitude];
                zoneSave.latitude=[NSNumber numberWithDouble:[latitude doubleValue]];
                NSString* longitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.longitude];
                zoneSave.longitude= [NSNumber numberWithDouble:[longitude doubleValue]];
                zoneSave.insecureZoneId= self.zone.idZone;
                zoneSave.isOwner=[NSNumber numberWithBool:YES];
                if ([self.textTimeHrInitial.text length]<2) {
                    self.textTimeHrInitial.text=[NSString stringWithFormat:@"0%@",self.textTimeHrInitial.text];
                }
                if ([self.textTimeMinInitial.text length]<2) {
                    self.textTimeMinInitial.text=[NSString stringWithFormat:@"0%@",self.textTimeMinInitial.text];
                }
                if ([self.textTimeHrFinal.text length]<2) {
                    self.textTimeHrFinal.text=[NSString stringWithFormat:@"0%@",self.textTimeHrFinal.text];
                }
                if ([self.textTimeMinFinal.text length]<2) {
                    self.textTimeMinFinal.text=[NSString stringWithFormat:@"0%@",self.textTimeMinFinal.text];
                }
                zoneSave.timeInitial=[NSString stringWithFormat:@"%@:%@",self.textTimeHrInitial.text, self.textTimeMinInitial.text];
                zoneSave.timeFinal=[NSString stringWithFormat:@"%@:%@",self.textTimeHrFinal.text, self.textTimeMinFinal.text];
                [zoneSave saveObject];
                self.tmpZone = nil;
                [self.delegate editInsecureZone:self SaveZone:self.zone];
                
            }
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            if(responseCode==410){
            //
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"not.premium.version", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                [self.delegate editInsecureZone:self CancelZone:self.zone];

             
            }
            if(responseCode==411){
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"premium.version.expired", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
                alert.delegate=self;
                [self.delegate editInsecureZone:self CancelZone:self.zone];

            }
            break;
        case kUPDATE_INSECURITY_ZONE:
            dictionary = [dictionary objectForKey:@"UpdateInsecureZoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
          
                InsecureZone *zoneSave = [[InsecureZone findWithZoneId:self.zone.idZone]objectAtIndex:0];
                zoneSave.name= self.zone.titleCircle;
                NSString* radius =[NSString  stringWithFormat:@"%f", self.zone.radius];
                zoneSave.radius= [NSNumber numberWithDouble:[radius doubleValue]];
                NSString* latitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.latitude];
                zoneSave.latitude=[NSNumber numberWithDouble:[latitude doubleValue]];
                NSString* longitude =[NSString  stringWithFormat:@"%f", self.zone.coordinate.longitude];
                zoneSave.longitude= [NSNumber numberWithDouble:[longitude doubleValue]];
                zoneSave.insecureZoneId= self.zone.idZone;
                zoneSave.isOwner=[NSNumber numberWithBool:YES];
                if ([self.textTimeHrInitial.text length]<2) {
                    self.textTimeHrInitial.text=[NSString stringWithFormat:@"0%@",self.textTimeHrInitial.text];
                }
                if ([self.textTimeMinInitial.text length]<2) {
                    self.textTimeMinInitial.text=[NSString stringWithFormat:@"0%@",self.textTimeMinInitial.text];
                }
                if ([self.textTimeHrFinal.text length]<2) {
                    self.textTimeHrFinal.text=[NSString stringWithFormat:@"0%@",self.textTimeHrFinal.text];
                }
                if ([self.textTimeMinFinal.text length]<2) {
                    self.textTimeMinFinal.text=[NSString stringWithFormat:@"0%@",self.textTimeMinFinal.text];
                }

                zoneSave.timeInitial=[NSString stringWithFormat:@"%@:%@",self.textTimeHrInitial.text, self.textTimeMinInitial.text];
                zoneSave.timeFinal=[NSString stringWithFormat:@"%@:%@",self.textTimeHrFinal.text, self.textTimeMinFinal.text];

                [zoneSave mergeObject];
                self.tmpZone = nil;
                [self.delegate editInsecureZone:self SaveZone:self.zone];
            }
            if (responseCode==405) {
               AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
            
        default:
            break;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (self.textTimeHrFinal == textField||self.textTimeHrInitial == textField ||self.textTimeMinInitial == textField ||self.textTimeMinFinal == textField) {
        if ([textField.text length]<=1)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:1];
        }
        
        return NO;
    }else{
        if ([textField.text length]<=59)
        {
            return YES;
        }
        else if([@"" isEqualToString:string])
        {
            textField.text=[textField.text substringToIndex:59];
        }
        
        return NO;
        }
   

}


@end
