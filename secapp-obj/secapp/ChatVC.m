 //
//  ChatVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ChatVC.h"
#import <UIKit/UIKit.h>
#import "MessagesService.h"
#import "Service.h"
#import "ImageUtils.h"
#import <CoreLocation/CoreLocation.h>
#import "MapSecApp.h"
#import "Friend.h"
#import "ContactService.h"
#import "Message.h"
#import "ConversationVC.h"
#import "ContainerVC.h"
#import "EditChatGroupExistingVC.h"
#import "ConvesationGroupVC.h"
#import "ConversationVC.h"
#import "GetNotification.h"
#import "Notification.h"
#import "GetChatList.h"
#import "AlertView.h"
#import "AppDelegate.h"

@interface ChatVC () <ResponseDelegate, UINavigationControllerDelegate, AlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSMutableArray *messagesArray;
@property GetChatList * updateInButton;
@property int indexTmp;
@property NSString *groupIdToLeave;
@property BOOL isMonitor;
@property NSDictionary * userMonitor;
@property NSMutableArray *arrayMonitors;
@property BOOL isOpenLastMessageView;
@end

@implementation ChatVC

/**
 * Initialize the messagesArray.
 */
-(void)viewWillAppear:(BOOL)animated{
 
}
-(void)updateData{
    if (self.isOpenLastMessageView==YES) {
        self.messagesArray = [[Message findAll] mutableCopy];
        
        if (!self.messagesArray) {
            self.messagesArray = [NSMutableArray array];
        }
        self.arrayMonitors = [NSMutableArray array];
        [self.tableView reloadData];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
}
-(void)viewDidAppear:(BOOL)animated{
    self.messagesArray = nil;
    self.isOpenLastMessageView=YES;
     [self updateData];
}

-(void)viewDidDisappear:(BOOL)animated{
    self.messagesArray =nil;
    self.arrayMonitors=nil;
    self.isOpenLastMessageView=NO;
    [self.tableView reloadData];
}

/**
 * Return the number of rows in the section.
 @return Return the number of rows in the section.
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.messagesArray count];
}

/**
 * Draw the cell in the table view.
 * @param indexPath Index for every cell in the table view.
 * @param tableView Table view with which it is working.
 * @return A cell with two labels and a image view with the user picture profile that sended the message.
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"LastMessageCell";
    
    
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LastMessageCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
  
    // Configure the cell...
    cell.buttonEdit.hidden=YES;
    cell.buttonDelete.hidden=YES;
    
    Message * message = [self.messagesArray objectAtIndex:indexPath.row];
        if ([message.groupId intValue]>0) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.buttonChatGrupal.hidden=NO;
            cell.buttonChat.hidden=YES;
            cell.buttonChatGrupal.tag= indexPath.row;
            [cell.buttonChatGrupal addTarget:self action:@selector(buttonChatGroupClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            if ([message.isOwner isEqualToNumber:@1]) {
                cell.buttonEdit.hidden= NO;
                cell.buttonEdit.tag= indexPath.row;
                [cell.buttonEdit addTarget:self action:@selector(buttonEditClicked:) forControlEvents:UIControlEventTouchUpInside];
            }
            else{
                cell.buttonDelete.hidden= NO;
                cell.buttonDelete.tag= indexPath.row;
                [cell.buttonDelete addTarget:self action:@selector(buttonLeaveClicked:) forControlEvents:UIControlEventTouchUpInside];
            }
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            if([message.imageGroup isEqual:@""]|| message.imageGroup==nil){
                
                cell.profileImageView.image = [UIImage imageNamed:@"chat-29"];
            }
            else{
                NSString *uri= message.imageGroup;
                if (uri && ![uri isEqual: [NSNull null]])  {
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            cell.profileImageView.image = [UIImage imageWithData:data];
                        });
                    });
                }
                else{
                    cell.profileImageView.image = [UIImage imageNamed:@"chat-29"];
                }
             }
            cell.nameLabel.text =[NSString stringWithFormat:@"%@", message.groupName];
            
        });
        
    }
    else{
        NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
        NSString * userId=[userDefault objectForKey:@"userId"];
        NSString * idSender;
        if ([userId isEqualToString:[message.userIdTo stringValue]]) {
            idSender = [message.userIdFrom stringValue];
        }
        else {
            idSender = [message.userIdTo stringValue];
        }
        
        if ([message.fromMonitor boolValue]==YES||[message.toMonitor boolValue]==YES) {
            NSDictionary * dic=@{
                                 @"UserId":idSender,
                                 @"IndexPath":[NSString stringWithFormat:@"%lu",(long)indexPath.row]
                                 };
            [self.arrayMonitors addObject:dic];
        }
        NSArray * dataFriend = [Friend findWithUserId:idSender];
        Friend* friend;
        if (dataFriend && [dataFriend count] > 0) {
            friend = [dataFriend objectAtIndex:0];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.buttonChat.hidden=NO;
                cell.buttonChatGrupal.hidden=YES;
                cell.buttonChat.tag= [friend.userId integerValue];
                [cell.buttonChat addTarget:self action:@selector(buttonChatIndividualClicked:) forControlEvents:UIControlEventTouchUpInside];
                
                cell.profileImageView.clipsToBounds = YES;
                int radius = cell.profileImageView.frame.size.width/2;
                cell.profileImageView.layer.cornerRadius = radius;
                cell.profileImageView.layer.borderWidth = 3;
                cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                
                if([friend.pictureProfile isEqual:@""]|| friend.pictureProfile==nil){
                    
                    cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
                }
                else{
                    cell.profileImageView.image = [UIImage imageWithData:friend.pictureProfile];
                }
                if ([message.fromMonitor boolValue]||[message.toMonitor boolValue]==YES) {
                    cell.nameLabel.text =[NSString stringWithFormat:@"Monitor/%@", friend.name];
                }else{
                cell.nameLabel.text =[NSString stringWithFormat:@"%@  %@", friend.name,friend.lastName];
                }
                
                
            });
        } else if ([message.fromMonitor boolValue]==YES||[message.toMonitor boolValue]==YES) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.buttonChat.hidden=NO;
                cell.buttonChatGrupal.hidden=YES;
                cell.buttonChat.tag= [idSender integerValue];
                [cell.buttonChat addTarget:self action:@selector(buttonChatIndividualClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.profileImageView.clipsToBounds = YES;
                int radius = cell.profileImageView.frame.size.width/2;
                cell.profileImageView.layer.cornerRadius = radius;
                cell.profileImageView.layer.borderWidth = 3;
                cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
                cell.nameLabel.text = [NSString stringWithFormat:@"Monitor: %@",idSender];
               
            });
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.buttonChat.hidden=NO;
                cell.buttonChatGrupal.hidden=YES;
                cell.buttonChat.tag= [idSender integerValue];
                [cell.buttonChat addTarget:self action:@selector(buttonChatIndividualClicked:) forControlEvents:UIControlEventTouchUpInside];
                cell.profileImageView.clipsToBounds = YES;
                int radius = cell.profileImageView.frame.size.width/2;
                cell.profileImageView.layer.cornerRadius = radius;
                cell.profileImageView.layer.borderWidth = 3;
                cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
                cell.nameLabel.text = [NSString stringWithFormat:@"Unknown contact"];
            });

        }
    }
    if ([message.isReaded isEqualToNumber:@0]) {
        cell.messageLabel.textColor=[UIColor whiteColor];
    }else {
     cell.messageLabel.textColor=[UIColor lightGrayColor];
    }
    cell.messageLabel.text = message.message;
    return cell;
}

-(void)buttonEditClicked:(UIButton *)sender{
    Message * message = [self.messagesArray objectAtIndex:sender.tag];
    EditChatGroupExistingVC * edit = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"EditGrop"];
    edit.nameGroup=message.groupName;
    edit.groupID=message.groupId;
    edit.imageGroup=message.imageGroup;
  if (self.delegate && [self.delegate respondsToSelector:@selector(showIndividualChat:)]) {
        [self.delegate showIndividualChat:edit];
    }
}

-(void)buttonChatIndividualClicked:(UIButton *)sender{
    
    ConversationVC * conversation = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationVC"];
    NSString* user=[NSString stringWithFormat:@"%ld",(long)sender.tag];
    conversation.userIdTo=user;
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    NSString *ind=[NSString stringWithFormat:@"%ld",(long)indexPath.row];
    for(NSDictionary* dic in self.arrayMonitors){
        
        if ([user isEqualToString:[dic objectForKey:@"UserId"]]&&[ind isEqualToString:[dic objectForKey:@"IndexPath"]]){
          conversation.isMonitor=YES;
            break;
        }
    }
    
 if (self.delegate && [self.delegate respondsToSelector:@selector(showIndividualChat:)]) {
        [self.delegate showIndividualChat:conversation];
    }
    
}
-(void)buttonChatGroupClicked:(UIButton *)sender{
    
    ConvesationGroupVC * conversationGroup = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationGrupalVC"];
    
    Message * message=[self.messagesArray objectAtIndex:sender.tag];
    if (message.groupName==nil ||message.groupId==nil) {
        return;
    }
    NSString *urlImageGroup= message.imageGroup;
    if (urlImageGroup==nil) {
        urlImageGroup=@"";
    }

    NSDictionary* dataDic = @{
                              @"Image":urlImageGroup,
                              @"IdChatGroup": message.groupId,
                              @"Name":message.groupName
                              };
    conversationGroup.chatGroup=dataDic;

    if (self.delegate && [self.delegate respondsToSelector:@selector(showIndividualChat:)]) {
        [self.delegate showIndividualChat:conversationGroup];
    }
}
-(void)buttonLeaveClicked:(UIButton *)sender{
Message * message = [self.messagesArray objectAtIndex:sender.tag];
    self.indexTmp= (int)sender.tag;
 //mensaje de esta seguro de abandonar el grupo
    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];

    [alert showWithTitle:nil Message:NSLocalizedString(@"abandonar.grupo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
    alert.delegate=self;
    self.groupIdToLeave=message.groupId;
   }
-(void)buttonPressed:(UIButton *)sender{
    if (sender.tag==1) {
        [MessagesService leaveChatGroupWithChatGroupId:self.groupIdToLeave Wait:NO Delegate:self];
    }
    
}
-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    NSLog(@"%@",dictionary);
    switch (service) {
        case kLEAVE_CHATGROUP:
            dictionary = [dictionary objectForKey:@"LeaveChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                
                Message *obje= [Message findWithGroupId:self.groupIdToLeave];
                [obje deleteObject];
                self.groupIdToLeave=nil;
                [self.messagesArray removeObjectAtIndex:self.indexTmp];
                [self.tableView reloadData];
   
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
            }
            break;
        
            
        default:
            break;
    }
}


@end
