//
//  TestViewController.h
//  secapp
//
//  Created by SecApp on 05/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapLocationVC.h"


@interface TestViewController : UIViewController
@property MapLocationVC* map;
- (void) drawInView:(UIView*) view;
@end
