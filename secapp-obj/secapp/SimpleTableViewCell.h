//
//  SimpleTableViewCell.h
//  secapp
//
//  Created by SecApp on 12/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *messageLabel;
@property (nonatomic, weak) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *width;

@property (weak, nonatomic) IBOutlet UIButton *buttonAdd;
@property (weak, nonatomic) IBOutlet UIButton *buttonChat;
@property (weak, nonatomic) IBOutlet UIButton *buttonEdit;
@property (weak, nonatomic) IBOutlet UIButton *buttonDelete;
@property (weak, nonatomic) IBOutlet UIButton *buttonChatGrupal;
@property (weak, nonatomic) IBOutlet UIButton *buttonWithoutPermissions;
@property (weak, nonatomic) IBOutlet UIButton *buttonWithPermissions;

@end
