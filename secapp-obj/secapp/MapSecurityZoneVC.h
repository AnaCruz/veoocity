//
//  MapSecurityZoneVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 27/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>
#import "MapSecApp.h"

@interface MapSecurityZoneVC : UIViewController

@property  (strong, nonatomic) MapSecApp *mapView;

@end
