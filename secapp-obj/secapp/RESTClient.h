//
//  RESTClient.h
//  TV UAA
//
//  Created by Marco Navarro on 26/10/14.
//  Copyright (c) 2014 TrioLabs. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResponseDelegate.h"

typedef enum { kGET, kPOST, kPUT, kDELETE } kMethod;

@interface RESTClient : NSObject

@property id<ResponseDelegate> delegate;
@property NSData* body;
@property NSString* paramsUri;
@property id extra;
@property NSString* messageWait;


/**
 * Constructor of the class
 * @param params Dictionary with the params to cast a JSON format
 * @param method Method of the HTML Protocol
 * @param uri Uri from service
 * @param wait Show the progress of a task with known duration.
 * @param delegate Delegate in the that notify the response
 * @return
 */
- (instancetype) initWithMethod:(kMethod)method Uri:(NSString*)uri Service:(kService)service Wait:(BOOL) wait Delegate:(id<ResponseDelegate>)delegate;

/**
 * Execute the call service
 */
-(void) execute;

@end
