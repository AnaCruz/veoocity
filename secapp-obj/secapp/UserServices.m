//
//  UserServices.m
//  secapp
//
//  Created by SecApp on 09/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "UserServices.h"

@implementation UserServices

+ (void) createAccountWithFirstName:(NSString*) firstName LastName:(NSString*) lastName Pin:(NSString*) pin Phone:(NSString*) phone Email:(NSString*) email UserName:(NSString*) userName Gender:(NSString*) gender Image:(NSString*) image FacebookId:(NSString*) facebookId TwitterId:(NSString*) twitterId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    
    NSString* uri = [NSString stringWithFormat:@"%@CreateUser", [super getUrl]];
    
    NSDictionary* dataDic = @{
                              @"InfoUser":@{
                                      @"Name":firstName,
                                      @"LastName":lastName,
                                      @"Password":pin,
                                      @"Phone":phone,
                                      @"Email":email,
                                      @"Username":userName,
                                      @"Gender":gender,
                                      @"Image":image,
                                      @"Battery":[self getBatteryLevel],
                                      @"FacebookId":facebookId,
                                      @"TwitterId":twitterId
                                      }
                              };
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kCREATE_USER Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}

+(void) updateAccountWithFirstName:(NSString*) firstName LastName:(NSString*) lastName Pin:(NSString*) pin UserName:(NSString*) userName Gender:(NSString*) gender Image:(NSString*) image FacebookId:(NSString*) facebookId TwitterId:(NSString*) twitterId Wait:(BOOL) wait Delegate:(id<ResponseDelegate>) delegate {
    
    NSString* uri = [NSString stringWithFormat:@"%@UpdateUser", [super getUrl]];
    
    NSDictionary* dataDic = @{
                              @"InfoUser":@{
                                      @"UserId":[super getUserId],
                                      @"SessionId": [super getSessionId],
                                      @"Name":firstName,
                                      @"LastName":lastName,
                                      @"Password":pin,
                                      @"Username":userName,
                                      @"Gender":gender,
                                      @"Image":image,
                                      @"Battery":[self getBatteryLevel],
                                      @"FacebookId":@"",
                                      @"TwitterId":@""
                                      }
                              };
  
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_USER Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];

}

+(void) getUserWithUserIdGetting:( NSString *) userIdGetting Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    
    NSString* uri = [NSString stringWithFormat:@"%@User/%@/%@", [super getUrl], [super getUserId], userIdGetting ];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_USER Wait:wait Delegate:delegate];
    [request execute];
}

+(void) getProfileUserWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra{
    NSString* uri = [NSString stringWithFormat:@"%@Profile/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_USER_PROFILE Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    [request execute];
}
+(void) getProfileUserOptimizedWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra{
    NSString* uri = [NSString stringWithFormat:@"%@OptimizedProfile/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_USER_PROFILE_OPTIMIZED Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    [request execute];
}
+(void) getProfileUserBasicWithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra{
    NSString* uri = [NSString stringWithFormat:@"%@BasicProfile/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_USER_PROFILE_BASIC Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    [request execute];
}

+(void) addEmailPhone:(NSString*) data IsEmail:(BOOL) isEmail Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@AddEmailPhone", [super getUrl]];
    
    NSDictionary* dataDic = @{@"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"Data":data,
                              @"IsEmail": isEmail ? @YES : @NO
                              };
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kUPDATE_MAIL_PHONE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}

+(void) deleteEmailPhone:(NSString*) data IsEmail:(BOOL) isEmail Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@DeleteEmailPhone", [super getUrl]];
    
    NSDictionary* dataDic = @{@"UserId":[super getUserId],
                              @"SessionId": [super getSessionId],
                              @"Data":data,
                              @"IsEmail": isEmail ? @YES : @NO
                              };
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kDELETE_MAIL_PHONE Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];
}

+(void) confirmPassword:(NSString*) pin WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@ConfirmPassword/%@/%@/%@", [super getUrl], [super getUserId], pin, [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kCONFIRM_PASSWORD Wait:wait Delegate:delegate];
    [request execute];
}
+(void) resetPassword: (NSString *)mail WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    NSString* uri = [NSString stringWithFormat:@"%@ResetPassword", [super getUrl]];
    
    NSDictionary* dataDic = @{@"Email":mail};
    
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kRESET_PASSWORD Wait:wait Delegate:delegate];
    NSData* paramsData = [super parsingWithObject:dataDic];
    request.body = paramsData;
    [request execute];


}
+(void) aceptToBeMonitoredWithInvitationId: (NSString *)invitationId WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra{
    NSString* uri = [NSString stringWithFormat:@"%@AcceptInvitationFromMonitor", [super getUrl]];
    NSDictionary* params = @{
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId],
                             @"InvitationId": invitationId
                             };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPOST Uri:uri Service:kACEPT_TO_BE_MONITORED Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    request.body = paramsData;
    [request execute];
}
+(void) rejectToBeMonitoredWithInvitationId: (NSString *)invitationId WithWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra{
    NSString* uri = [NSString stringWithFormat:@"%@RejectInvitationFromMonitor", [super getUrl]];
    NSDictionary* params = @{
                             @"UserId":[super getUserId],
                             @"SessionId":[super getSessionId],
                             @"InvitationId": invitationId
                             };
    NSData* paramsData = [super parsingWithObject:params];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kPUT Uri:uri Service:kREJECT_TO_BE_MONITORED Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    request.body = paramsData;
    [request execute];
}


@end
