//
//  ContactService.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"

@interface ContactService : Service

/**
 * Get data of a single user.
 * @param userId ID user
 * @param sessionId ID session
 * @param wait Show the progress of a task with known duration.
 * @param delegate In this object will notifies response of service.
 */
+(void) getCotactListWithUserId:( NSString *) userId AndSessionId:(NSString*) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) changePermissionStatusWithContactId: (NSString *) contactId AndPermission: (BOOL) permission Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;
+(void) getCotactListWithoutSomeDataWithUserId:( NSString *) userId AndSessionId:(NSString*) sessionId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate;

@end
