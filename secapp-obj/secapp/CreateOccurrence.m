//
//  CreateOccurrence.m
//  secapp
//
//  Created by SecApp on 02/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "CreateOccurrence.h"
#import "OccurenceServices.h"
#import "Occurrence.h"
#import "AppDelegate.h"
#import "ImageUtils.h"
#import <QuartzCore/QuartzCore.h>
#import "Alert/AlertView.h"

@interface CreateOccurrence () <UINavigationControllerDelegate, ResponseDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UITextFieldDelegate,AlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *viewDataIncidence;
@property (strong, nonatomic) IBOutlet UISwitch *isAnnonymous;
@property (strong, nonatomic) IBOutlet UIButton *pictureForSubmmit;
@property (strong, nonatomic) IBOutlet UITextField *details;
@property (strong, nonatomic) IBOutlet UILabel *nameOccurrence;
@property (strong, nonatomic) IBOutlet UIView *viewAnonymous;
@property int languaje;
@property BOOL selectedNewImage;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property NSString * selectedDate;

@property CGRect originalPosition;
@end

@implementation CreateOccurrence

- (void)viewDidLoad {
    [super viewDidLoad];
    self.originalPosition= self.viewDataIncidence.frame;
    [self registerForKeyboardNotifications];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"background_menuOcc"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
     self.languaje=[[Service getLanguaje]intValue];
    self.datePicker.maximumDate = [NSDate date];
    
    [self.datePicker sizeThatFits:self.details.frame.size];
    
    
}
-(void)viewWillAppear:(BOOL)animated{
     self.isShowInMap=YES;
    self.imageIcon.image = [UIImage imageNamed:[NSString stringWithFormat:@"d%@",self.typeOccurrence]];
    
    switch ([self.typeOccurrence intValue]) {
        case 1:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"ROBO";
            }
            else{
            self.nameOccurrence.text= @"ROBBERY";
            }
            break;
        case 2:
            if (self.languaje==1) {
            self.nameOccurrence.text= @"ACTIVIDAD SOSPECHOSA";
            }else{
            self.nameOccurrence.text= @"SUSPICIOUS ACTIVITY";
            }
            break;
        case 3:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"CRIMEN";
            }else{
             self.nameOccurrence.text= @"CRIME";
            }
            break;
        case 4:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"DESAPARICION DE NIÑOS";
            }else{
            self.nameOccurrence.text= @"MISSING CHILDREN";
            }
            break;
        case 5:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"CORRUPCION";
            }else{
               self.nameOccurrence.text= @"CORRUPTION";
            }
            break;
        case 6:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"ATRACO DE AUTOS";
            }else{
            self.nameOccurrence.text= @"CAR ROBBERY";
            }
            break;
        case 7:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"PERDIDA DE MASCOTAS";
            }else{
             self.nameOccurrence.text= @"PET LOSS";
            }
            break;
        case 8:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"MALTRATO ANIMAL";
            }else {
                self.nameOccurrence.text= @"ANIMAL ABUSE";
            }
            break;
        case 9:
            if (self.languaje==1) {
                self.nameOccurrence.text= @"ME SIENTO BIEN";
            }else{
                self.nameOccurrence.text= @"FEELING GOOD";
            }
            break;
            
        default:
            break;
    }
    self.pictureForSubmmit.clipsToBounds = YES;
    int radius = 30;
    self.pictureForSubmmit.layer.cornerRadius = radius;
    [self.pictureForSubmmit.layer setBorderWidth:3.0];
    [self.pictureForSubmmit.layer setBorderColor:[[UIColor whiteColor] CGColor]];
//    [self.pictureForSubmmit.layer setShadowColor:[[UIColor lightGrayColor] CGColor]];
//    [self.pictureForSubmmit.layer setShadowOffset:CGSizeMake(0, 2.0f)];
//    [self.pictureForSubmmit.layer setShadowOpacity:0.5f];
//    [self.pictureForSubmmit.layer setShadowRadius:0.0f];

//    self.pictureForSubmmit.layer.masksToBounds = YES;

  self.pictureForSubmmit.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    
   // self.nameOccurrence.text = self.typeOccurrence;
    
    CALayer *layer = self.viewAnonymous.layer;
    layer.cornerRadius = radius;
    layer.borderWidth=3.0;
    [layer setBorderColor:[[UIColor whiteColor] CGColor]];
  layer.masksToBounds = YES;
    
 UIGraphicsBeginImageContext(self.viewAnonymous.frame.size);
    [[UIImage imageNamed:@"dglass"] drawInRect:self.viewAnonymous.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
   UIGraphicsEndImageContext();
    
    self.viewAnonymous.backgroundColor = [UIColor colorWithPatternImage:image];
   

//    layer.shadowOffset = CGSizeMake(4, 4);
//    layer.shadowColor = [[UIColor blackColor] CGColor];
//    layer.shadowRadius = 2.0f;
//    layer.shadowOpacity = 0.60f;
//    layer.shadowPath = [[UIBezierPath bezierPathWithRect:layer.bounds] CGPath];

}
- (void) drawInView:(UIView*) view {
    self.isShowInMap=YES;
    CGRect frame =view.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.view.frame = frame;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
    
}
- (IBAction)createOccurrence:(id)sender {
    //servicio crear incidencia y guardar en memoria
    if ([self.details.text isEqualToString:@""]||[self.details.text isEqualToString:@" "]) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"description.empty", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
        return;
    }
    UIImage* imageOccurrence = self.pictureForSubmmit.currentBackgroundImage;
    NSString* image64 = imageOccurrence ? [ImageUtils encodingImageToBase64:imageOccurrence] : @"";
    if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:[UIImage imageNamed:@"dbtn_photo"]]]) {
        image64 = @"";
    }
    else{
    // grab the original image
   UIImage *originalImage = imageOccurrence;
   UIImage *scaledImage= [ImageUtils resizeImageFor72DPI:originalImage];
    image64 = scaledImage ? [ImageUtils encodingImageToBase64:scaledImage] : @"";
    
    }
   
/// BOOL ann = self.isAnnonymous.isOn ? NO : YES ;
    //[self.isAnnonymous setON:NO]
    
    self.selectedDate = [self getDateForOccurrence];
    NSLog(@"Selected date: %@",self.selectedDate);
    [OccurenceServices createOccurrenceType:self.typeOccurrence Details:self.details.text Latitude:self.latitude Longitude:self.longitude Image:image64 Anonymous:self.isAnnonymous.isOn HappenedDay:self.selectedDate Wait:YES Delegate:self];
    
}

- (IBAction)cancel:(id)sender{
    [self.map drawPinsOnMap];
    self.isShowInMap=NO;
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }

    
}
- (void) closeView{
    self.isShowInMap=NO;
      [self.map drawPinsOnMap];
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
    
    
}
- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    [self.view endEditing:YES]; 
}
-(void)successResponseWithService:(kService)service Response:(id)response{

    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kCREATE_OCCURRENCE:
            dictionary = [dictionary objectForKey:@"CreateOccurrenceResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               
                Occurrence * occurrence;
                occurrence = [Occurrence createObject];
                [occurrence saveObject];
                occurrence.occurrenceId = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"ID"]];
                occurrence.latitude =[NSNumber numberWithDouble:[self.latitude doubleValue]];
                occurrence.longitude =[NSNumber numberWithDouble:[self.longitude doubleValue]];
                
                occurrence.type=[NSNumber numberWithInt:[self.typeOccurrence intValue]];
                
                [occurrence mergeObject];
                
                [self closeView];
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            
            break;
                default:
            break;
    }


}
#pragma mark- takePhoto
- (IBAction)takePhoto:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];//close keyboard
    });
    
    // Show options to select a image.
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Take photo of:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            @"Clear",
                            nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
/**
 * Open the camara or galery for select a picture.
 */
-(void) selectImageFromCamera:(BOOL) fromCamera {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.allowsEditing = YES; // Editing image
    [self.map.parentViewController presentViewController:picker animated:YES completion:^{
        self.selectedNewImage = YES;
    }];
}

#pragma mark - Delegates methods of UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    // Show image edited
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.pictureForSubmmit setBackgroundImage:image forState:UIControlStateNormal];
    });
    
    //return to the editing view for continue with the profile edition
    [self.view setNeedsDisplay];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    
}

#pragma mark - Delegates methods of UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self selectImageFromCamera:YES];
            break;
        case 1:
            [self selectImageFromCamera:NO];
            break;
        case 2:
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.pictureForSubmmit setBackgroundImage:[UIImage imageNamed:@"dbtn_photo"] forState:UIControlStateNormal];
            });
            break;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=99)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:99];
    }
    
    return NO;
}
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.originalPosition;
    aRect.origin.y = aRect.origin.y - (kbSize.height-aRect.size.height)+60;
    self.viewDataIncidence.frame = aRect;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.viewDataIncidence.frame = self.originalPosition;
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (NSString *)getDateForOccurrence{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
   // [dateFormatter setDateFormat:@"dd MMM yyyy, hh:mm a"];
    if (self.languaje ==1) {
        [dateFormatter setDateFormat:@"dd MMM yyyy, hh:mm a"];
    }else{
        [dateFormatter setDateFormat:@"MMM dd yyyy, hh:mm a"];
    }
    NSString *formatedDate = [dateFormatter stringFromDate:self.datePicker.date];
     return formatedDate;
    
}


@end
