//
//  InfoUserVC.h
//  secapp
//
//  Created by SecApp on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@class CustomAnnotation;
@class ContainerVC;
@class Profile;
@class Friend;
@class ChatContainerVC;


@protocol InfoUserDelegate <NSObject>
@optional
-(void) showViewController:(UIViewController*) viewController;
-(void) showView:(NSString*) friendSelected;

@end

@interface InfoUserVC : UIViewController
@property id<InfoUserDelegate> delegate;
@property (weak, nonatomic) CustomAnnotation * user;
@property (weak, nonatomic) Profile *me;
@property BOOL isVisible;
@property ContainerVC *container;
@property  Friend * friendSelected;
@property ChatContainerVC * containerChat;
@property BOOL isSearch;
/**
 * Display the view above the mapView.
 * @param view Superview on which the subview shown.
 */
- (void) drawInView:(UIView*) view;

/**
 * Set data for the uilabels (friend name, friend status, and battery level), and uiimage (battery).
 */
- (void) setUser;

/**
 * Close the subview infoUser.
 * @param sender Sender value.
 */
- (IBAction)closeView:(id)sender;

@end
