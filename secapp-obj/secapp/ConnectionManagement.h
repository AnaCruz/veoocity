//
//  ConnectionManagement.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 16/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConnectionManagement : NSObject

/**
 * Check if the device have connection.
 * @return YES if have connection.
 */
+ (BOOL) isAvailableConnection;

+ (BOOL) isAvailableConnectionWiFi;

+ (BOOL) isAvailableConnectionWWAN;

@end
