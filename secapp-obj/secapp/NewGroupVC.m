//
//  NewGroupVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

//#import "NewGroupVC.h"
//#import "Friend.h"
//#import "Group.h"
//#import "UserGroupCell.h"
//#import "UserInGroupCell.h"
//#import "StringUtils.h"
//#import "ConfirmNewGroupVC.h"
//
//@interface NewGroupVC () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
//
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
//@property NSDictionary* sections;
//@property NSArray* friends;
//@property NSArray* usersList;
//@property NSArray* usersInGroupList;
//
//@property (weak, nonatomic) IBOutlet UITextField *inUser;
//@property (weak, nonatomic) IBOutlet UITextField *inNameGroup;
//
//@property BOOL edit;
//
//@end
//
//@implementation NewGroupVC
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.friends = [NSArray array];
//    self.usersList = [NSArray array];
//    self.usersInGroupList = [NSArray array];
//    self.sections = @{@"InGroup"    : self.usersInGroupList,
//                      @"Users"      : self.usersList};
//}
//
//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.friends = [NSArray array];
//    self.usersList = [NSArray array];
//    self.usersInGroupList = [NSArray array];
//    self.sections = @{@"InGroup"    : self.usersInGroupList,
//                      @"Users"      : self.usersList};
//    self.friends = [Friend findAll];
//    if (!self.friends) {
//        self.friends = [NSArray array];
//    }
//    self.usersList = [self.friends copy];
//    [self.tableView reloadData];
//}
//
//- (IBAction)backView:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}
//
//- (IBAction)closeKeyboard:(id)sender {
//    [self.view endEditing:YES];
//}
//
//- (IBAction)createGroup:(UITextField*)sender {
//    NSString* name = nil;
//    if ([self.inNameGroup.text isEqualToString:@""]) {
//        NSArray* tmp = [Group findAll];
//        int count = tmp ? (int)[tmp count] : 1;
//        name= [NSString stringWithFormat:@"Grupo %i", count];
//    } else {
//        name = self.inNameGroup.text;
//    }
//    if (self.usersInGroupList.count < 1) {
//        
//        // TODO Alert debe de seleccionar al menos 1 usuario
//        return;
//    }
//    // Hablar la seg. ventana
//    ConfirmNewGroupVC* vc = [[UIStoryboard storyboardWithName:@"Friends" bundle:nil] instantiateViewControllerWithIdentifier:@"ConfirmNewGroupVC"];
//    vc.usersInGroupList = [self.usersInGroupList copy];
//    vc.name = name;
//    self.usersInGroupList=nil;
//    self.usersList=nil;
//    [self.navigationController pushViewController:vc animated:YES];
//}
//
//- (IBAction)addInvitation:(id)sender {
//    int type = -1;
//    if ([StringUtils validateEmail:self.inUser.text]) {
//        type = 1;
//    } else if ([StringUtils validateNumberPhone:self.inUser.text]) {
//        type = 2;
//    } else {
//        return;
//    }
//    
//    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
// 
//    NSDictionary* data = @{@"data": self.inUser.text,
//                           @"type": @(type)};
//    
//    NSMutableArray* usersInGroup = [self.usersInGroupList mutableCopy];
//    
//    [usersInGroup addObject:data];
//    
//    self.usersInGroupList = [usersInGroup copy];
//
//    int newRow = (int)[self.usersInGroupList count];
//    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:0]];
//    
//    [self.tableView beginUpdates];
//    [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView endUpdates];
//}
//
//- (IBAction)searchFriend:(UITextField*)sender {
//    if ([sender.text isEqualToString:@""]) {
//        self.usersList = self.friends;
//        NSMutableArray* tmp = [self.usersList mutableCopy];
//        for (Friend* f in self.usersInGroupList) {
//            [tmp removeObject:f];
//        }
//        self.usersList = [tmp copy];
//        [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
//        return;
//    }
//    NSString* data = sender.text;
//
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"name CONTAINS '%@'", data]];
//    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"lastName CONTAINS '%@'", data]];
//    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(SUBQUERY(emails, $emails, $emails.email CONTAINS '%@').@count) > 0",data]];
//    NSPredicate *predicate4 = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"(SUBQUERY(phones, $phones, $phones.phone CONTAINS '%@').@count) > 0",data]];
//    NSPredicate *predicate5 = [NSCompoundPredicate orPredicateWithSubpredicates:@[predicate, predicate2, predicate3, predicate4]];
//    self.usersList = [self.friends filteredArrayUsingPredicate:predicate5];
//    
//    NSMutableArray* tmp = [self.usersList mutableCopy];
//    for (Friend* f in self.usersInGroupList) {
//        [tmp removeObject:f];
//    }
//    self.usersList = [tmp copy];
//    
//    [[self tableView] reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
//}
//
//#pragma mark - Table Delegate and dataSource
//
//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        return;
//    }
//    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//    
//    Friend* friend = [self.usersList objectAtIndex:indexPath.row];
//    NSMutableArray* users = [self.usersList mutableCopy];
//    NSMutableArray* usersInGroup = [self.usersInGroupList mutableCopy];
//    
//    [users removeObject:friend];
//    [usersInGroup addObject:friend];
//    
//    self.usersList = [users copy];
//    self.usersInGroupList = [usersInGroup copy];
//    
//    [indexPathsToDelete addObject:indexPath];
//    
//    int newRow = (int) [self.usersInGroupList count];
//    [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:0]];
//    
//    [tableView beginUpdates];
//    [tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//    [tableView endUpdates];
//}
//
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    return 2;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (section == 0) {
//        return [self.usersInGroupList count];
//    }
//    return [self.usersList count];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell* cell = nil;
//    if (indexPath.section == 0) {
//        id friend = [self.usersInGroupList objectAtIndex:indexPath.row];
//        cell = [self tableView:tableView UserInGroup:friend InRowAtIndexPath:indexPath];
//    } else {
//        Friend* friend = [self.usersList objectAtIndex:indexPath.row];
//        cell = [self tableView:tableView User:friend InRowAtIndexPath:indexPath];
//    }
//    return cell;
//}
//
//#pragma mark - Custom cells
//
//- (UITableViewCell*) tableView:(UITableView*) tableView User:(Friend*) friend InRowAtIndexPath:(NSIndexPath*) indexPath {
//    static NSString* identifier = @"User";
//    UserInGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UserInGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    cell.nameUser.text = [NSString stringWithFormat:@"%@ %@", friend.name, friend.lastName];
//    cell.pictureUser.image = [UIImage imageWithData:friend.pictureProfile];
//    cell.pictureUser.clipsToBounds = YES;
//    cell.pictureUser.layer.cornerRadius = 22;
//    cell.pictureUser.layer.borderWidth = 3;
//    cell.pictureUser.layer.borderColor = [[UIColor whiteColor] CGColor];
//    return cell;
//}
//
//- (UITableViewCell*) tableView:(UITableView*) tableView UserInGroup:(id) friend InRowAtIndexPath:(NSIndexPath*) indexPath {
//    static NSString* identifier = @"UserInGroup";
//    UserInGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
//    if (cell == nil) {
//        cell = [[UserInGroupCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
//    }
//    if ([friend isKindOfClass:Friend.class]) {
//        Friend *data = friend;
//        cell.nameUser.text = [NSString stringWithFormat:@"%@ %@", data.name, data.lastName];
//        cell.pictureUser.image = [UIImage imageWithData:data.pictureProfile];
//    } else {
//        NSDictionary *data = friend;
//        cell.nameUser.text = [data objectForKey:@"data"];
//    }
//    cell.pictureUser.clipsToBounds = YES;
//    cell.pictureUser.layer.cornerRadius = 22;
//    cell.pictureUser.layer.borderWidth = 3;
//    cell.pictureUser.layer.borderColor = [[UIColor whiteColor] CGColor];
//    cell.deleteUser.tag = indexPath.row;
//    [cell.deleteUser addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
//    return cell;
//}
//
//-(void) deleteUser:(UIButton*) sender {
//    NSMutableArray *indexPathsToDelete = [NSMutableArray new];
//    NSMutableArray *indexPathsToAdd = [NSMutableArray new];
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    
//    id friend = [self.usersInGroupList objectAtIndex:indexPath.row];
//    NSMutableArray* users = [self.usersList mutableCopy];
//    NSMutableArray* usersInGroup = [self.usersInGroupList mutableCopy];
//    
//    [usersInGroup removeObject:friend];
//    if ([friend isKindOfClass:Friend.class]) {
//        [users addObject:friend];
//        self.usersList = [users copy];
//        int newRow = (int)[self.usersList count];
//        [indexPathsToAdd addObject:[NSIndexPath indexPathForRow:(newRow-1) inSection:1]];
//    }
//    self.usersInGroupList = [usersInGroup copy];
//    
//    [indexPathsToDelete addObject:indexPath];
//    
//    [self.tableView beginUpdates];
//    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
//    [self.tableView endUpdates];
//}

//
//@end
