//
//  GroupsVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

//#import "GroupsVC.h"
//#import "Group.h"
//#import "EditGroupFrinedsVC.h"
//#import "GroupCell.h"
//#import "AlertViewSingleInput.h"
//#import "GroupService.h"
//#import "AlertView.h"
//#import "AppDelegate.h"
//
//@interface GroupsVC () <UICollectionViewDataSource, UICollectionViewDelegate, ResponseDelegate, AlertViewDelegate>
//
//@property NSArray* listGroups;
//@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
//@end
//
//@implementation GroupsVC

//-(id)initWithCoder:(NSCoder *)aDecoder {
//    self = [super initWithCoder:aDecoder];
//    if (self) {
//        _listGroups = [NSArray array];
//    }
//    return self;
//}
//- (void)viewDidLoad {
//    [super viewDidLoad];
//}
//
//- (void)viewDidAppear:(BOOL)animated {
//    [super viewDidAppear:animated];
//    self.listGroups = [Group findAll];
//    if (!self.listGroups) {
//        self.listGroups = [NSArray array];
//    }
//    [self.collectionView reloadData];
//}
//
//-(IBAction)insertCode:(id)sender{
//    AlertViewSingleInput* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertViewSingleInput"];
//   alert.textField.keyboardType = UIKeyboardTypeNumberPad;
// 
//    [alert.textField setKeyboardType:UIKeyboardTypeNumberPad];
//    __block GroupsVC* bSelsf = self;
//    alert.callbackOk =  ^(NSString* text){
//        if (![text isEqualToString:@""]) {
//            [GroupService addUserToGroupWithAccessCode:text WithWait:YES Delegate:bSelsf];
//        }
//    };
//    [alert showWithTitle:NSLocalizedString(@"codigo.invitacion", nil) Message:NSLocalizedString(@"introduce.codigo.invitación", nil) CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self.navigationController.parentViewController];
//}
//
//#pragma mark - Collection delegate
//
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return self.listGroups ? [self.listGroups count] + 1 : 1;
//}
//
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    float width = self.view.frame.size.width;
//    width = (width - 24)  / 2; // calcular el ancho de la vista
//    return CGSizeMake(width, 56);
//}
//
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Index %d", indexPath.row);
//   // if (indexPath.row > ([self.listGroups count]-1) || ([self.listGroups count] == 0))
//    if (indexPath.row ==0)
//    {
//        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"NewGroupCell" forIndexPath:indexPath];
//        return cell;
//    }
//    GroupCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GroupCell" forIndexPath:indexPath];
//    Group* group = [self.listGroups objectAtIndex:indexPath.row-1];
//    cell.nameGroup.text = group.name;
//    return cell;
//}
//
//- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        // nuevo grupo
//        UIViewController* viewController = [[UIStoryboard storyboardWithName:@"Friends" bundle:nil] instantiateViewControllerWithIdentifier:@"NewGroupVC"];
//        [self.navigationController pushViewController:viewController animated:YES];
//        return;
//    }
//    Group* group = [self.listGroups objectAtIndex:indexPath.row-1];
//    [self goToEditGroup:group];
//}
//
//- (void) goToEditGroup:(Group*) group {
//    EditGroupFrinedsVC* edit = [[UIStoryboard storyboardWithName:@"Friends" bundle:nil] instantiateViewControllerWithIdentifier:@"EditGroupFrinedsVC"];
//    edit.group = group;
//    [self.navigationController pushViewController:edit animated:YES];
//}
//-(void)successResponseWithService:(kService)service Response:(id)response {
//    NSError *parsingError = nil;
//    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
//    if (parsingError) {
//        NSLog(@"Error: %@", parsingError.description);
//        return;
//    }
//    int responseCode = -1;
//    // NSLog(@"%@",dictionary);
//    switch (service) {
//        case kADD_USER_TO_GROUP:
//            dictionary = [dictionary objectForKey:@"AddUserToGroupResult"];
//            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
//            if (responseCode == 200) {
//                //alert usuario agregado correctamente
//                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//                [alert showWithTitle:nil Message:NSLocalizedString(@"agregado.correctamente", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
//                alert.delegate=self;
//
//            }
//            if (responseCode==405) {
//             AppDelegate* app = [[UIApplication sharedApplication] delegate];
//                [app forceCloseSession];
//                //enviar a login
//                
//            }
//
//            if (responseCode == 407) {
//                //el codigo de acceso esta caducado o ya fue usado
//                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
//                [alert showWithTitle:nil Message:NSLocalizedString(@"codigo.invalido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.parentViewController.parentViewController];
//                alert.delegate=self;
//
//            }
//            break;
//            
//        default:
//            break;
//    }
//}

//
//@end
