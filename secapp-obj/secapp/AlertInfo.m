//
//  AlertInfo.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 29/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AlertInfo.h"

@implementation AlertInfo

+ (void) showAlertInViewController:(UIViewController*) controller
                         WithTitle:(NSString*) title
                           Message:(NSString*) message {
    AlertInfo* info = [[AlertInfo alloc] init];
    if ([UIAlertController class]) {
        UIAlertController* alert = [info alertControllerWithTitle:title Message:message];
        [controller presentViewController:alert animated:YES completion:nil];
        return;
    }
    UIAlertView* alert = [info alertViewWithTitle:title Message:message];
    [alert show];
}

/**
 *  Build to Alert controller to intput a secret text
 *
 *  @param title        Title of the alert
 *  @param message      Message of the alert
 *  @param placeholder  Placeholder of the alert
 *
 *  @return Alert Controller
 */
- (UIAlertController*) alertControllerWithTitle:(NSString*) title Message:(NSString*) message {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Aceptar"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alert addAction:ok];
    return alert;
}

/**
 *  Build to Alert View
 *
 *  @param title        Title of the alert
 *  @param message      Message of the alert
 *  @param placeholder  Placeholder of the alert
 *
 *  @return Alert View
 */
- (UIAlertView*) alertViewWithTitle:(NSString*) title Message:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"Cancelar"
                                          otherButtonTitles:nil];
    return alert;
}

@end
