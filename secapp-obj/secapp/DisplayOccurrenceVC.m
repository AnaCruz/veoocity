//
//  DisplayOccurrenceVC.m
//  secapp
//
//  Created by SecApp on 03/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "DisplayOccurrenceVC.h"
#import "OccurenceServices.h"
#import "Occurrence.h"
#import "AppDelegate.h"
#import "OccurrenceTableViewCell.h"
#import "Profile.h"
#import "StringUtils.h"
#import "Alert/AlertView.h"
@interface DisplayOccurrenceVC () <UINavigationControllerDelegate, ResponseDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, AlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIImageView *iconOccurrence;
@property (strong, nonatomic) IBOutlet UILabel *textType;
@property (strong, nonatomic) IBOutlet UILabel *details;
@property (strong, nonatomic) IBOutlet UILabel *dateCreatedLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameCreator;

@property (strong, nonatomic) IBOutlet UIImageView *pictureOccurrence;
@property int type;
@property NSString * datecreate;
@property NSString * username;
@property int languaje;
@property (strong, nonatomic) IBOutlet UIButton *likeButton;
@property (strong, nonatomic) IBOutlet UIButton *dislikeButton;
@property (strong, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (strong, nonatomic) IBOutlet UILabel *dislikeCountLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UITextField *textFieldComment;
@property (strong, nonatomic) IBOutlet UIButton *sendCommentButton;
@property (strong, nonatomic) IBOutlet UIButton *followbutton;
@property (strong, nonatomic) IBOutlet UIButton *unfollowButton;
@property (strong, nonatomic) IBOutlet UILabel *followLabel;

@property NSIndexPath * tmpIndexPath;
@property NSArray * commentsArray;
@property NSMutableArray * commentsLikeOrDislikeArray;
@property NSString * islikeComment;
@property BOOL hasPressButtonLikeOrDislike;
@property (strong, nonatomic) IBOutlet UIView *containerWriteComment;
@property float height;
@property NSString* indexTmp;
@property NSString * urlString;
@property BOOL isFollow;

@end

@implementation DisplayOccurrenceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.languaje=[[Service getLanguaje]intValue];
    self.tableView.delegate=self;
    self.tableView.dataSource=self;
    self.commentsArray = [[NSArray alloc] init];
    self.commentsLikeOrDislikeArray = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated{
    self.commentsArray = [[NSArray alloc] init];
    self.commentsLikeOrDislikeArray = [[NSMutableArray alloc] init];
    if (self.occ) {
       self.occurrenceiD=self.occ.occurrenceId;
    }
   
    if (!self.occurrenceiD) {
        [self closeView:nil];
    }
    self.height = self.view.frame.size.height;
    [self registerForKeyboardNotifications];
//service
    NSLog(@"Id de incidencias: %@",self.occurrenceiD);
    [OccurenceServices getOccurrenceInformationWithOccurrenceId:self.occurrenceiD Wait:NO Delegate:self];
    self.hasPressButtonLikeOrDislike = NO;
    self.islikeComment=@"";

}
-(void)userTapedLink{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.urlString]];
}
-(void)successResponseWithService:(kService)service Response:(id)response{
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    switch (service) {
        case kGET_OCCURRENCE_INFO:
            dictionary = [dictionary objectForKey:@"OccurrenceInformationResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
              // mover al hilo principal
                dispatch_async(dispatch_get_main_queue(), ^{
                 NSDictionary * occurrenceObj;
                    if ([[dictionary objectForKey:@"OccurrenceObj"]count]>0) {
                        occurrenceObj =[[dictionary objectForKey:@"OccurrenceObj"] objectAtIndex:0];
                    }else{
                        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                        [alert showWithTitle:nil Message:NSLocalizedString(@"incidencia.eliminada", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                        alert.delegate=self;
                    }
                self.type=[[occurrenceObj objectForKey:@"Type"]intValue];
                switch (self.type){
                    case 1:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i1"];
                        if (self.languaje==1) {
                            self.textType.text= @"ROBO";
                        }else{
                           self.textType.text= @"ROBBERY";
                        }
                        break;
                    case 2:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i2"];
                        if (self.languaje==1) {
                             self.textType.text= @"ACTIVIDAD SOSPECHOSA";
                        }else{
                            self.textType.text= @"SUSPICIOUS ACTIVITY";
                        }

                        break;
                    case 3:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i3"];
                        if (self.languaje==1) {
                        self.textType.text= @"CRIMEN";
                        }
                        else{
                        self.textType.text= @"CRIME";
                        }
                        break;
                    case 4:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i4"];
                        if (self.languaje==1) {
                            self.textType.text= @"DESAPARICION DE NIÑOS";
                        }else{
                            self.textType.text= @"MISSING CHILDREN";
                        }

                        break;
                    case 5:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i5"];
                        if (self.languaje==1) {
                            self.textType.text= @"CORRUPCION";
                        }else{
                            self.textType.text= @"CORRUPTION";
                        }

                        break;
                    case 6:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i6"];
                        if (self.languaje==1) {
                            self.textType.text= @"ATRACO DE AUTOS";
                        }else{
                           self.textType.text= @"CAR ROBBERY";
                        }

                        break;
                    case 7:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i7"];
                        if (self.languaje==1) {
                            self.textType.text=@"PERDIDA DE MASCOTAS";
                        }else{
                            self.textType.text= @"PET LOSS";
                        }

                        break;
                    case 8:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i8"];
                        if (self.languaje==1) {
                            self.textType.text= @"MALTRATO ANIMAL";
                         }
                        else{
                         self.textType.text= @"ANIMAL ABUSE";
                        }

                        break;
                    case 9:
                        self.iconOccurrence.image = [UIImage imageNamed:@"i9"];
                        if (self.languaje==1) {
                        self.textType.text= @"ME SIENTO BIEN";
                        }else{
                        self.textType.text= @"FEELING GOOD";
                        }

                        break;
                    default:
                        break;
                }
//                    NSString *string = [occurrenceObj objectForKey:@"Description"];
//                    NSURL *url;
//                    NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
//                    NSArray *matches = [linkDetector matchesInString:string options:0 range:NSMakeRange(0, [string length])];
//                    for (NSTextCheckingResult *match in matches) {
//                        if ([match resultType] == NSTextCheckingTypeLink) {
//                            url = [match URL];
//                            NSLog(@"found URL: %@", url);
//                            //Tap Gesture
//                            self.urlString= [NSString stringWithFormat:@"%@",url];
//                            UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userTapedLink)];
//                            //Adding userinteraction for label
//                            [self.details setUserInteractionEnabled:YES];
//                            //Adding label to tap gesture
//                            [self.details addGestureRecognizer:gesture];
//                            UIFont *systemFont = [UIFont systemFontOfSize:14.0];
//                            NSDictionary *systemDict = [NSDictionary dictionaryWithObject: systemFont forKey:NSFontAttributeName];
//                            NSMutableAttributedString *textString = [[NSMutableAttributedString alloc] initWithString:[string substringWithRange:NSMakeRange(0, string.length- self.urlString.length)] attributes: systemDict];
//                            NSMutableAttributedString *linkString = [[NSMutableAttributedString alloc]initWithString: [string substringWithRange:NSMakeRange(string.length- self.urlString.length, self.urlString.length)] attributes:systemDict];
//                            [linkString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [self.urlString length])];
//                            
//                            [textString appendAttributedString:linkString];
//                            self.details.attributedText = textString;
//                            break;
//                        }
//                    }
//                    
                        self.details.text=[occurrenceObj objectForKey:@"Description"];
                    
                   
                    NSDate * date;
                    if ([[occurrenceObj objectForKey:@"HappenedDay"]isEqual:[NSNull null]]||[occurrenceObj objectForKey:@"HappenedDay"]==nil) {
                        date= [NSDate dateWithTimeIntervalSince1970:[[occurrenceObj objectForKey:@"CreateDate"] doubleValue]/1000];
                        if (self.languaje ==1) {
                            self.datecreate=[NSString stringWithFormat:@"Suceso ocurrido el día: %@",[self formattedDate:date]];
                        }else{
                            self.datecreate=[NSString stringWithFormat:@"Incident occurred on: %@",[self formattedDate:date]];
                        }
                    }else{
                        if (self.languaje ==1) {
                            self.datecreate=[NSString stringWithFormat:@"Suceso ocurrido el día: %@",[occurrenceObj objectForKey:@"HappenedDay"]];
                        }else{
                            self.datecreate=[NSString stringWithFormat:@"Incident occurred on: %@",[occurrenceObj objectForKey:@"HappenedDay"]];
                        }
                    
                    }
                    
               self.dateCreatedLabel.text= [NSString stringWithFormat:@"%@", self.datecreate];
                
                if([[occurrenceObj objectForKey:@"Anonymous"]boolValue]){
                    if (self.languaje==1) {
                        self.nameCreator.text = @"Anónimo";
                        
                    }else {
                        self.nameCreator.text = @"Anonymous";
                      }
                }else{
                     self.nameCreator.text = [NSString stringWithFormat:@"%@",[occurrenceObj objectForKey:@"UserName"]];
                
                }
                 NSString *uri= [occurrenceObj objectForKey:@"Image"];
                if (uri && ![uri isEqual: [NSNull null]]){
                    dispatch_async(dispatch_get_main_queue(), ^{
                     NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
                    UIImage *image = [UIImage imageWithData:data];
                    [self.pictureOccurrence setImage:image];
                   // [self.view addSubview: self.pictureOccurrence];
                    });
                }
                self.isFollow=[[occurrenceObj objectForKey:@"Follow"] boolValue];
                if (self.isFollow) {
                    self.followbutton.hidden=YES;
                    self.unfollowButton.hidden=NO;
                    if (self.languaje==1) {
                        self.followLabel.text= @"Dejar de seguir";
                    }else{
                        self.followLabel.text= @"Unfollow";
                    }
                }else {
                    self.followbutton.hidden=NO;
                    self.unfollowButton.hidden=YES;
                    if (self.languaje==1) {
                        self.followLabel.text= @"Seguir";
                    }else{
                        self.followLabel.text= @"Follow";
                    }
                }
                self.likeCountLabel.text=[NSString stringWithFormat:@"%@",[occurrenceObj objectForKey:@"Likes"]];
                  self.dislikeCountLabel.text=[NSString stringWithFormat:@"%@",[occurrenceObj objectForKey:@"Dislikes"]];
                self.commentsArray = [[occurrenceObj objectForKey:@"OccurrenceComments"] copy];
                [self.tableView reloadData];
                   
                });
            }
            if (responseCode==404) {
               AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"incidencia.eliminada", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                alert.delegate=self;
            }
            
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            
            break;
        case kDISLIKE_OCCURRENCE:
            dictionary = [dictionary objectForKey:@"DislikeOccurrenceResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               dispatch_async(dispatch_get_main_queue(), ^{
                int count= [self.dislikeCountLabel.text intValue];
                
                self.dislikeCountLabel.text=[NSString stringWithFormat:@"%d",count+1];
                [self.dislikeButton setSelected:YES];
                self.likeButton.enabled=NO;
                
                self.dislikeButton.enabled=NO;
             });
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
            
        case kLIKE_OCCURRENCE:
                dictionary = [dictionary objectForKey:@"LikeOccurrenceResult"];
                responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               dispatch_async(dispatch_get_main_queue(), ^{
                int count= [self.likeCountLabel.text intValue];
                self.likeCountLabel.text=[NSString stringWithFormat:@"%d",count+1];
                
                [self.likeButton setSelected:YES];
                self.likeButton.enabled=NO;
                self.dislikeButton.enabled=NO;
             });
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kLIKE_COMMENT_OCCURRENCE:
            dictionary = [dictionary objectForKey:@"LikeCommentResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {

                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
                NSMutableArray *indexPathsToAdd = [NSMutableArray new];
                [indexPathsToDelete addObject:self.tmpIndexPath];
                [indexPathsToAdd addObject:self.tmpIndexPath];
                NSMutableArray *arrayTmp= [self.commentsArray mutableCopy];
                int x=0;
                for (NSDictionary * dic in arrayTmp) {
                    if ([[NSString stringWithFormat:@"%@",[dic objectForKey:@"CommentId"]] isEqualToString:self.indexTmp] ) {
                        NSMutableDictionary *mutableDict = [dic mutableCopy];
                        int likes= [[dic objectForKey:@"Likes"] intValue]+1;
                        ;
                        [mutableDict setObject:@(likes) forKey:@"Likes"];
                        [arrayTmp replaceObjectAtIndex:x withObject:mutableDict];
                        [self.commentsLikeOrDislikeArray addObject:[NSString stringWithFormat:@"%d",x]];
                        break;
                    }
                    x++;
                }
                self.commentsArray=[arrayTmp copy];
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic];
                self.hasPressButtonLikeOrDislike=YES;
                self.islikeComment=@"like";

                [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];

                [self.tableView endUpdates];
                self.tmpIndexPath=nil;
                self.indexTmp=nil;
  
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kDISLIKE_COMMENT_OCCURRENCE:
            dictionary = [dictionary objectForKey:@"DislikeCommentResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                NSMutableArray *indexPathsToDelete = [NSMutableArray new];
                NSMutableArray *indexPathsToAdd = [NSMutableArray new];
                if (!self.tmpIndexPath && !self.indexTmp) {
                    return;
                }
                [indexPathsToDelete addObject:self.tmpIndexPath];
                [indexPathsToAdd addObject:self.tmpIndexPath];
                
                NSMutableArray *arrayTmp= [self.commentsArray mutableCopy];
                int x =0;
                for (NSDictionary * dic in arrayTmp) {
                    if ([[NSString stringWithFormat:@"%@",[dic objectForKey:@"CommentId"]] isEqualToString:self.indexTmp] ) {
                      NSMutableDictionary *mutableDict = [dic mutableCopy];
                        int dislikes= [[dic objectForKey:@"Dislikes"] intValue]+1;
                       ;
                        [mutableDict setObject:@(dislikes) forKey:@"Dislikes"];
                        [arrayTmp replaceObjectAtIndex:x withObject:mutableDict];
                        [self.commentsLikeOrDislikeArray addObject:[NSString stringWithFormat:@"%d",x]];
                        break;
                    }
                    x++;
                }
                self.commentsArray=[arrayTmp copy];
              
                [self.tableView beginUpdates];
                [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationAutomatic]
                
                ;
                self.hasPressButtonLikeOrDislike=YES;
                self.islikeComment=@"dislike";
                [self.tableView insertRowsAtIndexPaths:indexPathsToAdd withRowAnimation:UITableViewRowAnimationAutomatic];
                [self.tableView endUpdates];

                self.tmpIndexPath=nil;
                self.indexTmp=nil;
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        case kCOMMENT_OCCURRENCES:
            dictionary = [dictionary objectForKey:@"CommentOccurrenceResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {

                NSString * commentId= [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"ID"]];
                
                NSTimeInterval milisecondedDate = [[NSDate date] timeIntervalSince1970] * 1000;
                Profile * profile= [Profile findWithUserId:[Service getUserId]];
               
                
                NSDictionary *dictionary2 = @{
                                             @"Comment": self.textFieldComment.text,
                                             @"CommentId": commentId,
                                             @"CreatedBy":[NSString stringWithFormat:@"%@ %@", profile.name, profile.lastName],
                                             @"Dislikes": @0,
                                             @"Likes": @0,
                                             @"OccurrenceId": self.occurrenceiD,
                                             @"Date":[NSString stringWithFormat:@"%f",milisecondedDate]
                                             };
                [self.tableView beginUpdates];
                NSMutableArray * mutableComments= [self.commentsArray mutableCopy];
                [mutableComments insertObject:dictionary2 atIndex:[self.commentsArray count]];
                self.commentsArray = [mutableComments copy];
                
                NSInteger row = [self.commentsArray count]-1;
                NSIndexPath *indexPath  =[NSIndexPath indexPathForRow:row inSection:0];
                self.hasPressButtonLikeOrDislike=NO;
                self.islikeComment=@"";
                [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView endUpdates];
                self.textFieldComment.text= @"";

                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
            case kFOLLOW_OR_UNFOLLOW_OCCURRENCES:
            
            dictionary = [dictionary objectForKey:@"FollowOccurrenceResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
         
                if (self.isFollow) {
                    self.followbutton.hidden=YES;
                    self.unfollowButton.hidden=NO;
                    if (self.languaje==1) {
                        self.followLabel.text= @"Dejar de seguir";
                    }else{
                        self.followLabel.text= @"Unfollow";
                    }
                }else {
                    self.followbutton.hidden=NO;
                    self.unfollowButton.hidden=YES;
                    if (self.languaje==1) {
                        self.followLabel.text= @"Seguir";
                    }else{
                        self.followLabel.text= @"Follow";
                    }
                }
            }

            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
            
        default:
            break;
    }

}
- (void) drawInView:(UIView*) view {
    self.isShowInMap=YES;
    CGRect frame =view.frame;
    frame.origin.x=0;
    frame.origin.y=0;
    self.view.frame = frame;
    [view addSubview:self.view];
    [UIView animateWithDuration:0.5 animations:^{
        self.view.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}

- (IBAction)closeView:(id)sender {
    self.hasPressButtonLikeOrDislike=NO;
    self.occurrenceiD=nil;
    self.occ=nil;
    self.likeButton.enabled=YES;
    self.dislikeButton.enabled=YES;
    self.isShowInMap=NO;
    self.commentsLikeOrDislikeArray=nil;
    self.commentsArray=nil;
    self.urlString=nil;
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self.view removeFromSuperview];
    }
    
}
- (NSString*) formattedDate:(NSDate*) date{
    NSString *language= [Service getLanguaje];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    if ([language isEqualToString:@"1"]) {
        [format setDateFormat:@"dd MMM, yyyy hh:mm a"];
    }else{
        [format setDateFormat:@"MMM dd, yyyy hh:mm a"];
        
    }
    NSString *dateString = [format stringFromDate:date];
    return dateString;
}
- (IBAction)like:(id)sender {
    [OccurenceServices likeOccurrenceWithOccurrenceId:self.occurrenceiD Wait:NO Delegate:self];
}

- (IBAction)dislike:(id)sender {
    [OccurenceServices dislikeOccurrenceWithOccurrenceId:self.occurrenceiD  Wait:NO Delegate:self];
}
- (IBAction)follow:(id)sender {
    self.isFollow=YES;
    [OccurenceServices followOccurrencesWithOccurrenceId:self.occurrenceiD Follow:YES Wait:NO Delegate:self];
}

- (IBAction)unfollow:(id)sender {
        [OccurenceServices followOccurrencesWithOccurrenceId:self.occurrenceiD Follow:NO Wait:NO Delegate:self];
     self.isFollow=NO;
}


#pragma mark - table View comments
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
       return [self.commentsArray count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OccurrenceTableViewCell * cell= (OccurrenceTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"Cell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
     NSDictionary* data = [self.commentsArray objectAtIndex:indexPath.row];
    cell.like.enabled=YES;
    cell.dislike.enabled=YES;
    cell.nameUser.text= [data objectForKey:@"CreatedBy"];
   cell.comments.text=  [data objectForKey:@"Comment"];
    NSDate * date= [NSDate dateWithTimeIntervalSince1970:[[data objectForKey:@"Date"] doubleValue]/1000];
    cell.date.text= [StringUtils formattedDateWithDate:date];
    for (NSString * x in self.commentsLikeOrDislikeArray) {
       
        if ([x isEqualToString:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            cell.like.enabled=NO;
            cell.dislike.enabled=NO;
            break;
        }
    }
    NSString * string1= [NSString stringWithFormat:@"%@",[data objectForKey:@"Likes"]];
    NSString * string2=[NSString stringWithFormat:@"%@",[data objectForKey:@"Dislikes"]];
    cell.countLike.text=string1;
    cell.countDislike.text=string2;
    cell.like.tag=[[data objectForKey:@"CommentId"] intValue];
    cell.dislike.tag=[[data objectForKey:@"CommentId"] intValue];
    [cell.like addTarget:self action:@selector(buttonLikeClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.dislike addTarget:self action:@selector(buttonDislikeClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

-(void)buttonLikeClicked:(UIButton *)sender{
    self.indexTmp= [NSString stringWithFormat:@"%ld",(long)sender.tag];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.tmpIndexPath =  [self.tableView indexPathForRowAtPoint:buttonPosition];
    [OccurenceServices likeCommentOccurrenceWithCommentId:[NSString stringWithFormat:@"%ld",(long)sender.tag] Wait:NO Delegate:self];
     
}
-(void)buttonDislikeClicked:(UIButton *)sender{
    self.indexTmp= [NSString stringWithFormat:@"%ld",(long)sender.tag];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    self.tmpIndexPath =  [self.tableView indexPathForRowAtPoint:buttonPosition];
      [OccurenceServices dislikeCommentOccurrenceWithCommentId:[NSString stringWithFormat:@"%ld",(long)sender.tag] Wait:NO Delegate:self];
    
}

- (IBAction)sendComment:(id)sender {
    if ([self.textFieldComment.text isEqualToString:@""]) {
        [self.view endEditing:YES];
        return;
    }
    else{
    [OccurenceServices commentOccurrencesWithOccurrenceId:self.occurrenceiD Comment:self.textFieldComment.text Wait:NO Delegate:self];
        
     [self.view endEditing:YES];
    }
}
-(NSIndexPath *)indexPathForLastMessage {
    return [NSIndexPath indexPathForRow:[self.commentsArray count]-1 inSection:0];
}
- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    [self.view endEditing:YES];
    
}
#pragma mark keyboard show and hide

- (void)registerForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterForKeyboardNotifications {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIView *container= self.view;
    CGRect mainFrame = container.frame;
    mainFrame.size.height = self.height - (kbSize.height-0);
    container.frame = mainFrame;
    [self performSelector:@selector(scrollToBottomTableView) withObject:nil afterDelay:.3];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    CGRect frame = self.view.frame;
    frame.size.height=self.height;
    self.view.frame = frame;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self performSelector:@selector(scrollToBottomTableView) withObject:nil afterDelay:.3];
}
-(void)scrollToBottomTableView {
    if (self.tableView.contentOffset.y > self.tableView.frame.size.height)
    {
        [self.tableView scrollToRowAtIndexPath:[self indexPathForLastMessage]
                              atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([textField.text length]<=99)
    {
        return YES;
    }
    else if([@"" isEqualToString:string])
    {
        textField.text=[textField.text substringToIndex:99];
    }
   return NO;
}
-(void)buttonPressed:(UIButton *)sender{
    [self closeView:nil];
}

@end
