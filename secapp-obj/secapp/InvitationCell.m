//
//  InvitationCell.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 17/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "InvitationCell.h"

@implementation InvitationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
