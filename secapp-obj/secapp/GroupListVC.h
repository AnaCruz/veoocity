//
//  GroupListVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GroupDelegate <NSObject>

@optional
-(void) showViewController:(UIViewController*) viewController;

@end

@interface GroupListVC : UIViewController 

@property id<GroupDelegate> delegate;

@end
