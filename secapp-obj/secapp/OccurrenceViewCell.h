//
//  OccurrenceViewCell.h
//  secapp
//
//  Created by SecApp on 30/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
//celda de las incidencias
@interface OccurrenceViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIButton *iconButton;
@property (strong, nonatomic) IBOutlet UILabel *textLabel;

@end
