//
//  SettingsVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 20/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ContainerVC.h"
@interface SettingsVC : UIViewController
@property ContainerVC * contView;
@end
