//
//  CreateOccurrence.h
//  secapp
//
//  Created by SecApp on 02/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MapLocationVC.h"
@class MapLocationVC;

@interface CreateOccurrence : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *imageIcon;
@property NSString * typeOccurrence;
@property NSString * latitude;
@property NSString * longitude;
@property MapLocationVC * map;
@property BOOL isShowInMap;
- (void) drawInView:(UIView*) view;
- (IBAction)cancel:(id)sender;
@end
