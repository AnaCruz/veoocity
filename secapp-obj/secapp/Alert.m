//
//  Alert.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 11/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "Alert.h"
#import "DeviceUtils.h"
#import "AppDelegate.h"

@implementation Alert

+ (void) showAlertWithTitle:(NSString*) title AndMessage:(NSString*) message {
    if ([UIAlertController class]) {
        [Alert alertCWithTitle:title AndMessage:message];
        return;
    }
    [Alert alertVWithTitle:title AndMessage:message];
}

+ (void) alertCWithTitle:(NSString*) title AndMessage:(NSString*) message {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                               }];
    [alert addAction:ok];
    UIWindow *window = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).window;
    UIViewController *presentedViewController = [Alert viewController:window.rootViewController];
    [presentedViewController presentViewController:alert animated:YES completion:nil];
}

+ (void) alertVWithTitle:(NSString*) title AndMessage:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

+ (UIViewController *)viewController:(UIViewController *)rootViewController {
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isMemberOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [Alert viewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [Alert viewController:presentedViewController];
}


+ (void) showAlertInViewController:(UIViewController*) controller
                         WithTitle:(NSString*) title
                           Message:(NSString*) message {
    if ([UIAlertController class]) {
        UIAlertController* alert = [Alert alertControllerWithTitle:title AndMessage:message];
        [controller presentViewController:alert animated:YES completion:nil];
        return;
    }
    UIAlertView* alert = [Alert alertViewWithTitle:title AndMessage:message];
    alert.alertViewStyle = UIAlertViewStyleDefault;
    [alert show];
}

/**
 *  Build to Alert controller
 *
 *  @param title   Title of the alert
 *  @param message Message of the alert
 *
 *  @return Alert Controller
 */
+ (UIAlertController*) alertControllerWithTitle:(NSString*) title AndMessage:(NSString*) message {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                               }];
    [alert addAction:ok];
    return alert;
}

/**
 *  Build to Alert Veiw
 *
 *  @param title   Tittle of the alert
 *  @param message Message of the alert
 *
 *  @return Alert View
 */
+ (UIAlertView*) alertViewWithTitle:(NSString*) title AndMessage:(NSString*) message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    return alert;
}

@end
