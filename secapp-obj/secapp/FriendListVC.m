//
//  FriendListVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "FriendListVC.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "ContactService.h"
#import "Friend.h"
#import "AppDelegate.h"
#import "Service.h"
#import "ChatContainerVC.h"
#import "ConversationVC.h"
@class ChatContainerVC;
@interface FriendListVC () <ResponseDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextField *searchField;
@property BOOL isSearch;
@property NSMutableArray *contactsArray;
@property NSArray *sortedArray;
@property ChatContainerVC *chatContainer;
@end

@implementation FriendListVC
-(void)viewWillAppear:(BOOL)animated{
  
    self.isSearch = NO;
    self.contactsArray = [[Friend findAll] mutableCopy];
    if (!self.contactsArray) {
        self.contactsArray=[NSMutableArray array];
    }
    self.sortedArray = [[NSMutableArray alloc] init];
    self.searchField = [[UITextField alloc] init];
    self.searchField.delegate = self;
    [self.view addSubview: self.searchField];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    self.contactsArray = nil;
    self.isSearch = NO;
    self.searchField.placeholder=@"";
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}


- (IBAction)textFieldDidChange:(UITextField *)sender {
    NSString * match = sender.text;
    if ([match isEqualToString:@""]) {
        self.isSearch = NO;
        [self.tableView reloadData];
        return;
    }
    NSArray *listFiles = [[NSMutableArray alloc]  init];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:
                             @"name CONTAINS[cd] %@", match];
    listFiles = [NSMutableArray arrayWithArray:[self.contactsArray
                                                filteredArrayUsingPredicate:predicate]];

    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"name"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.sortedArray = [listFiles sortedArrayUsingDescriptors:sortDescriptors];
    self.isSearch = YES;
    [self.tableView reloadData];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Friend * friend;
    if (self.isSearch) {
        
        friend =[self.sortedArray objectAtIndex:indexPath.row];
      
       } else {

       friend =[self.contactsArray objectAtIndex:indexPath.row];
   
    }
    
    
   


    
    
}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) {
        return [self.sortedArray count];
    }
    return [self.contactsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContactCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
   if (self.isSearch) {
        Friend *friendsSearch =[self.sortedArray objectAtIndex:indexPath.row];
        cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",friendsSearch.name, friendsSearch.lastName];
       cell.buttonChat.tag= [friendsSearch.userId integerValue];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            if([friendsSearch.pictureProfile isEqual:@""]|| friendsSearch.pictureProfile==nil){
                
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                cell.profileImageView.image = [UIImage imageWithData:friendsSearch.pictureProfile];
                
            }
            
            
        });
        
        return cell;
        
    }
   Friend * friends=[self.contactsArray objectAtIndex:indexPath.row];
   
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@ %@",friends.name, friends.lastName];
    cell.buttonChat.tag= [friends.userId integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        if([friends.pictureProfile isEqual:@""]|| friends.pictureProfile==nil){
            
            cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
        }
        else{
            cell.profileImageView.image = [UIImage imageWithData:friends.pictureProfile];
            
        }
        
    });
    
       [cell.buttonChat addTarget:self action:@selector(buttonChatClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)buttonChatClicked:(UIButton *)sender{
   

    ConversationVC * conversation = [[UIStoryboard storyboardWithName:@"Chat" bundle:nil] instantiateViewControllerWithIdentifier:@"ConversationVC"];
    conversation.userIdTo=[NSString stringWithFormat:@"%ld", (long)sender.tag];
    
//        [self.navigationController pushViewController:conversation animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(showViewController:)]) {
        [self.delegate showViewController:conversation];
       
    }
    
}

- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    
    [self.view endEditing:YES];
}

@end
