//
//  CellConversationSample.h
//  secapp
//
//  Created by SecApp on 29/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellConversationSample : UITableViewCell

//@property (weak, nonatomic) IBOutlet UILabel *nameSender;
//@property (weak, nonatomic) IBOutlet UILabel *message;
//@property (weak, nonatomic) IBOutlet UILabel *timeSend;

//@property (weak, nonatomic) NSString *senderId;
//@property (strong, nonatomic) NSString *message;
//@property NSDate *time;
@property (strong, nonatomic) NSDictionary * dictionaryMessage;
//Estimate BubbleCell Height
-(CGFloat)height;

//Estimate BubbleCell Witdh
-(CGFloat)width;

@end
