//
//  ContainerVC.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 09/04/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Friend.h"
#import "InfoUserVC.h"
//#import "GADBannerView.h"
//#import "GADInterstitial.h"
@class PairBeaconInterfaceVC;
@interface ContainerVC : UIViewController
@property BOOL isDisableAlert;
@property (weak, nonatomic) IBOutlet UIButton *btnAlert;
@property NSString *alertType;
@property NSString *notiId;
@property InfoUserVC *infoU;

- (void) showNotifications;
- (void) hideNotifications;
- (void) hideSearching;
-(void) showUsersCount: (int)count;
- (void) hideEditProfile;
- (void)showInfoBar:(Friend *)friendInfoBar;
-(void)showInfoBarWithMe;
-(void)disableAlertWithAlertType:(int)alertType;
-(void) showNotificationsCount: (int)count;

- (void) clearContainer;
-(void)closeInfoUser;
-(void)showView:(NSString *)friendSelected IsMonitor:(BOOL)isMonitor; //individual conversation / monitor
-(void)showViewChatGroup:(NSDictionary *)datadic GroupId:(NSString *)groupId; //conversation group
- (void) clearSelection:(UIButton*) sender;

-(void)updateContacts;
-(void)startAlertFromBeacon;
-(void)updateOccurrences;
-(void)setContainerVCDelegateOfBeaconCentral;

@end
