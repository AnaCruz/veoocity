//
//  ImageUtils.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 05/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface ImageUtils : NSObject

/**
 * Encode a Image to base64.
 * @param image Image to encode.
 * @return String with the content in base64.
 */
+(NSString*) encodingImageToBase64:(UIImage*)image;

/**
 * Dencode a base to image.
 * @param imageStr String with the content in base64.
 * @return Image.
 */
+(UIImage*) decodingBase64ToImage:(NSString*)imageStr;

+(UIImage *)resizeImageFor72DPI:(UIImage*)image;

@end
