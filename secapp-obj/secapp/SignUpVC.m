//
//  SignUpVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 04/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SignUpVC.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "Service.h"
#import "ErrorResult.h"
#import "Alert.h"
#import "AlertService.h"
#import "Alert/AlertView.h"
#import "AppDelegate.h"
#import "SessionServices.h"
#import "UserServices.h"
#import "CompleteSingUpAlertVC.h"
#import "AlertView.h"
#import "ConditionsVC.h"



@interface SignUpVC() <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, ResponseDelegate, AlertViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthImage;
@property (weak, nonatomic) IBOutlet UITextField *inFristName;
@property (weak, nonatomic) IBOutlet UITextField *inLastName;
@property (weak, nonatomic) IBOutlet UITextField *inPhone;
@property (weak, nonatomic) IBOutlet UITextField *inEMail;
@property (weak, nonatomic) IBOutlet UITextField *inPin;
@property (weak, nonatomic) IBOutlet UISwitch *inGender;
@property (weak, nonatomic) IBOutlet UIButton *image;
@property (weak, nonatomic) IBOutlet UISwitch *condition;
@property NSString* tmpUser;
@property NSString* tmpPin;
@property (weak, nonatomic) IBOutlet UIButton *faceSignButton;
@property (weak, nonatomic) IBOutlet UIButton *twitterSignButton;
@property NSString* facebookIdData;
@property NSString* firstNameData;
@property NSString* lastNameData;
@property NSString* phoneData;
@property NSString* emailData;
@property NSString* genderData;
@property NSString* image64Data;
@property  NSString* twitterIdData;
@property  CompleteSingUpAlertVC* alert;
@property (weak, nonatomic) IBOutlet UIButton *termsAndConditionsButton;
@property (weak, nonatomic) IBOutlet UIButton *termsAndConditionsButtonText;



@end

@implementation SignUpVC

-(void)viewDidLoad {
    [super viewDidLoad];
   // [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"background"]]];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"backg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];
//     UIColor *color = [UIColor whiteColor];
//    self.inFristName.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"Nombre" attributes:@{NSForegroundColorAttributeName: color}];
//    self.inLastName.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"Apellidos" attributes:@{NSForegroundColorAttributeName: color}];
//    self.inPhone.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"Número de teléfono" attributes:@{NSForegroundColorAttributeName: color}];
//    self.inEMail.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"Correo electrónico" attributes:@{NSForegroundColorAttributeName: color}];
//    self.inPin.attributedPlaceholder =[[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName: color}];
   UIColor *color = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1.0];
   NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
   NSString * text;
    int languaje= [[Service getLanguaje]intValue];
    if(languaje==1){
        text= @"Términos y condiciones";
    }else{
        text= @"Terms and conditions";
    }
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    // using text on button
    [self.termsAndConditionsButtonText setAttributedTitle: titleString forState:UIControlStateNormal];
    [self addPaddingToTextField:self.inFristName];
    [self addPaddingToTextField:self.inLastName];
    [self addPaddingToTextField:self.inPin];
    [self addPaddingToTextField:self.inPhone];
    [self addPaddingToTextField:self.inEMail];
    self.image.clipsToBounds = YES;
    int radius = self.widthImage.constant/2;
    self.image.layer.cornerRadius = radius;
    self.image.layer.borderWidth = 3;
    self.image.layer.borderColor = [[UIColor whiteColor] CGColor];
     self.alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertSignup"];
    [self addChildViewController:self.alert];
    self.alert.controller = self;
}

- (void) addPaddingToTextField:(UITextField*) sender {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.leftView = paddingView;
    sender.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.rightView = paddingView2;
    sender.rightViewMode = UITextFieldViewModeAlways;
}

- (IBAction)close:(id)sender {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    [app goToLogin];
}

//- (IBAction)signUpWithFacebook:(id)sender {
//    self.faceSignButton.selected=YES;
//    self.twitterSignButton.selected=NO;
//    FacebookManagement* fm = [[FacebookManagement alloc] initWithDelegate:self];
//    if ([fm isSessionActive]) {
//        [self completeLoginFacebook:fm];
//        return;
//    }
//    [fm login];
//}
//- (IBAction)signUpWithTwitter:(id)sender {
//    self.twitterSignButton.selected=YES;
//    self.faceSignButton.selected=NO;
//    TwitterManagement* tm = [[TwitterManagement alloc] initWithDelegate:self];
//    if ([tm isActiveSession]) {
//       [self completeLoginTwitter:tm];
//        return;
//    }
//    [tm login];
//}

- (IBAction)takePhoto:(id)sender {
//    self.firstNameData =self.inFristName.text;
//    self.lastNameData = self.inLastName.text;
//    self.phoneData= self.inPhone.text;
//    self.emailData= self.inEMail.text;
//    self.tmpPin=self.inPin.text;

    // Show options to select a image.
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Take photo of:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            @"Clear",
                            nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}
- (IBAction)showTermsAndConditions:(UIButton *)sender {
    UIViewController* conditions = [[UIStoryboard storyboardWithName:@"Views" bundle:nil] instantiateViewControllerWithIdentifier:@"ConditionsVC"];

     [self presentViewController:conditions animated:YES completion:nil];
}

- (IBAction)signUp:(id)sender {
    
    if (!self.condition.isOn) {
        [Alert showAlertWithTitle:@"Error" AndMessage:@"Para registrarse debe de aceptar términos y condiciones"];
        return;
    }
    NSString* firstName = self.inFristName.text;
    NSString* lastName = self.inLastName.text;
    NSString* phone = self.inPhone.text;
    NSString* email = self.inEMail.text;
    NSString* pin = self.inPin.text;
    NSString* gender = self.inGender.isOn ? @"1" : @"2";
    UIImage* imageProfile = self.image.currentBackgroundImage;
    NSString* image64 = imageProfile ? [ImageUtils encodingImageToBase64:imageProfile] : @"";
    
    if ([StringUtils isEmptyStringsInArray:@[firstName, lastName, phone, email, pin, gender]]) {
        
        [Alert showAlertWithTitle:@"Error" AndMessage:@"Alguno de los datos están vacios."];
        return;
    }
    if (![StringUtils validateEmail:email]) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"correo.no.valido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
        alert.delegate=self;
        return;
    }
    
    if (![StringUtils validateNumberPhone:phone]) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"telefono.no.valido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
        alert.delegate=self;
        return;
    }
    self.tmpPin = pin;
    self.tmpUser = email;
    [UserServices createAccountWithFirstName:firstName LastName:lastName Pin:pin Phone:phone Email:email UserName:firstName Gender:gender Image:image64 FacebookId:@"" TwitterId:@"" Wait:YES Delegate:self];
}

- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}

/**
 * Open to camera or gallery.
 * @param fromCamara If is YES open the camera, if is NO open the gallery
 */
-(void) selectImageFromCamera:(BOOL) fromCamera {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.allowsEditing = YES; // Editing image
    [self presentViewController:picker animated:YES completion:nil];
}


- (void) showAlert {
    self.alert.view.frame = self.view.frame;
//    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view addSubview:self.alert.view];
    self.alert.controller=self;
//    });
}

-(void)signUpComplete:(NSDictionary *)data{
//servicio de twittter o face
    if (self.faceSignButton.selected==YES) {
        [UserServices createAccountWithFirstName:self.firstNameData LastName:self.lastNameData Pin:[data objectForKey:@"Pin"] Phone:[data objectForKey:@"Phone"] Email:[data objectForKey:@"Email"] UserName:self.firstNameData Gender:self.genderData Image:self.image64Data FacebookId:self.facebookIdData TwitterId:@"" Wait:YES Delegate:self];
    }else{
        NSString* gender = self.inGender.isOn ? @"1" : @"2";
        
        [UserServices createAccountWithFirstName:self.firstNameData LastName:@" " Pin:[data objectForKey:@"Pin"] Phone:[data objectForKey:@"Phone"] Email:[data objectForKey:@"Email"] UserName:self.firstNameData Gender:gender Image:self.image64Data FacebookId:@"" TwitterId:self.twitterIdData Wait:YES Delegate:self];
    }

}

#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=4);
    }
    if (textField.tag == 200) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=30);
    }
    if (textField.tag == 300) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=50);
    }
    if (textField.tag == 400) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=15);
    }
    if (textField.tag == 500) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=50);
    }
    return YES;
}

#pragma mark - Delegates methods of UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage]; // Show image edited
    [self.image setBackgroundImage:image forState:UIControlStateNormal]; // Show image edited
//    self.inFristName.text= self.firstNameData;
//    self.inLastName.text=self.lastNameData;
//    self.inPhone.text=self.phoneData;
//    self.inEMail.text= self.emailData;
//    self.inPin.text= self.tmpPin;
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
    // Close picker
//    self.inFristName.text= self.firstNameData;
//    self.inLastName.text=self.lastNameData;
//    self.inPhone.text=self.phoneData;
//    self.inEMail.text= self.emailData;
//    self.inPin.text= self.tmpPin;
//
}

#pragma mark - Delegates methods of UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self selectImageFromCamera:YES];
            break;
        case 1:
            [self selectImageFromCamera:NO];
            break;
        case 2:
            [self.image setBackgroundImage:nil forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

#pragma mark - Delegates methods of FacebookDelagate

#pragma mark - Delegates methods of ResponseDelegate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    NSDictionary *dicResult;
    int respondeCode = -1;
    switch (service) {
        case kCREATE_USER:
            dicResult = [dictionary objectForKey:@"CreateUserResult"];
            respondeCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            if (dicResult && [ErrorResult errorResultWithErrorCode:respondeCode AndErrorMessage:[dicResult objectForKey:@"ErrorMessage"]]) {
               if (self.faceSignButton.selected==YES) {
                    [SessionServices loginWithFacebook:self.facebookIdData Wait:YES Delegate:self];
                }else{
                    [SessionServices loginWithMail:self.tmpUser Pin:self.tmpPin Wait:YES Delegate:self];
                }
            }
            break;
        case kLOGIN:
            dicResult = [dictionary objectForKey:@"LoginResult"];
            respondeCode = [[dicResult objectForKey:@"ResponseCode"] intValue];
            self.tmpPin = nil;
            self.tmpUser = nil;
            if (dicResult && [ErrorResult errorResultWithErrorCode:respondeCode AndErrorMessage:[dicResult objectForKey:@"ErrorMessage"]]) {
                
                NSString* userId = [dicResult objectForKey:@"UserId"];
                NSString* sessionId = [dicResult objectForKey:@"SessionId"];
                
                if (userId && sessionId) {
                 
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    [userDefault setObject:userId forKey:@"userId"];
                    [userDefault setObject:sessionId forKey:@"sessionId"];
                    AppDelegate* app = [[UIApplication sharedApplication] delegate];
                    [app goToMainView];
                }
            }
        default:
            break;
    }
}

-(void)errorResponseWithService:(kService)service Error:(NSError *)error {
    NSLog(@"SignUpVC: Error: %@",[error description]);
}
-(void)viewDidDisappear:(BOOL)animated{
//    self.faceSignButton.selected=NO;
//    self.twitterSignButton.selected=NO;
//    self.facebookIdData=nil;
//    self.twitterIdData=nil;
//    self.genderData=nil;
//    self.firstNameData=nil;
//    self.inFristName.text=@"";
//    self.inLastName.text=@"";
//    self.inPhone.text=@"";
//    self.inEMail.text=@"";
//   self.inPin.text=@"";
//   self.tmpUser=nil;
//    self.tmpPin=nil;
//
//    self.lastNameData=nil;
//    self.emailData=nil;
//    self.genderData=nil;
//    self.image64Data=nil;
    

    
}

@end
