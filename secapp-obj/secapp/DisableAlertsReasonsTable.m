//
//  DisableAlertsReasonsTable.m
//  secapp
//
//  Created by SecApp on 19/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "DisableAlertsReasonsTable.h"

@interface DisableAlertsReasonsTable ()

@end

@implementation DisableAlertsReasonsTable

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.callback) {
        return;
    }
    switch (indexPath.row) {
        case 0:
            self.callback(1);
            break;
        case 1:
            self.callback(2);
            break;
        case 2:
            self.callback(3);
            break;
        case 3:
            self.callback(4);
            break;
        case 4:
            self.callback(0);
            break;
        default:
            break;
    }
}


@end
