//
//  Occurrence.m
//  secapp
//
//  Created by SecApp on 28/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "Occurrence.h"
#import "AppDelegate.h"

@implementation Occurrence

@dynamic latitude;
@dynamic longitude;
@dynamic occurrenceId;
@dynamic type;
@dynamic isSpecial;
@dynamic createdUserId;

+ (instancetype)createObject {
    Occurrence * occurrence = [super createObjectWithNameEntity:@"Occurrence"];
    return occurrence;
}
+ (NSArray*) findAll {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Occurrence"];
    NSError *error;
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"occurrenceId" ascending:YES];
    [request setSortDescriptors:@[sortDescriptor]];
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Occurrence (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}
+ (NSArray*) findWithOccurrenceId:(NSString*) occurrenceId {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext* context = [app managedObjectContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Occurrence"];
    NSError *error;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:
                              [NSString stringWithFormat:@"occurrenceId == '%@'", occurrenceId]];
    [request setPredicate:predicate];
    
    NSArray *array = [context executeFetchRequest:request error:&error];
    
    if (error) {
        NSLog(@"Occurrence (ERROR: %@)", [error localizedDescription]);
        return nil;
    }
    return array;
}
@end
