//
//  SearchContactsVC.m
//  secapp
//
//  Created by SecApp on 02/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SearchContactsVC.h"
#import "UserServices.h"
#import "Email.h"
#import "Phone.h"
#import "Profile.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "Alert.h"
#import "AppDelegate.h"
#import "Service.h"
#import <CoreLocation/CoreLocation.h>
#import "ContactService.h"
#import "Friend.h"
#import "MapSecApp.h"
#import "ContainerVC.h"
#import "InfoUserVC.h"


@interface SearchContactsVC () //<InfoUserDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextField *searchField;
@property BOOL isSearch;
@property NSArray * allFriends;
@property NSMutableDictionary *contactsLetter;
@property NSMutableArray *contactsArray;
@property NSArray *contactSectionTitles;
@property NSArray *contactIndexTitles;
@property(nonatomic, retain) UIColor *sectionIndexColor;
@property NSArray *sortedArray;
@property InfoUserVC *infoU;
@property Friend * friendInfoBar;

@end

@implementation SearchContactsVC
-(void)viewWillAppear:(BOOL)animated{
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionIndexColor = [UIColor lightGrayColor]; 
    self.tableView.sectionIndexBackgroundColor = [UIColor clearColor];
      self.isSearch = NO;
    self.contactsLetter= [[NSMutableDictionary alloc] init];
    self.contactSectionTitles = [[NSMutableArray alloc] init];
    self.contactsArray = [[NSMutableArray alloc] init];
    self.contactIndexTitles = @[@"A",@"Á", @"B", @"C", @"D", @"E",@"É", @"F", @"G", @"H", @"I",@"Í", @"J", @"K", @"L", @"M", @"N", @"O",@"Ó", @"P", @"Q", @"R", @"S", @"T", @"U",@"Ú", @"V", @"W", @"X", @"Y", @"Z"];
    
    [self updateContacts];
   
    
    self.sortedArray = [[NSMutableArray alloc] init];
    self.searchField = [[UITextField alloc] init];
    self.searchField.delegate = self;
//    [self.searchField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.view addSubview: self.searchField];
    
    
}
-(void)viewDidDisappear:(BOOL)animated{
    
    self.contactsArray = nil;
    self.contactsLetter = nil;
    self.contactSectionTitles = nil;
    self.contactIndexTitles =nil;
    self.isSearch = NO;
    self.searchField.placeholder=@"";
    self.friendInfoBar=nil;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)contactList:(Friend *)friend{
    if (friend.userId==nil) {
        return;
    }
    NSString * name=friend.name;
    NSString *firstLetter = [name substringToIndex:1];
    NSString *uppercase = [firstLetter uppercaseString];
    NSMutableDictionary *contValue = [[NSMutableDictionary alloc] init];
    [contValue setObject:[NSString stringWithFormat:@"%@ %@",friend.name,friend.lastName] forKey:@"userName"];
    [contValue setObject:uppercase forKey:@"letter"];
    [contValue setObject:friend.userId forKey:@"userId"];
    [contValue setObject:friend.currentLatitude forKey: @"latitude"];
    [contValue setObject:friend.currentLongitude forKey:@"longitude"];
    if (friend.pictureProfile== nil) {
        NSString * picture = @"";
        [contValue setObject:picture forKey:@"pictureProfile"];
    }
    else{
    [contValue setObject:friend.pictureProfile forKey:@"pictureProfile"];
    }
    [self.contactsArray addObject:[contValue mutableCopy]];
}
- (IBAction)textFieldDidChange:(UITextField *)sender {
    NSString * match = sender.text;
    if ([match isEqualToString:@""]) {
        self.isSearch = NO;
        [self.tableView reloadData];
        return;
    }
    NSArray *listFiles = [[NSMutableArray alloc]  init];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:
                             @"userName CONTAINS[cd] %@", match];
    //or use Name like %@  //”Name” is the Key we are searching
    listFiles = [NSMutableArray arrayWithArray:[self.contactsArray
                                                filteredArrayUsingPredicate:predicate]];
    // Now if you want to sort search results Array
    //Sorting NSArray having NSDictionary as objects
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"userName"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.sortedArray = [listFiles sortedArrayUsingDescriptors:sortDescriptors];
    self.isSearch = YES;
    [self.tableView reloadData];
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.isSearch) {
        NSDictionary *dictionary =[self.sortedArray objectAtIndex:indexPath.row];
        NSString *contactSearch = [dictionary objectForKey:@"userId"];
        self.friendInfoBar=[[Friend findWithUserId:contactSearch]objectAtIndex:0];
        [self showContactCoordinateWithLongitude:[dictionary objectForKey:@"longitude"] Latitude:[dictionary objectForKey:@"latitude"]];
    } else {
        NSString *sectionTitle = [self.contactSectionTitles objectAtIndex:indexPath.section];
        NSArray *sectionContact = [self.contactsLetter objectForKey:sectionTitle];
        NSDictionary *dictionary =[sectionContact objectAtIndex:indexPath.row];
        NSString *contact = [dictionary objectForKey:@"userId"];
         self.friendInfoBar=[[Friend findWithUserId:contact]objectAtIndex:0];
        [self showContactCoordinateWithLongitude:[dictionary objectForKey:@"longitude"] Latitude:[dictionary objectForKey:@"latitude"]];
    }
    

}

-(void)showContactCoordinateWithLongitude:(NSNumber *)longitude Latitude:(NSNumber *)latitude{
    CGFloat newLat = [latitude floatValue];
    CGFloat newLon = [longitude floatValue];
    CLLocationCoordinate2D newCoord = {newLat, newLon};
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(newCoord, 50, 50);
    [[MapSecApp shareInstance] setRegion:region animated:YES];
    if (self.container) {
        [self.container hideSearching];
        [self.container showInfoBar:self.friendInfoBar];
    }
}

-(void)updateContacts{
    NSArray * result;
    if ([[Friend findAll]count]>0) {
        result = [Friend findAll];
        for (Friend *friend in result) {
            [self contactList:friend];
        }
    }else{
        return;
    }
    for (NSString* letter in self.contactIndexTitles) {
        NSMutableArray * letterArray =[[NSMutableArray alloc] init];
        for (NSDictionary* contactData in self.contactsArray) {
            NSString *value= [contactData objectForKey:@"letter"];
            if ([value isEqualToString:letter]) {
                
                [letterArray addObject:[contactData mutableCopy]];
            }
        }
        if ([letterArray count] > 0) {
            [self.contactsLetter setObject:[letterArray mutableCopy] forKey: letter];
        }
    }
    self.contactSectionTitles = [[self.contactsLetter allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    [self.tableView reloadData];
}



#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if (self.isSearch) {
        return 1;
    }
 return [self.contactSectionTitles count];
   
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) {
        return [self.sortedArray count];
    }
   // Return the number of rows in the section.
    NSString *sectionTitle = [self.contactSectionTitles objectAtIndex:section];
    NSArray *sectionContacts = [self.contactsLetter objectForKey:sectionTitle];
    return [sectionContacts count];
  
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (self.isSearch) {
        return @"Resultados";
    }
    return [self.contactSectionTitles objectAtIndex:section];
   
}
- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SimpleTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    tableView.sectionIndexColor = [UIColor lightGrayColor]; //color del indice de la izquierda
    tableView.sectionIndexBackgroundColor = [UIColor clearColor];
    if (self.isSearch) {
       NSDictionary *dictionary =[self.sortedArray objectAtIndex:indexPath.row];
        NSString *contactSearch = [dictionary objectForKey:@"userName"];
        cell.nameLabel.text = contactSearch;
        dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
            if([[dictionary objectForKey:@"pictureProfile"] isEqual:@""]){
                
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                cell.profileImageView.image = [UIImage imageWithData:[dictionary objectForKey:@"pictureProfile"]];
            }
        });
        return cell;
 }
    // Configure the cell...
    
   
    NSString *sectionTitle = [self.contactSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionContact = [self.contactsLetter objectForKey:sectionTitle];
    NSDictionary *dictionary =[sectionContact objectAtIndex:indexPath.row];
    NSString *contact = [dictionary objectForKey:@"userName"];
   cell.nameLabel.text = contact;
     dispatch_async(dispatch_get_main_queue(), ^{
    cell.profileImageView.clipsToBounds = YES;
    int radius = cell.profileImageView.frame.size.width/2;
    cell.profileImageView.layer.cornerRadius = radius;
    cell.profileImageView.layer.borderWidth = 3;
    cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
   
         if([[dictionary objectForKey:@"pictureProfile"] isEqual:@""]){
           
              cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
         }
         else{
               cell.profileImageView.image = [UIImage imageWithData:[dictionary objectForKey:@"pictureProfile"]];
            
         }
    
    });
  
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
   
    return self.contactIndexTitles;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [self.contactSectionTitles indexOfObject:title];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{

}

- (IBAction)Close:(id)sender {
   [self.view removeFromSuperview];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}


@end
