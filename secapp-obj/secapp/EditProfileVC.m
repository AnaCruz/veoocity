//
//  EditProfile.m
//  secapp
//
//  Created by SecApp on 19/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EditProfileVC.h"
#import "UserServices.h"
#import "Profile.h"
#import "Phone.h"
#import "Email.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "Service.h"
#import "ContainerVC.h"
#import "Alert/AlertView.h"
#import "CheckPin.h"
#import "AppDelegate.h"
#import "ContainerVC.h"


@interface EditProfileVC () <ResponseDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, AlertViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *userName;
@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *pin;
@property (weak, nonatomic) IBOutlet UIButton *picture;
@property (weak, nonatomic) IBOutlet UITextField *pinConfirmation;
@property UIImage * scaledImage;
@property (weak ,nonatomic) NSString * languaje;


@property Profile* userProfile;
@property BOOL selectedNewImage;
@end

@implementation EditProfileVC

-(void)viewWillAppear:(BOOL)animated{
    if (self.selectedNewImage) {
        self.selectedNewImage = NO;
        return;
    }
    for (Profile *userToDelete in [Profile findAllProfiles]) {
        if ([userToDelete.userId isEqualToString:[UserServices getUserId]]) {
            continue;
        }else{
            [userToDelete deleteObject];
        }
    }
    Profile* profile = [Profile findWithUserId:[UserServices getUserId]];
    if (profile==nil) {
        profile = [Profile createObject];
        [profile saveObject];
        [UserServices getProfileUserWithWait:NO Delegate:self Extra:profile];
    }else if ([profile.name isEqual:[NSNull null]]|| [profile.name isEqualToString:@""]|| profile.name==nil){
        [UserServices getProfileUserBasicWithWait:NO Delegate:self Extra:profile];
    }else{
        [self getUserProfile:profile];
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];
    self.languaje= [Service getLanguaje];
    self.selectedNewImage = NO;
    self.picture.clipsToBounds = YES;
    int radius = 70/2;
    self.picture.layer.cornerRadius = radius;
    self.picture.layer.borderWidth = 3;
    self.picture.layer.borderColor = [[UIColor whiteColor] CGColor];
}

/**
 * Get the information of the User from CoreData
 */
-(void)getUserProfile:(Profile *)profile{
   
    if (profile!=nil) {
        self.userProfile = profile;
        self.userName.text =self.userProfile.name;
        self.lastName.text = self.userProfile.lastName;
        if (self.userProfile.pictureProfile) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageWithData:self.userProfile.pictureProfile] forState:UIControlStateNormal];
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageNamed:@"avatar"] forState:UIControlStateNormal];
            });
        }
        
    }
    
}

/**
 * Show a Menu of select a picture to profile.
 */
- (IBAction)takePhoto:(id)sender {
  
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view endEditing:YES];//close keyboard
    });
    
    // Show options to select a image.
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:@"Take photo of:" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Camera",
                            @"Gallery",
                            @"Clear",
                            nil];
    [popup showInView:[UIApplication sharedApplication].keyWindow];
}

/**
 * Open the camera or galery for select a picture.
 */
-(void) selectImageFromCamera:(BOOL) fromCamera {
    UIImagePickerController* picker = [[UIImagePickerController alloc] init];
    [picker setDelegate:self];
    [picker setSourceType:fromCamera ? UIImagePickerControllerSourceTypeCamera : UIImagePickerControllerSourceTypePhotoLibrary];
    picker.modalPresentationStyle = UIModalPresentationCurrentContext;
    picker.allowsEditing = YES; // Editing image
    [self.mainContainer presentViewController:picker animated:YES completion:^{
        self.selectedNewImage = YES;
    }];
}

/**
 * Cancel the modified information before being saved.
 */
- (IBAction)cancel:(id)sender {
    self.userName.text =self.userProfile.name;
    self.lastName.text = self.userProfile.lastName;
    if (self.userProfile.pictureProfile) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.picture setBackgroundImage:[UIImage imageWithData:self.userProfile.pictureProfile] forState:UIControlStateNormal];
        });
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.picture setBackgroundImage:[UIImage imageNamed:@"avatar"] forState:UIControlStateNormal];
        });
    }
    self.pin.text = @"";
    self.pinConfirmation.text = @"";
     [self.view endEditing:YES];
    
    [self.mainContainer hideEditProfile];
}

/**
 * Send the information to WS for save changes.
 */
- (IBAction)save:(id)sender {
    [self.view endEditing:YES];
    if (![self.pin.text isEqualToString:@""]) {
        if(self.pin.text.length>4||self.pin.text.length<4){
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"pin.cuatro.digits", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        }else{
            if ([self.pin.text isEqualToString:self.pinConfirmation.text]) {
                CheckPin* checkPin = [[CheckPin alloc] init];
                [checkPin checkPinWithController:self.navigationController.parentViewController Callback:^(BOOL complete) {
                    if (complete) {
                        [self save];
                    }
                }];
            } else {
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"pin.no.coinciden", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
            }
        }
    } else {
        [self save];
    }
}

- (void) save {
    NSString* userName = self.userName.text;
    NSString* lastName = self.lastName.text;
    NSString* pin = self.pin.text;
    UIImage* imageProfile = self.picture.currentBackgroundImage;
    NSString* image64 = imageProfile ? [ImageUtils encodingImageToBase64:imageProfile] : @"";
    if ([image64 isEqualToString:[ImageUtils encodingImageToBase64:[UIImage imageWithData:self.userProfile.pictureProfile]]]) {
        image64 = @"";
    }
    else{

    UIImage *originalImage = imageProfile;
    self.scaledImage= [ImageUtils resizeImageFor72DPI:originalImage];
    image64 = self.scaledImage ? [ImageUtils encodingImageToBase64:self.scaledImage] : @"";
    }
    
    if ([StringUtils isEmptyStringsInArray:@[userName, lastName]]) {
       
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"datos.vacios", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
        return;
    }
    [UserServices updateAccountWithFirstName:userName LastName:lastName Pin:pin UserName:userName Gender:[NSString stringWithFormat:@"%@", self.userProfile.gender] Image:image64 FacebookId:@"" TwitterId:@"" Wait:YES Delegate:self];
}

- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}

#pragma mark - Delegates methods of UIImagePickerController

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
    // Show image edited
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.picture setBackgroundImage:image forState:UIControlStateNormal];
    });
   
    //return to the editing view for continue with the profile edition
    [self.view setNeedsDisplay];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil]; // Close picker
    
}

#pragma mark - Delegates methods of UIActionSheet

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            [self selectImageFromCamera:YES];
            break;
        case 1:
            [self selectImageFromCamera:NO];
            break;
        case 2:
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.picture setBackgroundImage:[UIImage imageNamed:@"avatar"] forState:UIControlStateNormal];
            });
            break;
    }
}

#pragma mark - ResponseDeleagete

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    
    switch (service) {
        case kUPDATE_USER:
            dictionary = [dictionary objectForKey:@"UpdateUserResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                self.userProfile.name = self.userName.text;
                self.userProfile.lastName = self.lastName.text;
                if (self.picture.currentBackgroundImage) {
                    self.userProfile.pictureProfile = UIImageJPEGRepresentation(self.scaledImage,0.5);
                    
                }
                [self.userProfile mergeObject];
                self.pin.text = @"";
                self.pinConfirmation.text = @"";
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"datos.guardados", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                self.pin.text = @"";
                self.pinConfirmation.text = @"";
                [self.view endEditing:YES];
                [self.mainContainer hideEditProfile];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
         break;
        default:
            break;
    }
}
-(void)successResponseWithService:(kService)service Response:(id)response Extra:(id)extra{
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
    
    switch (service) {
        case kGET_USER_PROFILE:
            dictionary = [dictionary objectForKey:@"ProfileResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                [self parseDictionary:dictionary ToProfile:extra];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                
            }
            
            break;

        case kGET_USER_PROFILE_BASIC:
            dictionary = [dictionary objectForKey:@"BasicProfileResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
               [self parseDictionary:dictionary ToProfileBasic:extra];
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
            }
            break;
        default:
            break;
    }



}
/**
 * Parsing the JSON to Profile Object
 * @param dictionary JSON Object
 * @param profile Profile Object
 */
- (void) parseDictionary:(NSDictionary*) dictionary ToProfile:(Profile*) profile {
    profile.userId = [UserServices getUserId];
    
    NSString * name;
    NSString * userName;
    NSString * lastname;
    if ([[dictionary objectForKey:@"Name"] isEqualToString:@""]||[[dictionary objectForKey:@"Name"]isEqual:[NSNull null]]) {
        if([[dictionary objectForKey:@"Username"] isEqualToString:@""]||[[dictionary objectForKey:@"Username"] isEqual:[NSNull null]]){
            if ([[dictionary objectForKey:@"LastName"] isEqualToString:@""]||[[dictionary objectForKey:@"LastName"] isEqual:[NSNull null]]){
                if ([self.languaje isEqualToString:@"1"]) {
                    name= @"Desconocido";
                    lastname = @"";
                }else{
                    name= @"Unknown";
                    lastname = @"";
                }
                
            }else{
                name=[dictionary objectForKey:@"LastName"];
                userName= [dictionary objectForKey:@"LastName"];
                lastname=@"";
                
            }
        } else{
            
            name=[dictionary objectForKey:@"Username"];
            userName=[dictionary objectForKey:@"Username"];
        }
    }else {
        name=[dictionary objectForKey:@"Name"];
        if([[dictionary objectForKey:@"Username"] isEqualToString:@""]||[[dictionary objectForKey:@"Username"] isEqual:[NSNull null]]){
            userName=name;
        }else{
            userName = [dictionary objectForKey:@"Username"];
        }
    }
    
    if ([[dictionary objectForKey:@"LastName"] isEqualToString:@""]||[[dictionary objectForKey:@"LastName"] isEqual:[NSNull null]]) {
        lastname = @"";
    }else{
        lastname=[dictionary objectForKey:@"LastName"];
    }
  
    profile.userName = userName;
    profile.name = name;
    profile.lastName = lastname;
    profile.status = [NSNumber numberWithInt:[[dictionary objectForKey:@"Status"] intValue]];
    
    if ([dictionary objectForKey:@"idFacebook"] && ![[dictionary objectForKey:@"idFacebook"] isEqual: [NSNull null]]) {
        profile.facebookId = [dictionary objectForKey:@"idFacebook"];
    }
    
    NSString *uri= [dictionary objectForKey:@"ProfileImageURL"];
    if (uri && ![uri isEqual: [NSNull null]])  {
      //  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
            profile.pictureProfile = data;
            [profile mergeObject];
     //   });
    }
    [profile removePhones:profile.phones];
    NSArray *tmpPhones = [dictionary objectForKey:@"Telephone"];
    for (NSString* phone in tmpPhones) {
        Phone *objPhone = [Phone createObject];
        objPhone.phone = phone;
        [objPhone saveObject];
        [profile addPhonesObject:objPhone];
    }
    [profile removeEmails:profile.emails];
    NSArray *tmpEmail = [dictionary objectForKey:@"Email"];
    for (NSString* email in tmpEmail) {
        Email *objEmail = [Email createObject];
        objEmail.email = email;
        [objEmail saveObject];
        [profile addEmailsObject:objEmail];
    }
    profile.aplicationType =[dictionary objectForKey:@"ApplicationType"];
    profile.hiddenPosition = [dictionary objectForKey:@"HiddenPosition"];
    profile.showOccurrenceNotifications =[dictionary objectForKey:@"ShowOccurrencesNot"];
    profile.showSecureZonesNotifications=[dictionary objectForKey:@"ShowSecureZonesNot"];
    profile.lastLatitude=[dictionary objectForKey:@"LastPositionLatitude"];
    profile.lastLongitude=[dictionary objectForKey:@"LastPositionLongitude"];
    if ([dictionary objectForKey:@"BeaconId"] && ![[dictionary objectForKey:@"BeaconId"] isEqual: [NSNull null]]) {
        profile.beaconId=[dictionary objectForKey:@"BeaconId"];
    }
    if ([dictionary objectForKey:@"AlertPhoneNumber"] && ![[dictionary objectForKey:@"AlertPhoneNumber"] isEqual: [NSNull null]]) {
        profile.phoneAlertCalls=[dictionary objectForKey:@"AlertPhoneNumber"];
    }
    if ([dictionary objectForKey:@"CountryCode"] && ![[dictionary objectForKey:@"CountryCode"] isEqual: [NSNull null]]) {
        profile.countryCode=[dictionary objectForKey:@"CountryCode"];
    }

    [profile mergeObject];
    [self getUserProfile:profile];
}
/**
 * Parsing the JSON to Profile Object
 * @param dictionary JSON Object
 * @param profile Profile Object
 */
- (void) parseDictionary:(NSDictionary*) dictionary ToProfileBasic:(Profile*) profile {
    profile.userId = [UserServices getUserId];
    NSString * name;
    NSString * userName;
    NSString * lastname;
    if ([[dictionary objectForKey:@"Name"] isEqualToString:@""]||[[dictionary objectForKey:@"Name"]isEqual:[NSNull null]]) {
        if([[dictionary objectForKey:@"Username"] isEqualToString:@""]||[[dictionary objectForKey:@"Username"] isEqual:[NSNull null]]){
            if ([[dictionary objectForKey:@"LastName"] isEqualToString:@""]||[[dictionary objectForKey:@"LastName"] isEqual:[NSNull null]]){
                if ([self.languaje isEqualToString:@"1"]) {
                    name= @"Desconocido";
                    lastname = @"";
                }else{
                    name= @"Unknown";
                    lastname = @"";
                }
                
            }else{
                name=[dictionary objectForKey:@"LastName"];
                userName= [dictionary objectForKey:@"LastName"];
                lastname=@"";
                
            }
        } else{
            
            name=[dictionary objectForKey:@"Username"];
            userName=[dictionary objectForKey:@"Username"];
        }
    }else {
        name=[dictionary objectForKey:@"Name"];
        if([[dictionary objectForKey:@"Username"] isEqualToString:@""]||[[dictionary objectForKey:@"Username"] isEqual:[NSNull null]]){
            userName=name;
        }else{
            userName = [dictionary objectForKey:@"Username"];
        }
    }
    
    if ([[dictionary objectForKey:@"LastName"] isEqualToString:@""]||[[dictionary objectForKey:@"LastName"] isEqual:[NSNull null]]) {
        lastname = @"";
    }else{
        lastname=[dictionary objectForKey:@"LastName"];
    }
    profile.userName = userName;
    profile.name = name;
    profile.lastName = lastname;
   
    if ([dictionary objectForKey:@"idFacebook"] && ![[dictionary objectForKey:@"idFacebook"] isEqual: [NSNull null]]) {
        profile.facebookId = [dictionary objectForKey:@"idFacebook"];
    }
    
    NSString *uri= [dictionary objectForKey:@"ProfileImageURL"];
    if (uri && ![uri isEqual: [NSNull null]])  {
       // dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:uri]];
            profile.pictureProfile = data;
            [profile mergeObject];
       // });
    }
    [profile removePhones:profile.phones];
    NSArray *tmpPhones = [dictionary objectForKey:@"Telephone"];
    for (NSString* phone in tmpPhones) {
        Phone *objPhone = [Phone createObject];
        objPhone.phone = phone;
        [objPhone saveObject];
        [profile addPhonesObject:objPhone];
    }
    [profile removeEmails:profile.emails];
    NSArray *tmpEmail = [dictionary objectForKey:@"Email"];
    for (NSString* email in tmpEmail) {
        Email *objEmail = [Email createObject];
        objEmail.email = email;
        [objEmail saveObject];
        [profile addEmailsObject:objEmail];
    }
    [profile mergeObject];
    NSLog(@"profile completed: %@",profile);
    [self getUserProfile:profile];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.pin.text = @"";
    self.pinConfirmation.text = @"";
  
}
#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=30);
    }
    if (textField.tag == 200) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=50);
    }
    if (textField.tag == 300) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=4);
    }
    return YES;
}

@end
