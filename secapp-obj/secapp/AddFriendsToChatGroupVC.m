//
//  AddFriendsToChatGroupVC.m
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AddFriendsToChatGroupVC.h"
#import "ImageUtils.h"
#import "StringUtils.h"
#import "ContactService.h"
#import "Friend.h"
#import "AppDelegate.h"
#import "Service.h"
#import "ChatContainerVC.h"
#import "SimpleTableViewCell.h"
#import "MessagesService.h"
#import "AlertInfo.h"
#import "Alert/AlertView.h"
#import "AlertViewSingleInput.h"
@class ChatContainerVC;
@interface AddFriendsToChatGroupVC () <ResponseDelegate, UINavigationControllerDelegate, AlertViewDelegate>{
    NSIndexPath *__selectedPath;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextField *searchField;
@property BOOL isSearch;
@property NSMutableArray *contactsArray;
@property NSArray *sortedArray;
@property NSArray *listArray;
@property (weak, nonatomic) IBOutlet UILabel *labelGroup;
@property NSMutableArray * cellSelected;
@property NSNumber * groupId;

@end

@implementation AddFriendsToChatGroupVC

-(void)viewWillAppear:(BOOL)animated{
    
    self.labelGroup.text= self.groupName;
    self.isSearch = NO;
    self.contactsArray = [[Friend findAll] mutableCopy];
    if (!self.contactsArray) {
        self.contactsArray=[NSMutableArray array];
    }
    self.sortedArray = [[NSMutableArray alloc] init];
    self.searchField = [[UITextField alloc] init];
    self.searchField.delegate = self;
    [self.view addSubview: self.searchField];
     self.cellSelected = [[NSMutableArray alloc] init];
    
}
-(void)viewDidDisappear:(BOOL)animated{
    self.contactsArray = nil;
    self.isSearch = NO;
    self.searchField.placeholder=@"";
    self.cellSelected= nil;
    
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
   
}


- (IBAction)textFieldDidChange:(UITextField *)sender {
    NSString * match = sender.text;
    if ([match isEqualToString:@""]) {
        self.isSearch = NO;
        [self.tableView reloadData];
        return;
    }
    NSArray *listFiles = [[NSMutableArray alloc]  init];
    NSPredicate *predicate =[NSPredicate predicateWithFormat:
                             @"userName CONTAINS[cd] %@", match];
    listFiles = [NSMutableArray arrayWithArray:[self.contactsArray
                                                filteredArrayUsingPredicate:predicate]];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"userName"  ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.sortedArray = [listFiles sortedArrayUsingDescriptors:sortDescriptors];
    self.isSearch = YES;
    [self.tableView reloadData];
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
    Friend * friend;
    
    if (self.isSearch) {
        
        friend =[self.sortedArray objectAtIndex:indexPath.row];
        if ([self.cellSelected containsObject:friend.userId])
        {
            [self.cellSelected removeObject:friend.userId];
        }
        else
        {
            [self.cellSelected addObject:friend.userId];

        }
   
        [tableView reloadData];
        
    } else {
        
        friend =[self.contactsArray objectAtIndex:indexPath.row];
        if ([self.cellSelected containsObject:friend.userId])
        {
            [self.cellSelected removeObject:friend.userId];
        }
        else
        {
           [self.cellSelected addObject:friend.userId];
            
        }
        
          [tableView reloadData];
    }
    
}

//- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
//    __selectedPath = nil;
//}
#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isSearch) {
        return [self.sortedArray count];
    }
    return [self.contactsArray count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SimpleTableViewCell *cell = (SimpleTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"AddCell"];
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AddCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }

    if (self.isSearch) {
        Friend *friendsSearch =[self.sortedArray objectAtIndex:indexPath.row];
        if ([self.cellSelected containsObject:friendsSearch.userId])
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        else
        {
            cell.accessoryType = UITableViewCellAccessoryNone;
            
        }
        cell.nameLabel.text = friendsSearch.userName;
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.profileImageView.clipsToBounds = YES;
            int radius = cell.profileImageView.frame.size.width/2;
            cell.profileImageView.layer.cornerRadius = radius;
            cell.profileImageView.layer.borderWidth = 3;
            cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
            
            if([friendsSearch.pictureProfile isEqual:@""]|| friendsSearch.pictureProfile==nil){
                
                cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
            }
            else{
                cell.profileImageView.image = [UIImage imageWithData:friendsSearch.pictureProfile];
                
            }
            
            
        });
      
                return cell;
        
    }
    Friend * friends=[self.contactsArray objectAtIndex:indexPath.row];
    if ([self.cellSelected containsObject:friends.userId])
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        
    }
    
    cell.nameLabel.text = friends.userName;
    dispatch_async(dispatch_get_main_queue(), ^{
        cell.profileImageView.clipsToBounds = YES;
        int radius = cell.profileImageView.frame.size.width/2;
        cell.profileImageView.layer.cornerRadius = radius;
        cell.profileImageView.layer.borderWidth = 3;
        cell.profileImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
        
        if([friends.pictureProfile isEqual:@""]|| friends.pictureProfile==nil){
            
            cell.profileImageView.image = [UIImage imageNamed:@"avatar"];
        }
        else{
            cell.profileImageView.image = [UIImage imageWithData:friends.pictureProfile];
            
        }
        
    });
    return cell;
}

- (IBAction)goBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)userDidTapScreen:(id)sender
{ //ocultar teclado
    
    [self.view endEditing:YES];
}
- (IBAction)addContacts:(UIButton *)sender {
    if (self.isNew) {
        if ([self.cellSelected count]<=1) {
          
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"numero.de.amigos.min", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
            alert.delegate=self;
            
        }else{
            [MessagesService createChatGroupWithName:self.groupName AndImage:self.image64 Wait:NO Delegate:self];
            
        }
        
    }
    else{
      
        [MessagesService addUsersToChatGroupWithIdGroup:[self.groupID stringValue] AndListUsers:[self.cellSelected copy] Wait:NO Delegate:self];
        [self.navigationController popViewControllerAnimated:YES];
        
    }
}


-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kCREATE_CHATGROUP:
            dictionary = [dictionary objectForKey:@"CreateChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                self.groupId = [dictionary objectForKey:@"ID"];
               
                [MessagesService addUsersToChatGroupWithIdGroup:[self.groupId stringValue] AndListUsers:[self.cellSelected copy] Wait:NO Delegate:self];
 
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            break;
        case kADD_USERS_TO_CHATGROUP:
            dictionary = [dictionary objectForKey:@"AddUserToChatGroupResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                if (self.isNew) {
                    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                    [alert showWithTitle:nil Message:NSLocalizedString(@"grupo.creado.con.contactos", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                    alert.delegate=self;
                  
                    [self.navigationController popToRootViewControllerAnimated:YES];
                  
                }
                else{
                    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                    [alert showWithTitle:nil Message:NSLocalizedString(@"contactos.agregados.chat", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                    alert.delegate=self;
                 
                }
                
            }
            if (responseCode==405) {
                AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
        default:
            break;
    }
}
- (IBAction)cancel:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
