//
//  LocationManagement.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 16/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationDelegate.h"

@interface LocationManagement : NSObject

@property NSArray* delegates;
@property NSNumber* longitude;
@property NSNumber* latitude;

/**
 * Create a only instance this class.
 * @return Instance of a class.
 */
+ (LocationManagement*) sharedInstance;

/**
 * Start the gps.
 */
- (void) startLocation;

/**
 * Stop the gps.
 */
- (void) stopLocation;

/**
 * Start the monitor of location user .
 */
- (void) startMonitoring;

/**
 * Stop the gps.
 */
- (void) stopMonitoring;

/**
 * Response if the GPS is active in the settings from the device.
 */
- (BOOL) isGPSActive;
/**
 * Start location monitoring with significant changes.
 */
- (void) startMonitoringSignificantLocationChanges;
/**
 * Stop location monitoring with significant changes.
 */
- (void) stopMonitoringSignificantLocationChanges;

@end
