//
//  LocationDelegate.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 16/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

@class CLLocation;

@protocol LocationDelegate <NSObject>

@optional
-(void) onLocationChange:(CLLocation*) location;

@end