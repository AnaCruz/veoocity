//
//  FriendListVC.h
//  secapp
//
//  Created by SecApp on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
@class ChatContainerVC;

@protocol ContactDelegate <NSObject>

@optional
-(void) showViewController:(UIViewController*) viewController;

@end

@interface FriendListVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property id<ContactDelegate> delegate;
@property ChatContainerVC * container;
@end
