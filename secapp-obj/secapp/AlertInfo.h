//
//  AlertInfo.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 29/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface AlertInfo : NSObject

/**
 *  Show a alert in view controller
 *
 *  @param controller Controller of the alert to show.
 *  @param title        Title of Alert
 *  @param message      Message of Alert
 */
+ (void) showAlertInViewController:(UIViewController*) controller
                         WithTitle:(NSString*) title
                           Message:(NSString*) message;

@end
