//
//  OcurrenceVC.h
//  secapp
//
//  Created by SecApp on 28/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "MapLocationVC.h"
#import "CreateOccurrence.h"
@class CustomAnnotation;
@class ContainerVC;
@class Profile;
@class Friend;
@class MapLocationVC;
@class CreateOccurrence;

@interface OcurrenceVC : UIViewController
@property BOOL isVisible;
@property NSString *type;
@property NSString *latitude;
@property NSString *longitude;
@property ContainerVC * contView;
@property MapLocationVC * map;
@property CreateOccurrence *createOccurrenceView;
- (void) drawInView:(UIView*) view;
- (IBAction)closeView:(id)sender;
@end
