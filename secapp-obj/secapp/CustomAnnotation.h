//
//  CustomAnnotation.h
//  secapp
//
//  Created by SecApp on 08/05/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import <MapKit/MapKit.h>
@class Friend;
@class Occurrence;

@interface CustomAnnotation : NSObject  <MKAnnotation>
//{
//    NSString *title;
//    NSString *subtitle;
//    CLLocationCoordinate2D coordinate;
//    
//}
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property Friend * friendInMap;
@property Occurrence * occurrenceInMap;
@property NSString *occurrenceID;
/**
 * Creates an annotation with a title (UserName), a subtitle (status), and a coordinate (in the mapView).
 * @param aTitle Title for the annotation (optionality contains the user name.
 * @param aSubtitle Subtitle for the annotation (optionality contains the user status).
 * @param coord Coordinate of the friend.
 */
- (id)initWithTitle:(NSString *)aTitle subtitle:(NSString*)aSubtitle andCoordinate:(CLLocationCoordinate2D)coord;
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate;
@end
