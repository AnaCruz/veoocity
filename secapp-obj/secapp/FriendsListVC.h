//
//  FriendsListVC.h
//  secapp
//
//  Created by SecApp on 14/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"
@class ContainerVC;
@interface FriendsListVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property ContainerVC * container;

@end
