//
//  BeaconServices.h
//  secapp
//
//  Created by SECAPP on 29/01/16.
//  Copyright © 2016 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Service.h"
@interface BeaconServices : Service
+(void) connectBeaconWithBeaconUUID:(NSString *)uuid Wait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;
+(void) disconnectBeaconWithWait:(BOOL)wait Delegate:(id<ResponseDelegate>)delegate;

@end
