//
//  ViewController.m
//  secapp
//
//  Created by Marco Navarro on 23/01/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "StringUtils.h"
#import "Service.h"
#import "Alert.h"
#import "ErrorResult.h"
#import "AppDelegate.h"
#import "ReviewVC.h"
#import "SessionServices.h"
#import "AlertViewSingleInput.h"
#import "UserServices.h"
#import "AlertView.h"


@interface ViewController () <ResponseDelegate, AlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *inUser;
@property (weak, nonatomic) IBOutlet UITextField *inPin;
@property NSData* dataRequest;
@property (weak, nonatomic) IBOutlet UIButton *forgotPass;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"backg"] drawInRect:self.view.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:image];

    UIColor *color = [UIColor colorWithRed:0.26 green:0.26 blue:0.26 alpha:1.0];
    NSDictionary *attrs = @{ NSForegroundColorAttributeName : color };
    NSString * text;
    int languaje= [[Service getLanguaje]intValue];
    if(languaje==1){
        text= @"Recuperar contraseña";
    }else{
        text= @"Forgot password?";
    }
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:text attributes:attrs];
    
    // making text property to underline text-
    [titleString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [titleString length])];
    
    // using text on button
    [self.forgotPass setAttributedTitle: titleString forState:UIControlStateNormal];
    [self addPaddingToTextField:self.inPin];
    [self addPaddingToTextField:self.inUser];
}
-(void)viewDidAppear:(BOOL)animated{
    if ([self.isforceClose isEqualToString:@"1"]) {
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"force.close.session", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
        alert.delegate=self;
        
    }
}

- (void) addPaddingToTextField:(UITextField*) sender {
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.leftView = paddingView;
    sender.leftViewMode = UITextFieldViewModeAlways;
    UIView *paddingViewPin = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 16, 20)];
    sender.rightView = paddingViewPin;
    sender.rightViewMode = UITextFieldViewModeAlways;
}

- (IBAction)closeKeyboard:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)goToSignUp:(id)sender {
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    [app goToSignUp];
}

- (IBAction)loginWithFacebook:(id)sender {
 
}





- (IBAction)loginWithSecApp:(id)sender {
       NSString* userName = self.inUser.text;
    NSString* pin = self.inPin.text;
    
    if ([StringUtils isEmpty:userName] || [StringUtils isEmpty:pin]) {
       
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"datos.vacios", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
        alert.delegate=self;

        return;
    }
    if ([StringUtils validateEmail:userName]) {
        self.dataRequest = [SessionServices loginWithMail:userName Pin:pin Wait:YES Delegate:self];
        return;
    }
    
    if ([StringUtils validateNumberPhone:userName]) {
        self.dataRequest = [SessionServices loginWithPhone:userName Pin:pin Wait:YES Delegate:self];
        return;
    }

    AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
    [alert showWithTitle:nil Message:NSLocalizedString(@"formato.icorrecto.usuario", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
    alert.delegate=self;
}

#pragma - mark Facebook delegates




#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=4);
    }
    return YES;
}

#pragma - mark Response delegates

- (void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
   // NSString *str = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
    

    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return; 
    }
    int respondeCode = -1;
    switch (service) {
        case kLOGIN:
            dictionary =  [dictionary objectForKey:@"LoginResult"];
              respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (respondeCode==200) {
                NSString* userId = [dictionary objectForKey:@"UserId"];
                NSString* sessionId = [dictionary objectForKey:@"SessionId"];
                
                                if (userId && sessionId) {
                                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                                    [userDefault setObject:userId forKey:@"userId"];
                                    [userDefault setObject:sessionId forKey:@"sessionId"];
                                    AppDelegate* app = [[UIApplication sharedApplication] delegate];
                                    [app goToMainView];
                                }
                                self.dataRequest = nil;
            }
            if (respondeCode == 403){
              if (self.dataRequest) {
                 self.dataRequest = [SessionServices loginWithData:self.dataRequest Wait:YES Delegate:self];
                }

            }
            if (respondeCode == 404){
               //[Alert showAlertWithTitle:@"Error" AndMessage:@"Usuario no encontrado"];
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"user.not.found", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                alert.delegate=self;

            }
            if (respondeCode == 401){
                //[Alert showAlertWithTitle:@"Error" AndMessage:@"Password incorrect"];
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"pass.incorrect", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                alert.delegate=self;
                
            }

            break;
        case kRESET_PASSWORD:
            dictionary =  [dictionary objectForKey:@"ResetPasswordResult"];
            respondeCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (respondeCode==200) {
             // [Alert showAlertWithTitle:@"Listo!" AndMessage:@"Tu nueva contraseña se ha enviado al correo"];
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"new.pass.send", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                alert.delegate=self;
            
            }
            if (respondeCode == 404) {
               // [Alert showAlertWithTitle:@"Error" AndMessage:@"Correo no encontrado en SecApp"];
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"email.not.found", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self];
                alert.delegate=self;
            }
            
            break;
        default:
            break;
    }

    
   
    
}
- (IBAction)forgotPass:(id)sender {
    
   
        AlertViewSingleInput* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertViewSingleInput"];
        alert.textField.keyboardType = UIKeyboardTypeEmailAddress;
    
        __block ViewController* bSelsf = self;
        alert.callbackOk =  ^(NSString* text){
            if (![text isEqualToString:@""]&& [StringUtils validateEmail:text]) {
               
                [UserServices resetPassword:text WithWait:YES Delegate:bSelsf];
            }else{
                
         
                int languaje= [[Service getLanguaje]intValue];
                if(languaje==1){
                     [Alert showAlertWithTitle:@"Error" AndMessage:@"Formato incorrecto de correo"];
                }else{
                    [Alert showAlertWithTitle:@"Error" AndMessage:@"Email is invalid"];
                }


            }
        };
        [alert showWithTitle:NSLocalizedString(@"recupera.password", nil) Message:NSLocalizedString(@"introduce.correo.recuperar", nil) CancelTitle:NSLocalizedString(@"cancelar", nil) OkTitle:NSLocalizedString(@"aceptar", nil) InController:self];
 
}

@end
