//
//  SearchContactsVC.h
//  secapp
//
//  Created by SecApp on 02/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SimpleTableViewCell.h"

@class SearchContactsVC;
@class ContainerVC;
@protocol SearchContactsVCDelegate <NSObject>
@end

@interface SearchContactsVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property id<SearchContactsVCDelegate> delegate;
@property ContainerVC* container;


@end
