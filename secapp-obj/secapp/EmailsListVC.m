//
//  EmailsListVC.m
//  secapp
//
//  Created by SecApp on 17/06/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "EmailsListVC.h"
#import "EditProfileVC.h"
#import "Service.h"
#import "UserServices.h"
#import "Profile.h"
#import "Email.h"
#import "StringUtils.h"
#import "AlertView.h"
#import "AppDelegate.h"

@interface EmailsListVC () <ResponseDelegate, UITextFieldDelegate>

@property Profile *userProfile;
@property NSArray * arrayEmails;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *inEmail;
@property NSIndexPath* indexPathDelete;

@end

@implementation EmailsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayEmails = [[NSArray alloc] init];
    [self getArrayEmails];
    self.tableView.layer.cornerRadius = 3;
}

/**
 * Get all emails in the user
 */
-(void)getArrayEmails{
    Profile* profile = [Profile findWithUserId:[UserServices getUserId]];
    if (profile) {
        self.userProfile = profile;
        self.arrayEmails = [self.userProfile.emails allObjects];
        [self.tableView reloadData];
    }
}

/**
 * Validated the email and if is correct send the email to WS.
 */
- (IBAction)saveEmail:(id)sender {
    if (![self.inEmail.text isEqualToString:@""]
        && [self.inEmail.text length] <= 50
        && [StringUtils validateEmail:self.inEmail.text]) {
        [UserServices addEmailPhone:self.inEmail.text IsEmail:YES Wait:YES Delegate:self];
        [self.view endEditing:YES];
        
    } else {
        // TODO ALERT Correo vacio, mas de 50 caracteres o correo erroneo
        AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
        [alert showWithTitle:nil Message:NSLocalizedString(@"correo.no.valido", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
    }
}

- (IBAction)goBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)closeKeyboard:(id)sender {
//    if (self.isEditing) {
        [self.view endEditing:YES];
//    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.arrayEmails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = (UITableViewCell*) [tableView dequeueReusableCellWithIdentifier:@"EmailCell"];
    NSString *email = ((Email*)[self.arrayEmails objectAtIndex:indexPath.row]).email;
    
    cell.textLabel.text = email;
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if ([self.arrayEmails count] <= 1) {
            // TODO ALERT No puedes eliminar cuando tienes un solo correo.
            AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
            [alert showWithTitle:nil Message:NSLocalizedString(@"eliminar.un.solo.correo", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
            [self.tableView setEditing:NO animated:YES];
            return;
        }
        self.indexPathDelete = indexPath;
      
        [UserServices deleteEmailPhone:((Email*)[self.arrayEmails objectAtIndex:indexPath.row]).email IsEmail:YES Wait:YES Delegate:self];
    }
}

#pragma mark - ResponseDelegate

-(void)successResponseWithService:(kService)service Response:(id)response {
    NSError *parsingError = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:&parsingError];
    if (parsingError) {
        NSLog(@"Error: %@", parsingError.description);
        return;
    }
    int responseCode = -1;
   
    switch (service) {
        case kUPDATE_MAIL_PHONE:
            dictionary = [dictionary objectForKey:@"AddEmailPhoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Email *email = [Email createObject];
                email.email = self.inEmail.text;
                [email saveObject];
                [self.userProfile addEmailsObject:email];
                [self.userProfile mergeObject];
                [self getArrayEmails];
                self.inEmail.text = @"";
            }
            if (responseCode == 409) {
                AlertView* alert = [[UIStoryboard storyboardWithName:@"Alert" bundle:nil] instantiateViewControllerWithIdentifier:@"AlertView"];
                [alert showWithTitle:nil Message:NSLocalizedString(@"email.ya.existe", nil) Delegate:nil CancelTitle:NSLocalizedString(@"aceptar", nil) OkTitle:nil InController:self.navigationController.parentViewController];
                self.inEmail.text = @"";

            }
            if (responseCode==405) {
              AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }
            break;
        case kDELETE_MAIL_PHONE:
          
            dictionary = [dictionary objectForKey:@"DeleteEmailPhoneResult"];
            responseCode = [[dictionary objectForKey:@"ResponseCode"] intValue];
            if (responseCode == 200) {
                Email *email =  (Email*)[self.arrayEmails objectAtIndex:self.indexPathDelete.row];
                [self.userProfile removeEmailsObject:email];
                [self.userProfile mergeObject];
                [self getArrayEmails];
            }
            
             self.indexPathDelete = nil;
            if (responseCode==405) {
             AppDelegate* app = [[UIApplication sharedApplication] delegate];
                [app forceCloseSession];
                //enviar a login
                
            }

            break;
        default:
            break;
    }
    [self.tableView setEditing:NO animated:YES];
}
#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.tag == 100) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=50);
    }
        return YES;
}
@end
