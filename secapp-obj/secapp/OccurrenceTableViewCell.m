//
//  OccurrenceTableViewCell.m
//  secapp
//
//  Created by SecApp on 04/11/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import "OccurrenceTableViewCell.h"

@implementation OccurrenceTableViewCell
@synthesize nameUser = _nameUser;
@synthesize comments= _comments;
@synthesize date=_date;
@synthesize like=_like;
@synthesize dislike=_dislike;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
