//
//  AlertViewSingleInput.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 13/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertViewSingleInput : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (nonatomic, copy) void (^callbackOk)(NSString* text);

- (void) numberToCharacter:(int) lenght;

- (void) showWithTitle:(NSString*) title Message:(NSString*) message CancelTitle:(NSString*) cancelTitle OkTitle:(NSString*) okTitle InController:(UIViewController*) controller;
@end
