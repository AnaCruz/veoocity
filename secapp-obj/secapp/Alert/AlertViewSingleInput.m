//
//  AlertViewSingleInput.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 13/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "AlertViewSingleInput.h"

@interface AlertViewSingleInput () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnOk;

@property (weak, nonatomic) IBOutlet UIView *viewAlert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerView;

@property int lenghtCharacter;

@property NSString* titleAlert;
@property NSString* message;
@property NSString* okTitle;
@property NSString* cancelTitle;

@end

@implementation AlertViewSingleInput

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.lenghtCharacter = 0;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.viewAlert.layer.cornerRadius = 5;
    self.btnCancel.layer.cornerRadius = 5;
    self.btnOk.layer.cornerRadius = 5;
    
    CGAffineTransform trans = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
    self.viewAlert.transform = trans; // do it instantly, no animation
    
    self.titleLabel.text = self.titleAlert;
    self.label.text = self.message;
    [self.btnCancel setTitle:self.cancelTitle forState:UIControlStateNormal];
    [self.btnOk setTitle:self.okTitle forState:UIControlStateNormal];
    if (self.okTitle == nil || [self.okTitle isEqualToString:@""]) {
        [self.btnOk removeFromSuperview];
        self.btnOk = nil;
        [self.view updateConstraints];
    }
}

- (void) showWithTitle:(NSString*) title Message:(NSString*) message CancelTitle:(NSString*) cancelTitle OkTitle:(NSString*) okTitle InController:(UIViewController*) controller {
    [controller addChildViewController:self];
    // show AlertView
//    [controller.view insertSubview:self.view atIndex:[[controller.view subviews] count]];
    // Animate Alert view
    self.titleAlert = title;
    self.message = message;
    self.okTitle = okTitle;
    self.cancelTitle = cancelTitle;
    [controller.view addSubview:self.view];
}

-(void)viewWillAppear:(BOOL)animated {
    [self registerForKeyboardNotifications];
    // now return the view to normal dimension, animating this tranformation
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.15f
                              delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 100.0, 100.0);
                         }
                         completion:^(BOOL finished) {
                             
                             [self.viewAlert updateConstraints];
                         } ];
    });
}

-(void)viewWillDisappear:(BOOL)animated {
    [self unregisterForKeyboardNotifications];
}

- (void) numberToCharacter:(int) lenght {
    self.lenghtCharacter = lenght;
}

- (IBAction)buttonPressed:(UIButton*)sender {
    if (self.callbackOk && sender.tag == 1) {
        self.callbackOk(self.textField.text);
    }
    [UIView animateWithDuration:0.15f
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.viewAlert.transform = CGAffineTransformScale(self.viewAlert.transform, 0.01, 0.01);
                     }
                     completion:^(BOOL finished) {
                         [self.view removeFromSuperview];
                         [self removeFromParentViewController];
                     }];
}

-(void) animationStopped {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
}

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect aRect = self.viewAlert.frame;
    
    if ((aRect.origin.y+aRect.size.height) >= (self.view.frame.size.height - kbSize.height)) {
//        aRect.origin.y = self.view.frame.size.height - kbSize.height - aRect.size.height;
        [UIView animateWithDuration:0.15f
                              delay:0.0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             self.centerView.constant = 80;
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.15f
                          delay:0.0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.centerView.constant = 0;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (IBAction)endEditingText:(id)sender {
    [self.view endEditing:YES];
}

#pragma - mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.lenghtCharacter > 0) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength<=self.lenghtCharacter);
    }
    return YES;
}

@end
