//
//  AlertView.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 09/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AlertViewDelegate <NSObject>

@optional
- (void) buttonPressed:(UIButton*) sender;

@end

@interface AlertView : UIViewController

@property id<AlertViewDelegate> delegate;

- (void) showWithTitle:(NSString*) title Message:(NSString*) message Delegate:(id) delegte CancelTitle:(NSString*) cancelTitle OkTitle:(NSString*) okTitle InController:(UIViewController*) controller;

@end
