//
//  SettingListVC.m
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 20/07/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "SettingListVC.h"
#import "Profile.h"
#import "Service.h"

@interface SettingListVC ()
@property  Profile * user;
@end

@implementation SettingListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = [Profile findWithUserId:[Service getUserId]];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animate{
     self.user = [Profile findWithUserId:[Service getUserId]];
  
    if ([self.user.hiddenPosition boolValue]) {
        self.toggleLocationHide.on=NO;
    }else {
        self.toggleLocationHide.on=YES;
    }
    if ([self.user.showOccurrenceNotifications boolValue]) {
        self.toggleOccurrences.on=YES;
    }else {
        self.toggleOccurrences.on=NO;
    }
    if ([self.user.showSecureZonesNotifications boolValue]) {
        self.toggleZone.on=YES;
    }else {
        self.toggleZone.on=NO;
    }
   
}
- (IBAction)changeOption:(UISwitch*)sender {
    if (!self.callbackSwitch) {
        return;
    }
    
    if (sender.tag == 1) {
        self.callbackSwitch(1, sender.isOn);
    } else if(sender.tag==0) {
        self.callbackSwitch(0, sender.isOn);
    }
    else if (sender.tag==2){
     self.callbackSwitch(2, sender.isOn);
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.callback) {
        return;
    }
    switch (indexPath.row) {
       
        case 3:
            self.callback(3);
            break;
        case 4:
            self.callback(4);
            break;
        case 5:
            self.callback(5);
            break;
        case 6:
            self.callback(6);
            break;
        case 7:
            self.callback(7);
            break;
        case 8:
            self.callback(8);
            break;
        case 9:
            self.callback(9);
            break;
         default:
            break;
    }
}
@end
