//
//  UserNotifications.m
//  secapp
//
//  Created by SecApp on 12/03/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import "UserNotifications.h"

@implementation UserNotifications

+ (void) getNotificationsWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate{
    NSString* uri = [NSString stringWithFormat:@"%@NotificationsByUser/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kGET Uri:uri Service:kGET_NOTIFICATIONS Wait:wait Delegate:delegate];
    [request execute];
}

+ (void) deleteNotification:(NSString*) notificationId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate Extra:(id)extra {
    NSString* uri = [NSString stringWithFormat:@"%@NotificationReaded/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], notificationId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kREADED_NOTIFICATIONS Wait:wait Delegate:delegate];
    if (extra) {
        request.extra = extra;
    }
    [request execute];
}
+ (void) deleteNotificationWith:(NSString*) notificationId Wait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@NotificationReaded/%@/%@/%@", [super getUrl], [super getUserId], [super getSessionId], notificationId];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kREADED_NOTIFICATIONS Wait:wait Delegate:delegate];
   
    [request execute];
}
+ (void) deleteAllNotificationWait:(BOOL) wait Delegate: (id<ResponseDelegate>) delegate {
    NSString* uri = [NSString stringWithFormat:@"%@DeleteAllNotifications/%@/%@", [super getUrl], [super getUserId], [super getSessionId]];
    RESTClient* request = [[RESTClient alloc] initWithMethod:kDELETE Uri:uri Service:kDELETE_ALL_NOTIFICATIONS Wait:wait Delegate:delegate];
    
    [request execute];
}



@end
