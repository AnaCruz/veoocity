//
//  Occurrence.h
//  secapp
//
//  Created by SecApp on 28/10/15.
//  Copyright © 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "GenericObject.h"

@interface Occurrence : GenericObject
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSString * occurrenceId;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSNumber * isSpecial; //Occurrences created by user from "Special incidences" web utility
@property (nonatomic, retain) NSString * createdUserId;
@end

@interface Occurrence (CoreDataGeneratedAccessors)

/**
 * Create a object using the name of the entity.
 */
+ (instancetype) createObject;

/**
 * Find a occurrence across a occurrenceId in the data base.
 * @param occurrenceId Occurrence Identifier.
 * @return array Array with occurrence data.
 */
+ (NSArray*) findWithOccurrenceId:(NSString*) occurrenceId;

/**
 * Find all occurrrences in the data base
 * @return array Array with all occurrences data.
 */
+ (NSArray*) findAll;
@end
