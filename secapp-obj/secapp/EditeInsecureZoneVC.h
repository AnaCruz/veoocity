//
//  EditeInsecureZoneVC.h
//  secapp
//
//  Created by SecApp on 06/08/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EditeInsecureZoneVC;
@class CircleOverlay;
@protocol  EditeInsecureZoneDelegate <NSObject>
@optional
- (void) editInsecureZone:(EditeInsecureZoneVC*) editZone SaveZone:(id) zone;
- (void) editInsecureZone:(EditeInsecureZoneVC*) editZone CancelZone:(id) zone;



@end
@interface EditeInsecureZoneVC  : UIViewController


@property id<EditeInsecureZoneDelegate> delegate;
@property (weak, nonatomic) CircleOverlay *zone;
@property BOOL isEdtiting;
@property BOOL isShow;
- (void) drawInView:(UIView*) view;


@end
