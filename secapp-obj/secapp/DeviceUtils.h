//
//  DeviceUtils.h
//  secapp
//
//  Created by Marco Antonio Navarro Montoya on 10/02/15.
//  Copyright (c) 2015 SecApp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceUtils : NSObject

+ (float) deviceVersion;

@end
