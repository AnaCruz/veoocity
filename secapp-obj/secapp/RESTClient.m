//
//  RESTClient.m
//
//  Created by Marco Navarro on 26/10/14.
//  Copyright (c) 2014 TrioLabs. All rights reserved.
//

#import "RESTClient.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"

@interface RESTClient() <NSURLConnectionDelegate>

@property NSString *uri;
@property NSURLConnection *currentConnection;
@property kMethod method;
@property kService service;
@property NSMutableData *data;
@property BOOL wait;
@property UIView* view;

@end

@implementation RESTClient

- (instancetype) initWithMethod:(kMethod)method Uri:(NSString*)uri Service:(kService)service Wait:(BOOL) wait Delegate:(id<ResponseDelegate>)delegate {
    self = [super init];
    if (self) {
        self.uri = uri;
        self.method = method;
        self.delegate = delegate;
        self.service = service;
        self.wait = wait;
    }
    return self;
}

-(void) showProgress {
    if (self.wait) {
        AppDelegate *app = [[UIApplication sharedApplication] delegate];
        UIWindow *window = [app window];
        UIView* view = [[window subviews] lastObject];
        self.view = view;
        MBProgressHUD* hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
        if (self.messageWait) {
            hud.labelText = self.messageWait;
        }
    }
}

-(void) execute {
    
    [self showProgress];
    
    // Config the request.
    NSString *tmpURI = self.uri;
    if (self.paramsUri) { // If the uri have params (is more need in the GET and DELETE methods)
        tmpURI = [NSString stringWithFormat:@"%@/%@", self.uri, self.paramsUri];
    }
    NSURL *uri = [NSURL URLWithString:tmpURI];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:uri];
    [request setHTTPMethod:[self getMethodString]];
    [request setTimeoutInterval:120.0];
    
    // Params
    if (self.body) {
        [request setHTTPBody:self.body];
        NSString *dataLength = [NSString stringWithFormat:@"%lu", (unsigned long)[self.body length]];
        NSLog(@"RESTClient: Data length: %@", dataLength);
        NSString *str = [[NSString alloc] initWithData:self.body encoding:NSUTF8StringEncoding];
        NSLog(@"RESTClient: Data: %@",str);
        [request setValue:dataLength forHTTPHeaderField:@"Content-Length"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    
    NSURLConnection *connection=[[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
    if (connection) {
        self.data = [NSMutableData data];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    // receivedData is an instance variable declared elsewhere.
  //  NSLog(@"RESTClient: Code: %ld", (long)[(NSHTTPURLResponse*)response statusCode]);
    [self.data setLength:0];
}
//- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse{
//    return nil;
//}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to receivedData.
    // receivedData is an instance variable declared elsewhere.
    [self.data appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
   // NSLog(@"RESTClient: Error connection did fail: %@ %@", [error localizedDescription], [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);

    if (self.wait) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(errorResponseWithService:Error:)]){
        [self.delegate errorResponseWithService:self.service Error:error];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // do something with the data
    // receivedData is declared as a method instance elsewhere
 //   NSLog(@"RESTClient: Succeeded! Received %lu bytes of data",(unsigned long)[self.data length]);
    
    if (self.wait) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    
    if (self.delegate){
        if (self.extra && [self.delegate respondsToSelector:@selector(successResponseWithService:Response:Extra:)]) {
            [self.delegate successResponseWithService:self.service Response:self.data Extra:self.extra];
        } else if ([self.delegate respondsToSelector:@selector(successResponseWithService:Response:)]) {
            [self.delegate successResponseWithService:self.service Response:self.data];
        }
    }
    
}


/**
 * Get the Method protocol of HTML
 * @return The Protocol in type NSString, in default is GET
 */
-(NSString*) getMethodString {
    NSString *tmpMethod;
    switch (self.method) {
        case kGET:
            tmpMethod = @"GET";
            break;
        case kPOST:
            tmpMethod = @"POST";
            break;
        case kPUT:
            tmpMethod = @"PUT";
            break;
        case kDELETE:
            tmpMethod = @"DELETE";
            break;
        default:
            tmpMethod = @"GET";
            break;
    }
    return tmpMethod;
}

@end
